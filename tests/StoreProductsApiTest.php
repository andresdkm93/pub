<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StoreProductsApiTest extends TestCase
{
    use MakeStoreProductsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateStoreProducts()
    {
        $storeProducts = $this->fakeStoreProductsData();
        $this->json('POST', '/api/v1/storeProducts', $storeProducts);

        $this->assertApiResponse($storeProducts);
    }

    /**
     * @test
     */
    public function testReadStoreProducts()
    {
        $storeProducts = $this->makeStoreProducts();
        $this->json('GET', '/api/v1/storeProducts/'.$storeProducts->id);

        $this->assertApiResponse($storeProducts->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStoreProducts()
    {
        $storeProducts = $this->makeStoreProducts();
        $editedStoreProducts = $this->fakeStoreProductsData();

        $this->json('PUT', '/api/v1/storeProducts/'.$storeProducts->id, $editedStoreProducts);

        $this->assertApiResponse($editedStoreProducts);
    }

    /**
     * @test
     */
    public function testDeleteStoreProducts()
    {
        $storeProducts = $this->makeStoreProducts();
        $this->json('DELETE', '/api/v1/storeProducts/'.$storeProducts->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/storeProducts/'.$storeProducts->id);

        $this->assertResponseStatus(404);
    }
}
