<?php

use App\Models\PromotionType;
use App\Repositories\PromotionTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PromotionTypeRepositoryTest extends TestCase
{
    use MakePromotionTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PromotionTypeRepository
     */
    protected $promotionTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->promotionTypeRepo = App::make(PromotionTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePromotionType()
    {
        $promotionType = $this->fakePromotionTypeData();
        $createdPromotionType = $this->promotionTypeRepo->create($promotionType);
        $createdPromotionType = $createdPromotionType->toArray();
        $this->assertArrayHasKey('id', $createdPromotionType);
        $this->assertNotNull($createdPromotionType['id'], 'Created PromotionType must have id specified');
        $this->assertNotNull(PromotionType::find($createdPromotionType['id']), 'PromotionType with given id must be in DB');
        $this->assertModelData($promotionType, $createdPromotionType);
    }

    /**
     * @test read
     */
    public function testReadPromotionType()
    {
        $promotionType = $this->makePromotionType();
        $dbPromotionType = $this->promotionTypeRepo->find($promotionType->id);
        $dbPromotionType = $dbPromotionType->toArray();
        $this->assertModelData($promotionType->toArray(), $dbPromotionType);
    }

    /**
     * @test update
     */
    public function testUpdatePromotionType()
    {
        $promotionType = $this->makePromotionType();
        $fakePromotionType = $this->fakePromotionTypeData();
        $updatedPromotionType = $this->promotionTypeRepo->update($fakePromotionType, $promotionType->id);
        $this->assertModelData($fakePromotionType, $updatedPromotionType->toArray());
        $dbPromotionType = $this->promotionTypeRepo->find($promotionType->id);
        $this->assertModelData($fakePromotionType, $dbPromotionType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePromotionType()
    {
        $promotionType = $this->makePromotionType();
        $resp = $this->promotionTypeRepo->delete($promotionType->id);
        $this->assertTrue($resp);
        $this->assertNull(PromotionType::find($promotionType->id), 'PromotionType should not exist in DB');
    }
}
