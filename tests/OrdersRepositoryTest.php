<?php

use App\Models\Orders;
use App\Repositories\OrdersRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrdersRepositoryTest extends TestCase
{
    use MakeOrdersTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrdersRepository
     */
    protected $ordersRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ordersRepo = App::make(OrdersRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateOrders()
    {
        $orders = $this->fakeOrdersData();
        $createdOrders = $this->ordersRepo->create($orders);
        $createdOrders = $createdOrders->toArray();
        $this->assertArrayHasKey('id', $createdOrders);
        $this->assertNotNull($createdOrders['id'], 'Created Orders must have id specified');
        $this->assertNotNull(Orders::find($createdOrders['id']), 'Orders with given id must be in DB');
        $this->assertModelData($orders, $createdOrders);
    }

    /**
     * @test read
     */
    public function testReadOrders()
    {
        $orders = $this->makeOrders();
        $dbOrders = $this->ordersRepo->find($orders->id);
        $dbOrders = $dbOrders->toArray();
        $this->assertModelData($orders->toArray(), $dbOrders);
    }

    /**
     * @test update
     */
    public function testUpdateOrders()
    {
        $orders = $this->makeOrders();
        $fakeOrders = $this->fakeOrdersData();
        $updatedOrders = $this->ordersRepo->update($fakeOrders, $orders->id);
        $this->assertModelData($fakeOrders, $updatedOrders->toArray());
        $dbOrders = $this->ordersRepo->find($orders->id);
        $this->assertModelData($fakeOrders, $dbOrders->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteOrders()
    {
        $orders = $this->makeOrders();
        $resp = $this->ordersRepo->delete($orders->id);
        $this->assertTrue($resp);
        $this->assertNull(Orders::find($orders->id), 'Orders should not exist in DB');
    }
}
