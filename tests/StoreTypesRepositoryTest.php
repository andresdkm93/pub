<?php

use App\Models\StoreTypes;
use App\Repositories\StoreTypesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StoreTypesRepositoryTest extends TestCase
{
    use MakeStoreTypesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StoreTypesRepository
     */
    protected $storeTypesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->storeTypesRepo = App::make(StoreTypesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStoreTypes()
    {
        $storeTypes = $this->fakeStoreTypesData();
        $createdStoreTypes = $this->storeTypesRepo->create($storeTypes);
        $createdStoreTypes = $createdStoreTypes->toArray();
        $this->assertArrayHasKey('id', $createdStoreTypes);
        $this->assertNotNull($createdStoreTypes['id'], 'Created StoreTypes must have id specified');
        $this->assertNotNull(StoreTypes::find($createdStoreTypes['id']), 'StoreTypes with given id must be in DB');
        $this->assertModelData($storeTypes, $createdStoreTypes);
    }

    /**
     * @test read
     */
    public function testReadStoreTypes()
    {
        $storeTypes = $this->makeStoreTypes();
        $dbStoreTypes = $this->storeTypesRepo->find($storeTypes->id);
        $dbStoreTypes = $dbStoreTypes->toArray();
        $this->assertModelData($storeTypes->toArray(), $dbStoreTypes);
    }

    /**
     * @test update
     */
    public function testUpdateStoreTypes()
    {
        $storeTypes = $this->makeStoreTypes();
        $fakeStoreTypes = $this->fakeStoreTypesData();
        $updatedStoreTypes = $this->storeTypesRepo->update($fakeStoreTypes, $storeTypes->id);
        $this->assertModelData($fakeStoreTypes, $updatedStoreTypes->toArray());
        $dbStoreTypes = $this->storeTypesRepo->find($storeTypes->id);
        $this->assertModelData($fakeStoreTypes, $dbStoreTypes->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStoreTypes()
    {
        $storeTypes = $this->makeStoreTypes();
        $resp = $this->storeTypesRepo->delete($storeTypes->id);
        $this->assertTrue($resp);
        $this->assertNull(StoreTypes::find($storeTypes->id), 'StoreTypes should not exist in DB');
    }
}
