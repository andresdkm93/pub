<?php

use App\Models\Shedule;
use App\Repositories\SheduleRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SheduleRepositoryTest extends TestCase
{
    use MakeSheduleTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SheduleRepository
     */
    protected $sheduleRepo;

    public function setUp()
    {
        parent::setUp();
        $this->sheduleRepo = App::make(SheduleRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateShedule()
    {
        $shedule = $this->fakeSheduleData();
        $createdShedule = $this->sheduleRepo->create($shedule);
        $createdShedule = $createdShedule->toArray();
        $this->assertArrayHasKey('id', $createdShedule);
        $this->assertNotNull($createdShedule['id'], 'Created Shedule must have id specified');
        $this->assertNotNull(Shedule::find($createdShedule['id']), 'Shedule with given id must be in DB');
        $this->assertModelData($shedule, $createdShedule);
    }

    /**
     * @test read
     */
    public function testReadShedule()
    {
        $shedule = $this->makeShedule();
        $dbShedule = $this->sheduleRepo->find($shedule->id);
        $dbShedule = $dbShedule->toArray();
        $this->assertModelData($shedule->toArray(), $dbShedule);
    }

    /**
     * @test update
     */
    public function testUpdateShedule()
    {
        $shedule = $this->makeShedule();
        $fakeShedule = $this->fakeSheduleData();
        $updatedShedule = $this->sheduleRepo->update($fakeShedule, $shedule->id);
        $this->assertModelData($fakeShedule, $updatedShedule->toArray());
        $dbShedule = $this->sheduleRepo->find($shedule->id);
        $this->assertModelData($fakeShedule, $dbShedule->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteShedule()
    {
        $shedule = $this->makeShedule();
        $resp = $this->sheduleRepo->delete($shedule->id);
        $this->assertTrue($resp);
        $this->assertNull(Shedule::find($shedule->id), 'Shedule should not exist in DB');
    }
}
