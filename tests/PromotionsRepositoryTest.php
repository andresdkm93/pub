<?php

use App\Models\Promotions;
use App\Repositories\PromotionsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PromotionsRepositoryTest extends TestCase
{
    use MakePromotionsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PromotionsRepository
     */
    protected $promotionsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->promotionsRepo = App::make(PromotionsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePromotions()
    {
        $promotions = $this->fakePromotionsData();
        $createdPromotions = $this->promotionsRepo->create($promotions);
        $createdPromotions = $createdPromotions->toArray();
        $this->assertArrayHasKey('id', $createdPromotions);
        $this->assertNotNull($createdPromotions['id'], 'Created Promotions must have id specified');
        $this->assertNotNull(Promotions::find($createdPromotions['id']), 'Promotions with given id must be in DB');
        $this->assertModelData($promotions, $createdPromotions);
    }

    /**
     * @test read
     */
    public function testReadPromotions()
    {
        $promotions = $this->makePromotions();
        $dbPromotions = $this->promotionsRepo->find($promotions->id);
        $dbPromotions = $dbPromotions->toArray();
        $this->assertModelData($promotions->toArray(), $dbPromotions);
    }

    /**
     * @test update
     */
    public function testUpdatePromotions()
    {
        $promotions = $this->makePromotions();
        $fakePromotions = $this->fakePromotionsData();
        $updatedPromotions = $this->promotionsRepo->update($fakePromotions, $promotions->id);
        $this->assertModelData($fakePromotions, $updatedPromotions->toArray());
        $dbPromotions = $this->promotionsRepo->find($promotions->id);
        $this->assertModelData($fakePromotions, $dbPromotions->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePromotions()
    {
        $promotions = $this->makePromotions();
        $resp = $this->promotionsRepo->delete($promotions->id);
        $this->assertTrue($resp);
        $this->assertNull(Promotions::find($promotions->id), 'Promotions should not exist in DB');
    }
}
