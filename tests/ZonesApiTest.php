<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ZonesApiTest extends TestCase
{
    use MakeZonesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateZones()
    {
        $zones = $this->fakeZonesData();
        $this->json('POST', '/api/v1/zones', $zones);

        $this->assertApiResponse($zones);
    }

    /**
     * @test
     */
    public function testReadZones()
    {
        $zones = $this->makeZones();
        $this->json('GET', '/api/v1/zones/'.$zones->id);

        $this->assertApiResponse($zones->toArray());
    }

    /**
     * @test
     */
    public function testUpdateZones()
    {
        $zones = $this->makeZones();
        $editedZones = $this->fakeZonesData();

        $this->json('PUT', '/api/v1/zones/'.$zones->id, $editedZones);

        $this->assertApiResponse($editedZones);
    }

    /**
     * @test
     */
    public function testDeleteZones()
    {
        $zones = $this->makeZones();
        $this->json('DELETE', '/api/v1/zones/'.$zones->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/zones/'.$zones->id);

        $this->assertResponseStatus(404);
    }
}
