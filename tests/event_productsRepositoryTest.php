<?php

use App\Models\event_products;
use App\Repositories\event_productsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class event_productsRepositoryTest extends TestCase
{
    use Makeevent_productsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var event_productsRepository
     */
    protected $eventProductsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->eventProductsRepo = App::make(event_productsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateevent_products()
    {
        $eventProducts = $this->fakeevent_productsData();
        $createdevent_products = $this->eventProductsRepo->create($eventProducts);
        $createdevent_products = $createdevent_products->toArray();
        $this->assertArrayHasKey('id', $createdevent_products);
        $this->assertNotNull($createdevent_products['id'], 'Created event_products must have id specified');
        $this->assertNotNull(event_products::find($createdevent_products['id']), 'event_products with given id must be in DB');
        $this->assertModelData($eventProducts, $createdevent_products);
    }

    /**
     * @test read
     */
    public function testReadevent_products()
    {
        $eventProducts = $this->makeevent_products();
        $dbevent_products = $this->eventProductsRepo->find($eventProducts->id);
        $dbevent_products = $dbevent_products->toArray();
        $this->assertModelData($eventProducts->toArray(), $dbevent_products);
    }

    /**
     * @test update
     */
    public function testUpdateevent_products()
    {
        $eventProducts = $this->makeevent_products();
        $fakeevent_products = $this->fakeevent_productsData();
        $updatedevent_products = $this->eventProductsRepo->update($fakeevent_products, $eventProducts->id);
        $this->assertModelData($fakeevent_products, $updatedevent_products->toArray());
        $dbevent_products = $this->eventProductsRepo->find($eventProducts->id);
        $this->assertModelData($fakeevent_products, $dbevent_products->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteevent_products()
    {
        $eventProducts = $this->makeevent_products();
        $resp = $this->eventProductsRepo->delete($eventProducts->id);
        $this->assertTrue($resp);
        $this->assertNull(event_products::find($eventProducts->id), 'event_products should not exist in DB');
    }
}
