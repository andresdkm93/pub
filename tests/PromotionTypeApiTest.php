<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PromotionTypeApiTest extends TestCase
{
    use MakePromotionTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePromotionType()
    {
        $promotionType = $this->fakePromotionTypeData();
        $this->json('POST', '/api/v1/promotionTypes', $promotionType);

        $this->assertApiResponse($promotionType);
    }

    /**
     * @test
     */
    public function testReadPromotionType()
    {
        $promotionType = $this->makePromotionType();
        $this->json('GET', '/api/v1/promotionTypes/'.$promotionType->id);

        $this->assertApiResponse($promotionType->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePromotionType()
    {
        $promotionType = $this->makePromotionType();
        $editedPromotionType = $this->fakePromotionTypeData();

        $this->json('PUT', '/api/v1/promotionTypes/'.$promotionType->id, $editedPromotionType);

        $this->assertApiResponse($editedPromotionType);
    }

    /**
     * @test
     */
    public function testDeletePromotionType()
    {
        $promotionType = $this->makePromotionType();
        $this->json('DELETE', '/api/v1/promotionTypes/'.$promotionType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/promotionTypes/'.$promotionType->id);

        $this->assertResponseStatus(404);
    }
}
