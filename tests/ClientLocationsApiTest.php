<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClientLocationsApiTest extends TestCase
{
    use MakeClientLocationsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateClientLocations()
    {
        $clientLocations = $this->fakeClientLocationsData();
        $this->json('POST', '/api/v1/clientLocations', $clientLocations);

        $this->assertApiResponse($clientLocations);
    }

    /**
     * @test
     */
    public function testReadClientLocations()
    {
        $clientLocations = $this->makeClientLocations();
        $this->json('GET', '/api/v1/clientLocations/'.$clientLocations->id);

        $this->assertApiResponse($clientLocations->toArray());
    }

    /**
     * @test
     */
    public function testUpdateClientLocations()
    {
        $clientLocations = $this->makeClientLocations();
        $editedClientLocations = $this->fakeClientLocationsData();

        $this->json('PUT', '/api/v1/clientLocations/'.$clientLocations->id, $editedClientLocations);

        $this->assertApiResponse($editedClientLocations);
    }

    /**
     * @test
     */
    public function testDeleteClientLocations()
    {
        $clientLocations = $this->makeClientLocations();
        $this->json('DELETE', '/api/v1/clientLocations/'.$clientLocations->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/clientLocations/'.$clientLocations->id);

        $this->assertResponseStatus(404);
    }
}
