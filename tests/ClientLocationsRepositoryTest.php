<?php

use App\Models\ClientLocations;
use App\Repositories\ClientLocationsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClientLocationsRepositoryTest extends TestCase
{
    use MakeClientLocationsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ClientLocationsRepository
     */
    protected $clientLocationsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->clientLocationsRepo = App::make(ClientLocationsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateClientLocations()
    {
        $clientLocations = $this->fakeClientLocationsData();
        $createdClientLocations = $this->clientLocationsRepo->create($clientLocations);
        $createdClientLocations = $createdClientLocations->toArray();
        $this->assertArrayHasKey('id', $createdClientLocations);
        $this->assertNotNull($createdClientLocations['id'], 'Created ClientLocations must have id specified');
        $this->assertNotNull(ClientLocations::find($createdClientLocations['id']), 'ClientLocations with given id must be in DB');
        $this->assertModelData($clientLocations, $createdClientLocations);
    }

    /**
     * @test read
     */
    public function testReadClientLocations()
    {
        $clientLocations = $this->makeClientLocations();
        $dbClientLocations = $this->clientLocationsRepo->find($clientLocations->id);
        $dbClientLocations = $dbClientLocations->toArray();
        $this->assertModelData($clientLocations->toArray(), $dbClientLocations);
    }

    /**
     * @test update
     */
    public function testUpdateClientLocations()
    {
        $clientLocations = $this->makeClientLocations();
        $fakeClientLocations = $this->fakeClientLocationsData();
        $updatedClientLocations = $this->clientLocationsRepo->update($fakeClientLocations, $clientLocations->id);
        $this->assertModelData($fakeClientLocations, $updatedClientLocations->toArray());
        $dbClientLocations = $this->clientLocationsRepo->find($clientLocations->id);
        $this->assertModelData($fakeClientLocations, $dbClientLocations->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteClientLocations()
    {
        $clientLocations = $this->makeClientLocations();
        $resp = $this->clientLocationsRepo->delete($clientLocations->id);
        $this->assertTrue($resp);
        $this->assertNull(ClientLocations::find($clientLocations->id), 'ClientLocations should not exist in DB');
    }
}
