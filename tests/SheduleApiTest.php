<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SheduleApiTest extends TestCase
{
    use MakeSheduleTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateShedule()
    {
        $shedule = $this->fakeSheduleData();
        $this->json('POST', '/api/v1/shedules', $shedule);

        $this->assertApiResponse($shedule);
    }

    /**
     * @test
     */
    public function testReadShedule()
    {
        $shedule = $this->makeShedule();
        $this->json('GET', '/api/v1/shedules/'.$shedule->id);

        $this->assertApiResponse($shedule->toArray());
    }

    /**
     * @test
     */
    public function testUpdateShedule()
    {
        $shedule = $this->makeShedule();
        $editedShedule = $this->fakeSheduleData();

        $this->json('PUT', '/api/v1/shedules/'.$shedule->id, $editedShedule);

        $this->assertApiResponse($editedShedule);
    }

    /**
     * @test
     */
    public function testDeleteShedule()
    {
        $shedule = $this->makeShedule();
        $this->json('DELETE', '/api/v1/shedules/'.$shedule->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/shedules/'.$shedule->id);

        $this->assertResponseStatus(404);
    }
}
