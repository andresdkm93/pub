<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class event_productsApiTest extends TestCase
{
    use Makeevent_productsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateevent_products()
    {
        $eventProducts = $this->fakeevent_productsData();
        $this->json('POST', '/api/v1/eventProducts', $eventProducts);

        $this->assertApiResponse($eventProducts);
    }

    /**
     * @test
     */
    public function testReadevent_products()
    {
        $eventProducts = $this->makeevent_products();
        $this->json('GET', '/api/v1/eventProducts/'.$eventProducts->id);

        $this->assertApiResponse($eventProducts->toArray());
    }

    /**
     * @test
     */
    public function testUpdateevent_products()
    {
        $eventProducts = $this->makeevent_products();
        $editedevent_products = $this->fakeevent_productsData();

        $this->json('PUT', '/api/v1/eventProducts/'.$eventProducts->id, $editedevent_products);

        $this->assertApiResponse($editedevent_products);
    }

    /**
     * @test
     */
    public function testDeleteevent_products()
    {
        $eventProducts = $this->makeevent_products();
        $this->json('DELETE', '/api/v1/eventProducts/'.$eventProducts->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/eventProducts/'.$eventProducts->id);

        $this->assertResponseStatus(404);
    }
}
