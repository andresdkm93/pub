<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExchangueApiTest extends TestCase
{
    use MakeExchangueTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateExchangue()
    {
        $exchangue = $this->fakeExchangueData();
        $this->json('POST', '/api/v1/exchangues', $exchangue);

        $this->assertApiResponse($exchangue);
    }

    /**
     * @test
     */
    public function testReadExchangue()
    {
        $exchangue = $this->makeExchangue();
        $this->json('GET', '/api/v1/exchangues/'.$exchangue->id);

        $this->assertApiResponse($exchangue->toArray());
    }

    /**
     * @test
     */
    public function testUpdateExchangue()
    {
        $exchangue = $this->makeExchangue();
        $editedExchangue = $this->fakeExchangueData();

        $this->json('PUT', '/api/v1/exchangues/'.$exchangue->id, $editedExchangue);

        $this->assertApiResponse($editedExchangue);
    }

    /**
     * @test
     */
    public function testDeleteExchangue()
    {
        $exchangue = $this->makeExchangue();
        $this->json('DELETE', '/api/v1/exchangues/'.$exchangue->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/exchangues/'.$exchangue->id);

        $this->assertResponseStatus(404);
    }
}
