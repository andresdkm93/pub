<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StoreTypesApiTest extends TestCase
{
    use MakeStoreTypesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateStoreTypes()
    {
        $storeTypes = $this->fakeStoreTypesData();
        $this->json('POST', '/api/v1/storeTypes', $storeTypes);

        $this->assertApiResponse($storeTypes);
    }

    /**
     * @test
     */
    public function testReadStoreTypes()
    {
        $storeTypes = $this->makeStoreTypes();
        $this->json('GET', '/api/v1/storeTypes/'.$storeTypes->id);

        $this->assertApiResponse($storeTypes->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStoreTypes()
    {
        $storeTypes = $this->makeStoreTypes();
        $editedStoreTypes = $this->fakeStoreTypesData();

        $this->json('PUT', '/api/v1/storeTypes/'.$storeTypes->id, $editedStoreTypes);

        $this->assertApiResponse($editedStoreTypes);
    }

    /**
     * @test
     */
    public function testDeleteStoreTypes()
    {
        $storeTypes = $this->makeStoreTypes();
        $this->json('DELETE', '/api/v1/storeTypes/'.$storeTypes->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/storeTypes/'.$storeTypes->id);

        $this->assertResponseStatus(404);
    }
}
