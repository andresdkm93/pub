<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderStatusesApiTest extends TestCase
{
    use MakeOrderStatusesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateOrderStatuses()
    {
        $orderStatuses = $this->fakeOrderStatusesData();
        $this->json('POST', '/api/v1/orderStatuses', $orderStatuses);

        $this->assertApiResponse($orderStatuses);
    }

    /**
     * @test
     */
    public function testReadOrderStatuses()
    {
        $orderStatuses = $this->makeOrderStatuses();
        $this->json('GET', '/api/v1/orderStatuses/'.$orderStatuses->id);

        $this->assertApiResponse($orderStatuses->toArray());
    }

    /**
     * @test
     */
    public function testUpdateOrderStatuses()
    {
        $orderStatuses = $this->makeOrderStatuses();
        $editedOrderStatuses = $this->fakeOrderStatusesData();

        $this->json('PUT', '/api/v1/orderStatuses/'.$orderStatuses->id, $editedOrderStatuses);

        $this->assertApiResponse($editedOrderStatuses);
    }

    /**
     * @test
     */
    public function testDeleteOrderStatuses()
    {
        $orderStatuses = $this->makeOrderStatuses();
        $this->json('DELETE', '/api/v1/orderStatuses/'.$orderStatuses->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/orderStatuses/'.$orderStatuses->id);

        $this->assertResponseStatus(404);
    }
}
