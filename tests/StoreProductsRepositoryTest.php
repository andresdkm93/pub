<?php

use App\Models\StoreProducts;
use App\Repositories\StoreProductsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StoreProductsRepositoryTest extends TestCase
{
    use MakeStoreProductsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StoreProductsRepository
     */
    protected $storeProductsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->storeProductsRepo = App::make(StoreProductsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStoreProducts()
    {
        $storeProducts = $this->fakeStoreProductsData();
        $createdStoreProducts = $this->storeProductsRepo->create($storeProducts);
        $createdStoreProducts = $createdStoreProducts->toArray();
        $this->assertArrayHasKey('id', $createdStoreProducts);
        $this->assertNotNull($createdStoreProducts['id'], 'Created StoreProducts must have id specified');
        $this->assertNotNull(StoreProducts::find($createdStoreProducts['id']), 'StoreProducts with given id must be in DB');
        $this->assertModelData($storeProducts, $createdStoreProducts);
    }

    /**
     * @test read
     */
    public function testReadStoreProducts()
    {
        $storeProducts = $this->makeStoreProducts();
        $dbStoreProducts = $this->storeProductsRepo->find($storeProducts->id);
        $dbStoreProducts = $dbStoreProducts->toArray();
        $this->assertModelData($storeProducts->toArray(), $dbStoreProducts);
    }

    /**
     * @test update
     */
    public function testUpdateStoreProducts()
    {
        $storeProducts = $this->makeStoreProducts();
        $fakeStoreProducts = $this->fakeStoreProductsData();
        $updatedStoreProducts = $this->storeProductsRepo->update($fakeStoreProducts, $storeProducts->id);
        $this->assertModelData($fakeStoreProducts, $updatedStoreProducts->toArray());
        $dbStoreProducts = $this->storeProductsRepo->find($storeProducts->id);
        $this->assertModelData($fakeStoreProducts, $dbStoreProducts->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStoreProducts()
    {
        $storeProducts = $this->makeStoreProducts();
        $resp = $this->storeProductsRepo->delete($storeProducts->id);
        $this->assertTrue($resp);
        $this->assertNull(StoreProducts::find($storeProducts->id), 'StoreProducts should not exist in DB');
    }
}
