<?php

use App\Models\OrderStatuses;
use App\Repositories\OrderStatusesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderStatusesRepositoryTest extends TestCase
{
    use MakeOrderStatusesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrderStatusesRepository
     */
    protected $orderStatusesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->orderStatusesRepo = App::make(OrderStatusesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateOrderStatuses()
    {
        $orderStatuses = $this->fakeOrderStatusesData();
        $createdOrderStatuses = $this->orderStatusesRepo->create($orderStatuses);
        $createdOrderStatuses = $createdOrderStatuses->toArray();
        $this->assertArrayHasKey('id', $createdOrderStatuses);
        $this->assertNotNull($createdOrderStatuses['id'], 'Created OrderStatuses must have id specified');
        $this->assertNotNull(OrderStatuses::find($createdOrderStatuses['id']), 'OrderStatuses with given id must be in DB');
        $this->assertModelData($orderStatuses, $createdOrderStatuses);
    }

    /**
     * @test read
     */
    public function testReadOrderStatuses()
    {
        $orderStatuses = $this->makeOrderStatuses();
        $dbOrderStatuses = $this->orderStatusesRepo->find($orderStatuses->id);
        $dbOrderStatuses = $dbOrderStatuses->toArray();
        $this->assertModelData($orderStatuses->toArray(), $dbOrderStatuses);
    }

    /**
     * @test update
     */
    public function testUpdateOrderStatuses()
    {
        $orderStatuses = $this->makeOrderStatuses();
        $fakeOrderStatuses = $this->fakeOrderStatusesData();
        $updatedOrderStatuses = $this->orderStatusesRepo->update($fakeOrderStatuses, $orderStatuses->id);
        $this->assertModelData($fakeOrderStatuses, $updatedOrderStatuses->toArray());
        $dbOrderStatuses = $this->orderStatusesRepo->find($orderStatuses->id);
        $this->assertModelData($fakeOrderStatuses, $dbOrderStatuses->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteOrderStatuses()
    {
        $orderStatuses = $this->makeOrderStatuses();
        $resp = $this->orderStatusesRepo->delete($orderStatuses->id);
        $this->assertTrue($resp);
        $this->assertNull(OrderStatuses::find($orderStatuses->id), 'OrderStatuses should not exist in DB');
    }
}
