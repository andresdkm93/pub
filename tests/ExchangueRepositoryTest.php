<?php

use App\Models\Exchangue;
use App\Repositories\ExchangueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExchangueRepositoryTest extends TestCase
{
    use MakeExchangueTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ExchangueRepository
     */
    protected $exchangueRepo;

    public function setUp()
    {
        parent::setUp();
        $this->exchangueRepo = App::make(ExchangueRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateExchangue()
    {
        $exchangue = $this->fakeExchangueData();
        $createdExchangue = $this->exchangueRepo->create($exchangue);
        $createdExchangue = $createdExchangue->toArray();
        $this->assertArrayHasKey('id', $createdExchangue);
        $this->assertNotNull($createdExchangue['id'], 'Created Exchangue must have id specified');
        $this->assertNotNull(Exchangue::find($createdExchangue['id']), 'Exchangue with given id must be in DB');
        $this->assertModelData($exchangue, $createdExchangue);
    }

    /**
     * @test read
     */
    public function testReadExchangue()
    {
        $exchangue = $this->makeExchangue();
        $dbExchangue = $this->exchangueRepo->find($exchangue->id);
        $dbExchangue = $dbExchangue->toArray();
        $this->assertModelData($exchangue->toArray(), $dbExchangue);
    }

    /**
     * @test update
     */
    public function testUpdateExchangue()
    {
        $exchangue = $this->makeExchangue();
        $fakeExchangue = $this->fakeExchangueData();
        $updatedExchangue = $this->exchangueRepo->update($fakeExchangue, $exchangue->id);
        $this->assertModelData($fakeExchangue, $updatedExchangue->toArray());
        $dbExchangue = $this->exchangueRepo->find($exchangue->id);
        $this->assertModelData($fakeExchangue, $dbExchangue->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteExchangue()
    {
        $exchangue = $this->makeExchangue();
        $resp = $this->exchangueRepo->delete($exchangue->id);
        $this->assertTrue($resp);
        $this->assertNull(Exchangue::find($exchangue->id), 'Exchangue should not exist in DB');
    }
}
