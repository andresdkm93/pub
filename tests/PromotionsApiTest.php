<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PromotionsApiTest extends TestCase
{
    use MakePromotionsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePromotions()
    {
        $promotions = $this->fakePromotionsData();
        $this->json('POST', '/api/v1/promotions', $promotions);

        $this->assertApiResponse($promotions);
    }

    /**
     * @test
     */
    public function testReadPromotions()
    {
        $promotions = $this->makePromotions();
        $this->json('GET', '/api/v1/promotions/'.$promotions->id);

        $this->assertApiResponse($promotions->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePromotions()
    {
        $promotions = $this->makePromotions();
        $editedPromotions = $this->fakePromotionsData();

        $this->json('PUT', '/api/v1/promotions/'.$promotions->id, $editedPromotions);

        $this->assertApiResponse($editedPromotions);
    }

    /**
     * @test
     */
    public function testDeletePromotions()
    {
        $promotions = $this->makePromotions();
        $this->json('DELETE', '/api/v1/promotions/'.$promotions->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/promotions/'.$promotions->id);

        $this->assertResponseStatus(404);
    }
}
