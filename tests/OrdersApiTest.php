<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrdersApiTest extends TestCase
{
    use MakeOrdersTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateOrders()
    {
        $orders = $this->fakeOrdersData();
        $this->json('POST', '/api/v1/orders', $orders);

        $this->assertApiResponse($orders);
    }

    /**
     * @test
     */
    public function testReadOrders()
    {
        $orders = $this->makeOrders();
        $this->json('GET', '/api/v1/orders/'.$orders->id);

        $this->assertApiResponse($orders->toArray());
    }

    /**
     * @test
     */
    public function testUpdateOrders()
    {
        $orders = $this->makeOrders();
        $editedOrders = $this->fakeOrdersData();

        $this->json('PUT', '/api/v1/orders/'.$orders->id, $editedOrders);

        $this->assertApiResponse($editedOrders);
    }

    /**
     * @test
     */
    public function testDeleteOrders()
    {
        $orders = $this->makeOrders();
        $this->json('DELETE', '/api/v1/orders/'.$orders->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/orders/'.$orders->id);

        $this->assertResponseStatus(404);
    }
}
