<?php

use App\Models\OrderProducts;
use App\Repositories\OrderProductsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderProductsRepositoryTest extends TestCase
{
    use MakeOrderProductsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrderProductsRepository
     */
    protected $orderProductsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->orderProductsRepo = App::make(OrderProductsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateOrderProducts()
    {
        $orderProducts = $this->fakeOrderProductsData();
        $createdOrderProducts = $this->orderProductsRepo->create($orderProducts);
        $createdOrderProducts = $createdOrderProducts->toArray();
        $this->assertArrayHasKey('id', $createdOrderProducts);
        $this->assertNotNull($createdOrderProducts['id'], 'Created OrderProducts must have id specified');
        $this->assertNotNull(OrderProducts::find($createdOrderProducts['id']), 'OrderProducts with given id must be in DB');
        $this->assertModelData($orderProducts, $createdOrderProducts);
    }

    /**
     * @test read
     */
    public function testReadOrderProducts()
    {
        $orderProducts = $this->makeOrderProducts();
        $dbOrderProducts = $this->orderProductsRepo->find($orderProducts->id);
        $dbOrderProducts = $dbOrderProducts->toArray();
        $this->assertModelData($orderProducts->toArray(), $dbOrderProducts);
    }

    /**
     * @test update
     */
    public function testUpdateOrderProducts()
    {
        $orderProducts = $this->makeOrderProducts();
        $fakeOrderProducts = $this->fakeOrderProductsData();
        $updatedOrderProducts = $this->orderProductsRepo->update($fakeOrderProducts, $orderProducts->id);
        $this->assertModelData($fakeOrderProducts, $updatedOrderProducts->toArray());
        $dbOrderProducts = $this->orderProductsRepo->find($orderProducts->id);
        $this->assertModelData($fakeOrderProducts, $dbOrderProducts->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteOrderProducts()
    {
        $orderProducts = $this->makeOrderProducts();
        $resp = $this->orderProductsRepo->delete($orderProducts->id);
        $this->assertTrue($resp);
        $this->assertNull(OrderProducts::find($orderProducts->id), 'OrderProducts should not exist in DB');
    }
}
