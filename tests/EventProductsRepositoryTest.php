<?php

use App\Models\EventProducts;
use App\Repositories\EventProductsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventProductsRepositoryTest extends TestCase
{
    use MakeEventProductsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var EventProductsRepository
     */
    protected $eventProductsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->eventProductsRepo = App::make(EventProductsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateEventProducts()
    {
        $eventProducts = $this->fakeEventProductsData();
        $createdEventProducts = $this->eventProductsRepo->create($eventProducts);
        $createdEventProducts = $createdEventProducts->toArray();
        $this->assertArrayHasKey('id', $createdEventProducts);
        $this->assertNotNull($createdEventProducts['id'], 'Created EventProducts must have id specified');
        $this->assertNotNull(EventProducts::find($createdEventProducts['id']), 'EventProducts with given id must be in DB');
        $this->assertModelData($eventProducts, $createdEventProducts);
    }

    /**
     * @test read
     */
    public function testReadEventProducts()
    {
        $eventProducts = $this->makeEventProducts();
        $dbEventProducts = $this->eventProductsRepo->find($eventProducts->id);
        $dbEventProducts = $dbEventProducts->toArray();
        $this->assertModelData($eventProducts->toArray(), $dbEventProducts);
    }

    /**
     * @test update
     */
    public function testUpdateEventProducts()
    {
        $eventProducts = $this->makeEventProducts();
        $fakeEventProducts = $this->fakeEventProductsData();
        $updatedEventProducts = $this->eventProductsRepo->update($fakeEventProducts, $eventProducts->id);
        $this->assertModelData($fakeEventProducts, $updatedEventProducts->toArray());
        $dbEventProducts = $this->eventProductsRepo->find($eventProducts->id);
        $this->assertModelData($fakeEventProducts, $dbEventProducts->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteEventProducts()
    {
        $eventProducts = $this->makeEventProducts();
        $resp = $this->eventProductsRepo->delete($eventProducts->id);
        $this->assertTrue($resp);
        $this->assertNull(EventProducts::find($eventProducts->id), 'EventProducts should not exist in DB');
    }
}
