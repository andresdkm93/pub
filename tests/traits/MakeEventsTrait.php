<?php

use Faker\Factory as Faker;
use App\Models\Events;
use App\Repositories\EventsRepository;

trait MakeEventsTrait
{
    /**
     * Create fake instance of Events and save it in database
     *
     * @param array $eventsFields
     * @return Events
     */
    public function makeEvents($eventsFields = [])
    {
        /** @var EventsRepository $eventsRepo */
        $eventsRepo = App::make(EventsRepository::class);
        $theme = $this->fakeEventsData($eventsFields);
        return $eventsRepo->create($theme);
    }

    /**
     * Get fake instance of Events
     *
     * @param array $eventsFields
     * @return Events
     */
    public function fakeEvents($eventsFields = [])
    {
        return new Events($this->fakeEventsData($eventsFields));
    }

    /**
     * Get fake data of Events
     *
     * @param array $postFields
     * @return array
     */
    public function fakeEventsData($eventsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'date' => $fake->word,
            'name' => $fake->word,
            'place' => $fake->word,
            'description' => $fake->word,
            'image' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $eventsFields);
    }
}
