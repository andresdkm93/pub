<?php

use Faker\Factory as Faker;
use App\Models\Promotions;
use App\Repositories\PromotionsRepository;

trait MakePromotionsTrait
{
    /**
     * Create fake instance of Promotions and save it in database
     *
     * @param array $promotionsFields
     * @return Promotions
     */
    public function makePromotions($promotionsFields = [])
    {
        /** @var PromotionsRepository $promotionsRepo */
        $promotionsRepo = App::make(PromotionsRepository::class);
        $theme = $this->fakePromotionsData($promotionsFields);
        return $promotionsRepo->create($theme);
    }

    /**
     * Get fake instance of Promotions
     *
     * @param array $promotionsFields
     * @return Promotions
     */
    public function fakePromotions($promotionsFields = [])
    {
        return new Promotions($this->fakePromotionsData($promotionsFields));
    }

    /**
     * Get fake data of Promotions
     *
     * @param array $postFields
     * @return array
     */
    public function fakePromotionsData($promotionsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'image' => $fake->word,
            'quantity' => $fake->randomDigitNotNull,
            'start_date' => $fake->word,
            'end_date' => $fake->word,
            'promotion_type' => $fake->randomDigitNotNull,
            'store_product_id' => $fake->randomDigitNotNull,
            'discount' => $fake->randomDigitNotNull,
            'quantity_minimal' => $fake->randomDigitNotNull,
            'divider' => $fake->randomDigitNotNull,
            'multiplier' => $fake->randomDigitNotNull,
            'description' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $promotionsFields);
    }
}
