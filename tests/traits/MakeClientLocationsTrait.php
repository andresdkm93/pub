<?php

use Faker\Factory as Faker;
use App\Models\ClientLocations;
use App\Repositories\ClientLocationsRepository;

trait MakeClientLocationsTrait
{
    /**
     * Create fake instance of ClientLocations and save it in database
     *
     * @param array $clientLocationsFields
     * @return ClientLocations
     */
    public function makeClientLocations($clientLocationsFields = [])
    {
        /** @var ClientLocationsRepository $clientLocationsRepo */
        $clientLocationsRepo = App::make(ClientLocationsRepository::class);
        $theme = $this->fakeClientLocationsData($clientLocationsFields);
        return $clientLocationsRepo->create($theme);
    }

    /**
     * Get fake instance of ClientLocations
     *
     * @param array $clientLocationsFields
     * @return ClientLocations
     */
    public function fakeClientLocations($clientLocationsFields = [])
    {
        return new ClientLocations($this->fakeClientLocationsData($clientLocationsFields));
    }

    /**
     * Get fake data of ClientLocations
     *
     * @param array $postFields
     * @return array
     */
    public function fakeClientLocationsData($clientLocationsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'address' => $fake->word,
            'latitude' => $fake->word,
            'longitude' => $fake->word,
            'client_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $clientLocationsFields);
    }
}
