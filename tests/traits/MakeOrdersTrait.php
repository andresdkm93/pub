<?php

use Faker\Factory as Faker;
use App\Models\Orders;
use App\Repositories\OrdersRepository;

trait MakeOrdersTrait
{
    /**
     * Create fake instance of Orders and save it in database
     *
     * @param array $ordersFields
     * @return Orders
     */
    public function makeOrders($ordersFields = [])
    {
        /** @var OrdersRepository $ordersRepo */
        $ordersRepo = App::make(OrdersRepository::class);
        $theme = $this->fakeOrdersData($ordersFields);
        return $ordersRepo->create($theme);
    }

    /**
     * Get fake instance of Orders
     *
     * @param array $ordersFields
     * @return Orders
     */
    public function fakeOrders($ordersFields = [])
    {
        return new Orders($this->fakeOrdersData($ordersFields));
    }

    /**
     * Get fake data of Orders
     *
     * @param array $postFields
     * @return array
     */
    public function fakeOrdersData($ordersFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'amount' => $fake->word,
            'latitude' => $fake->word,
            'longitude' => $fake->word,
            'address' => $fake->word,
            'client_id' => $fake->randomDigitNotNull,
            'order_status_id' => $fake->randomDigitNotNull,
            'code' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ordersFields);
    }
}
