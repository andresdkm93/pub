<?php

use Faker\Factory as Faker;
use App\Models\OrderStatuses;
use App\Repositories\OrderStatusesRepository;

trait MakeOrderStatusesTrait
{
    /**
     * Create fake instance of OrderStatuses and save it in database
     *
     * @param array $orderStatusesFields
     * @return OrderStatuses
     */
    public function makeOrderStatuses($orderStatusesFields = [])
    {
        /** @var OrderStatusesRepository $orderStatusesRepo */
        $orderStatusesRepo = App::make(OrderStatusesRepository::class);
        $theme = $this->fakeOrderStatusesData($orderStatusesFields);
        return $orderStatusesRepo->create($theme);
    }

    /**
     * Get fake instance of OrderStatuses
     *
     * @param array $orderStatusesFields
     * @return OrderStatuses
     */
    public function fakeOrderStatuses($orderStatusesFields = [])
    {
        return new OrderStatuses($this->fakeOrderStatusesData($orderStatusesFields));
    }

    /**
     * Get fake data of OrderStatuses
     *
     * @param array $postFields
     * @return array
     */
    public function fakeOrderStatusesData($orderStatusesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $orderStatusesFields);
    }
}
