<?php

use Faker\Factory as Faker;
use App\Models\Exchangue;
use App\Repositories\ExchangueRepository;

trait MakeExchangueTrait
{
    /**
     * Create fake instance of Exchangue and save it in database
     *
     * @param array $exchangueFields
     * @return Exchangue
     */
    public function makeExchangue($exchangueFields = [])
    {
        /** @var ExchangueRepository $exchangueRepo */
        $exchangueRepo = App::make(ExchangueRepository::class);
        $theme = $this->fakeExchangueData($exchangueFields);
        return $exchangueRepo->create($theme);
    }

    /**
     * Get fake instance of Exchangue
     *
     * @param array $exchangueFields
     * @return Exchangue
     */
    public function fakeExchangue($exchangueFields = [])
    {
        return new Exchangue($this->fakeExchangueData($exchangueFields));
    }

    /**
     * Get fake data of Exchangue
     *
     * @param array $postFields
     * @return array
     */
    public function fakeExchangueData($exchangueFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $exchangueFields);
    }
}
