<?php

use Faker\Factory as Faker;
use App\Models\Shedule;
use App\Repositories\SheduleRepository;

trait MakeSheduleTrait
{
    /**
     * Create fake instance of Shedule and save it in database
     *
     * @param array $sheduleFields
     * @return Shedule
     */
    public function makeShedule($sheduleFields = [])
    {
        /** @var SheduleRepository $sheduleRepo */
        $sheduleRepo = App::make(SheduleRepository::class);
        $theme = $this->fakeSheduleData($sheduleFields);
        return $sheduleRepo->create($theme);
    }

    /**
     * Get fake instance of Shedule
     *
     * @param array $sheduleFields
     * @return Shedule
     */
    public function fakeShedule($sheduleFields = [])
    {
        return new Shedule($this->fakeSheduleData($sheduleFields));
    }

    /**
     * Get fake data of Shedule
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSheduleData($sheduleFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            
        ], $sheduleFields);
    }
}
