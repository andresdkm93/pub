<?php

use Faker\Factory as Faker;
use App\Models\Codes;
use App\Repositories\CodesRepository;

trait MakeCodesTrait
{
    /**
     * Create fake instance of Codes and save it in database
     *
     * @param array $codesFields
     * @return Codes
     */
    public function makeCodes($codesFields = [])
    {
        /** @var CodesRepository $codesRepo */
        $codesRepo = App::make(CodesRepository::class);
        $theme = $this->fakeCodesData($codesFields);
        return $codesRepo->create($theme);
    }

    /**
     * Get fake instance of Codes
     *
     * @param array $codesFields
     * @return Codes
     */
    public function fakeCodes($codesFields = [])
    {
        return new Codes($this->fakeCodesData($codesFields));
    }

    /**
     * Get fake data of Codes
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCodesData($codesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'code' => $fake->word,
            'discount' => $fake->randomDigitNotNull,
            'start_date' => $fake->word,
            'end_date' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $codesFields);
    }
}
