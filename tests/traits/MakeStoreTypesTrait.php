<?php

use Faker\Factory as Faker;
use App\Models\StoreTypes;
use App\Repositories\StoreTypesRepository;

trait MakeStoreTypesTrait
{
    /**
     * Create fake instance of StoreTypes and save it in database
     *
     * @param array $storeTypesFields
     * @return StoreTypes
     */
    public function makeStoreTypes($storeTypesFields = [])
    {
        /** @var StoreTypesRepository $storeTypesRepo */
        $storeTypesRepo = App::make(StoreTypesRepository::class);
        $theme = $this->fakeStoreTypesData($storeTypesFields);
        return $storeTypesRepo->create($theme);
    }

    /**
     * Get fake instance of StoreTypes
     *
     * @param array $storeTypesFields
     * @return StoreTypes
     */
    public function fakeStoreTypes($storeTypesFields = [])
    {
        return new StoreTypes($this->fakeStoreTypesData($storeTypesFields));
    }

    /**
     * Get fake data of StoreTypes
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStoreTypesData($storeTypesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $storeTypesFields);
    }
}
