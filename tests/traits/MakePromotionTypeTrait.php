<?php

use Faker\Factory as Faker;
use App\Models\Web\PromotionType;
use App\Repositories\Web\PromotionTypeRepository;

trait MakePromotionTypeTrait
{
    /**
     * Create fake instance of PromotionType and save it in database
     *
     * @param array $promotionTypeFields
     * @return PromotionType
     */
    public function makePromotionType($promotionTypeFields = [])
    {
        /** @var PromotionTypeRepository $promotionTypeRepo */
        $promotionTypeRepo = App::make(PromotionTypeRepository::class);
        $theme = $this->fakePromotionTypeData($promotionTypeFields);
        return $promotionTypeRepo->create($theme);
    }

    /**
     * Get fake instance of PromotionType
     *
     * @param array $promotionTypeFields
     * @return PromotionType
     */
    public function fakePromotionType($promotionTypeFields = [])
    {
        return new PromotionType($this->fakePromotionTypeData($promotionTypeFields));
    }

    /**
     * Get fake data of PromotionType
     *
     * @param array $postFields
     * @return array
     */
    public function fakePromotionTypeData($promotionTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $promotionTypeFields);
    }
}
