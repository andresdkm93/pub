<?php

use Faker\Factory as Faker;
use App\Models\event_products;
use App\Repositories\event_productsRepository;

trait Makeevent_productsTrait
{
    /**
     * Create fake instance of event_products and save it in database
     *
     * @param array $eventProductsFields
     * @return event_products
     */
    public function makeevent_products($eventProductsFields = [])
    {
        /** @var event_productsRepository $eventProductsRepo */
        $eventProductsRepo = App::make(event_productsRepository::class);
        $theme = $this->fakeevent_productsData($eventProductsFields);
        return $eventProductsRepo->create($theme);
    }

    /**
     * Get fake instance of event_products
     *
     * @param array $eventProductsFields
     * @return event_products
     */
    public function fakeevent_products($eventProductsFields = [])
    {
        return new event_products($this->fakeevent_productsData($eventProductsFields));
    }

    /**
     * Get fake data of event_products
     *
     * @param array $postFields
     * @return array
     */
    public function fakeevent_productsData($eventProductsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'events_id' => $fake->randomDigitNotNull,
            'store_product_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $eventProductsFields);
    }
}
