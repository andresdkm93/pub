<?php

use Faker\Factory as Faker;
use App\Models\OrderProducts;
use App\Repositories\OrderProductsRepository;

trait MakeOrderProductsTrait
{
    /**
     * Create fake instance of OrderProducts and save it in database
     *
     * @param array $orderProductsFields
     * @return OrderProducts
     */
    public function makeOrderProducts($orderProductsFields = [])
    {
        /** @var OrderProductsRepository $orderProductsRepo */
        $orderProductsRepo = App::make(OrderProductsRepository::class);
        $theme = $this->fakeOrderProductsData($orderProductsFields);
        return $orderProductsRepo->create($theme);
    }

    /**
     * Get fake instance of OrderProducts
     *
     * @param array $orderProductsFields
     * @return OrderProducts
     */
    public function fakeOrderProducts($orderProductsFields = [])
    {
        return new OrderProducts($this->fakeOrderProductsData($orderProductsFields));
    }

    /**
     * Get fake data of OrderProducts
     *
     * @param array $postFields
     * @return array
     */
    public function fakeOrderProductsData($orderProductsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            
        ], $orderProductsFields);
    }
}
