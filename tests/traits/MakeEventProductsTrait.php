<?php

use Faker\Factory as Faker;
use App\Models\EventProducts;
use App\Repositories\EventProductsRepository;

trait MakeEventProductsTrait
{
    /**
     * Create fake instance of EventProducts and save it in database
     *
     * @param array $eventProductsFields
     * @return EventProducts
     */
    public function makeEventProducts($eventProductsFields = [])
    {
        /** @var EventProductsRepository $eventProductsRepo */
        $eventProductsRepo = App::make(EventProductsRepository::class);
        $theme = $this->fakeEventProductsData($eventProductsFields);
        return $eventProductsRepo->create($theme);
    }

    /**
     * Get fake instance of EventProducts
     *
     * @param array $eventProductsFields
     * @return EventProducts
     */
    public function fakeEventProducts($eventProductsFields = [])
    {
        return new EventProducts($this->fakeEventProductsData($eventProductsFields));
    }

    /**
     * Get fake data of EventProducts
     *
     * @param array $postFields
     * @return array
     */
    public function fakeEventProductsData($eventProductsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'events_id' => $fake->randomDigitNotNull,
            'store_product_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $eventProductsFields);
    }
}
