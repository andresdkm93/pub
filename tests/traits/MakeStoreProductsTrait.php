<?php

use Faker\Factory as Faker;
use App\Models\StoreProducts;
use App\Repositories\StoreProductsRepository;

trait MakeStoreProductsTrait
{
    /**
     * Create fake instance of StoreProducts and save it in database
     *
     * @param array $storeProductsFields
     * @return StoreProducts
     */
    public function makeStoreProducts($storeProductsFields = [])
    {
        /** @var StoreProductsRepository $storeProductsRepo */
        $storeProductsRepo = App::make(StoreProductsRepository::class);
        $theme = $this->fakeStoreProductsData($storeProductsFields);
        return $storeProductsRepo->create($theme);
    }

    /**
     * Get fake instance of StoreProducts
     *
     * @param array $storeProductsFields
     * @return StoreProducts
     */
    public function fakeStoreProducts($storeProductsFields = [])
    {
        return new StoreProducts($this->fakeStoreProductsData($storeProductsFields));
    }

    /**
     * Get fake data of StoreProducts
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStoreProductsData($storeProductsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_id' => $fake->randomDigitNotNull,
            'quantity' => $fake->randomDigitNotNull,
            'price' => $fake->randomDigitNotNull,
            'status' => $fake->word,
            'start_date' => $fake->word,
            'end_date' => $fake->word,
            'store_type' => $fake->randomDigitNotNull,
            'points' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $storeProductsFields);
    }
}
