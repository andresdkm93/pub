<?php

use Faker\Factory as Faker;
use App\Models\Zones;
use App\Repositories\ZonesRepository;

trait MakeZonesTrait
{
    /**
     * Create fake instance of Zones and save it in database
     *
     * @param array $zonesFields
     * @return Zones
     */
    public function makeZones($zonesFields = [])
    {
        /** @var ZonesRepository $zonesRepo */
        $zonesRepo = App::make(ZonesRepository::class);
        $theme = $this->fakeZonesData($zonesFields);
        return $zonesRepo->create($theme);
    }

    /**
     * Get fake instance of Zones
     *
     * @param array $zonesFields
     * @return Zones
     */
    public function fakeZones($zonesFields = [])
    {
        return new Zones($this->fakeZonesData($zonesFields));
    }

    /**
     * Get fake data of Zones
     *
     * @param array $postFields
     * @return array
     */
    public function fakeZonesData($zonesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'latitude' => $fake->word,
            'longitude' => $fake->word,
            'radio' => $fake->randomDigitNotNull,
            'price' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $zonesFields);
    }
}
