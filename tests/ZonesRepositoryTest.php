<?php

use App\Models\Zones;
use App\Repositories\ZonesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ZonesRepositoryTest extends TestCase
{
    use MakeZonesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ZonesRepository
     */
    protected $zonesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->zonesRepo = App::make(ZonesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateZones()
    {
        $zones = $this->fakeZonesData();
        $createdZones = $this->zonesRepo->create($zones);
        $createdZones = $createdZones->toArray();
        $this->assertArrayHasKey('id', $createdZones);
        $this->assertNotNull($createdZones['id'], 'Created Zones must have id specified');
        $this->assertNotNull(Zones::find($createdZones['id']), 'Zones with given id must be in DB');
        $this->assertModelData($zones, $createdZones);
    }

    /**
     * @test read
     */
    public function testReadZones()
    {
        $zones = $this->makeZones();
        $dbZones = $this->zonesRepo->find($zones->id);
        $dbZones = $dbZones->toArray();
        $this->assertModelData($zones->toArray(), $dbZones);
    }

    /**
     * @test update
     */
    public function testUpdateZones()
    {
        $zones = $this->makeZones();
        $fakeZones = $this->fakeZonesData();
        $updatedZones = $this->zonesRepo->update($fakeZones, $zones->id);
        $this->assertModelData($fakeZones, $updatedZones->toArray());
        $dbZones = $this->zonesRepo->find($zones->id);
        $this->assertModelData($fakeZones, $dbZones->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteZones()
    {
        $zones = $this->makeZones();
        $resp = $this->zonesRepo->delete($zones->id);
        $this->assertTrue($resp);
        $this->assertNull(Zones::find($zones->id), 'Zones should not exist in DB');
    }
}
