<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderProductsApiTest extends TestCase
{
    use MakeOrderProductsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateOrderProducts()
    {
        $orderProducts = $this->fakeOrderProductsData();
        $this->json('POST', '/api/v1/orderProducts', $orderProducts);

        $this->assertApiResponse($orderProducts);
    }

    /**
     * @test
     */
    public function testReadOrderProducts()
    {
        $orderProducts = $this->makeOrderProducts();
        $this->json('GET', '/api/v1/orderProducts/'.$orderProducts->id);

        $this->assertApiResponse($orderProducts->toArray());
    }

    /**
     * @test
     */
    public function testUpdateOrderProducts()
    {
        $orderProducts = $this->makeOrderProducts();
        $editedOrderProducts = $this->fakeOrderProductsData();

        $this->json('PUT', '/api/v1/orderProducts/'.$orderProducts->id, $editedOrderProducts);

        $this->assertApiResponse($editedOrderProducts);
    }

    /**
     * @test
     */
    public function testDeleteOrderProducts()
    {
        $orderProducts = $this->makeOrderProducts();
        $this->json('DELETE', '/api/v1/orderProducts/'.$orderProducts->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/orderProducts/'.$orderProducts->id);

        $this->assertResponseStatus(404);
    }
}
