<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventProductsApiTest extends TestCase
{
    use MakeEventProductsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateEventProducts()
    {
        $eventProducts = $this->fakeEventProductsData();
        $this->json('POST', '/api/v1/eventProducts', $eventProducts);

        $this->assertApiResponse($eventProducts);
    }

    /**
     * @test
     */
    public function testReadEventProducts()
    {
        $eventProducts = $this->makeEventProducts();
        $this->json('GET', '/api/v1/eventProducts/'.$eventProducts->id);

        $this->assertApiResponse($eventProducts->toArray());
    }

    /**
     * @test
     */
    public function testUpdateEventProducts()
    {
        $eventProducts = $this->makeEventProducts();
        $editedEventProducts = $this->fakeEventProductsData();

        $this->json('PUT', '/api/v1/eventProducts/'.$eventProducts->id, $editedEventProducts);

        $this->assertApiResponse($editedEventProducts);
    }

    /**
     * @test
     */
    public function testDeleteEventProducts()
    {
        $eventProducts = $this->makeEventProducts();
        $this->json('DELETE', '/api/v1/eventProducts/'.$eventProducts->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/eventProducts/'.$eventProducts->id);

        $this->assertResponseStatus(404);
    }
}
