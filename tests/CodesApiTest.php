<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CodesApiTest extends TestCase
{
    use MakeCodesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCodes()
    {
        $codes = $this->fakeCodesData();
        $this->json('POST', '/api/v1/codes', $codes);

        $this->assertApiResponse($codes);
    }

    /**
     * @test
     */
    public function testReadCodes()
    {
        $codes = $this->makeCodes();
        $this->json('GET', '/api/v1/codes/'.$codes->id);

        $this->assertApiResponse($codes->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCodes()
    {
        $codes = $this->makeCodes();
        $editedCodes = $this->fakeCodesData();

        $this->json('PUT', '/api/v1/codes/'.$codes->id, $editedCodes);

        $this->assertApiResponse($editedCodes);
    }

    /**
     * @test
     */
    public function testDeleteCodes()
    {
        $codes = $this->makeCodes();
        $this->json('DELETE', '/api/v1/codes/'.$codes->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/codes/'.$codes->id);

        $this->assertResponseStatus(404);
    }
}
