<?php

use App\Models\Codes;
use App\Repositories\CodesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CodesRepositoryTest extends TestCase
{
    use MakeCodesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CodesRepository
     */
    protected $codesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->codesRepo = App::make(CodesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCodes()
    {
        $codes = $this->fakeCodesData();
        $createdCodes = $this->codesRepo->create($codes);
        $createdCodes = $createdCodes->toArray();
        $this->assertArrayHasKey('id', $createdCodes);
        $this->assertNotNull($createdCodes['id'], 'Created Codes must have id specified');
        $this->assertNotNull(Codes::find($createdCodes['id']), 'Codes with given id must be in DB');
        $this->assertModelData($codes, $createdCodes);
    }

    /**
     * @test read
     */
    public function testReadCodes()
    {
        $codes = $this->makeCodes();
        $dbCodes = $this->codesRepo->find($codes->id);
        $dbCodes = $dbCodes->toArray();
        $this->assertModelData($codes->toArray(), $dbCodes);
    }

    /**
     * @test update
     */
    public function testUpdateCodes()
    {
        $codes = $this->makeCodes();
        $fakeCodes = $this->fakeCodesData();
        $updatedCodes = $this->codesRepo->update($fakeCodes, $codes->id);
        $this->assertModelData($fakeCodes, $updatedCodes->toArray());
        $dbCodes = $this->codesRepo->find($codes->id);
        $this->assertModelData($fakeCodes, $dbCodes->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCodes()
    {
        $codes = $this->makeCodes();
        $resp = $this->codesRepo->delete($codes->id);
        $this->assertTrue($resp);
        $this->assertNull(Codes::find($codes->id), 'Codes should not exist in DB');
    }
}
