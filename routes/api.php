<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/





Route::resource('categories', 'CategoriesAPIController');



Route::resource('clients', 'ClientsAPIController');

Route::resource('clients/{clientId}/orders', 'OrderClientsAPIController');
Route::resource('clients/{clientId}/exchangues', 'ExchangueAPIController');


Route::post('clients/sendpush', 'ClientsAPIController@sendPush');

Route::post('clients/sendmasivepush', 'ClientsAPIController@sendMasivePush');

Route::resource('codes', 'CodesAPIController');

Route::resource('devices', 'DevicesAPIController');

Route::resource('events', 'EventsAPIController');

Route::resource('events/{eventId}/eventProducts', 'EventProductsAPIController');

Route::resource('orderProducts', 'OrderProductsAPIController');

Route::resource('orders', 'OrdersAPIController');

Route::resource('orderStatuses', 'OrderStatusesAPIController');

Route::resource('os', 'OsAPIController');

Route::resource('products', 'ProductsAPIController');

Route::resource('storeProducts', 'StoreProductsAPIController');


Route::resource('stockProducts', 'StockProductsAPIController');

Route::resource('promotionProducts', 'PromotionProductsAPIController');


Route::resource('storeTypes', 'StoreTypesAPIController');

Route::resource('users', 'UserAPIController');


Route::post("updateResource", "ResourceAPIController@updateResource");


Route::resource('shedules', 'SheduleAPIController');


Route::get('productValues', 'OrdersAPIController@values');



Route::post('search/{name}', 'QueryAPIController@search');


Route::resource('promotionTypes', 'PromotionTypeAPIController');

Route::resource('promotions', 'PromotionsAPIController');

Route::resource('promotionTypes', 'PromotionTypeAPIController');

Route::resource('promotions', 'PromotionsAPIController');

Route::resource('promotionTypes', 'PromotionTypeAPIController');

Route::resource('zones', 'ZonesAPIController');

Route::resource('exchangues', 'ExchangueAPIController',["only"=>['index','show']]);

Route::resource('printer', 'PrinterAPIController');

Route::get('orders/printer/{id}', 'OrderPrinterAPIController@show');

