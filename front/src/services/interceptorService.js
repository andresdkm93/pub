import Vue from 'vue'
import {Storage} from './storageService'
import {sideBar} from './sideBar'
import {home} from '../js/home'

let interceptor = new Vue({
    el: '#spinner-dash',
    data: {
        active: false
    },
    methods: {
        show: function () {
            this.active = true;
        },
        hide: function () {
            this.active = false
        }
    }
});
Vue.http.interceptors.push(function (request, next) {
    interceptor.show();
    next(function(response) {
        interceptor.hide();

    });
});


let initComponents=function()
{
    if(document.getElementById("sideBar"))
    {
        new Vue(sideBar);
    }
    if (document.getElementById("home")) {
        new Vue(home);
    }
}
Vue.http.get('/api/constants').then((response)=>{
    if(response.body.data){
        Storage.setAll(response.body.data);
        initComponents();
    }
});

$('#logout').click(function () {
    $('#logout-spinner').show();
});

