/**
 * Created by andresdkm on 7/04/17.
 */
let defaultStorage = window.localStorage;
export default {
    clean(){
        delete defaultStorage['orders'];
    },
    getProduct: function (storeId) {
        if (!defaultStorage['orders']) {
            return null;
        } else {
            let products = JSON.parse(defaultStorage['orders'])
            let productAux = null;
            products.forEach(function (product) {
                if (product.storeId == storeId) {
                    console.log("localized", product);
                    productAux = product;
                    return false;
                }
            });
            return productAux;
        }

    },
    saveProduct(product) {
        let products = [];
        if (defaultStorage['orders']) {
            products = JSON.parse(defaultStorage['orders']);
        }
        let productAux = null;
        products.forEach(function (item) {
            if (item.storeId == product.storeId) {
                productAux = item;
                return false;
            }
        });
        if (productAux) {
            productAux.quantityShop = product.quantityShop;
            productAux.total = product.total;
        } else {
            products.push(product);
        }
        defaultStorage['orders'] = JSON.stringify(products);
    },
    removeProduct(product) {
        let index = null;
        let products = [];
        if (defaultStorage['orders']) {
            products = JSON.parse(defaultStorage['orders']);
        }
        products.forEach(function (item, k) {
            if (item.storeId == product.storeId) {
                index = k;
                return false;
            }
        });
        if (index != null) {
            products.splice(index, 1);
            defaultStorage['orders'] = JSON.stringify(products);
        }
    },
    count() {
        if (!defaultStorage['orders']) {
            return 0;
        }
        let count = 0;
        let products = JSON.parse(defaultStorage['orders']);
        products.forEach(function (item, k) {
            count += item.quantityShop;
        });
        return count;
    },
    sum() {
        if (!defaultStorage['orders']) {
            return 0;
        }
        let count = 0;
        let products = JSON.parse(defaultStorage['orders']);
        products.forEach(function (item, k) {
            count += item.total;
        });
        return count;
    },
    sumProducts() {
        if (!defaultStorage['orders']) {
            return 0;
        }
        let count = 0;
        let products = JSON.parse(defaultStorage['orders']);
        products.forEach(function (item, k) {
            if (!item.event) {
                count += item.total;
            }
        });
        return count;
    },
    sumEvents() {
        if (!defaultStorage['orders']) {
            return 0;
        }
        let count = 0;
        let products = JSON.parse(defaultStorage['orders']);
        products.forEach(function (item, k) {
            if (item.event) {
                count += item.total;
            }
        });
        return count;
    },
    getProducts() {
        if (!defaultStorage['orders']) {
            return [];
        }
        let products = JSON.parse(defaultStorage['orders']);
        return products;
    }
}