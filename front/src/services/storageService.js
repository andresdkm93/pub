/**
 * Created by andresdkm on 7/04/17.
 */
let defaultStorage = window.localStorage;
export default {
    get: function (key) {
        if (defaultStorage[key])
            return JSON.parse(defaultStorage[key]);
        return "";
    },
    save(object, key) {
        let data = JSON.stringify(object);
        defaultStorage[key] = data;
    },
    saveValue(object, key) {
        defaultStorage[key] = object;
    },
    getValue(key) {
        if (defaultStorage[key])
            return defaultStorage[key];
        return null;
    },
    remove(key) {
        delete defaultStorage[key];
    },
    isEmpty: function () {
        return defaultStorage.length == 0
    }
}