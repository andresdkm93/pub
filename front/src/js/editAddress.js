import orderService from '../services/orderService.js'
import storage from '../services/storageService.js'
import {EventBus} from '../services/eventBus'

export const editAddress = {
    data() {
        return {
            map: null,
            merker: null,
            products: [],
            orderSum: 0,
            orderEventsSum: 0,
            nameAddress: null,
            discount: 0,
            saveDirection: false,
            details: null,
            phone: null,
            address: null,
            latitude: null,
            longitude: null,
            deliveryValue: 0,
            addressValid: false,
            code: null,
            codeValid: false,
            payMethod: null,
            showSaveAddress: false,
            showSuccessOrder: false,
            deliveryTime: 0,
            deliveryValueTotal: 0,
            component1: null,
            component2: null,
            component3: null,
            component4: null,
            component5: null,
            geocoder: new google.maps.Geocoder()
    }
    },
    mounted() {
        setTimeout(() => {
            if (window.myLatitude && window.myLongitude) {
                this.map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: window.myLatitude, lng: window.myLongitude},
                    scrollwheel: false,
                    zoom: 11
                });
                let myLatLng = {lat: window.myLatitude, lng: window.myLongitude};
                this.latitude = window.myLatitude;
                this.longitude = window.myLongitude;
                let url = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAABRCAYAAABhVK7IAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAwcSURBVHic1Vx9cFRXFf+d+z52s4RkNx+k0Go/rA4jpVMaQwHpNNNhmEFLCYTaL+0nYK2jTj/sP7YlYx11pmMdnam2qTMt1lo1JFmgYypqgQZEmmJRSmulFmghUkjYTUh283bfu8c/NqEh2bd733ubor9/knn33HPO79377se55y5hisAAnWpe/lmNxGIGzSbwbMl0MQEVAKYh93cQwDADgwJ4jwnvEPifDsudVe1b3iKAp8I3KqUyXrfOSPYdX0agm5noWgAzfCsjfEiMVyT4xVh13cvU2potlZ8lId2/atUFguR9YNwGQk0pdE7ASQDPO8J+oqbtpWNBlQUi3b9ixSeEJh4F8W0AzKDOKMBi8C+Zte9Wd3Qc9avEF2let85I9p+4F8BjAKb7NR4AKRAejw5bP6CuLstrZc+k+5uXzxHQfgPgMq91pwD7HUk31nR2vu2lkvAinGheeZeA9hr+NwgDwFxNcE+ieeXtXioptTQDlFzdtB6M9f58+zjAP43OnXcftbTIYpJFSXNjo56sjv4CgKe3eY6wIdqfXEPbt9uFhAp2bwYoWR19Gv8fhAHg9mR19FluaSnISy9UmFzd9DgYdwXxgqUDaVlwMhmwbUPaNsASLBkkCCABoesgXYdmmBDhEEhoQUx+Obl/XxLAN9wEXLt3snnlWga3+rHKzJAjadipFJxMBmAPq0kiaKYJPRKBCJeByOdSgviu2MZNz+Ytyvfw1MqVl5HgPQAingwxw06nkT09CHYc745OdE7TYUyfDj3izQ0AYPAISbEg1tn590l6JwkvWxYaiJj7GDTbixFp28gkEpDZjGcHi0GYJsxoDEIv+DXmAR2ICv1Kams7y6lJH3xyWughr4SddAojJ09OCWEAkJkMRk6egJ1Oe6zJcxKceXDi07NaOtncfAnDeRNAmara7PAQsgMDHp3xD6OiEkZ5uZcqKTiYE4vHD489OKu/MMv1oKkkTEMAHwGhlxhJJkTBmAXgIuT22MVtDubseSAegaBHgY9moTMtnbxh+cUstX+hyDQ2BiedgpVIKEjSEAMvCHB8OJrY9umudydtEA4uuzRUnohe6xA1EXArFF6AWVUFPazePnDwmbHWPkOQpXgAioSlbcNKFm5hArLMeMZw+LEZPT3HC8mOvoguAF0nGhrW2zqvB+huBgy3OplkAqLGUB3cDGh4AKNzNwHAoTvuCEdPJ3sBxIpWZ8ZIfx9kpsCgRegTjNUzd/fsUPEoH44t+NznQdQOoM5NRoRCCFcrxywS0ZQ1k7q6LAEAsaGB5VAhDMBOpwsTBt4iIeuDEAaA8//6+i4i+yqAXbeN0rJgp5RH9NhAJHQdMDplMfNNKrWYGdnTg4VE+h2S18/aufd9VU8KYdZf3jgiWP8CgBNuMkX8OQsM3AgAxC0tIrl/30kAVcUqOakUrKTr4OWQlI2z9uzdqeyFIv6zsOEaCbwClw1SKBaDVqawamP0RS+/ok4MHNg3DwqEAcBOpwppfG4qCAPAzN09Owh43q3cThXyaxwINckD+y4XUmKhijxLJ7d5yI80S61FzbI/sI1HAOT9gB3LAsuisYOcHocXCQIrLTkdy323xIz2C/bs8R2dVMH5PT0fABx3K5cZtfggCZotoLjOLjxF0WYli0HBtMmtqMiMMh6zBYALlezZrgcMMiSMl1UtBkGYRReAvP3YsQtGiD4C40IBUotbS8f1mzlRu2vXaTWLwVC9Z88gcqcdk8CKpAlcIcCKwXr3gaJXqX7pkN8eKw5koOkCyscx+Qcxyp08fmzg4PZCAsCwkqhLrIrBrmvjqQAB57mWqOG0AEhpqiGRP6pKoFmq1kqEmXn9UI+gHhUAv6UiKbT8WzgGKo/Pn3+xqsUg+HBxwyXIHeZPgtDVSDPwtmDm7SrCZLjvW6UmVyhZDIisTU1uZaS7br0nSm4TQtIfVESFGXItYxLNihYDgYhd7Wghd//GQ0BsFdF4/N8A3i0qHDJdv2swLz66aP61SlZ94tiChiUAFuUrI0EgQ6mlD0bb298TAECEtmLSBIJWFnYvZ/4Rezz6VQUDAgI/dCvXyiJKJyHE1AaMBRFstMJleTceeqRgvO6K3oUNDxe17AO9CxvWg1HvVq6XKQVSHQa1AqOkY/H4YRB3FaslDBNayL21AbQcW9Bwo4oHquhd0LAKwCNu5VooDGEqde3fxzo6jgDjuiMzfqZS06ioLFRMIGw4tqjhThVdxdC7cP4tTPgVCqw8jIq8M9gkSEln+J1RxgAlm5t6APduNIbMwADs4aGCMgT6sQPtO5/YvdvrWQx66+sj0hTfJ+BbheSMaeUwKgs2whh6ou3xq8aS8c60NAEsJSl9k0ZFBUSR0ZLB9wnYB48tnH83NzYqBae5sVHvXTB/DZviYDHCwjChV6q1sgA/Mj77cFK3Saxq6gZhcTFF0rFhnTypFqYh9AF4iSS2kIZ3MhTqvXDnzsSRxYtjJluzmHk2mJYz4YtgheQ7TUNZdS1IZRXG2BnriF99tjsTcGr1iquJaUe+somQ2Sysvj6w4rZukjs+UrpIEELVtUV72pgNJr6mauOm7vEPJ82rVRs3dYP51yoahWEgXFsL0nylS/ggrCFUo0wYAF6YSBhwWUxkNfNBAErHkaTrCNfOKDaVBYYWCiE8oxZCeY2Ngawwvp2vIC/pGW1txwF6VFU7CYFQdXVuOvObI+IGQTArKxGqrvGUgMPAwzkeeVS6VYoK/UkAr3vxzygvR1ldHfRp5YAISF4QjPJyRGpH9XkB47WYMH7uVlzQs77m5tkanL/BQ2bCGbuS4aRTcEZyB36skGFERBCGAVEWgV4WyaVceYcl4dRXt2854GqnmIbk6hUPMtPjfqyfATOcbBaczYJtO/cCWAIkcjskbTSPzDRL8HnQA7H2zicKSRRdNFReNu+J5D/2XQfCNf79yOWGwZzqlHDeFRXGT4q6o6JqNDXjDQBKa75zhCQJ58po25ZDxQSV9r/Rti2HiHlNcL+mDgzcq0IY8LDpj3Zs2kiA64h4bsFPVrXHX1SV9pbkPj16P4BJaYfnGPuS02OTEuQKwfNQmWhquggCPVN0K8crEuSgYTTOpwzPMa1YPH6YmG4BEDzjNRgkE271ShjwGciLdnb+kUHn9GoDMx6u2hgvGuLKB98rgdy9jpW/A/Nqvzp8g2hjdGPnl/xeS/QdsiWAh0i/jRi7/erwidfTln17kHuYgbdEgzcvr3Ey2m4AlwbVpYDDRlYuKN+8+cMgSgIH5yte3NLnSLoegEp2bBAMsqTlQQkDJTqRqOnsfBssmwB4vgKoiCyBm6s6O98shbKSHcPEOja/CsY9pdJ3NmhttH3Tn0qlraRnT7GO+HMgfK+UOsF4LNbeuaGUKksc28lNZYnmFU8TaG0J1G2ItsfvLPWN+ZKfMhLAMWF+jYHfBlQVj/Yn10zFTwRMydEqtbU5seoZX6Fctr4f/Dmasm4qdmfSL6aENABQa2s2lXFWg+EpM5iBPVlhNPm5DK7s21QpHsOpG26oJJndBmCegvh+IYzGyra2U1PpUylJ09KlSyOO44SFECFmDjuOYwDQrw6Fzvv6tNAzJsg1CykDPvTksLW227KOA7A1TcsS0YiU0tI0bWTr1q0plOj7DkR66dKl02zbjkoppwshIszsqm+hYVR9szzyVIhwycQyi3HkqeH0PTsymbx5nwBARCylTAkhTuu6nty6data0l8+XT7qiGXLllVns9nzHMfxFN7MR1yFcD5ompaxLOt4d3d3PxRSR8bDE+k5c+aYM2fO/JTjON6vuY5iPHEL/P5TQyNf9Up4PDRNSyUSiXf37t2r/OMvno4b586de5GUMtBPfByVMn2CsW2OoX2yNWU99Gom0xdEHzMb4XA4dOjQIeUNj6e7ubZtS83fsexZ6Lasvm7Luj+wolFIKT2FrjzN03V1de8z88d3hVYBzDxQW1v7gZc6vkbvJUuWVDJzXdCuHgRCiNOmaR7v6urynP8daMqqr683IpFItKysrDyTyUwjIrUETR9gZss0zeF0Oj2USqWSXgauiSjpiqwxl0UUBhAWQoRs2zYB6KZp6rZtaxgdQ4QQgplpdO4dm25sXdedTCZjj/6fkVJaAEYAjGwv4Tr8v77Fjy7JrxFaAAAAAElFTkSuQmCC';
                this.marker = new google.maps.Marker({
                    position: myLatLng,
                    map: this.map,
                    icon: url,
                    draggable: true
                });
                google.maps.event.addListener(this.marker, 'dragend', (event) => {
                    self.latitude = event.latLng.lat();
                    self.longitude = event.latLng.lng();
                    this.setLatitude(event.latLng.lat(), event.latLng.lng());
                    self.validateDeliveryValue();
                });
            } else {
                this.map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: 7.1095358, lng: -73.1771069},
                    scrollwheel: false,
                    zoom: 11
                });
            }

        }, 1200);
    },
    methods: {
        centerOnUser() {
            if (this.userPosition) {
                this.center = this.userPosition
            }
        },
        setUserPosition(position) {
            this.userPosition = position
        },
        validateDeliveryValue() {
            this.$http.post('/api/mobile/deliveries', {
                latitude: this.latitude,
                longitude: this.longitude,
                total: this.orderSum + this.orderEventsSum
            }).then(response => {
                if (response.body.success) {
                    this.addressValid = true;
                    this.deliveryValue = parseFloat(response.body.data.value);
                } else {
                    this.deliveryValue = 0;
                    this.deliveryError = response.body.message;
                    this.addressValid = false;
                    this.$notify({
                        type: 'danger',
                        title: 'Error!',
                        content: response.body.message
                    })
                }
            });
        },
        setMarker() {
            let myLatLng = {lat: this.latitude, lng: this.longitude};
            if (!this.marker) {
                let url = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAABRCAYAAABhVK7IAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAwcSURBVHic1Vx9cFRXFf+d+z52s4RkNx+k0Go/rA4jpVMaQwHpNNNhmEFLCYTaL+0nYK2jTj/sP7YlYx11pmMdnam2qTMt1lo1JFmgYypqgQZEmmJRSmulFmghUkjYTUh283bfu8c/NqEh2bd733ubor9/knn33HPO79377se55y5hisAAnWpe/lmNxGIGzSbwbMl0MQEVAKYh93cQwDADgwJ4jwnvEPifDsudVe1b3iKAp8I3KqUyXrfOSPYdX0agm5noWgAzfCsjfEiMVyT4xVh13cvU2potlZ8lId2/atUFguR9YNwGQk0pdE7ASQDPO8J+oqbtpWNBlQUi3b9ixSeEJh4F8W0AzKDOKMBi8C+Zte9Wd3Qc9avEF2let85I9p+4F8BjAKb7NR4AKRAejw5bP6CuLstrZc+k+5uXzxHQfgPgMq91pwD7HUk31nR2vu2lkvAinGheeZeA9hr+NwgDwFxNcE+ieeXtXioptTQDlFzdtB6M9f58+zjAP43OnXcftbTIYpJFSXNjo56sjv4CgKe3eY6wIdqfXEPbt9uFhAp2bwYoWR19Gv8fhAHg9mR19FluaSnISy9UmFzd9DgYdwXxgqUDaVlwMhmwbUPaNsASLBkkCCABoesgXYdmmBDhEEhoQUx+Obl/XxLAN9wEXLt3snnlWga3+rHKzJAjadipFJxMBmAPq0kiaKYJPRKBCJeByOdSgviu2MZNz+Ytyvfw1MqVl5HgPQAingwxw06nkT09CHYc745OdE7TYUyfDj3izQ0AYPAISbEg1tn590l6JwkvWxYaiJj7GDTbixFp28gkEpDZjGcHi0GYJsxoDEIv+DXmAR2ICv1Kams7y6lJH3xyWughr4SddAojJ09OCWEAkJkMRk6egJ1Oe6zJcxKceXDi07NaOtncfAnDeRNAmara7PAQsgMDHp3xD6OiEkZ5uZcqKTiYE4vHD489OKu/MMv1oKkkTEMAHwGhlxhJJkTBmAXgIuT22MVtDubseSAegaBHgY9moTMtnbxh+cUstX+hyDQ2BiedgpVIKEjSEAMvCHB8OJrY9umudydtEA4uuzRUnohe6xA1EXArFF6AWVUFPazePnDwmbHWPkOQpXgAioSlbcNKFm5hArLMeMZw+LEZPT3HC8mOvoguAF0nGhrW2zqvB+huBgy3OplkAqLGUB3cDGh4AKNzNwHAoTvuCEdPJ3sBxIpWZ8ZIfx9kpsCgRegTjNUzd/fsUPEoH44t+NznQdQOoM5NRoRCCFcrxywS0ZQ1k7q6LAEAsaGB5VAhDMBOpwsTBt4iIeuDEAaA8//6+i4i+yqAXbeN0rJgp5RH9NhAJHQdMDplMfNNKrWYGdnTg4VE+h2S18/aufd9VU8KYdZf3jgiWP8CgBNuMkX8OQsM3AgAxC0tIrl/30kAVcUqOakUrKTr4OWQlI2z9uzdqeyFIv6zsOEaCbwClw1SKBaDVqawamP0RS+/ok4MHNg3DwqEAcBOpwppfG4qCAPAzN09Owh43q3cThXyaxwINckD+y4XUmKhijxLJ7d5yI80S61FzbI/sI1HAOT9gB3LAsuisYOcHocXCQIrLTkdy323xIz2C/bs8R2dVMH5PT0fABx3K5cZtfggCZotoLjOLjxF0WYli0HBtMmtqMiMMh6zBYALlezZrgcMMiSMl1UtBkGYRReAvP3YsQtGiD4C40IBUotbS8f1mzlRu2vXaTWLwVC9Z88gcqcdk8CKpAlcIcCKwXr3gaJXqX7pkN8eKw5koOkCyscx+Qcxyp08fmzg4PZCAsCwkqhLrIrBrmvjqQAB57mWqOG0AEhpqiGRP6pKoFmq1kqEmXn9UI+gHhUAv6UiKbT8WzgGKo/Pn3+xqsUg+HBxwyXIHeZPgtDVSDPwtmDm7SrCZLjvW6UmVyhZDIisTU1uZaS7br0nSm4TQtIfVESFGXItYxLNihYDgYhd7Wghd//GQ0BsFdF4/N8A3i0qHDJdv2swLz66aP61SlZ94tiChiUAFuUrI0EgQ6mlD0bb298TAECEtmLSBIJWFnYvZ/4Rezz6VQUDAgI/dCvXyiJKJyHE1AaMBRFstMJleTceeqRgvO6K3oUNDxe17AO9CxvWg1HvVq6XKQVSHQa1AqOkY/H4YRB3FaslDBNayL21AbQcW9Bwo4oHquhd0LAKwCNu5VooDGEqde3fxzo6jgDjuiMzfqZS06ioLFRMIGw4tqjhThVdxdC7cP4tTPgVCqw8jIq8M9gkSEln+J1RxgAlm5t6APduNIbMwADs4aGCMgT6sQPtO5/YvdvrWQx66+sj0hTfJ+BbheSMaeUwKgs2whh6ou3xq8aS8c60NAEsJSl9k0ZFBUSR0ZLB9wnYB48tnH83NzYqBae5sVHvXTB/DZviYDHCwjChV6q1sgA/Mj77cFK3Saxq6gZhcTFF0rFhnTypFqYh9AF4iSS2kIZ3MhTqvXDnzsSRxYtjJluzmHk2mJYz4YtgheQ7TUNZdS1IZRXG2BnriF99tjsTcGr1iquJaUe+somQ2Sysvj6w4rZukjs+UrpIEELVtUV72pgNJr6mauOm7vEPJ82rVRs3dYP51yoahWEgXFsL0nylS/ggrCFUo0wYAF6YSBhwWUxkNfNBAErHkaTrCNfOKDaVBYYWCiE8oxZCeY2Ngawwvp2vIC/pGW1txwF6VFU7CYFQdXVuOvObI+IGQTArKxGqrvGUgMPAwzkeeVS6VYoK/UkAr3vxzygvR1ldHfRp5YAISF4QjPJyRGpH9XkB47WYMH7uVlzQs77m5tkanL/BQ2bCGbuS4aRTcEZyB36skGFERBCGAVEWgV4WyaVceYcl4dRXt2854GqnmIbk6hUPMtPjfqyfATOcbBaczYJtO/cCWAIkcjskbTSPzDRL8HnQA7H2zicKSRRdNFReNu+J5D/2XQfCNf79yOWGwZzqlHDeFRXGT4q6o6JqNDXjDQBKa75zhCQJ58po25ZDxQSV9r/Rti2HiHlNcL+mDgzcq0IY8LDpj3Zs2kiA64h4bsFPVrXHX1SV9pbkPj16P4BJaYfnGPuS02OTEuQKwfNQmWhquggCPVN0K8crEuSgYTTOpwzPMa1YPH6YmG4BEDzjNRgkE271ShjwGciLdnb+kUHn9GoDMx6u2hgvGuLKB98rgdy9jpW/A/Nqvzp8g2hjdGPnl/xeS/QdsiWAh0i/jRi7/erwidfTln17kHuYgbdEgzcvr3Ey2m4AlwbVpYDDRlYuKN+8+cMgSgIH5yte3NLnSLoegEp2bBAMsqTlQQkDJTqRqOnsfBssmwB4vgKoiCyBm6s6O98shbKSHcPEOja/CsY9pdJ3NmhttH3Tn0qlraRnT7GO+HMgfK+UOsF4LNbeuaGUKksc28lNZYnmFU8TaG0J1G2ItsfvLPWN+ZKfMhLAMWF+jYHfBlQVj/Yn10zFTwRMydEqtbU5seoZX6Fctr4f/Dmasm4qdmfSL6aENABQa2s2lXFWg+EpM5iBPVlhNPm5DK7s21QpHsOpG26oJJndBmCegvh+IYzGyra2U1PpUylJ09KlSyOO44SFECFmDjuOYwDQrw6Fzvv6tNAzJsg1CykDPvTksLW227KOA7A1TcsS0YiU0tI0bWTr1q0plOj7DkR66dKl02zbjkoppwshIszsqm+hYVR9szzyVIhwycQyi3HkqeH0PTsymbx5nwBARCylTAkhTuu6nty6data0l8+XT7qiGXLllVns9nzHMfxFN7MR1yFcD5ompaxLOt4d3d3PxRSR8bDE+k5c+aYM2fO/JTjON6vuY5iPHEL/P5TQyNf9Up4PDRNSyUSiXf37t2r/OMvno4b586de5GUMtBPfByVMn2CsW2OoX2yNWU99Gom0xdEHzMb4XA4dOjQIeUNj6e7ubZtS83fsexZ6Lasvm7Luj+wolFIKT2FrjzN03V1de8z88d3hVYBzDxQW1v7gZc6vkbvJUuWVDJzXdCuHgRCiNOmaR7v6urynP8daMqqr683IpFItKysrDyTyUwjIrUETR9gZss0zeF0Oj2USqWSXgauiSjpiqwxl0UUBhAWQoRs2zYB6KZp6rZtaxgdQ4QQgplpdO4dm25sXdedTCZjj/6fkVJaAEYAjGwv4Tr8v77Fjy7JrxFaAAAAAElFTkSuQmCC';
                this.marker = new google.maps.Marker({
                    position: myLatLng,
                    map: this.map,
                    icon: url,
                    draggable: true
                });
                let self = this;
                google.maps.event.addListener(this.marker, 'dragend', (event) => {
                    self.latitude = event.latLng.lat();
                    self.longitude = event.latLng.lng();
                    this.setLatitude(event.latLng.lat(), event.latLng.lng());
                    self.validateDeliveryValue();
                });

                let center = new google.maps.LatLng(this.latitude, this.longitude);
                this.map.panTo(center);
                this.map.setZoom(16);
            } else {
                let center = new google.maps.LatLng(this.latitude, this.longitude);
                this.marker.setPosition(center);
                this.map.panTo(center);
                this.map.setZoom(16);
            }
        },
        setLatitude(latitude, longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        },
        cleanLocation() {
            this.addressValid = false;
            this.address = null;
            this.latitude = null;
            this.longitude = null;
            this.details = null;
            this.deliveryValue = 0;
        },
        searchAddress() {
            if (this.component1 && this.component2 && this.component3 && this.component4 && this.component5) {
                let addrees = this.component1 + ' ' + this.component2 + ' #' + this.component3 + ' -' + this.component4 + ' ' + this.component5;
                let self= this;
                this.geocoder.geocode({'address': addrees},(results, status)=> {
                    if (status === 'OK') {
                        self.address = results[0]['formatted_address'];
                        self.latitude = results[0]['geometry']['location'].lat();
                        self.longitude = results[0]['geometry']['location'].lng();
                        self.setMarker();
                        self.validateDeliveryValue();
                    } else {
                        console.log('Geocode was not successful for the following reason: ' + status);
                        self.address = '';
                        self.latitude = 0;
                        self.longitude = 0;
                        self.validateDeliveryValue();
                    }
                });
            }
        }
    },
    watch: {
        component1(newValue) {
            if (newValue) {
                this.searchAddress();
            }
        },
        component2(newValue) {
            if (newValue) {
                this.searchAddress();
            }
        },
        component3(newValue) {
            if (newValue) {
                this.searchAddress();
            }
        },
        component4(newValue) {
            if (newValue) {
                this.searchAddress();
            }
        },
        component5(newValue) {
            if (newValue) {
                this.searchAddress();
            }
        },
    }
};