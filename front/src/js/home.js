import storage from '../services/storageService.js'
import orderService from '../services/orderService.js'
import Autocomplete from 'vuejs-auto-complete'
import {EventBus} from '../services/eventBus'
import {cart} from './cart'
import {editAddress} from './editAddress'

export const home = {
    el: "#home",
    components: {
        Autocomplete,
        'CartComponent': cart,
        'EditAddressComponent': editAddress
    },
    data: {
        email: '',
        password: '',
        showLogin: false,
        showRegister: false,
        showUserProfile: false,
        showBuyProduct: false,
        showValidateAge: false,
        validateAgeData: {},
        buyProduct: null,
        registerError: '',
        fbSignInParams: {
            scope: 'public_profile,email',
            return_scopes: true
        },
        googleSignInParams: {
            client_id: '217930413910-t4q3p76plsc2bug5u0si7dol0316s7ih.apps.googleusercontent.com'
        },
        register: {},
        user: null,
        totalProducts: 0,
        orderProducts: [],
        orderSum: 0,
        lastQuery: '',
        buyEvent: null,
        showBuyEvent: false,
        itemEvent: null,
        showPromotion: false,
        showSuggest: false,
        showRemoveData: false,
        removeData: {},
        suggest: {},

    },
    beforeMount() {
        this.user = storage.getValue('session');
    },
    mounted() {
        this.totalProducts = orderService.count();
        $(".init-hide").removeClass('hidden');
        this.$refs.autocomplete.$on('enter', this.searchProduct);
        this.$refs.autocomplete.$on('nothingSelected', this.searchProduct);
        this.$refs.autocomplete.$on('noResults', (value) => {
            console.log(value);
            this.lastQuery = value;
        });
        if (!window.localStorage['validate-age']) {
            this.showValidateAge = true;
        }
        EventBus.$on('count-order', () => {
            this.totalProducts = orderService.count();
        });
        let productLoad =$('#productLoad').val();
        if(productLoad){
            this.addToCart(productLoad);
        }
    },
    methods: {
        logout() {
            storage.remove('session');
            window.location.href = '/logoutWeb';
        },
        validateOrder() {
            let date = new Date(this.validateAgeData.year + '-' + this.validateAgeData.month + '-' + this.validateAgeData.day);
            let date2 = new Date();
            let diffDays = parseInt((date2 - date) / (1000 * 60 * 60 * 24));
            let validDate = diffDays >= 6480;
            if (!validDate) {
                this.showRegisterError('Debes de ser mayor de edad.');
                return false;
            }
            window.localStorage['validate-age'] = true;
            this.showValidateAge = false;
        },
        showRegisterError(msg) {
            this.registerError = msg;
            setTimeout(() => {
                this.registerError = '';
            }, 5000)
        },
        onSuggest() {
            this.$http.post('/api/mobile/emailsuggest', this.suggest)
                .then((response) => {
                    this.showSuggest = false;
                });
        },
        onRemoveData(){
            this.removeData.title = 'Eliminar cuenta e información personal';
            this.$http.post('/api/mobile/emailsuggest', this.removeData)
                .then((response) => {
                    this.removeData = false;
                    this.showRemoveData = false;
                });
        },
        onRegister() {
            if (this.register.password != this.register.password_confirm) {
                this.showRegisterError('Las contraseñas no coinciden');
                return false;
            }
            let date = new Date(this.register.birthday)
            let date2 = new Date();
            let diffDays = parseInt((date2 - date) / (1000 * 60 * 60 * 24));
            let validDate = diffDays >= 6480;
            if (!validDate) {
                this.showRegisterError('Debes de ser mayor de edad.');
                return false;
            }
            this.$http.post('/api/mobile/clients', this.register)
                .then((response) => {
                    if (response.body.data) {
                        this.$http.post('/api/mobile/loginWeb', {
                            email: this.register.email,
                            password: this.register.password
                        })
                            .then(response => {
                                storage.saveValue(response.body.data.token, 'session');
                                window.location = '/';
                            })
                            .error(data => {
                                this.$notify({
                                    type: 'danger',
                                    title: 'Error!',
                                    content: 'Usuario no encontrado, por favor registrate'
                                })
                            });
                    } else {
                        this.showRegisterError('Ha ocurrido un error durante el proceso de registro, intentalo de nuevo');
                    }
                }, error => {
                    console.log(error);
                    this.showRegisterError('Ha ocurrido un error durante el proceso de registro, intentalo de nuevo');
                })
        },
        onSignInSuccessFb(data) {
            FB.api('/me/?fields=id,email,name', data => {
                if(!data.email){
                    return this.showRegisterError('Error obteniendo el email');
                }
                this.$http.post('/api/mobile/loginWeb', {
                    email: data.email,
                    password: data.id
                })
                    .then(response => {
                        if (response.body.data) {
                            storage.saveValue(response.body.data.token, 'session');
                            window.location = '/';
                        } else {
                            this.showRegisterError('Usuario no encontrado, por favor registrate');
                        }
                    })
                    .error(data => {
                        this.showRegisterError('Usuario no encontrado, por favor registrate');
                    });
            })
        },
        onRegisterFb(data) {
            FB.api('/me/?fields=id,email,name', data => {
                let user = {
                    id: data.id,
                    name: data.name,
                    email: data.email,
                    password: data.id,
                    social_id: data.id,
                    photo: "https://graph.facebook.com/" + data.id + "/picture?type=large",
                };
                if(!data.email){
                    return this.showRegisterError('Error obteniendo el email');
                }
                this.$http.post('/api/mobile/clients', user)
                    .then((response) => {
                        if (response.body.data) {
                            this.$http.post('/api/mobile/loginWeb', {
                                email: user.email,
                                password: user.password
                            })
                                .then(response => {
                                    storage.saveValue(response.body.data.token, 'session');
                                    window.location = '/';
                                })
                                .error(data => {
                                    this.showRegisterError('Usuario no encontrado, por favor registrate');

                                });
                        } else {
                            this.showRegisterError('Ha ocurrido un error durante el proceso de registro, intentalo de nuevo');
                        }
                    }, error => {
                        console.log(error);
                        this.showRegisterError('Ha ocurrido un error durante el proceso de registro, intentalo de nuevo');
                    })

            })
        },
        onSignInErrorFb(error) {
            console.log('OH NOES', error)
        },
        onSignInSuccessGm(googleUser) {
            const profile = googleUser.getBasicProfile();
            console.log(profile);
            this.$http.post('/api/mobile/loginWeb', {
                email: profile.U3,
                password: profile.Eea
            })
                .then(response => {
                    if (response.body.data) {
                        storage.saveValue(response.body.data.token, 'session');
                        window.location = '/';
                    } else {
                        this.showRegisterError('Usuario no encontrado, por favor registrate');
                    }
                })
                .error(data => {
                    this.showRegisterError('Usuario no encontrado, por favor registrate');
                });
        },
        onRegisterGm(googleUser) {
            const profile = googleUser.getBasicProfile() // etc etc
            console.log(profile);
            var data = {
                id: profile.Eea,
                name: profile.ig,
                email: profile.U3,
                password: profile.Eea,
                social_id: profile.Eea,
                photo: profile.Paa,
                birthdate: null
            };
            this.$http.post('/api/mobile/clients', data)
                .then((response) => {
                    if (response.body.data) {
                        this.$http.post('/api/mobile/loginWeb', {
                            email: data.email,
                            password: data.password
                        })
                            .then(response => {
                                storage.saveValue(response.body.data.token, 'session');
                                window.location = '/';
                            })
                            .error(data => {
                                this.$notify({
                                    type: 'danger',
                                    title: 'Error!',
                                    content: 'Usuario no encontrado, por favor registrate'
                                })
                            });
                    } else {
                        this.showRegisterError('Ha ocurrido un error durante el proceso de registro, intentalo de nuevo');
                    }
                }, error => {
                    console.log(error);
                    this.showRegisterError('Ha ocurrido un error durante el proceso de registro, intentalo de nuevo');
                })

        },
        onSignInErrorGm(error) {
            // `error` contains any error occurred.
            this.showRegisterError('Usuario no encontrado, por favor registrate');
            console.log('OH NOES', error)
        },
        submit() {
            this.$http.post('/api/mobile/loginWeb', {
                email: this.email,
                password: this.password
            }).then((response) => {
                if (response.body.data) {
                    storage.saveValue(response.body.data.token, 'session');
                    window.location = '/';
                } else {
                    this.showRegisterError('Datos invalidos');
                }
            })
        },
        addEvent(eventId) {
            this.buyProduct = null;
            this.$http.get('/api/mobile/events/' + eventId)
                .then(response => {
                    this.buyEvent = response.body.data;
                    this.showBuyEvent = true;
                })
        },
        setBuyProduct(item) {
            this.buyProduct = item;
            this.buyProduct.product.image = this.buyEvent.image;
            this.buyProduct.product.name = this.buyEvent.name + '-' + this.buyProduct.product.name;
            this.buyProduct.quantityShop = 1;
        },
        addToCart(productId) {
            let product = orderService.getProduct(productId);
            if (!product) {
                this.$http.get('/api/mobile/storeProducts/' + productId)
                    .then(response => {
                        console.log(response);
                        this.buyProduct = response.body.data;
                        this.buyProduct.quantityShop = 1;
                        this.showBuyProduct = true;
                    })
            } else {
                this.buyProduct = product;
                this.showBuyProduct = true;
            }
        },
        addPromotion(productId) {
            this.showPromotion = true;
        },
        removePromotion(product) {
            if (product.points) {
                orderService.removeProduct(product);
                this.totalProducts = orderService.count();
                this.orderSum = orderService.sum();
                let index = this.orderProducts.indexOf(product);
                this.orderProducts.splice(index, 1);
                this.$forceUpdate();
            }

        },
        addToCartPromotion(promotionId) {
            this.$http.get('/api/mobile/promotions/' + promotionId)
                .then(response => {
                    this.buyProduct = response.body.data.store_product;
                    this.buyProduct.quantityShop = 1;
                    this.buyProduct.promotion_type = 2;
                    this.buyProduct.multiplier = response.body.data.multiplier;
                    this.buyProduct.price = (this.buyProduct.price * response.body.data.multiplier);
                    this.buyProduct.product.image = response.body.data.image;
                    this.buyProduct.product.name = response.body.data.name;
                    this.showBuyProduct = true;
                })
        },
        changeQuantity(qty) {
            if (this.buyProduct) {
                this.buyProduct.quantityShop = this.buyProduct.quantityShop + qty;
                if (this.buyProduct.quantityShop < 1) {
                    this.showBuyProduct = false;
                    orderService.removeProduct(this.buyProduct);
                    this.buyProduct = null;
                } else {
                    this.buyProduct.total = this.buyProduct.quantityShop * this.buyProduct.price;
                }
                this.totalProducts = orderService.count();
                this.$forceUpdate();
            }
        },
        changeQuantityOrder(product, qty) {
            product.quantityShop = product.quantityShop + qty;
            product.total = product.quantityShop * product.price;
            orderService.saveProduct(product);
            this.totalProducts = orderService.count();
            this.orderSum = orderService.sum();
            this.$forceUpdate();
        },
        removeProductOrder(product) {
            orderService.removeProduct(product);
            this.totalProducts = orderService.count();
            this.orderSum = orderService.sum();
            let index = this.orderProducts.indexOf(product);
            this.orderProducts.splice(index, 1);
            this.$forceUpdate();
        },
        saveInOrder() {
            if (this.buyProduct) {
                let newProduct = null;
                if (this.buyProduct.storeId) {
                    newProduct = this.buyProduct;
                } else {
                    newProduct = {
                        storeId: this.buyProduct.id,
                        price: this.buyProduct.price,
                        product: {
                            image: this.buyProduct.product.image,
                            name: this.buyProduct.product.name
                        },
                        quantityShop: this.buyProduct.quantityShop,
                        total: this.buyProduct.quantityShop * this.buyProduct.price
                    };
                }
                orderService.saveProduct(newProduct);
                this.showBuyProduct = false;
                this.buyProduct = null;
                this.totalProducts = orderService.count();
            }
        },
        saveInOrderEvent() {
            if (this.buyProduct) {
                let newProduct = null;
                if (this.buyProduct.storeId) {
                    newProduct = this.buyProduct;
                } else {
                    newProduct = {
                        storeId: this.buyProduct.id,
                        price: this.buyProduct.price,
                        product: {
                            image: this.buyProduct.product.image,
                            name: this.buyProduct.product.name
                        },
                        event: true,
                        quantityShop: this.buyProduct.quantityShop,
                        total: this.buyProduct.quantityShop * this.buyProduct.price
                    };
                }
                orderService.saveProduct(newProduct);
                this.showBuyEvent = false;
                this.buyProduct = null;
                this.totalProducts = orderService.count();
            }
        },
        showOrder() {
            this.orderProducts = orderService.getProducts();
            this.orderSum = orderService.sum();
        },
        hideOrder() {
            this.orderProducts = null;
        },
        formattedDisplay(result) {
            return result.product.name
        },
        selectedProduct(product) {
            window.location = '/tienda/productos/' + product.selectedObject.url;
        },
        searchProduct(term) {
            window.location = '/tienda/busqueda/' + encodeURI(term);
        },
        enterSearchProduct() {
            if (this.lastQuery !== '') {
                window.location = '/tienda/busqueda/' + encodeURI(this.lastQuery);
            }
        },
        makeOrder() {
            if (!this.user) {
                return this.showLogin = true
            }
        },
        createCart() {
            if (!this.user) {
                return this.showLogin = true
            }
            window.location = '/cart';
        }
    }
};
