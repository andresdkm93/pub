/**
 * Created by andresdkm on 10/03/17.
 */
import Vue from 'vue'
import VueResource from 'vue-resource'
import * as uiv from 'uiv'
import FBSignInButton from 'vue-facebook-signin-button'
import GSignInButton from 'vue-google-signin-button'
import Vue2Filters from 'vue2-filters'
import {home} from './js/home'
Vue.use(uiv)
Vue.use(VueResource);
Vue.use(FBSignInButton);
Vue.use(GSignInButton);
Vue.use(Vue2Filters);
if(window.localStorage['session']){
    Vue.http.interceptors.push((request, next) => {
        request.headers.set('Authorization',window.localStorage['session']);
        request.headers['Accept'] = 'application/json';
        next()
    });
}
new Vue(home);

/*
require('./js/categories.js');
require('./js/activationProductsTable.js');
require('./js/settings');
require('./js/topBar');
require('./js/footer');
require('./js/contactSupport')
require('./js/login.js');
require('./js/favorites');
require('./js/reports.js');

require('./services/interceptorService');
*/
