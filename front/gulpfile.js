var gulp = require('gulp');
var gutil = require('gulp-util');
var sequence = require('run-sequence');
var notify = require('gulp-notify');


var pumped = require('./gulp/utils/pumped');
var notifaker = require('./gulp/utils/notifaker');

var config = require('./config.json');

var path = require('path');
var webpack = require('webpack');
var webpackConfig = require("./webpack.config.js");


gutil.log(gutil.colors.green('Starting to Gulp! Please wait...'));



var myDevConfig = Object.create(webpackConfig);
myDevConfig.devtool = "sourcemap";
var devCompiler = webpack(myDevConfig);

gulp.task("webpack:build-dev", function(callback) {
    devCompiler.run(function(err, stats) {
        if(err) throw new gutil.PluginError("webpack:build-dev", err);
        gutil.log("[webpack:build-dev]", stats.toString({
            colors: true
        }));
        notifaker(pumped('JS Generated!'));
        callback();
    });
});



gulp.task('bundle', ["webpack:build-dev"]);

gulp.task("watch", ["webpack:build-dev"], function() {
    gulp.watch(["./src/**/*"], ["webpack:build-dev"]);
});

gulp.task('styles', function() {
    return gulp.watch('./src/less/**/*.less', ['styles:site']);
});

