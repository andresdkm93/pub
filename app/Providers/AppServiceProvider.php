<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Observers\OrderObserver;
use App\Observers\OrdersObserver;
use App\Models\Mobile\Order;
use App\Models\Orders;
use App\Models\Promotions;
use App\Observers\PromotionsObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Order::observe(OrderObserver::class);
        Orders::observe(OrdersObserver::class);
        Promotions::observe(PromotionsObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind('path.public', function() {
            return base_path().'/public_html';
        });

    }
}
