<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Clients",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="last_name",
 *          description="last_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="birthday",
 *          description="birthday",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="password",
 *          description="password",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="social_id",
 *          description="social_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="photo",
 *          description="photo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cellphone",
 *          description="cellphone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="points",
 *          description="points",
 *          type="string"
 *      )
 * )
 */
class Clients extends Model
{

    public $table = 'clients';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $hidden = [
        'password',
    ];

    public $fillable = [
        'id',
        'name',
        'last_name',
        'birthday',
        'email',
        'password',
        'social_id',
        'photo',
        'cellphone',
        'points',
        'uuid',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'last_name' => 'string',
        'birthday' => 'string',
        'email' => 'string',
        'password' => 'string',
        'social_id' => 'string',
        'photo' => 'string',
        'cellphone' => 'string',
        'points' => 'integer',
        'uuid' => 'string',
        'remember_token'=>'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getNameAttribute()
    {
        if(!isset($this->attributes['name']))
        {
            return "";
        }
        return $this->attributes['name'];
    }

    public function getLastNameAttribute()
    {
        if(!isset($this->attributes['last_name']))
        {
            return "";
        }
        return $this->attributes['last_name'];
    }
}
