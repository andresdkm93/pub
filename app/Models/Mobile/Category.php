<?php

namespace App\Models\Mobile;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models\Mobile
 */
class Category extends Model
{
    use SoftDeletes;

    public $table = 'categories';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const DELETE_AT = 'delete_at';



    public $fillable = [
        'name',
        'deleted_at',
        'image',
        'color',
        'banner'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'deleted_at' => 'datetime',
        'image' => 'string',
        'color' => 'string',
        'banner' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function products()
    {
        return $this->hasMany('App\Models\Mobile\Product','category_id');

    }

   
}
