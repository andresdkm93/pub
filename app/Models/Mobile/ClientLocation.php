<?php

namespace App\Models\Mobile;

use Eloquent as Model;

/**
 * Class ClientLocation
 * @package App\Models\Mobile
 */
class ClientLocation extends Model
{

    public $table = 'client_locations';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'name',
        'address',
        'latitude',
        'longitude',
        'client_id',
        'detail',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'address' => 'string',
        'latitude' => 'string',
        'longitude' => 'string',
        'detail' => 'string',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
