<?php

namespace App\Models\Mobile;
use App\Scopes\QuantityScope;
use App\Scopes\DateScope;
use App\Scopes\StatusScope;
use Carbon\Carbon;
use Eloquent as Model;

/**
 * Class StoreProduct
 * @package App\Models\Mobile
 */
class StoreProduct extends Model
{

    public $table = 'store_products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new QuantityScope());
        static::addGlobalScope(new DateScope());
        static::addGlobalScope(new StatusScope());
    }

    public $fillable = [
        'product_id',
        'quantity',
        'price',
        'status',
        'start_date',
        'end_date',
        'store_type',
        'points',
        'ranking',
        'recommended'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'quantity' => 'integer',
        'price' => 'float',
        'status' => 'boolean',
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'store_type' => 'integer',
        'points' => 'integer',
        'recommended' => 'boolean',
        'ranking' => 'float',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function type()
    {
        return $this->hasOne('App\Models\Mobile\StoreType', 'id','store_type');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Mobile\Product', 'id','product_id');
    }

	public function getEndDateAttribute()
	{
		$dt = Carbon::parse($this->attributes['end_date']);
		return $dt->toDateString();
	}

}
