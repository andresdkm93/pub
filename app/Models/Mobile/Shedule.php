<?php

namespace App\Models\Mobile;

use Eloquent as Model;

/**
 * Class Shedule
 * @package App\Models\Mobile
 */
class Shedule extends Model
{

    public $table = 'shedules';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'opening_time',
        'closing_time',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
