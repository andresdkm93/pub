<?php

namespace App\Models\Mobile;

use Eloquent as Model;
use Carbon\Carbon;

/**
 * Class Client
 * @package App\Models\Mobile
 */
class Client extends Model
{

    public $table = 'clients';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'id',
        'name',
        'last_name',
        'birthday',
        'email',
        'password',
        'social_id'
	,
        'photo',
        'cellphone',
        'points',
        'uuid',
        'deleted_at',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'last_name' => 'string',
        'birthday' => 'string',
        'email' => 'string',
        'password' => 'string',
        'social_id' => 'string',
        'photo' => 'string',
        'cellphone' => 'string',
        'points' => 'integer',
        'uuid' => 'string',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */



    public static $rules = [
    ];

    public function setBirthdayAttribute($value)
    {
        if($value!=null)
        {
            $date=Carbon::parse($value);
            $this->attributes['birthday'] = $date->toDateString();
        }else{
            $this->attributes['birthday'] = null;

        }

    }

    public function devices()
    {
        return $this->hasMany('App\Models\Mobile\Device','uuid');
    }

    public function locations()
    {
        return $this->hasMany('App\Models\Mobile\ClientLocation','client_id');

    }

    public function getNameAttribute()
    {
        if(!isset($this->attributes['name']))
        {
            return "";
        }
        return $this->attributes['name'];
    }

    public function getLastNameAttribute()
    {
        if(!isset($this->attributes['last_name']))
        {
            return "";
        }
        return $this->attributes['last_name'];
    }

}
