<?php

namespace App\Models\Mobile;

use Eloquent as Model;

/**
 * Class OrderProduct
 * @package App\Models\Mobile
 */
class OrderProduct extends Model
{

    public $table = 'order_products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'order_id',
        'store_product_id',
        'price',
        'points',
        'quantity',
        'subtotal',
        'total_points'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'order_id' => 'integer',
        'store_product_id' => 'integer',
        'price' => 'float',
        'points' => 'integer',
        'quantity' => 'integer',
        'deleted_at' => 'datetime',
        'subtotal'=>"double",
        'total_points'=>"int",
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function store()
    {
        return $this->hasOne('App\Models\StoreProducts', 'id','store_product_id')->with('product');
    }
}
