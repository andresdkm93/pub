<?php

namespace App\Models\Mobile;

use Eloquent as Model;

/**
 * Class Event
 * @package App\Models\Mobile
 */
class Event extends Model
{

    public $table = 'events';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'date',
        'name',
        'place',
        'description',
        'image',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'date' => 'datetime',
        'name' => 'string',
        'place' => 'string',
        'description' => 'string',
        'image' => 'string',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function items(){
        return $this->belongsToMany('App\Models\Mobile\StoreProduct','event_products', 'events_id', 'store_product_id')->with('product');
    }

}
