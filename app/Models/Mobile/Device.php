<?php

namespace App\Models\Mobile;

use Eloquent as Model;

/**
 * Class Device
 * @package App\Models\Mobile
 */
class Device extends Model
{

    public $table = 'devices';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'uuid';

    public $fillable = [
        'uuid',
        'pushkey',
        'os_id',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'uuid' => 'string',
        'pushkey' => 'string',
        'os_id' => 'integer',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
