<?php

namespace App\Models\Mobile;

use Eloquent as Model;

/**
 * Class EventProduct
 * @package App\Models\Mobile
 */
class EventProduct extends Model
{

    public $table = 'event_products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'events_id',
        'store_product_id',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'events_id' => 'integer',
        'store_product_id' => 'integer',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
    
    public function store_product(){
        return $this->hasOne('App\Models\Mobile\StoreProducts','id', 'store_product_id')->with('product');
    }
}
