<?php

namespace App\Models\Mobile;
use App\Scopes\QuantityScope;
use App\Scopes\DateScope;
use Eloquent as Model;

/**
 * Class Code
 * @package App\Models\Mobile
 */
class Code extends Model
{

    public $table = 'codes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new QuantityScope());
        static::addGlobalScope(new DateScope());

    }

    public $fillable = [
        'code',
        'quantity',
        'discount',
        'start_date',
        'end_date',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'string',
        'quantity' => 'integer',
        'discount' => 'integer',
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
