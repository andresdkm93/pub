<?php

namespace App\Models\Mobile;
use App\Scopes\QuantityScope;
use App\Scopes\DateScope;
use App\Scopes\StatusScope;
use Eloquent as Model;
use Carbon\Carbon;

/**
 * Class Promotion
 * @package App\Models\Mobile
 */
class Promotion extends Model
{

    public $table = 'promotions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new QuantityScope());
        static::addGlobalScope(new DateScope());
    }


    public $fillable = [
        'name',
        'image',
        'quantity',
        'start_date',
        'end_date',
        'promotion_type',
        'store_product_id',
        'discount',
        'quantity_minimal',
        'divider',
        'multiplier',
        'description',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'image' => 'string',
        'quantity' => 'integer',
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'promotion_type' => 'integer',
        'store_product_id' => 'integer',
        'discount' => 'integer',
        'quantity_minimal' => 'integer',
        'divider' => 'integer',
        'multiplier' => 'integer',
        'description' => 'string',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getStartDateAttribute()
    {
        $dt = Carbon::parse($this->attributes['start_date']);
        return  $dt->toDateString();
    }

    public function getEndDateAttribute()
    {
        $dt = Carbon::parse($this->attributes['end_date']);
        return $dt->toDateString();
    }

    public function store_product(){
        return $this->hasOne('App\Models\Mobile\StoreProduct','id', 'store_product_id')->with('product');
    }
}
