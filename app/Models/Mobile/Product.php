<?php

namespace App\Models\Mobile;

use Eloquent as Model;

/**
 * Class Product
 * @package App\Models\Mobile
 */
class Product extends Model
{

    public $table = 'products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

   

    public $fillable = [
        'name',
        'serial',
        'lot',
        'image',
        'description',
        'category_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'serial' => 'string',
        'lot' => 'string',
        'image' => 'string',
        'description' => 'string',
        'category_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function category()
    {
        return $this->hasOne('App\Models\Mobile\Category', 'id','category_id');
    }
    public function store()
    {
        return $this->hasOne('App\Models\Mobile\StoreProduct', 'product_id','id');

    }
}
