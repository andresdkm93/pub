<?php

namespace App\Models\Mobile;

use App\Models\OrderProducts;
use Eloquent as Model;

/**
 * Class Order
 * @package App\Models\Mobile
 */
class Order extends Model
{

    public $table = 'orders';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $appends = ['earned_points','products_quantity'];


    public $fillable = [
        'amount',
        'latitude',
        'longitude',
        'address',
        'client_id',
        'order_status_id',
        'phone',
        'qualification',
        'code',
        'delivery_zone',
        'delivery_value',
        'delivery_time',
        'pay_method',
        'points',
        'total',
        'instructions',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'amount' => 'string',
        'latitude' => 'string',
        'longitude' => 'string',
        'address' => 'string',
        'qualification'=>'integer',
        'order_status_id' => 'integer',
        'code' => 'string',
        'delivery_time' => 'string',
        'delivery_zone' => 'string',
        'delivery_value' => 'string',
        'pay_method' => 'string',
        'points' => 'string',
        'total' => 'string',
        'instructions' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getEarnedPointsAttribute(){
        if($this->attributes['order_status_id']==3 || $this->attributes['order_status_id']==1 || $this->attributes['order_status_id']==2){
            return intval($this->attributes['amount']/10000);
        }else{
            return 0;
        }
    }

    public function products()
    {
        return $this->hasMany('App\Models\OrderProducts','order_id','id')->with('store');
    }

    public function getProductsQuantityAttribute(){
        $products = OrderProducts::where('order_id', '=',$this->attributes['id'])->get();
        $qty = 0;
        foreach ($products as $product){
            $qty+= $product->quantity;
        }
        return $qty;
    }

    public function status()
    {
        return $this->hasOne('App\Models\OrderStatuses','id','order_status_id');
    }
}
