<?php

namespace App\Models\Mobile;

use Eloquent as Model;

/**
 * Class PromotionType
 * @package App\Models\Mobile
 */
class PromotionType extends Model
{

    public $table = 'promotion_types';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'name',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
