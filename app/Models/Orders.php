<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Orders",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="amount",
 *          description="amount",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="latitude",
 *          description="latitude",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="longitude",
 *          description="longitude",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address",
 *          description="address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="client_id",
 *          description="client_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="order_status_id",
 *          description="order_status_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Orders extends Model
{

    public $table = 'orders';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'amount',
        'latitude',
        'longitude',
        'address',
        'client_id',
        'phone',
        'qualification',
        'order_status_id',
        'code',
        'instructions',
        'delivery_zone',
        'delivery_value',
        'delivery_time',
        'pay_method',
        'points',
        'total',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'amount' => 'string',
        'latitude' => 'string',
        'longitude' => 'string',
        'address' => 'string',
        'client_id' => 'string',
        'qualification'=>'integer',
        'order_status_id' => 'integer',
        'code' => 'string',
        'instructions' => 'string',
        'deleted_at' => 'datetime',
        'delivery_time' => 'string',
        'delivery_zone' => 'string',
        'delivery_value' => 'string',
        'pay_method' => 'string',
        'points' => 'string',
        'total'=>'string',
        'description'=>'string'

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    public function products()
    {
        return $this->hasMany('App\Models\OrderProducts','order_id','id')->with('store');

    }

    public function client()
    {
        return $this->hasOne('App\Models\Clients','id','client_id');

    }
}
