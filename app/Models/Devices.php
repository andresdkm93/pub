<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Devices",
 *      required={},
 *      @SWG\Property(
 *          property="uuid",
 *          description="uuid",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pushkey",
 *          description="pushkey",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="os_id",
 *          description="os_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="clients_id",
 *          description="clients_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Devices extends Model
{
    use SoftDeletes;

    public $table = 'devices';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $primaryKey = 'uuid';

    public $fillable = [
        'uuid',
        'pushkey',
        'os_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'uuid' => 'integer',
        'pushkey' => 'string',
        'os_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
