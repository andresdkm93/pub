<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
/**
 * @SWG\Definition(
 *      definition="Shedule",
 *      required={},

 * )
 */
class Shedule extends Model
{
    public $table = 'shedules';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'closing_time',
        'opening_time',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'=>'integer',
        'opening_time'=>'string',
        'closing_time'=>'string',


    ];


    public function setOpeningTimeAttribute($value)
    {
        $hour=Carbon::parse($value);
        $hour->setTimezone('America/Bogota');
        $this->attributes['opening_time'] =$hour->toTimeString() ;
    }

    public function setClosingTimeAttribute($value)
    {
        $hour=Carbon::parse($value);
        $hour->setTimezone('America/Bogota');
        $this->attributes['closing_time'] =$hour->toTimeString() ;
    }

    public function getOpeningTimeAttribute($value)
    {
        return Carbon::parse($value)->toDateTimeString();
    }
    public function getClosingTimeAttribute($value)
    {
        return Carbon::parse($value)->toDateTimeString();

    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
