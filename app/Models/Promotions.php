<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

/**
 * @SWG\Definition(
 *      definition="Promotions",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="image",
 *          description="image",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="quantity",
 *          description="quantity",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="promotion_type",
 *          description="promotion_type",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="store_product_id",
 *          description="store_product_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="discount",
 *          description="discount",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="quantity_minimal",
 *          description="quantity_minimal",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="divider",
 *          description="divider",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="multiplier",
 *          description="multiplier",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      )
 * )
 */
class Promotions extends Model
{

    public $table = 'promotions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'name',
        'image',
        'quantity',
        'start_date',
        'end_date',
        'promotion_type',
        'store_product_id',
        'discount',
        'diageo',
        'quantity_minimal',
        'divider',
        'multiplier',
        'description',
        'price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'image' => 'string',
        'quantity' => 'integer',
        'start_date' => 'string',
        'end_date' => 'string',
        'promotion_type' => 'integer',
        'diageo' => 'boolean',
        'store_product_id' => 'integer',
        'discount' => 'integer',
        'quantity_minimal' => 'integer',
        'divider' => 'integer',
        'multiplier' => 'integer',
        'description' => 'string',
        'price' => 'float',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getStartDateAttribute()
    {
        $dt = Carbon::parse($this->attributes['start_date'], 'America/Bogota');
        return  $dt->toDateString();
    }

    public function getEndDateAttribute()
    {
        $dt = Carbon::parse($this->attributes['end_date'],'America/Bogota');
        return $dt->toDateString();
    }

    public function store_product(){
        return $this->hasOne('App\Models\StoreProducts','id', 'store_product_id')->with('product');
    }
}
