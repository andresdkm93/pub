<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="OrderProducts",
 *      required={},

 * )
 */
class OrderProducts extends Model
{

    public $table = 'order_products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'order_id',
        'store_product_id',
        'price',
        'points',
        'quantity',
        'subtotal',
        'total_points'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'order_id'=>"integer",
        'store_product_id'=>"integer",
        'price'=>"string",
        'points'=>"string",
        'quantity'=>"integer",
        'subtotal'=>"double",
        'total_points'=>"int",

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function store()
    {
        return $this->hasOne('App\Models\StoreProducts', 'id','store_product_id')->with('product');
    }

}
