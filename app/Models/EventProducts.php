<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="EventProducts",
 *      required={},
 *      @SWG\Property(
 *          property="events_id",
 *          description="events_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="store_product_id",
 *          description="store_product_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class EventProducts extends Model
{

    public $table = 'event_products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'id',
        'events_id',
        'store_product_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'events_id' => 'integer',
        'store_product_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function store_product(){
        return $this->hasOne('App\Models\StoreProducts','id', 'store_product_id')->with('product');
    }
}
