<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Products",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="serial",
 *          description="serial",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="lot",
 *          description="lot",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="photo",
 *          description="photo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="category_id",
 *          description="category_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Products extends Model {

	public $table = 'products';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	public $appends = ['url'];

	public $fillable = [
		'name',
		'serial',
		'lot',
		'image',
		'description',
		'category_id',
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id'          => 'integer',
		'name'        => 'string',
		'serial'      => 'string',
		'lot'         => 'string',
		'image'       => 'string',
		'description' => 'string',
		'category_id' => 'integer',
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	public function category() {
		return $this->hasOne( 'App\Models\Categories', 'id', 'category_id' );
	}

	public function store() {
		return $this->hasOne( 'App\Models\StoreProducts', 'product_id', 'id' );

	}

	public function getUrlAttribute() {
		return env( 'APP_URL' ) . '/tienda/identificador?productId=' . $this->attributes['id'];
	}
}
