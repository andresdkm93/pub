<?php

namespace App\Models;


use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="StoreProducts",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="quantity",
 *          description="quantity",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="price",
 *          description="price",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="store_type",
 *          description="store_type",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="points",
 *          description="points",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class StoreProducts extends Model
{

    public $table = 'store_products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    

    public $fillable = [
        'product_id',
        'quantity',
        'price',
        'status',
        'start_date',
        'end_date',
        'store_type',
        'points',
        'deleted_at',
        'recommended'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'quantity' => 'integer',
        'price' => 'float',
        'status' => 'boolean',
        'start_date' => 'string',
        'end_date' => 'string',
        'store_type' => 'integer',
        'points' => 'integer',
        'deleted_at' => 'datetime',
        'recommended' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function type()
    {
        return $this->hasOne('App\Models\StoreTypes', 'id','store_type');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Products', 'id','product_id');
    }
}
