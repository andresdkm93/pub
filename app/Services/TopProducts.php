<?php
/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 10/07/18
 * Time: 03:58 PM
 */

namespace App\Services;


use App\Models\Mobile\StoreProduct;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class TopProducts
{

    public function getProducts()
    {
        return Cache::remember('top_products_show', 1440, function () {
            return StoreProduct::with('product')
                ->orderBy('ranking', 'DESC')
                ->take(17)
                ->get();
        });

    }
}