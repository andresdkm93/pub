<?php
/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 21/07/18
 * Time: 07:37 PM
 */

namespace App\Services;


use App\Extensions\Collection\CollectionRedis;
use App\Models\Mobile\StoreProduct;
use Illuminate\Support\Facades\Cache;

class AllProductService
{

    public function getProducts(){
        return Cache::remember('products_all', 60, function () {
            $products = StoreProduct::with('product')
                ->get();
            $collection = new CollectionRedis($products);
            $collection = $collection->each(function ($item, $key) {
                $url=$item->product->name;
                $item->url=urlencode(strtolower($url));
            });
            return $collection;
        });

    }
}