<?php

namespace App\Console\Commands;


use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Models\Devices;
use App\Repositories\Mobile\DeviceRepository;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Console\Command;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class Masive extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'masive:send';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

    /**
     * Execute the console command.
     *
     * @param DeviceRepository $deviceRepository
     * @return mixed
     */
	public function handle() {
		$this->info("enviando email");
		Mail::raw( "Un nuevo pedido por: $", function ( $message ){
			$message->subject( 'Tienes un nuevo domicilio' );
			$message->to( 'andresdkm@gmail.com' );
		} );
	}
}
