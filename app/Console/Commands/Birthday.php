<?php

namespace App\Console\Commands;

use App\Push\Push;
use App\Repositories\Mobile\ClientRepository;
use Illuminate\Console\Command;

class Birthday extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'birthday:send';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle( ClientRepository $clientsRepository ) {
		$client = $clientsRepository->find( 1098670369 );
		/*$push   = array(
			"title" => "Feliz cumpleaños",
			"body"  => "Feliz cumpleaños, te desea Licorera Tres Jotas",
			"case"  => 2,
			'value' => 'CROMERO15'
		);*/

		$push   = array(
			"title" => "Aguila Original 330 ml",
			"body"  => "Oferta de Aguila Original",
			"case"  => 7,
			'value' => 1
		);

		Push::sendPush( 1098670369, $push );
	}
}
