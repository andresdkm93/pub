<?php

namespace App\Console\Commands;

use App\Models\Mobile\StoreProduct;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateRanking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pub:ranking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Ranking of products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now()->subDays(15);
        $storeProducts = StoreProduct::all();
        $globalRanking = DB::table('order_products')
            ->select(DB::raw('sum(subtotal) as ranking'))
            ->where('created_at', '>=', $date)
            ->first()->ranking;
        foreach ($storeProducts as $storeProduct) {
            $ranking = DB::table('order_products')
                ->select(DB::raw('sum(subtotal) as ranking'))
                ->where('created_at', '>=', $date)
                ->where('store_product_id', '=', $storeProduct->id)
                ->first()->ranking;
            if ($ranking != null) {
                $storeProduct->ranking = $ranking / $globalRanking;
            } else {
                $storeProduct->ranking = 0;
            }
            $storeProduct->save();
        }

    }
}
