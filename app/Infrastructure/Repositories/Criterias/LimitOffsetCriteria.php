<?php
/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 23/04/19
 * Time: 05:01 PM
 */

namespace App\Infrastructure\Repositories\Criterias;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class LimitOffsetCriteria implements CriteriaInterface
{

    private $limit;

    private $offset;

    /**
     * LimitOffsetCriteria constructor.
     * @param $limit
     * @param $offset
     */
    public function __construct($limit = null, $offset = null)
    {
        $this->limit = $limit;
        $this->offset = $offset;
    }


    /**
     * Apply criteria in query repository.
     *
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, \Prettus\Repository\Contracts\RepositoryInterface $repository)
    {

        if ($this->limit) {
            $model = $model->limit($this->limit);
        }

        if ($this->offset && $this->limit) {
            $model = $model->skip($this->offset);
        }

        return $model;
    }
}