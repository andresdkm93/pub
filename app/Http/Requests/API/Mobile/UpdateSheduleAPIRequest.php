<?php

namespace App\Http\Requests\API\Mobile;

use App\Models\Mobile\Shedule;
use InfyOm\Generator\Request\APIRequest;

class UpdateSheduleAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Shedule::$rules;
    }
}
