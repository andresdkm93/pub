<?php

namespace App\Http\Requests\API;

use App\Models\PromotionType;
use InfyOm\Generator\Request\APIRequest;

class CreatePromotionTypeAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return PromotionType::$rules;
    }
}
