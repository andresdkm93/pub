<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Mobile\Client;
use App\Models\Mobile\StoreProduct;
use App\Repositories\Mobile\CategoryRepository;
use App\Repositories\Mobile\ProductRepository;
use App\Repositories\StoreProductsRepository;
use App\Services\AllProductService;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Response;

class ProductController extends InfyOmBaseController {
	/** @var  ProductRepository */
	private $storeProductsRepository;

	private $categoryRepository;

	private $productRepository;

	public function __construct(
		StoreProductsRepository $storeProductsRepo,
		CategoryRepository $categoryRepository,
		ProductRepository $productRepository
	) {
		$this->storeProductsRepository = $storeProductsRepo;
		$this->categoryRepository      = $categoryRepository;
		$this->productRepository       = $productRepository;
	}


	public function show( $name, Request $request ) {
		$user = $request->session()->get( 'user', null );
		if ( ! empty( $user ) ) {
			$user = Client::where( 'id', $user->id )->first();
		}
		$service  = new AllProductService();
		$items    = $service->getProducts();
		$product  = $items->where( 'url', urlencode( urldecode( $name ) ) )->first();
		$products = $items->where( 'product.category_id', $product->product->category_id )
		                  ->random( 8 )
		                  ->all();

		$categories = $this->categoryRepository->all()->toArray();

		return view( 'products.show' )
			->with( 'name', urldecode( $name ) )
			->with( 'categories', $categories )
			->with( 'product', $product )
			->with( 'user', $user )
			->with( 'products', $products );
	}

	public function showId( Request $request ) {
		$user = $request->session()->get( 'user', null );
		if ( ! empty( $user ) ) {
			$user = Client::where( 'id', $user->id )->first();
		}
		if ( ! $request->has( 'productId' ) ) {
			Redirect::to( '/tienda' );
		}
		$service = new AllProductService();
		$items   = $service->getProducts();
		$product = StoreProduct::with( 'product' )->whereHas( 'product', function ( $q ) use ( $request ) {
			$q->where( 'id', '=', $request->productId );
		} )->get()->first();
		if ( empty($product)) {
			Redirect::to( '/tienda' );
		}
		$products = $items->where( 'product.category_id', $product->product->category_id )
		                  ->random( 8 )
		                  ->all();

		$categories = $this->categoryRepository->all()->toArray();

		return view( 'products.show' )
			->with( 'name', urldecode( $product->product->name ) )
			->with( 'categories', $categories )
			->with( 'product', $product )
			->with( 'user', $user )
			->with( 'products', $products );
	}

	public function search( $name, Request $request ) {
		$user = $request->session()->get( 'user', null );
		if ( ! empty( $user ) ) {
			$user = Client::where( 'id', $user->id )->first();
		}
		$service = new AllProductService();
		$items   = $service->getProducts();
		$ids     = $items->whereLike( 'product.name', ( urldecode( $name ) ) );
		$ids     = $ids->merge( $items->whereLike( 'product.category.name', ( urldecode( $name ) ) ) );
		if ( $ids->count() < 1 ) {
			$ids = $items->whereLike( 'product.description', ( urldecode( $name ) ) );
		}
		$this->productRepository->pushCriteria( new WhereFieldCriteria( 'id', $ids->pluck( 'id' ), 'in' ) );
		$this->productRepository->pushCriteria( new WithRelationshipsCriteria( [ 'store' ] ) );
		$products   = $this->productRepository->paginate( 16 )->toArray();
		$categories = $this->categoryRepository->all()->toArray();

		return view( 'products.search' )
			->with( 'name', urldecode( $name ) )
			->with( 'categories', $categories )
			->with( 'user', $user )
			->with( 'products', $products );
	}

	public function terms() {
		return view( 'privacy' );
	}

	public function download() {
		return view( 'download' );

	}

	public function conditions( Request $request ) {
		{
			$user = $request->session()->get( 'user', null );

			return view( 'terms' )
				->with( 'user', $user );
		}
	}

}
