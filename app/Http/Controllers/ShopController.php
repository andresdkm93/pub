<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Infrastructure\Repositories\Criterias\OrderByCriteria;
use App\Infrastructure\Repositories\Criterias\OrderByRelation;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Infrastructure\Repositories\Criterias\WhereHasCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Mobile\Category;
use App\Models\Mobile\Client;
use App\Repositories\Mobile\CategoryRepository;
use App\Repositories\Mobile\EventRepository;
use App\Repositories\Mobile\ProductRepository;
use App\Repositories\Mobile\PromotionRepository;
use App\Repositories\Mobile\StoreProductRepository;
use App\Services\TopProducts;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $categoryRepository;

    private $productsRepository;

    private $storeProductRepository;

    public function __construct(CategoryRepository $categoryRepository,
                                ProductRepository $productRepository,
                                StoreProductRepository $storeProductRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->productsRepository = $productRepository;
        $this->storeProductRepository = $storeProductRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->session()->get('user', null);
        if(!empty($user)){
            $user = Client::where('id', $user->id)->first();
        }
        $categories = $this->categoryRepository->all()->toArray();
        $topProductsService = new TopProducts();
        $topProducts = $topProductsService->getProducts()->toArray();
        return view('shop.home')
            ->with('categories', $categories)
            ->with('user', $user)
            ->with('topProducts', $topProducts);

    }

    public function categories($name, Request $request)
    {
        $user = $request->session()->get('user', null);
        if(!empty($user)){
            $user = Client::where('id', $user->id)->first();
        }
        $categoryName = urldecode($name);
        $categories = $this->categoryRepository->all()->toArray();
        $category = Category::whereRaw("lower(name) = '$categoryName'")->get()->first();
        if (empty($category)) {
            return redirect('/shop');
        }

        if ($request->has('sort')) {
            $sort = $request->get('sort');
            $order = explode(":", $sort);
            if ($order[0] == 'alphabetically') {
                $this->productsRepository->pushCriteria(new WhereFieldCriteria('category_id', $category->id, '='));
                $this->productsRepository->pushCriteria(new WhereHasCriteria('store', 'store_type', 1));
                $this->productsRepository->pushCriteria(new OrderByCriteria("name", "asc"));
                $products = $this->productsRepository->paginate(16)->toArray();
                $nameOrder = 'Alfabéticamente';
                return view('shop.category')
                    ->with('name', $name)
                    ->with('order', $nameOrder)
                    ->with('categories', $categories)
                    ->with('currentCategory', $category)
                    ->with('user', $user)
                    ->with('products', $products);
            } else if ($order[0] == 'price') {
                if ($order[0] == 'price') {
                    $this->storeProductRepository->pushCriteria(new OrderByCriteria("price", $order[1]));
                    if ($order[1] == 'desc') {
                        $nameOrder = 'Mayor precio';
                    } else {
                        $nameOrder = 'Menor Precio';
                    }
                }
            } else if ($order[0] == 'recommended') {
                $this->storeProductRepository->pushCriteria(new OrderByCriteria("recommended", 'desc'));
                $nameOrder = 'Recomendados';
            }
        } else {
            $this->storeProductRepository->pushCriteria(new OrderByCriteria("ranking", 'desc'));
            $nameOrder = 'Popularidad';
        }
        $this->storeProductRepository->pushCriteria(new WhereHasCriteria('product', 'category_id', $category->id));
        $products = $this->storeProductRepository->paginate(16)->toArray();
        return view('shop.order')
            ->with('name', $name)
            ->with('order', $nameOrder)
            ->with('categories', $categories)
            ->with('currentCategory', $category)
            ->with('user', $user)
            ->with('products', $products);

    }


}
