<?php
/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 21/04/19
 * Time: 01:41 PM
 */

namespace App\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;


class ReportController extends Controller {

	public function store( Request $request ) {

		$input = $request->all();

		$startDate = Carbon::parse( $input['start_date'] );
		$startDate = $startDate->toDateString();

		$endDate = Carbon::parse( $input['end_date'] );
		$endDate = $endDate->toDateString();

		$results = DB::select( DB::raw( "SELECT p.name, p.description,p.serial,SUM(op.quantity) as quantity,(SUM(op.quantity)*sp.price) as total 
				FROM `order_products` op 
				JOIN store_products sp ON(op.store_product_id=sp.id)
				JOIN products p ON (p.id=sp.product_id)
				WHERE op.created_at >= '$startDate 00:00:00' and op.created_at <= '$endDate 23:59:59'
				GROUP BY op.store_product_id
				ORDER BY total desc" ) );

		$data = [];

		foreach ( $results as $result ) {
			$data[] = [ $result->name, $result->description, $result->serial, $result->quantity, $result->total ];
		}

		$headers = [ 'Nombre', 'Descripción', 'Serial', 'Cantidad', 'Valor' ];

		return $this->download( $headers, $data );
	}


	public function download( $head, $data ) {
		$now     = Carbon::now();
		$headers = [
			'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
			,
			'Content-type'        => 'text/csv'
			,
			'Content-Disposition' => 'attachment; filename=report_' . $now->timestamp . '.csv'
			,
			'Expires'             => '0'
			,
			'Pragma'              => 'public'
		];

		$callback = function () use ( $head, $data ) {
			$FH = fopen( 'php://output', 'w' );
			fputcsv( $FH, $head );
			foreach ( $data as $row ) {
				fputcsv( $FH, $row );
			}
			fclose( $FH );
		};

		return Response::stream( $callback, 200, $headers ); //use Illuminate\Support\Facades\Response;

	}
}