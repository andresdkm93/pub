<?php

namespace App\Http\Controllers;

use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Mobile\Client;
use App\Repositories\Mobile\ClientRepository;
use App\Services\TopProducts;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;

class CartController extends Controller
{

    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function index(Request $request)
    {
        $user = $request->session()->get('user', null);
        if ($user==null) {
            return redirect('/');
        }
        if(!empty($user)){
            $user = Client::where('id', $user->id)->first();
        }
        try{
            $this->clientRepository->pushCriteria(new WithRelationshipsCriteria('locations'));
            $client = $this->clientRepository->find($user->id);
            $topProductsService = new TopProducts();
            if($topProductsService->getProducts()->count()>0) {
                $topProducts = $topProductsService->getProducts()->random(3)->toArray();
            }else{
                $topProducts = [];
            }
            return view('checkout.cart')
                ->with('products', $topProducts)
                ->with('user', $user)
                ->with('client', $client);
        } catch (DecryptException $e) {
            Log::error('Error in decrypt',['error'=>$e]);
            return redirect('');
        }
    }
}
