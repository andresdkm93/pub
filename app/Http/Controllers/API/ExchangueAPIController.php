<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateExchangueAPIRequest;
use App\Http\Requests\API\UpdateExchangueAPIRequest;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Models\Exchangue;
use App\Repositories\ExchangueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ExchangueController
 * @package App\Http\Controllers\API
 */

class ExchangueAPIController extends InfyOmBaseController
{
    /** @var  ExchangueRepository */
    private $exchangueRepository;

    public function __construct(ExchangueRepository $exchangueRepo)
    {
        $this->exchangueRepository = $exchangueRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/exchangues",
     *      summary="Get a listing of the Exchangues.",
     *      tags={"Exchangue"},
     *      description="Get all Exchangues",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Exchangue")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index($clientId,Request $request)
    {
        $this->exchangueRepository->pushCriteria(new RequestCriteria($request));
        $this->exchangueRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->exchangueRepository->pushCriteria(new WhereFieldCriteria('client_id',$clientId));
        $exchangues = $this->exchangueRepository->paginate(10);

        return $this->sendResponse($exchangues->toArray(), 'Exchangues retrieved successfully');
    }

}
