<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateOrdersAPIRequest;
use App\Http\Requests\API\UpdateOrdersAPIRequest;
use App\Infrastructure\Repositories\Criterias\OrderByCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Orders;
use App\Repositories\CodesRepository;
use App\Repositories\OrderProductsRepository;
use App\Repositories\OrdersRepository;
use App\Repositories\StoreProductsRepository;
use Firebase\Firebase;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class OrdersController
 * @package App\Http\Controllers\API
 */
class OrdersAPIController extends AppBaseController
{
    /** @var  OrdersRepository */
    private $ordersRepository;
    private $orderProductsRepository;
    private $storeProductsRepository;
    private $codesRepository;


    public function __construct(OrdersRepository $ordersRepo, OrderProductsRepository $orderProductsRepo, StoreProductsRepository $storeProductsRepo, CodesRepository $codesRepo)
    {
        $this->ordersRepository = $ordersRepo;
        $this->orderProductsRepository = $orderProductsRepo;
        $this->storeProductsRepository = $storeProductsRepo;
        $this->codesRepository = $codesRepo;

    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/orders",
     *      summary="Get a listing of the Orders.",
     *      tags={"Orders"},
     *      description="Get all Orders",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Orders")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->ordersRepository->pushCriteria(new RequestCriteria($request));
        $this->ordersRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->ordersRepository->pushCriteria(new OrderByCriteria('id', 'desc'));
        $orders = $this->ordersRepository->paginate(10);

        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }

    /**
     * @param CreateOrdersAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/orders",
     *      summary="Store a newly created Orders in storage",
     *      tags={"Orders"},
     *      description="Store Orders",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Orders that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Orders")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Orders"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/orders/{id}",
     *      summary="Display the specified Orders",
     *      tags={"Orders"},
     *      description="Get Orders",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Orders",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Orders"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Orders $orders */
        $this->ordersRepository->pushCriteria(new WithRelationshipsCriteria(['products', 'client']));

        $orders = $this->ordersRepository->find($id);

        if (empty($orders)) {
            return Response::json(ResponseUtil::makeError('Orders not found'), 400);
        }

        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }


    public function values()
    {
        /** @var Orders $orders */
        $orders = $this->ordersRepository->values();
        return $this->sendResponse($orders, 'Orders retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOrdersAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/orders/{id}",
     *      summary="Update the specified Orders in storage",
     *      tags={"Orders"},
     *      description="Update Orders",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Orders",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Orders that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Orders")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Orders"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOrdersAPIRequest $request)
    {
        $input = $request->all();

        $orders = $this->ordersRepository->find($id);

        $firebase = new \Firebase\FirebaseLib("https://pubjjj-1363.firebaseio.com/", "T8iamaDWdy89OpK1WOrFKO6qf6vADkMvcsbxwaNC");

        if ($input['order_status_id'] == 5) {
            $firebase->delete('/orders/' . $id, $orders);
        } else {
            if (empty($orders)) {
                return Response::json(ResponseUtil::makeError('Orders not found'), 400);
            }
            $orders = $this->ordersRepository->update($input, $id);
            if ($input['order_status_id'] == 2) {
                $firebase->update('/orders/' . $id, $orders);
            } else {
                $firebase->delete('/orders/' . $id, $orders);
            }
            /** @var Orders $orders */
        }


        return $this->sendResponse($orders->toArray(), 'Orders updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/orders/{id}",
     *      summary="Remove the specified Orders from storage",
     *      tags={"Orders"},
     *      description="Delete Orders",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Orders",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Orders $orders */
        $orders = $this->ordersRepository->find($id);

        if (empty($orders)) {
            return Response::json(ResponseUtil::makeError('Orders not found'), 400);
        }

        $orders->delete();

        return $this->sendResponse($id, 'Orders deleted successfully');
    }
}
