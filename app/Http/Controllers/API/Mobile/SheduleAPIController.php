<?php

namespace App\Http\Controllers\API\Mobile;

use Edujugon\PushNotification\PushNotification;

use App\Http\Requests\API\Mobile\CreateSheduleAPIRequest;
use App\Http\Requests\API\Mobile\UpdateSheduleAPIRequest;
use App\Models\Mobile\Shedule;
use App\Repositories\Mobile\SheduleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Facades\DB;

/**
 * Class SheduleController
 * @package App\Http\Controllers\API\Mobile
 */
class SheduleAPIController extends InfyOmBaseController
{
    /** @var  SheduleRepository */
    private $sheduleRepository;

    public function __construct(SheduleRepository $sheduleRepo)
    {
        $this->sheduleRepository = $sheduleRepo;
    }

    public function isActive()
    {

        $now = Carbon::now();
        $times = DB::table('shedules')->first();
        $open= Carbon::parse($times->opening_time);
        $close=Carbon::parse($times->closing_time);

        if($open->greaterThan($close))
        {
            if($now->hour>=0 && $now->hour<6)
            {
                $open=$open->subDay();
            }else if($now->hour<24) {
                $close=$close->addDay();
            }
        }

        $active=$now->lessThan($close) && $now->greaterThan($open);

        return $this->sendResponse([
            'active'=>$active,
            'open'=>$open,
            'close'=>$close,
            'now'=>$now
        ], 'Shedules retrieved successfully');

    }
}
