<?php

namespace App\Http\Controllers\API\Mobile;

use App\Infrastructure\Repositories\Criterias\OrderByCriteria;
use App\Infrastructure\Repositories\Criterias\WhereHasCriteria;
use App\Models\Products;
use App\Repositories\Mobile\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;

use Response;

/**
 * Class ProductsController
 * @package App\Http\Controllers\API
 */

class CategoriesProductsAPIController extends AppBaseController
{
    /** @var  ProductsRepository */
    private $productsRepository;

    public function __construct(ProductRepository $productsRepo)
    {
        $this->productsRepository = $productsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/products",
     *      summary="Get a listing of the Products.",
     *      tags={"Products"},
     *      description="Get all Products",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Products")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index($categoryId,Request $request)
    {
        $this->productsRepository->pushCriteria(new RequestCriteria($request));
        $this->productsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->productsRepository->pushCriteria(new WhereFieldCriteria('category_id',$categoryId,'='));
        $this->productsRepository->pushCriteria(new WhereHasCriteria('store','store_type',1));
        $this->productsRepository->pushCriteria(new OrderByCriteria("name","asc"));

        $products = $this->productsRepository->paginate(10);

        return $this->sendResponse($products->toArray(), 'Products retrieved successfully');
    }

    /**
     * @param CreateProductsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/products",
     *      summary="Store a newly created Products in storage",
     *      tags={"Products"},
     *      description="Store Products",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Products that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Products")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Products"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */


}
