<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Requests\API\Mobile\CreateCodeAPIRequest;
use App\Http\Requests\API\Mobile\UpdateCodeAPIRequest;
use App\Models\Mobile\Code;
use App\Repositories\Mobile\CodeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CodeController
 * @package App\Http\Controllers\API\Mobile
 */

class CodeAPIController extends InfyOmBaseController
{
    /** @var  CodeRepository */
    private $codeRepository;

    public function __construct(CodeRepository $codeRepo)
    {
        $this->codeRepository = $codeRepo;
    }

    public function search(Request $request)
    {


        /** @var Codes $codes */
        $input=$request->only('code');
        $codes = $this->codeRepository->findWhere(['code'=>$input['code']]);

        if (empty($codes)) {
            return Response::json(ResponseUtil::makeError('Codes not found'), 400);
        }

        return $this->sendResponse($codes->toArray(), 'Codes retrieved successfully');
    }
}
