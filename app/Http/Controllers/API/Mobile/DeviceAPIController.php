<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Requests\API\Mobile\CreateDeviceAPIRequest;
use App\Http\Requests\API\Mobile\UpdateDeviceAPIRequest;
use App\Models\Mobile\Device;
use App\Repositories\Mobile\DeviceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class DeviceController
 * @package App\Http\Controllers\API\Mobile
 */

class DeviceAPIController extends InfyOmBaseController
{
    /** @var  DeviceRepository */
    private $deviceRepository;

    public function __construct(DeviceRepository $deviceRepo)
    {
        $this->deviceRepository = $deviceRepo;
    }

    /**
     * Display a listing of the Device.
     * GET|HEAD /devices
     *
     * @param Request $request
     * @return Response
     */

    /**
     * Store a newly created Device in storage.
     * POST /devices
     *
     * @param CreateDeviceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDeviceAPIRequest $request)
    {
        $input = $request->all();

        $device = $this->deviceRepository->findWhere(["uuid"=>$input['uuid']]);
        if (count($device)==0) {
            $devices = $this->deviceRepository->create($input);
        }else{
            $devices = $this->deviceRepository->update($input, $input['uuid']);
        }


        return $this->sendResponse($devices->toArray(), 'Device saved successfully');
    }

    /**
     * Display the specified Device.
     * GET|HEAD /devices/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Device $device */
        $device = $this->deviceRepository->find($id);

        if (empty($device)) {
            return Response::json(ResponseUtil::makeError('Device not found'), 404);
        }

        return $this->sendResponse($device->toArray(), 'Device retrieved successfully');
    }

    /**
     * Update the specified Device in storage.
     * PUT/PATCH /devices/{id}
     *
     * @param  int $id
     * @param UpdateDeviceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeviceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Device $device */
        $device = $this->deviceRepository->find($id);

        if (empty($device)) {
            return Response::json(ResponseUtil::makeError('Device not found'), 404);
        }

        $device = $this->deviceRepository->update($input, $id);

        return $this->sendResponse($device->toArray(), 'Device updated successfully');
    }

    /**
     * Remove the specified Device from storage.
     * DELETE /devices/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Device $device */
        $device = $this->deviceRepository->find($id);

        if (empty($device)) {
            return Response::json(ResponseUtil::makeError('Device not found'), 404);
        }

        $device->delete();

        return $this->sendResponse($id, 'Device deleted successfully');
    }
}
