<?php

namespace App\Http\Controllers\API\Mobile;

use App\Infrastructure\Repositories\Criterias\OrderByCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Http\Requests\API\Mobile\CreatePromotionAPIRequest;
use App\Http\Requests\API\Mobile\UpdatePromotionAPIRequest;
use App\Models\Mobile\Product;
use App\Models\Mobile\Promotion;
use App\Models\Mobile\StoreProduct;
use App\Repositories\Mobile\PromotionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PromotionController
 * @package App\Http\Controllers\API\Mobile
 */
class PromotionAPIController extends InfyOmBaseController {
	/** @var  PromotionRepository */
	private $promotionRepository;

	public function __construct( PromotionRepository $promotionRepo ) {
		$this->promotionRepository = $promotionRepo;
	}

	/**
	 * Display a listing of the Promotion.
	 * GET|HEAD /promotions
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index( Request $request ) {
		$this->promotionRepository->pushCriteria( new OrderByCriteria( 'diageo', 'desc' ) );
		$this->promotionRepository->pushCriteria( new OrderByCriteria( 'start_date', 'desc' ) );
		$this->promotionRepository->pushCriteria( new RequestCriteria( $request ) );
		$this->promotionRepository->pushCriteria( new LimitOffsetCriteria( $request ) );
		$promotions = $this->promotionRepository->all()->toArray();
		foreach ( $promotions as &$promotion ) {
			if ( $promotion['promotion_type'] && $promotion['promotion_type'] == 2 ) {
				$product = StoreProduct::find( $promotion['store_product_id'] );
				$promotion['price'] =  $product->price * $promotion['multiplier'];
			} else if ( $promotion['promotion_type'] && $promotion['promotion_type'] == 3 ) {
				$product = StoreProduct::find( $promotion['store_product_id'] );
				$promotion['price'] = $product->price;
			}

		}

		return $this->sendResponse( $promotions, 'Promotions retrieved successfully' );
	}

	/**
	 * Store a newly created Promotion in storage.
	 * POST /promotions
	 *
	 * @param CreatePromotionAPIRequest $request
	 *
	 * @return Response
	 */
	public function store( CreatePromotionAPIRequest $request ) {
		$input = $request->all();

		$promotions = $this->promotionRepository->create( $input );

		return $this->sendResponse( $promotions->toArray(), 'Promotion saved successfully' );
	}

	/**
	 * Display the specified Promotion.
	 * GET|HEAD /promotions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show( $id ) {
		$this->promotionRepository->pushCriteria( new WithRelationshipsCriteria(
			[ 'store_product' ]
		) );
		/** @var Promotion $promotion */
		$promotion = $this->promotionRepository->find( $id );

		if ( empty( $promotion ) ) {
			return Response::json( ResponseUtil::makeError( 'Promotion not found' ), 404 );
		}

		return $this->sendResponse( $promotion->toArray(), 'Promotion retrieved successfully' );
	}

	/**
	 * Update the specified Promotion in storage.
	 * PUT/PATCH /promotions/{id}
	 *
	 * @param  int $id
	 * @param UpdatePromotionAPIRequest $request
	 *
	 * @return Response
	 */
	public function update( $id, UpdatePromotionAPIRequest $request ) {
		$input = $request->all();

		/** @var Promotion $promotion */
		$promotion = $this->promotionRepository->find( $id );

		if ( empty( $promotion ) ) {
			return Response::json( ResponseUtil::makeError( 'Promotion not found' ), 404 );
		}

		$promotion = $this->promotionRepository->update( $input, $id );

		return $this->sendResponse( $promotion->toArray(), 'Promotion updated successfully' );
	}

	/**
	 * Remove the specified Promotion from storage.
	 * DELETE /promotions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy( $id ) {
		/** @var Promotion $promotion */
		$promotion = $this->promotionRepository->find( $id );

		if ( empty( $promotion ) ) {
			return Response::json( ResponseUtil::makeError( 'Promotion not found' ), 404 );
		}

		$promotion->delete();

		return $this->sendResponse( $id, 'Promotion deleted successfully' );
	}
}
