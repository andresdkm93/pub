<?php

namespace App\Http\Controllers\API\Mobile;


use App\Models\Mobile\StoreProduct;
use App\Repositories\Mobile\ProductRepository;
use App\Repositories\Mobile\StoreProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;

use Response;

/**
 * Class StoreProductsController
 * @package App\Http\Controllers\API
 */

class PromotionProductAPIController extends AppBaseController
{
    /** @var  StoreProductsRepository */
    private $storeProductsRepository;
    private $productsRepository;


    public function __construct(StoreProductRepository $storeProductsRepo,ProductRepository $productsRepo)
    {
        $this->storeProductsRepository = $storeProductsRepo;
        $this->productsRepository = $productsRepo;

    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/storeProducts",
     *      summary="Get a listing of the StoreProducts.",
     *      tags={"StoreProducts"},
     *      description="Get all StoreProducts",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/StoreProducts")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->storeProductsRepository->pushCriteria(new RequestCriteria($request));
        $this->storeProductsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->storeProductsRepository->pushCriteria(new WhereFieldCriteria( 'store_type',2, '='));

        $this->storeProductsRepository->pushCriteria(new WithRelationshipsCriteria(
            ['product']
        ));
        $storeProducts = $this->storeProductsRepository->paginate(100,['id','quantity','points','status','start_date','end_date','product_id']);

        return $this->sendResponse($storeProducts->toArray(), 'StoreProducts retrieved successfully');
    }



    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/storeProducts/{id}",
     *      summary="Display the specified StoreProducts",
     *      tags={"StoreProducts"},
     *      description="Get StoreProducts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StoreProducts",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StoreProducts"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var StoreProducts $storeProducts */
        $this->storeProductsRepository->pushCriteria(new WithRelationshipsCriteria(
            ['product',]
        ));
        $storeProducts = $this->storeProductsRepository->find($id);

        if (empty($storeProducts)) {
            return Response::json(ResponseUtil::makeError('StoreProducts not found'), 400);
        }

        return $this->sendResponse($storeProducts->toArray(), 'StoreProducts retrieved successfully');
    }

  
}
