<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Requests\API\CreateStoreProductsAPIRequest;
use App\Http\Requests\API\UpdateStoreProductsAPIRequest;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Models\StoreProducts;
use App\Repositories\StoreProductsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;

use Response;

/**
 * Class StoreProductsController
 * @package App\Http\Controllers\API
 */

class StoreProductsAPIController extends AppBaseController
{
    /** @var  StoreProductsRepository */
    private $storeProductsRepository;

    public function __construct(StoreProductsRepository $storeProductsRepo)
    {
        $this->storeProductsRepository = $storeProductsRepo;
    }


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/storeProducts/{id}",
     *      summary="Display the specified StoreProducts",
     *      tags={"StoreProducts"},
     *      description="Get StoreProducts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StoreProducts",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StoreProducts"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var StoreProducts $storeProducts */
        $this->storeProductsRepository->pushCriteria(new WithRelationshipsCriteria(
            ['product',]
        ));
        $storeProducts = $this->storeProductsRepository->find($id);

        if (empty($storeProducts)) {
            return Response::json(ResponseUtil::makeError('StoreProducts not found'), 400);
        }

        return $this->sendResponse($storeProducts->toArray(), 'StoreProducts retrieved successfully');
    }


    public function suggestedProducts(Request $request){
        $this->storeProductsRepository->pushCriteria(new WhereFieldCriteria("recommended", 1));
        $this->storeProductsRepository->pushCriteria(new WithRelationshipsCriteria(
            ['product',]
        ));
        $storeProducts = $this->storeProductsRepository->all();

        return $this->sendResponse($storeProducts->toArray(), 'StoreProducts retrieved successfully');
    }
}
