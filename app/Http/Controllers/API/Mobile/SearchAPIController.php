<?php

namespace App\Http\Controllers\API\Mobile;

use App\Extensions\Collection\CollectionRedis;
use App\Repositories\Mobile\StoreProductRepository;
use App\Services\AllProductService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Cache;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;

use Response;

/**
 * Class ProductsController
 * @package App\Http\Controllers\API
 */
class SearchAPIController extends AppBaseController
{


    private $storeProductsRepository;

    public function __construct(StoreProductRepository $storeProductsRepo)
    {
        $this->storeProductsRepository = $storeProductsRepo;
    }

    private function getProducts()
    {
       /* return Cache::remember('all_products', 60, function () {
            $this->storeProductsRepository->pushCriteria(new WithRelationshipsCriteria(
                ['product',]
            ));
            $products = $this->storeProductsRepository->all(['id', 'product_id']);
            return $products;
        });*/
        $this->storeProductsRepository->pushCriteria(new WithRelationshipsCriteria(
            ['product.category',]
        ));
        $products = $this->storeProductsRepository->all(['id', 'product_id']);
        return $products;
    }


    public function search(Request $request)
    {
        $input = $request->all();
        $service = new AllProductService();
        $collection = $service->getProducts();
        $filter = $collection->whereLike('product.name', $input['q']);
        $filterCategory = $collection->whereLike('product.category.name', $input['q']);
        $filter= $filter->merge($filterCategory);
        $data = $filter->take(10);
        $result = array();
        foreach ($data as $clave => $valor) {
            $result[] = $valor;
        }
        return $this->sendResponse($result, 'Products retrieved successfully');
    }


}
