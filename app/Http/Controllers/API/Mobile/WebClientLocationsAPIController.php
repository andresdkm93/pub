<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Requests\API\CreateClientLocationsAPIRequest;
use App\Http\Requests\API\UpdateClientLocationsAPIRequest;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Models\Mobile\ClientLocation;
use App\Repositories\Mobile\ClientLocationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Crypt;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ClientLocationsController
 * @package App\Http\Controllers\API
 */

class WebClientLocationsAPIController extends AppBaseController
{
    /** @var  ClientLocationsRepository */
    private $clientLocationsRepository;

    public function __construct(ClientLocationRepository $clientLocationsRepo)
    {
        $this->clientLocationsRepository = $clientLocationsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/clientLocations",
     *      summary="Get a listing of the ClientLocations.",
     *      tags={"ClientLocations"},
     *      description="Get all ClientLocations",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ClientLocations")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index($clientId,Request $request)
    {
        $userId = $request->get('userId');
        $this->clientLocationsRepository->pushCriteria(new WhereFieldCriteria('client_id',$userId));
        $this->clientLocationsRepository->pushCriteria(new RequestCriteria($request));
        $this->clientLocationsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $clientLocations = $this->clientLocationsRepository->findWhere(['client_id'=>$clientId]);

        return $this->sendResponse($clientLocations->toArray(), 'ClientLocations retrieved successfully');
    }


   
    /**
     * @param CreateClientLocationsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/clientLocations",
     *      summary="Store a newly created ClientLocations in storage",
     *      tags={"ClientLocations"},
     *      description="Store ClientLocations",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ClientLocations that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ClientLocations")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ClientLocations"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $userId = $request->get('userId');
        $input['client_id']=$userId;

        $clientLocations = $this->clientLocationsRepository->create($input);

        return $this->sendResponse($clientLocations->toArray(), 'ClientLocations saved successfully');
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     * @SWG\Get(
     *      path="/clientLocations/{id}",
     *      summary="Display the specified ClientLocations",
     *      tags={"ClientLocations"},
     *      description="Get ClientLocations",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ClientLocations",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ClientLocations"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id, Request $request)
    {
        $userId = $request->get('userId');
        /** @var ClientLocations $clientLocations */
        $this->clientLocationsRepository->pushCriteria(new WhereFieldCriteria('client_id',$userId));
        $clientLocations = $this->clientLocationsRepository->find($id);
        if (empty($clientLocations)) {
            return Response::json(ResponseUtil::makeError('ClientLocations not found'), 400);
        }

        return $this->sendResponse($clientLocations->toArray(), 'ClientLocations retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateClientLocationsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/clientLocations/{id}",
     *      summary="Update the specified ClientLocations in storage",
     *      tags={"ClientLocations"},
     *      description="Update ClientLocations",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ClientLocations",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ClientLocations that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ClientLocations")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ClientLocations"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var ClientLocations $clientLocations */
        $clientLocations = $this->clientLocationsRepository->find($id);

        if (empty($clientLocations)) {
            return Response::json(ResponseUtil::makeError('ClientLocations not found'), 400);
        }

        $clientLocations = $this->clientLocationsRepository->update($input, $id);

        return $this->sendResponse($clientLocations->toArray(), 'ClientLocations updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/clientLocations/{id}",
     *      summary="Remove the specified ClientLocations from storage",
     *      tags={"ClientLocations"},
     *      description="Delete ClientLocations",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ClientLocations",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ClientLocations $clientLocations */
        $clientLocations = $this->clientLocationsRepository->find($id);

        if (empty($clientLocations)) {
            return Response::json(ResponseUtil::makeError('ClientLocations not found'), 400);
        }

        $clientLocations->delete();

        return $this->sendResponse($id, 'ClientLocations deleted successfully');
    }
}
