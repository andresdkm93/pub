<?php

namespace App\Http\Controllers\API\Mobile;

use App\Repositories\Mobile\OrderRepository;
use App\Repositories\Mobile\CodeRepository;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Push;
use Response;

/**
 * Class OrderController
 * @package App\Http\Controllers\API\Mobile
 */
class LocationAPIController extends InfyOmBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;
    private $codesRepository;
    private $client;

    public function __construct(OrderRepository $orderRepo, CodeRepository $codesRepo)
    {
        $this->orderRepository = $orderRepo;
        $this->codesRepository = $codesRepo;
        $this->client = new \GuzzleHttp\Client();

    }


    public function isActive()
    {
        $now = Carbon::now();
        $times = DB::table('shedules')->first();
        $open = Carbon::parse($times->opening_time);
        $close = Carbon::parse($times->closing_time);

        if ($open->greaterThan($close)) {
            if ($now->hour >= 0 && $now->hour < 6) {
                $open = $open->subDay();
            } else if ($now->hour < 24) {
                $close = $close->addDay();
            }
        }

        $active = $now->lessThan($close) && $now->greaterThan($open);
        return $active;
    }


    public function check(Request $request)
    {
        if ($this->isActive()) {
            $input = $request->all();

            $zone = null;
            $sql = '((acos(sin(radians(latitude)) * sin(radians(' . $input['latitude'] . ')) + cos(radians(latitude)) * cos(radians(' . $input['latitude'] . ')) * cos(radians(longitude) - radians(' . $input['longitude'] . '))) * 6378) ) ';
            $zone = DB::table('zones')
                ->select('price', 'name', DB::raw($sql . ' as distance'), 'radius')
                ->whereRaw('radius >= (' . $sql . ' * 1000 )')
                ->orderBy('distance', 'asc')->first();

            if (is_null($zone) || is_null($zone->name)) {
                return $this->sendError("Lo sentimos,esa zona no tiene cobertura");
            }

            Log::debug("Nueva orden", ["order mobile" => $input, "products" => $request->get('products')]);
            $value = $zone->price;
            if (config('app.freeOrder') && $input['total'] > config('app.freeOrder') && $zone->price <= 8000) {
                $value = 0;
            }
            return $this->sendResponse(['value' => $value], 'Orders saved successfully');
        } else {
            return $this->sendError("Lo sentimos,la licorera se encuentra fuera de servicio");
        }
    }


}
