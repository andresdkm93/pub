<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Requests\API\Mobile\CreateEventAPIRequest;
use App\Http\Requests\API\Mobile\UpdateEventAPIRequest;
use App\Models\Mobile\Event;
use App\Repositories\Mobile\EventRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use Response;

/**
 * Class EventController
 * @package App\Http\Controllers\API\Mobile
 */

class EventAPIController extends InfyOmBaseController
{
    /** @var  EventRepository */
    private $eventRepository;

    public function __construct(EventRepository $eventRepo)
    {
        $this->eventRepository = $eventRepo;
    }

    /**
     * Display a listing of the Event.
     * GET|HEAD /events
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->eventRepository->pushCriteria(new RequestCriteria($request));
        $this->eventRepository->pushCriteria(new LimitOffsetCriteria($request));
        $events = $this->eventRepository->all();

        return $this->sendResponse($events->toArray(), 'Events retrieved successfully');
    }


    /**
     * Display the specified Event.
     * GET|HEAD /events/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Event $event */
        $this->eventRepository->pushCriteria(new WithRelationshipsCriteria(
            ['items']
        ));
        $event = $this->eventRepository->find($id);

        if (empty($event)) {
            return Response::json(ResponseUtil::makeError('Event not found'), 404);
        }

        return $this->sendResponse($event->toArray(), 'Event retrieved successfully');
    }


}
