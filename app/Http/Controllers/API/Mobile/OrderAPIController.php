<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use App\Http\Requests\API\Mobile\CreateOrderAPIRequest;
use App\Http\Requests\API\Mobile\UpdateOrderAPIRequest;
use App\Models\Mobile\Client;
use App\Models\Mobile\Order;
use App\Models\Mobile\OrderProduct;
use App\Repositories\Mobile\CodeRepository;
use App\Repositories\Mobile\OrderRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class OrderController
 * @package App\Http\Controllers\API\Mobile
 */
class OrderAPIController extends InfyOmBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;
    private $codesRepository;
    private $client;

    public function __construct(OrderRepository $orderRepo, CodeRepository $codesRepo)
    {
        $this->orderRepository = $orderRepo;
        $this->codesRepository = $codesRepo;
        $this->client = new \GuzzleHttp\Client();

    }

    /**
     * Display a listing of the Order.
     * GET|HEAD /orders
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $this->orderRepository->pushCriteria(new RequestCriteria($request));
        $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $orders = $this->orderRepository->all();

        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }

    public function isActive()
    {
        $now = Carbon::now();
        $times = DB::table('shedules')->first();
        $open = Carbon::parse($times->opening_time);
        $close = Carbon::parse($times->closing_time);

        if ($open->greaterThan($close)) {
            if ($now->hour >= 0 && $now->hour < 6) {
                $open = $open->subDay();
            } else if ($now->hour < 24) {
                $close = $close->addDay();
            }
        }

        $active = $now->lessThan($close) && $now->greaterThan($open);

        return $active;
    }

    /**
     * Store a newly created Order in storage.
     * POST /orders
     *
     * @param CreateOrderAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if ($this->isActive()) {
            $firebase = new \Firebase\FirebaseLib("https://pubjjj-1363.firebaseio.com/", "T8iamaDWdy89OpK1WOrFKO6qf6vADkMvcsbxwaNC");
            $input = $request->except('products');
            $input['order_status_id'] = 1;

            $zone = null;
            $sql = '((acos(sin(radians(latitude)) * sin(radians(' . $input['latitude'] . ')) + cos(radians(latitude)) * cos(radians(' . $input['latitude'] . ')) * cos(radians(longitude) - radians(' . $input['longitude'] . '))) * 6378) ) ';
            $zone = DB::table('zones')
                ->select('price', 'name', DB::raw($sql . ' as distance'), 'radius')
                ->whereRaw('radius >= (' . $sql . ' * 1000 )')
                ->orderBy('distance', 'asc')->first();

            if (is_null($zone) || is_null($zone->name)) {
                return Response::json(ResponseUtil::makeError("Lo sentimos,esa zona no tiene cobertura"), 404);
            }


            Log::debug("Nueva orden", ["order mobile" => $input, "products" => $request->get('products')]);

            $orders = $this->orderRepository->create($input);
            $client = Client::where('id', $orders->client_id)->first();
            $values = $this->orderRepository->createOrderProducts($orders->id, $client->points, explode(",", $request->get('products')));
            $discount = 0;
            $orders->delivery_value = $zone->price;
            $orders->delivery_zone = $zone->name;

            if (isset($orders->code)) {
                $codes = $this->codesRepository->findByField('code', $orders->code)->toArray();
                if (count($codes) > 0) {
                    $discount = $values['storeValue'] * ($codes[0]['discount'] / 100);
                }
            } else {
                $orders->code = "";
            }
            $orders->amount = $values['storeValue'] + $values['eventsValue'];
            $orders->total = ($values['storeValue'] - $discount) + $values['eventsValue'];
            $orders->points = $values['pointsValue'];
            $orders->description = $values['description'];
            //modificacion requerimiento de diego
            if (config('app.freeOrder') && $orders->total > config('app.freeOrder') && $orders->delivery_value <= 8000) {
                $orders->delivery_value = 0;
                $orders->delivery_zone = "GRATIS";
            }

            $orders->total += $orders->delivery_value;
            $firebase->set('/orders/' . $orders->id, $orders);
            $response = \GoogleMaps::load('distancematrix')
                ->setParam(['origins' => '7.0685091,-73.1044817'])
                ->setParam(['destinations' => $orders->latitude . "," . $orders->longitude])
                ->setParam(['mode' => 'driving '])
                ->get('rows.elements.duration');
            $time = round(((int)($response['rows'][0]['elements'][0]['duration']['value'])) / 60);
            if ($time < 15) {
                $time = 15;
            } else {
                $time += 5;
            }
            $orders->delivery_time = $time;
            $orders->save();
            $products = OrderProduct::with('store')->where('order_id', $orders->id)->get();
            $data = [];
            foreach ($products as $product) {
                $data[] = "Producto :" . $product->store->product->name . ", Cantidad :" . $product->quantity;
            }
            Log::debug("Nuevo pedido", ["pedido" => "Un nuevo pedido por: $" . $orders->total . " , para " . $orders->address . " ha llegado, pago en  " . $orders->pay_method . "," . join(",", $data)]);

         /*   Mail::raw("Un nuevo pedido por: $" . $orders->total . "<br/>Cliente:" . $client->email . "<br/>Direccion " . $orders->address . " ha llegado, pago en  " . $orders->pay_method . "<br/>" . join("<br/>", $data), function ($message) {
                $message->subject('Tienes un nuevo domicilio');
                $message->to('domicilios@applicorera3jjjs.com');
            });*/

            return $this->sendResponse(['time' => $orders->delivery_time,
                'delivery_time' => $orders->delivery_time,
                'value' => $orders->total,
                'delivery' => $orders->delivery_value,
                'delivery_value' => $orders->delivery_value

            ], 'Orders saved successfully');

        } else {
            return Response::json(ResponseUtil::makeError("Lo sentimos,la licorera se encuentra fuera de servicio"), 404);
        }
    }

    /**
     * Display the specified Order.
     * GET|HEAD /orders/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return Response::json(ResponseUtil::makeError('Order not found'), 404);
        }

        return $this->sendResponse($order->toArray(), 'Order retrieved successfully');
    }

    /**
     * Update the specified Order in storage.
     * PUT/PATCH /orders/{id}
     *
     * @param int $id
     * @param UpdateOrderAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderAPIRequest $request)
    {
        $input = $request->all();

        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return Response::json(ResponseUtil::makeError('Order not found'), 404);
        }

        $order = $this->orderRepository->update($input, $id);


        return $this->sendResponse($order->toArray(), 'Order updated successfully');
    }

    /**
     * Remove the specified Order from storage.
     * DELETE /orders/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return Response::json(ResponseUtil::makeError('Order not found'), 404);
        }

        $order->delete();

        return $this->sendResponse($id, 'Order deleted successfully');
    }
}
