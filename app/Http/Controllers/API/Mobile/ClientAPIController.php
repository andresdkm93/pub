<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Requests\API\Mobile\CreateClientAPIRequest;
use App\Http\Requests\API\Mobile\UpdateClientAPIRequest;
use App\Models\Mobile\Client;
use App\Repositories\Mobile\ClientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Log;

/**
 * Class ClientController
 * @package App\Http\Controllers\API\Mobile
 */

class ClientAPIController extends InfyOmBaseController
{
    /** @var  ClientRepository */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepo)
    {
        $this->clientRepository = $clientRepo;
    }


    /**
     * Store a newly created Client in storage.
     * POST /clients
     *
     * @param CreateClientAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateClientAPIRequest $request)
    {
        $input = $request->all();
	    $input['password'] = Hash::make($input['password']);
        Log::debug('recibiendo data',['data' => $request->all()]);
	    $clients = $this->clientRepository->findWhere([
            'id' => $input['id']
        ]);
        Log::debug('buscando clientes por id',['data' => $clients]);
        if (count($clients) > 0) {
            return $this->sendError('Este número de indentificación ya se encuentra en uso', 200);
        }

        $clients = $this->clientRepository->findWhere([
            'email' => $input['email']
        ]);
        Log::debug('buscando clientes por email',['data' => $clients]);

        if (count($clients) > 0) {
            return $this->sendError('Este correo ya se encuentra en uso', 200);
        }

        $client = $this->clientRepository->create($input);

        return $this->sendResponse($client->toArray(), 'Clients saved successfully');
    }

    /**
     * Display the specified Client.
     * GET|HEAD /clients/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Client $client */
        $client = $this->clientRepository->find($id);

        if (empty($client)) {
            return Response::json(ResponseUtil::makeError('Client not found'), 404);
        }

        return $this->sendResponse($client->toArray(), 'Client retrieved successfully');
    }

    public function points($id)
    {
        /** @var Client $client */
        $client = $this->clientRepository->find($id,['points']);

        if (empty($client)) {
            return Response::json(ResponseUtil::makeError('Client not found'), 404);
        }

        return $this->sendResponse($client->toArray(), 'Client retrieved successfully');
    }

    /**
     * Update the specified Client in storage.
     * PUT/PATCH /clients/{id}
     *
     * @param  int $id
     * @param UpdateClientAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClientAPIRequest $request)
    {
        $input = $request->all();

        /** @var Client $client */
        $client = $this->clientRepository->find($id);

        if (empty($client)) {
            return Response::json(ResponseUtil::makeError('Client not found'), 404);
        }

        $client = $this->clientRepository->update($input, $id);

        return $this->sendResponse($client->toArray(), 'Client updated successfully');
    }

    public function login(Request $request)
    {
        $input = $request->all();
        $tclientes = $this->clientRepository->findWhere(array("email" => $input['email']));
        if(isset($tclientes[0]))
        {
            if (Hash::check($input['password'], $tclientes[0]['password'])) {
                $token = Crypt::encrypt(''.$tclientes[0]['id']);
                $tclientes[0]['token'] = $token;
                return $this->sendResponse($tclientes[0], "Tclientes retrieved successfully");
            } else {
                return $this->sendResponse(null, "Tclientes retrieved successfully");

            }
        }else{
            return $this->sendResponse(null, "Tclientes retrieved successfully");

        }
    }

    public function loginWeb(Request $request)
    {
        $input = $request->all();
        $tclientes = $this->clientRepository->findWhere(array("email" => $input['email']));
        if(isset($tclientes[0]))
        {
            if (Hash::check($input['password'], $tclientes[0]['password'])) {
                $token = Crypt::encrypt(''.$tclientes[0]['id']);
                $tclientes[0]['token'] = $token;
                $request->session()->put('user', $tclientes[0]);
                return $this->sendResponse([
                    'token'=>$token
                ], "Tclientes retrieved successfully");
            } else {
                return $this->sendResponse(null, "Tclientes retrieved successfully");

            }
        }else{
            return $this->sendResponse(null, "Tclientes retrieved successfully");

        }
    }


}
