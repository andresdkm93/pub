<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use App\Http\Requests\API\CreateEmailAPIRequest;
use App\Http\Requests\API\UpdateEmailAPIRequest;
use App\Models\Email;
use App\Repositories\EmailRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Response;
use Snowfire;

/**
 * Class EmailController
 * @package App\Http\Controllers\API
 */
class EmailAPIController extends InfyOmBaseController {


	public function suggest( Request $request ) {
		$input = $request->all();

		Mail::raw( $input['suggest'], function ( $message ) use ( $input ) {
			$message->subject( $input['title'] );
			$message->to( 'atencionalcliente@applicorera3jjjs.com' );
        } );

		return $this->sendResponse( [], 'email send successfully' );
	}

}
