<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use App\Infrastructure\Repositories\Criterias\OrderByCriteria;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Repositories\Mobile\ClientRepository;
use App\Repositories\Mobile\OrderRepository;
use App\Repositories\Mobile\StoreProductRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ClientController
 * @package App\Http\Controllers\API\Mobile
 */
class ClientOrderAPIController extends InfyOmBaseController
{
    /** @var  ClientRepository */
    private $clientRepository;

    private $orderRepository;

    private $storeProductRepository;

    public function __construct(ClientRepository $clientRepo, OrderRepository $orderRepository, StoreProductRepository $storeProductsRepository)
    {
        $this->clientRepository = $clientRepo;

        $this->orderRepository = $orderRepository;

        $this->storeProductRepository = $storeProductsRepository;
    }

    public function index($clientId, Request $request)
    {

        $this->orderRepository->pushCriteria(new RequestCriteria($request));
        $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->orderRepository->pushCriteria(new WhereFieldCriteria('client_id', $clientId));
        $this->orderRepository->pushCriteria(new WithRelationshipsCriteria(['status']));
        $this->orderRepository->pushCriteria(new OrderByCriteria('created_at', 'desc'));
        $orders = $this->orderRepository->paginate(10);
        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }

    public function show($clientId, $orderId, Request $request)
    {
        $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->orderRepository->pushCriteria(new WhereFieldCriteria('client_id', $clientId));
        $this->orderRepository->pushCriteria(new WithRelationshipsCriteria(['products', 'status']));
        $order = $this->orderRepository->findWithoutFail($orderId);
        if (empty($order)) {
            return $this->sendResponse(null, 'Order retrieved successfully');
        }
        return $this->sendResponse($order->toArray(), 'Orders retrieved successfully');
    }

    public function validOrder($clientId, $orderId){

        $this->orderRepository->pushCriteria(new WhereFieldCriteria('client_id', $clientId));
        $this->orderRepository->pushCriteria(new WithRelationshipsCriteria(['products', 'status']));
        $this->orderRepository->pushCriteria(new OrderByCriteria('created_at', 'desc'));
        $this->storeProductRepository->pushCriteria(new WithRelationshipsCriteria(['product']));
        $order = $this->orderRepository->findWithoutFail($orderId);
        if (empty($order)) {
            return $this->sendResponse(null, 'Order retrieved successfully');
        }
        $products = [];
        foreach ($order->products as $product){
                $storeProduct = $this->storeProductRepository->findWithoutFail($product->store->id);
                if(!empty($storeProduct)){
                    $storeProduct->quantity = $product->quantity;
                    $products[] = $storeProduct;
                }
        }
        unset($order->products);

        $order->products_available = $products;

        return $this->sendResponse($order->toArray(), 'Orders retrieved successfully');
    }

    public function current($clientId)
    {

        $this->orderRepository->pushCriteria(new WhereFieldCriteria('client_id', $clientId));
        $this->orderRepository->pushCriteria(new WhereFieldCriteria('order_status_id', [1, 2], 'in'));
        $this->orderRepository->pushCriteria(new WithRelationshipsCriteria(['products', 'status']));
        $this->orderRepository->pushCriteria(new OrderByCriteria('created_at', 'desc'));
        $this->orderRepository->pushCriteria(new \App\Infrastructure\Repositories\Criterias\LimitOffsetCriteria('1'));
        $order = $this->orderRepository->all()->first();
        if(!empty($order)){
	        $now = Carbon::now();
	        $orderDate = Carbon::parse($order->created_at)->addMinute(5);
	        $order->cancelable = $now->lte($orderDate);
        }

        return $this->sendResponse($order, 'Order retrieved successfully');
    }

	public function hasCurrent($clientId)
	{

		$this->orderRepository->pushCriteria(new WhereFieldCriteria('client_id', $clientId));
		$this->orderRepository->pushCriteria(new WhereFieldCriteria('order_status_id', [1, 2], 'in'));
		$this->orderRepository->pushCriteria(new WithRelationshipsCriteria(['products', 'status']));
		$this->orderRepository->pushCriteria(new OrderByCriteria('created_at', 'desc'));
		$this->orderRepository->pushCriteria(new \App\Infrastructure\Repositories\Criterias\LimitOffsetCriteria('1'));
		$order = $this->orderRepository->all()->first();

		return $this->sendResponse($order != null, 'Order retrieved successfully');
	}


    public function updateCurrent($clientId)
    {

        $this->orderRepository->pushCriteria(new WhereFieldCriteria('client_id', $clientId));
        $this->orderRepository->pushCriteria(new WhereFieldCriteria('order_status_id', [1, 2], 'in'));
        $this->orderRepository->pushCriteria(new WithRelationshipsCriteria(['status']));
        $this->orderRepository->pushCriteria(new OrderByCriteria('created_at', 'desc'));
        $this->orderRepository->pushCriteria(new \App\Infrastructure\Repositories\Criterias\LimitOffsetCriteria('1'));
        $order = $this->orderRepository->all()->first();
        if (empty($order)) {
            return $this->sendResponse(null, 'Order retrieved successfully');
        }
        $order->order_status_id = 5;

        $order->save();

        $firebase = new \Firebase\FirebaseLib("https://pubjjj-1363.firebaseio.com/", "T8iamaDWdy89OpK1WOrFKO6qf6vADkMvcsbxwaNC");

        $firebase->update('/orders/' . $order->id, $order);

        return $this->sendResponse($order, 'Order retrieved successfully');

    }


}
