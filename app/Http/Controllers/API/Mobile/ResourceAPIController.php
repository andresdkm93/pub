<?php
namespace App\Http\Controllers\API\Mobile;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ResourceAPIController extends AppBaseController
{

    public function updateResource(Request $request)
    {
        $file = $request->file('file');
        $nombre = $file->getClientOriginalName();
        $now = new \DateTime();
        Storage::disk('local')->put($now->format("Y_m_d_H_i_s") . "_" . $nombre, File::get($file));
        $url = "https://images.applicorera3jjjs.com/ImageHandler.php?src=../storage/".$now->format("Y_m_d_H_i_s") . "_" . $nombre;
        return $this->sendResponse(["url" => $url], "resource store");
    }

}