<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Requests\API\Mobile\CreateCategoryAPIRequest;
use App\Http\Requests\API\Mobile\UpdateCategoryAPIRequest;
use App\Models\Mobile\Category;
use App\Repositories\Mobile\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CategoryController
 * @package App\Http\Controllers\API\Mobile
 */

class CategoryAPIController extends InfyOmBaseController
{
    /** @var  CategoryRepository */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->categoryRepository = $categoryRepo;
    }

    /**
     * Display a listing of the Category.
     * GET|HEAD /categories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->categoryRepository->pushCriteria(new RequestCriteria($request));
        $this->categoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $categories = $this->categoryRepository->all();

        return $this->sendResponse($categories->toArray(), 'Categories retrieved successfully');
    }



    /**
     * Display the specified Category.
     * GET|HEAD /categories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Category $category */
        $categories = $this->categoryRepository->getStoreProducts($id);
        if(count($categories)>0)
        {
            $categories=$categories[0];
        }

        if (empty($categories)) {
            return Response::json(ResponseUtil::makeError('Categories not found'), 400);
        }
        return $this->sendResponse($categories->toArray(), 'Categories retrieved successfully');

    }

    /**
     * Update the specified Category in storage.
     * PUT/PATCH /categories/{id}
     *
     * @param  int $id
     * @param UpdateCategoryAPIRequest $request
     *
     * @return Response
     */

}
