<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use App\Http\Requests\API\UpdateSheduleAPIRequest;
use App\Repositories\SheduleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Response;

/**
 * Class SheduleController
 * @package App\Http\Controllers\API
 */
class PrinterAPIController extends InfyOmBaseController {
	/** @var  SheduleRepository */

	public function __construct() {
	}

	/**
	 * @param Request $request
	 *
	 * @return Response
	 *
	 * @SWG\Get(
	 *      path="/shedules",
	 *      summary="Get a listing of the Shedules.",
	 *      tags={"Shedule"},
	 *      description="Get all Shedules",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="array",
	 *                  @SWG\Items(ref="#/definitions/Shedule")
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function index( Request $request ) {
		$value = Cache::get( 'printer', '10' );

		return $this->sendResponse( [[
			'id'    => 1,
			'value' => $value . ' mm'
		]], 'Printer retrieved successfully' );
	}


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/shedules/{id}",
     *      summary="Display the specified Shedule",
     *      tags={"Shedule"},
     *      description="Get Shedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Shedule",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Shedule"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
	    $value = Cache::get( 'printer', '10' );

	    return $this->sendResponse( [
		    'id'    => 1,
		    'value' => doubleval($value)
	    ], 'Printer retrieved successfully' );
    }

    /**
     * @param int $id
     * @param UpdateSheduleAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/shedules/{id}",
     *      summary="Update the specified Shedule in storage",
     *      tags={"Shedule"},
     *      description="Update Shedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Shedule",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Shedule that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Shedule")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Shedule"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSheduleAPIRequest $request)
    {
        $input = $request->all();

		$value = $input['value'];


		Cache::forever( 'printer', $value );

		return $this->sendResponse( [[
			'id'    => 1,
			'value' => $value . ' mm'
		]], 'Printer retrieved successfully' );
	}
}
