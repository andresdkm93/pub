<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStoreProductsAPIRequest;
use App\Http\Requests\API\UpdateStoreProductsAPIRequest;
use App\Models\StoreProducts;
use App\Repositories\ProductsRepository;
use App\Repositories\StoreProductsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;

use Response;

/**
 * Class StoreProductsController
 * @package App\Http\Controllers\API
 */

class PromotionProductsAPIController extends AppBaseController
{
    /** @var  StoreProductsRepository */
    private $storeProductsRepository;
    private $productsRepository;


    public function __construct(StoreProductsRepository $storeProductsRepo,ProductsRepository $productsRepo)
    {
        $this->storeProductsRepository = $storeProductsRepo;
        $this->productsRepository = $productsRepo;

    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/storeProducts",
     *      summary="Get a listing of the StoreProducts.",
     *      tags={"StoreProducts"},
     *      description="Get all StoreProducts",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/StoreProducts")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->storeProductsRepository->pushCriteria(new RequestCriteria($request));
        $this->storeProductsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->storeProductsRepository->pushCriteria(new WhereFieldCriteria( 'store_type',2, '='));

        $this->storeProductsRepository->pushCriteria(new WithRelationshipsCriteria(
            ['product']
        ));
        $storeProducts = $this->storeProductsRepository->paginate(10,['id','quantity','points','status','start_date','end_date','product_id']);

        return $this->sendResponse($storeProducts->toArray(), 'StoreProducts retrieved successfully');
    }

    /**
     * @param CreateStoreProductsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/storeProducts",
     *      summary="Store a newly created StoreProducts in storage",
     *      tags={"StoreProducts"},
     *      description="Store StoreProducts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="StoreProducts that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/StoreProducts")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StoreProducts"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateStoreProductsAPIRequest $request)
    {
        $input = $request->except('product');
        $product=$request->get('product');
        $product['image']=$input['image'];
        $product = $this->productsRepository->create($product);
        if( !is_null($product) )
        {
            $input['store_type']=2;
            $input['product_id']=$product->id;
            $storeProducts = $this->storeProductsRepository->create($input);
            return $this->sendResponse($storeProducts->toArray(), 'StoreProducts saved successfully');

        }else{
            return Response::json(ResponseUtil::makeError('Error '), 400);
        }
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/storeProducts/{id}",
     *      summary="Display the specified StoreProducts",
     *      tags={"StoreProducts"},
     *      description="Get StoreProducts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StoreProducts",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StoreProducts"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var StoreProducts $storeProducts */
        $this->storeProductsRepository->pushCriteria(new WithRelationshipsCriteria(
            ['product',]
        ));
        $storeProducts = $this->storeProductsRepository->find($id);

        if (empty($storeProducts)) {
            return Response::json(ResponseUtil::makeError('StoreProducts not found'), 400);
        }

        return $this->sendResponse($storeProducts->toArray(), 'StoreProducts retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateStoreProductsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/storeProducts/{id}",
     *      summary="Update the specified StoreProducts in storage",
     *      tags={"StoreProducts"},
     *      description="Update StoreProducts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StoreProducts",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="StoreProducts that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/StoreProducts")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StoreProducts"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateStoreProductsAPIRequest $request)
    {
        $input = $request->except('product');
        $product=$request->get('product');
        if(isset($input['image']))
            $product['image']=$input['image'];
        $this->productsRepository->update($product,$product['id']);

        /** @var StoreProducts $storeProducts */
        $storeProducts = $this->storeProductsRepository->find($id);

        if (empty($storeProducts)) {
            return Response::json(ResponseUtil::makeError('StoreProducts not found'), 400);
        }

        $storeProducts = $this->storeProductsRepository->update($input, $id);

        return $this->sendResponse($storeProducts->toArray(), 'StoreProducts updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/storeProducts/{id}",
     *      summary="Remove the specified StoreProducts from storage",
     *      tags={"StoreProducts"},
     *      description="Delete StoreProducts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StoreProducts",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var StoreProducts $storeProducts */
        $storeProducts = $this->storeProductsRepository->find($id);

        if (empty($storeProducts)) {
            return Response::json(ResponseUtil::makeError('StoreProducts not found'), 400);
        }

        $storeProducts->delete();

        return $this->sendResponse($id, 'StoreProducts deleted successfully');
    }
}
