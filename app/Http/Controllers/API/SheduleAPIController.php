<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSheduleAPIRequest;
use App\Http\Requests\API\UpdateSheduleAPIRequest;
use App\Models\Shedule;
use App\Repositories\SheduleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SheduleController
 * @package App\Http\Controllers\API
 */

class SheduleAPIController extends InfyOmBaseController
{
    /** @var  SheduleRepository */
    private $sheduleRepository;

    public function __construct(SheduleRepository $sheduleRepo)
    {
        $this->sheduleRepository = $sheduleRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/shedules",
     *      summary="Get a listing of the Shedules.",
     *      tags={"Shedule"},
     *      description="Get all Shedules",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Shedule")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->sheduleRepository->pushCriteria(new RequestCriteria($request));
        $this->sheduleRepository->pushCriteria(new LimitOffsetCriteria($request));
        $shedules = $this->sheduleRepository->all();

        return $this->sendResponse($shedules->toArray(), 'Shedules retrieved successfully');
    }

    public function isActive()
    {
        return $this->sendResponse(['active'=>true], 'Shedules retrieved successfully');

    }

    /**
     * @param CreateSheduleAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/shedules",
     *      summary="Store a newly created Shedule in storage",
     *      tags={"Shedule"},
     *      description="Store Shedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Shedule that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Shedule")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Shedule"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSheduleAPIRequest $request)
    {
        $input = $request->all();

        $shedules = $this->sheduleRepository->create($input);

        return $this->sendResponse($shedules->toArray(), 'Shedule saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/shedules/{id}",
     *      summary="Display the specified Shedule",
     *      tags={"Shedule"},
     *      description="Get Shedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Shedule",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Shedule"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Shedule $shedule */
        $shedule = $this->sheduleRepository->find($id);

        if (empty($shedule)) {
            return Response::json(ResponseUtil::makeError('Shedule not found'), 404);
        }

        return $this->sendResponse($shedule->toArray(), 'Shedule retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSheduleAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/shedules/{id}",
     *      summary="Update the specified Shedule in storage",
     *      tags={"Shedule"},
     *      description="Update Shedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Shedule",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Shedule that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Shedule")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Shedule"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSheduleAPIRequest $request)
    {
        $input = $request->all();

        /** @var Shedule $shedule */
        $shedule = $this->sheduleRepository->find($id);

        if (empty($shedule)) {
            return Response::json(ResponseUtil::makeError('Shedule not found'), 404);
        }

        $shedule = $this->sheduleRepository->update($input, $id);

        return $this->sendResponse($shedule->toArray(), 'Shedule updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/shedules/{id}",
     *      summary="Remove the specified Shedule from storage",
     *      tags={"Shedule"},
     *      description="Delete Shedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Shedule",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Shedule $shedule */
        $shedule = $this->sheduleRepository->find($id);

        if (empty($shedule)) {
            return Response::json(ResponseUtil::makeError('Shedule not found'), 404);
        }

        $shedule->delete();

        return $this->sendResponse($id, 'Shedule deleted successfully');
    }
}
