<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCodesAPIRequest;
use App\Http\Requests\API\UpdateCodesAPIRequest;
use App\Models\Codes;
use App\Repositories\CodesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CodesController
 * @package App\Http\Controllers\API
 */

class CodesAPIController extends AppBaseController
{
    /** @var  CodesRepository */
    private $codesRepository;

    public function __construct(CodesRepository $codesRepo)
    {
        $this->codesRepository = $codesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/codes",
     *      summary="Get a listing of the Codes.",
     *      tags={"Codes"},
     *      description="Get all Codes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Codes")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->codesRepository->pushCriteria(new RequestCriteria($request));
        $this->codesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $codes = $this->codesRepository->all();

        return $this->sendResponse($codes->toArray(), 'Codes retrieved successfully');
    }

    /**
     * @param CreateCodesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/codes",
     *      summary="Store a newly created Codes in storage",
     *      tags={"Codes"},
     *      description="Store Codes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Codes that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Codes")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Codes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCodesAPIRequest $request)
    {
        $input = $request->all();

        $codes = $this->codesRepository->create($input);

        return $this->sendResponse($codes->toArray(), 'Codes saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/codes/{id}",
     *      summary="Display the specified Codes",
     *      tags={"Codes"},
     *      description="Get Codes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Codes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Codes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Codes $codes */
        $codes = $this->codesRepository->find($id);

        if (empty($codes)) {
            return Response::json(ResponseUtil::makeError('Codes not found'), 400);
        }

        return $this->sendResponse($codes->toArray(), 'Codes retrieved successfully');
    }


   
    /**
     * @param int $id
     * @param UpdateCodesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/codes/{id}",
     *      summary="Update the specified Codes in storage",
     *      tags={"Codes"},
     *      description="Update Codes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Codes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Codes that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Codes")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Codes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCodesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Codes $codes */
        $codes = $this->codesRepository->find($id);

        if (empty($codes)) {
            return Response::json(ResponseUtil::makeError('Codes not found'), 400);
        }

        $codes = $this->codesRepository->update($input, $id);

        return $this->sendResponse($codes->toArray(), 'Codes updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/codes/{id}",
     *      summary="Remove the specified Codes from storage",
     *      tags={"Codes"},
     *      description="Delete Codes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Codes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Codes $codes */
        $codes = $this->codesRepository->find($id);

        if (empty($codes)) {
            return Response::json(ResponseUtil::makeError('Codes not found'), 400);
        }

        $codes->delete();

        return $this->sendResponse($id, 'Codes deleted successfully');
    }
}
