<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateZonesAPIRequest;
use App\Http\Requests\API\UpdateZonesAPIRequest;
use App\Models\Zones;
use App\Repositories\ZonesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ZonesController
 * @package App\Http\Controllers\API
 */

class ZonesAPIController extends InfyOmBaseController
{
    /** @var  ZonesRepository */
    private $zonesRepository;

    public function __construct(ZonesRepository $zonesRepo)
    {
        $this->zonesRepository = $zonesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/zones",
     *      summary="Get a listing of the Zones.",
     *      tags={"Zones"},
     *      description="Get all Zones",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Zones")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->zonesRepository->pushCriteria(new RequestCriteria($request));
        $this->zonesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $zones = $this->zonesRepository->all();

        return $this->sendResponse($zones->toArray(), 'Zones retrieved successfully');
    }

    /**
     * @param CreateZonesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/zones",
     *      summary="Store a newly created Zones in storage",
     *      tags={"Zones"},
     *      description="Store Zones",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Zones that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Zones")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Zones"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateZonesAPIRequest $request)
    {
        $input = $request->all();

        $zones = $this->zonesRepository->create($input);

        return $this->sendResponse($zones->toArray(), 'Zones saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/zones/{id}",
     *      summary="Display the specified Zones",
     *      tags={"Zones"},
     *      description="Get Zones",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Zones",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Zones"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Zones $zones */
        $zones = $this->zonesRepository->find($id);

        if (empty($zones)) {
            return Response::json(ResponseUtil::makeError('Zones not found'), 404);
        }

        return $this->sendResponse($zones->toArray(), 'Zones retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateZonesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/zones/{id}",
     *      summary="Update the specified Zones in storage",
     *      tags={"Zones"},
     *      description="Update Zones",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Zones",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Zones that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Zones")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Zones"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateZonesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Zones $zones */
        $zones = $this->zonesRepository->find($id);

        if (empty($zones)) {
            return Response::json(ResponseUtil::makeError('Zones not found'), 404);
        }

        $zones = $this->zonesRepository->update($input, $id);

        return $this->sendResponse($zones->toArray(), 'Zones updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/zones/{id}",
     *      summary="Remove the specified Zones from storage",
     *      tags={"Zones"},
     *      description="Delete Zones",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Zones",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Zones $zones */
        $zones = $this->zonesRepository->find($id);

        if (empty($zones)) {
            return Response::json(ResponseUtil::makeError('Zones not found'), 404);
        }

        $zones->delete();

        return $this->sendResponse($id, 'Zones deleted successfully');
    }
}
