<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Orders;
use App\Repositories\CodesRepository;
use App\Repositories\OrderProductsRepository;
use App\Repositories\OrdersRepository;
use App\Repositories\StoreProductsRepository;
use App\Util\Printer;
use Firebase\Firebase;
use Illuminate\Support\Facades\Cache;
use InfyOm\Generator\Utils\ResponseUtil;
use Response;

/**
 * Class OrdersController
 * @package App\Http\Controllers\API
 */
class OrderPrinterAPIController extends AppBaseController {
	/** @var  OrdersRepository */
	private $ordersRepository;
	private $orderProductsRepository;
	private $storeProductsRepository;
	private $codesRepository;


	public function __construct( OrdersRepository $ordersRepo, OrderProductsRepository $orderProductsRepo, StoreProductsRepository $storeProductsRepo, CodesRepository $codesRepo ) {
		$this->ordersRepository        = $ordersRepo;
		$this->orderProductsRepository = $orderProductsRepo;
		$this->storeProductsRepository = $storeProductsRepo;
		$this->codesRepository         = $codesRepo;

	}


	/**
	 * @param int $id
	 *
	 * @return Response
	 *
	 * @SWG\Get(
	 *      path="/orders/{id}",
	 *      summary="Display the specified Orders",
	 *      tags={"Orders"},
	 *      description="Get Orders",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="id",
	 *          description="id of Orders",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="data",
	 *                  ref="#/definitions/Orders"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
	public function show( $id ) {
		/** @var Orders $orders */
		$this->ordersRepository->pushCriteria( new WithRelationshipsCriteria( [ 'products', 'client' ] ) );

		$order = $this->ordersRepository->find( $id );

		if ( empty( $order ) ) {
			return Response::json( ResponseUtil::makeError( 'Orders not found' ), 400 );
		}

		$value = Cache::get( 'printer', '10' );
		$data  = ( new Printer( doubleval( $value ) ,'/img/logo.png') )
			->textCenter( 'LICORERA TRES JOTAS' )
			->blank( 2 )
			->textCenter( 'LUZ MARINA CARVAJAL OSORIO' )
			->textCenter( 'NIT: 37.514.026-2' )
			->textCenter( 'CR 26 # 33-17 CAÑAVERAL' )
			->textCenter( 'TEL: 6392474-6799493' )
			->blank( 2 )
			->textCenter( 'Orden Número ' . $id )
			->textCenter( 'Fecha ' . $order->created_at )
			->blank( 2 )
			->separator( '-' )
			->threeLine( 'Producto', 'Cant.','Valor' )
			->separator( '-' );
		$qty   = 0;
		$value = 0;
		foreach ( $order->products as $product ) {
			$qty   += $product->quantity;
			$value += $product->subtotal;
			$data->threeLine( $product->store->product->name, $product->quantity, sprintf( '$ %s', number_format( $product->store->price, 0 ) ))
			     ->separator( '-' );
		}
		$data->blank( 1 );
		$data->line( 'Usted compró', $qty . ' articulos' );
		$data->line( 'Subtotal', sprintf( '$ %s', number_format( $value, 0 ) ) );
		$data->line( 'Valor domicilio', sprintf( '$ %s', number_format( $order->delivery_value, 0 ) ) );
		$data->line( 'Total a pagar', sprintf( '$ %s', number_format( $order->total, 0 ) ) );
		$data->blank( 3 );
		$data->textCenter( 'Servicio a domicilio' );
		$data->blank( 1 );
		$data->line( 'Nombre', $order->client->name . ' ' . $order->client->last_name )
		     ->line( 'Teléfono', $order->phone )
		     ->left( 'Correo: '.$order->client->email )
		     ->left( 'Dirección' )
		     ->text( $order->address )
		     ->left( 'Instrucciones' )
		     ->text( $order->instructions )
		     ->blank( 4 );
		$data->textCenter( 'Gracias por su compra' );

		return $this->sendPrinter( $data, 'Orders retrieved successfully' );
	}


}
