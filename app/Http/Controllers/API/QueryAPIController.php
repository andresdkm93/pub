<?php
/**
 * Created by PhpStorm.
 * User: AndresFabian
 * Date: 3/07/2016
 * Time: 12:17 AM
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Extensions\Collection\CollectionRedis;
use App\Extensions\Factory\FactoryRepository;

use Illuminate\Http\Request;


class QueryAPIController extends AppBaseController
{


    public function search($name,Request $request)
    {
        $factory = new FactoryRepository;
        $repo = $factory->build('App\Repositories\\'.$name.'Repository');
        $input = $request->only('key','value');
        $collection = new CollectionRedis($repo->search());
        $filter=$collection->whereLike($input['key'],$input['value']);
        $data=$filter->take(10);
        $result=array();
        foreach ($data as $clave => $valor) {
            $result[]=$valor;
        }
        return response()->json(["data"=>$result],200);
    }

}