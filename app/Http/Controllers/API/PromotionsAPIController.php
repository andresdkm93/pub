<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePromotionsAPIRequest;
use App\Http\Requests\API\UpdatePromotionsAPIRequest;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Promotions;
use App\Repositories\PromotionsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PromotionsController
 * @package App\Http\Controllers\API
 */

class PromotionsAPIController extends InfyOmBaseController
{
    /** @var  PromotionsRepository */
    private $promotionsRepository;

    public function __construct(PromotionsRepository $promotionsRepo)
    {
        $this->promotionsRepository = $promotionsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/promotions",
     *      summary="Get a listing of the Promotions.",
     *      tags={"Promotions"},
     *      description="Get all Promotions",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Promotions")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->promotionsRepository->pushCriteria(new RequestCriteria($request));
        $this->promotionsRepository->pushCriteria(new LimitOffsetCriteria($request));
	    $this->promotionsRepository->pushCriteria(new WithRelationshipsCriteria(
		    ['store_product',]
	    ));
	    $promotions = $this->promotionsRepository->all();
	    return $this->sendResponse($promotions->toArray(), 'Promotions retrieved successfully');
    }

    /**
     * @param CreatePromotionsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/promotions",
     *      summary="Store a newly created Promotions in storage",
     *      tags={"Promotions"},
     *      description="Store Promotions",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Promotions that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Promotions")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Promotions"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePromotionsAPIRequest $request)
    {
        $input = $request->all();
        if($input['promotion_type']==1)
            $promotions = $this->promotionsRepository->createWithProduct($input);
        else
            $promotions = $this->promotionsRepository->create($input);


        return $this->sendResponse($promotions->toArray(), 'Promotions saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/promotions/{id}",
     *      summary="Display the specified Promotions",
     *      tags={"Promotions"},
     *      description="Get Promotions",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Promotions",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Promotions"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Promotions $promotions */
        $promotions = $this->promotionsRepository->find($id);

        if (empty($promotions)) {
            return Response::json(ResponseUtil::makeError('Promotions not found'), 404);
        }

        return $this->sendResponse($promotions->toArray(), 'Promotions retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePromotionsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/promotions/{id}",
     *      summary="Update the specified Promotions in storage",
     *      tags={"Promotions"},
     *      description="Update Promotions",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Promotions",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Promotions that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Promotions")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Promotions"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePromotionsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Promotions $promotions */
        $promotions = $this->promotionsRepository->find($id);

        if (empty($promotions)) {
            return Response::json(ResponseUtil::makeError('Promotions not found'), 404);
        }

        $promotions = $this->promotionsRepository->update($input, $id);

        return $this->sendResponse($promotions->toArray(), 'Promotions updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/promotions/{id}",
     *      summary="Remove the specified Promotions from storage",
     *      tags={"Promotions"},
     *      description="Delete Promotions",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Promotions",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Promotions $promotions */
        $promotions = $this->promotionsRepository->find($id);

        if (empty($promotions)) {
            return Response::json(ResponseUtil::makeError('Promotions not found'), 404);
        }

        $promotions->delete();

        return $this->sendResponse($id, 'Promotions deleted successfully');
    }
}
