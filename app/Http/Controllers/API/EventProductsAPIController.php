<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEventProductsAPIRequest;
use App\Http\Requests\API\UpdateEventProductsAPIRequest;
use App\Models\EventProducts;
use App\Repositories\EventProductsRepository;
use App\Repositories\StoreProductsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Repositories\ProductsRepository;

use Response;

/**
 * Class EventProductsController
 * @package App\Http\Controllers\API
 */

class EventProductsAPIController extends AppBaseController
{
    /** @var  EventProductsRepository */
    private $eventProductsRepository;
    private $storeProductsRepository;
    private $productsRepository;


    public function __construct(EventProductsRepository $eventProductsRepo,StoreProductsRepository $storeProductsRepo,ProductsRepository $productsRepo)
    {
        $this->eventProductsRepository = $eventProductsRepo;
        $this->storeProductsRepository = $storeProductsRepo;
        $this->productsRepository = $productsRepo;

    }




    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/eventProducts",
     *      summary="Get a listing of the EventProducts.",
     *      tags={"EventProducts"},
     *      description="Get all EventProducts",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/EventProducts")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index($eventId,Request $request)
    {
        $this->eventProductsRepository->pushCriteria(new RequestCriteria($request));
        $this->eventProductsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->eventProductsRepository->pushCriteria(new WithRelationshipsCriteria(
            ['store_product']
        ));
        $eventProducts = $this->eventProductsRepository->findWhere(["events_id"=>$eventId]);

        return $this->sendResponse($eventProducts->toArray(), 'EventProducts retrieved successfully');
    }

    /**
     * @param CreateEventProductsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/eventProducts",
     *      summary="Store a newly created EventProducts in storage",
     *      tags={"EventProducts"},
     *      description="Store EventProducts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="EventProducts that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/EventProducts")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EventProducts"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store($eventId,CreateEventProductsAPIRequest $request)
    {
        $input = $request->get('store_product')['product'];
        $input['image']=$request->get('image');
        $product = $this->productsRepository->create($input);
        if( !is_null($product) )
        {
            $input = $request->get('store_product');
            $input['store_type']=4;
            $input['product_id']=$product->id;
            $storeProducts = $this->storeProductsRepository->create($input);
            $eventProducts = $this->eventProductsRepository->create(['events_id'=>$eventId,'store_product_id'=>$storeProducts->id]);


            return $this->sendResponse($storeProducts->toArray(), 'StoreProducts saved successfully');

        }else{
            return Response::json(ResponseUtil::makeError('Error '), 400);
        }

        return $this->sendResponse($eventProducts->toArray(), 'EventProducts saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/eventProducts/{id}",
     *      summary="Display the specified EventProducts",
     *      tags={"EventProducts"},
     *      description="Get EventProducts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EventProducts",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EventProducts"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($eventId,$id)
    {
        /** @var EventProducts $eventProducts */
        $this->eventProductsRepository->pushCriteria(new WithRelationshipsCriteria(
            ['store_product']
        ));
        $eventProducts = $this->eventProductsRepository->find($id);

        if (empty($eventProducts)) {
            return Response::json(ResponseUtil::makeError('EventProducts not found'), 400);
        }

        return $this->sendResponse($eventProducts->toArray(), 'EventProducts retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEventProductsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/eventProducts/{id}",
     *      summary="Update the specified EventProducts in storage",
     *      tags={"EventProducts"},
     *      description="Update EventProducts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EventProducts",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="EventProducts that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/EventProducts")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EventProducts"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEventProductsAPIRequest $request)
    {
        $input = $request->all();
        $product=$input['store_product']['product'];
        if(isset($input['image']))
            $product['image']=$input['image'];
        $this->productsRepository->update($product,$product['id']);

        /** @var StoreProducts $storeProducts */
        $storeProducts = $this->storeProductsRepository->find($input['store_product']['id']);

        if (empty($storeProducts)) {
            return Response::json(ResponseUtil::makeError('StoreProducts not found'), 400);
        }

        $storeProducts = $this->storeProductsRepository->update($input['store_product'],$input['store_product']['id']);

        return $this->sendResponse($storeProducts->toArray(), 'StoreProducts updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/eventProducts/{id}",
     *      summary="Remove the specified EventProducts from storage",
     *      tags={"EventProducts"},
     *      description="Delete EventProducts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EventProducts",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var EventProducts $eventProducts */
        $eventProducts = $this->eventProductsRepository->find($id);

        if (empty($eventProducts)) {
            return Response::json(ResponseUtil::makeError('EventProducts not found'), 400);
        }

        $eventProducts->delete();

        return $this->sendResponse($id, 'EventProducts deleted successfully');
    }
}
