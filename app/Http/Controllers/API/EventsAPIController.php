<?php

namespace App\Http\Controllers\API;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;

use App\Http\Requests\API\CreateEventsAPIRequest;
use App\Http\Requests\API\UpdateEventsAPIRequest;
use App\Models\Events;
use App\Repositories\EventsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EventsController
 * @package App\Http\Controllers\API
 */

class EventsAPIController extends AppBaseController
{
    /** @var  EventsRepository */
    private $eventsRepository;

    public function __construct(EventsRepository $eventsRepo)
    {
        $this->eventsRepository = $eventsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/events",
     *      summary="Get a listing of the Events.",
     *      tags={"Events"},
     *      description="Get all Events",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Events")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->eventsRepository->pushCriteria(new RequestCriteria($request));
        $this->eventsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $events = $this->eventsRepository->all();

        return $this->sendResponse($events->toArray(), 'Events retrieved successfully');
    }

    /**
     * @param CreateEventsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/events",
     *      summary="Store a newly created Events in storage",
     *      tags={"Events"},
     *      description="Store Events",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Events that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Events")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Events"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEventsAPIRequest $request)
    {
        $input = $request->all();

        $events = $this->eventsRepository->create($input);

        return $this->sendResponse($events->toArray(), 'Events saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/events/{id}",
     *      summary="Display the specified Events",
     *      tags={"Events"},
     *      description="Get Events",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Events",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Events"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Events $events */
        $this->eventsRepository->pushCriteria(new WithRelationshipsCriteria(
            ['items']
        ));
        $events = $this->eventsRepository->find($id);


        if (empty($events)) {
            return Response::json(ResponseUtil::makeError('Events not found'), 400);
        }

        return $this->sendResponse($events->toArray(), 'Events retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEventsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/events/{id}",
     *      summary="Update the specified Events in storage",
     *      tags={"Events"},
     *      description="Update Events",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Events",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Events that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Events")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Events"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEventsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Events $events */
        $events = $this->eventsRepository->find($id);

        if (empty($events)) {
            return Response::json(ResponseUtil::makeError('Events not found'), 400);
        }

        $events = $this->eventsRepository->update($input, $id);

        return $this->sendResponse($events->toArray(), 'Events updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/events/{id}",
     *      summary="Remove the specified Events from storage",
     *      tags={"Events"},
     *      description="Delete Events",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Events",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Events $events */
        $events = $this->eventsRepository->find($id);

        if (empty($events)) {
            return Response::json(ResponseUtil::makeError('Events not found'), 400);
        }

        $events->delete();

        return $this->sendResponse($id, 'Events deleted successfully');
    }
}
