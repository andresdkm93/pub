<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateClientsAPIRequest;
use App\Http\Requests\API\UpdateClientsAPIRequest;
use App\Models\Clients;
use App\Push\Push;
use App\Repositories\ClientsRepository;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ClientsController
 * @package App\Http\Controllers\API
 */
class ClientsAPIController extends AppBaseController
{
    /** @var  ClientsRepository */
    private $clientsRepository;

    public function __construct(ClientsRepository $clientsRepo)
    {
        $this->clientsRepository = $clientsRepo;
    }

    public function sendPush(Request $input)
    {
        $push = array(
            "title" => $input['title'],
            "body" => $input['message'],
            "case" => 1,
            "value" => ""
        );
        Push::sendPush($input['id'], $push);
    }

    public function sendMasivePush(Request $request)
    {
        $input = $request->all();
        if ($request->has('productId')) {
            $push = array(
                "title" => $input['title'],
                "body" => $input['message'],
                "case" => 7,
                "value" => $input['productId']
            );
            Push::sendMasivePush($push);
        } else {
            $push = array(
                "title" => $input['title'],
                "body" => $input['message'],
                "case" => 1,
                "value" => ""
            );
            Push::sendMasivePush($push);
        }

    }


    /**
     * @param Request $request
     *
     * @return Response
     *
     * @SWG\Get(
     *      path="/clients",
     *      summary="Get a listing of the Clients.",
     *      tags={"Clients"},
     *      description="Get all Clients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Clients")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->clientsRepository->pushCriteria(new RequestCriteria($request));
        $this->clientsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $clients = $this->clientsRepository->paginate(10);

        return $this->sendResponse($clients->toArray(), 'Clients retrieved successfully');
    }

    /**
     * @param CreateClientsAPIRequest $request
     *
     * @return Response
     *
     * @SWG\Post(
     *      path="/clients",
     *      summary="Store a newly created Clients in storage",
     *      tags={"Clients"},
     *      description="Store Clients",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Clients that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Clients")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Clients"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */


    public function store(CreateClientsAPIRequest $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $clients = $this->clientsRepository->findWhere([
            'id' => $input['id']
        ]);
        if (count($clients) > 0) {
            return $this->sendError('Este número de indentificación ya se encuentra en uso', 200);
        }

        $clients = $this->clientsRepository->findWhere([
            'email' => $input['email']
        ]);
        if (count($clients) > 0) {
            return $this->sendError('Este correo ya se encuentra en uso', 200);
        }

        $client = $this->clientsRepository->create($input);

        return $this->sendResponse($client->toArray(), 'Clients saved successfully');
    }

    /**
     * @param int $id
     *
     * @return Response
     *
     * @SWG\Get(
     *      path="/clients/{id}",
     *      summary="Display the specified Clients",
     *      tags={"Clients"},
     *      description="Get Clients",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Clients",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Clients"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Clients $clients */
        $clients = $this->clientsRepository->find($id);

        if (empty($clients)) {
            return Response::json(ResponseUtil::makeError('Clients not found'), 400);
        }

        return $this->sendResponse($clients->toArray(), 'Clients retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateClientsAPIRequest $request
     *
     * @return Response
     *
     * @SWG\Put(
     *      path="/clients/{id}",
     *      summary="Update the specified Clients in storage",
     *      tags={"Clients"},
     *      description="Update Clients",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Clients",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Clients that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Clients")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Clients"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateClientsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Clients $clients */
        $clients = $this->clientsRepository->find($id);

        if (empty($clients)) {
            return Response::json(ResponseUtil::makeError('Clients not found'), 400);
        }

        $clients = $this->clientsRepository->update($input, $id);
        if (isset($input['points'])) {
            $push = array(
                "title" => "Tienes un ajuste de puntos",
                "body" => "Hola, se te ha hecho un ajuste de " . $input['points'] . " puntos",
                "case" => 6,
                "value" => $input['points']
            );
        }
        Push::sendPush($input['id'], $push);

        return $this->sendResponse($clients->toArray(), 'Clients updated successfully');
    }

    /**
     * @param int $id
     *
     * @return Response
     *
     * @SWG\Delete(
     *      path="/clients/{id}",
     *      summary="Remove the specified Clients from storage",
     *      tags={"Clients"},
     *      description="Delete Clients",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Clients",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Clients $clients */
        $clients = $this->clientsRepository->find($id);

        if (empty($clients)) {
            return Response::json(ResponseUtil::makeError('Clients not found'), 400);
        }

        $clients->delete();

        return $this->sendResponse($id, 'Clients deleted successfully');
    }


    public function rememeberPassword(Request $request)
    {
        $client = $this->clientsRepository->findWhere(['email' => $request['email']]);
        $client = $client->toArray();
        $now = new \DateTime();
        $code = md5($now->format("Y:M:d H:m:s"));
        $this->clientsRepository->update(['remember_token' => $code], $client[0]['id']);
        $url = env('APP_URL');
        Mail::send('emails.remember', ['url' => $url, 'code' => $code], function ($message) use ($client) {
            $message
                ->from('licorera@licorera3jjjs.com');
            $message->subject('Restablecimiento de contraseña');
            $message->to($client[0]['email']);
        });

        return $this->sendResponse(['success' => true], "Enviado email");
    }

}
