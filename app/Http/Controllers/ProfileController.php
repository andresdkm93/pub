<?php

namespace App\Http\Controllers;

use App\Infrastructure\Repositories\Criterias\OrderByCriteria;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Mobile\Client;
use App\Repositories\Mobile\ClientLocationRepository;
use App\Repositories\Mobile\ClientRepository;
use App\Repositories\Mobile\OrderRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

class ProfileController extends Controller
{

    private $clientRepository;
    private $orderRepository;
    private $clientLocationsRepository;

    public function __construct(ClientRepository $clientRepository,
                                OrderRepository $orderRepo,
                                ClientLocationRepository $clientLocationRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->orderRepository = $orderRepo;
        $this->clientLocationsRepository = $clientLocationRepository;
    }

    public function index(Request $request)
    {
        $user = $request->session()->get('user', null);
        if ($user==null) {
            return redirect('/');
        }
        if(!empty($user)){
            $user = Client::where('id', $user->id)->first();
        }
        $this->clientRepository->pushCriteria(new WithRelationshipsCriteria('locations'));
        $client = $this->clientRepository->find($user->id);
        return view('profile.main')
            ->with('user', $user)
            ->with('client', $client);

    }

    public function edit(Request $request)
    {
        $user = $request->session()->get('user', null);
        if ($user==null) {
            return redirect('/');
        }
        if(!empty($user)){
            $user = Client::where('id', $user->id)->first();
        }
        $this->clientRepository->pushCriteria(new WithRelationshipsCriteria('locations'));
        $client = $this->clientRepository->find($user->id);
        return view('profile.editar')
            ->with('user', $user)
            ->with('client', $client);

    }

    public function update(Request $request)
    {
        $input= $request->all();
        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $nombre = $file->getClientOriginalName();
            $now = new \DateTime();
            Storage::disk('local')->put($now->format("Y_m_d_H_i_s") . "_" . $nombre, File::get($file));
            $url = "http://images.applicorera3jjjs.com/ImageHandler.php?src=../storage/".$now->format("Y_m_d_H_i_s") . "_" . $nombre;
            $input['photo'] = $url;
        }
        $user = $request->session()->get('user', null);
        if ($user==null) {
            return redirect('/');
        }
        $this->clientRepository->update($input, $user->id);
        return Redirect::to('/profile');

    }

    public function newAddress(Request $request){
        $user = $request->session()->get('user', null);
        /** @var ClientLocations $clientLocations */
        $this->clientLocationsRepository->pushCriteria(new WhereFieldCriteria('client_id',$user->id));
        $client = $this->clientRepository->find($user->id);
        return
            view('profile.address_create')
                ->with('client', $client)
                ->with('user', $user);
    }

    public function createAddress(Request $request){
        $input= $request->all();
        $user = $request->session()->get('user', null);
        /** @var ClientLocations $clientLocations */
        $input['client_id'] = $user->id;
        $this->clientLocationsRepository->create($input);
        $this->clientRepository->pushCriteria(new WithRelationshipsCriteria('locations'));
        $client = $this->clientRepository->find($user->id);
        return Redirect::to('/profile');
    }

    public function removeAddress($locationId,Request $request){
        $user = $request->session()->get('user', null);
        /** @var ClientLocations $clientLocations */
        $clientLocations = $this->clientLocationsRepository->find($locationId);
        if (empty($clientLocations)) {
            return Redirect::to('/profile');
        }
        $clientLocations->delete();
        $this->clientRepository->pushCriteria(new WithRelationshipsCriteria('locations'));
        $client = $this->clientRepository->find($user->id);
        return Redirect::to('/profile');
    }

    public function editAddress($locationId,Request $request){
        $user = $request->session()->get('user', null);
        if(!empty($user)){
            $user = Client::where('id', $user->id)->first();
        }
        /** @var ClientLocations $clientLocations */
        $this->clientLocationsRepository->pushCriteria(new WhereFieldCriteria('client_id',$user->id));
        $location = $this->clientLocationsRepository->find($locationId);
        $client = $this->clientRepository->find($user->id);
        return
            view('profile.address')
                ->with('location',$location)
                ->with('client', $client)
                ->with('user', $user);
    }

    public function saveAddress($locationId,Request $request){
        $input= $request->all();
        $user = $request->session()->get('user', null);
        /** @var ClientLocations $clientLocations */

        $this->clientLocationsRepository->update($input, $locationId);
        $this->clientRepository->pushCriteria(new WithRelationshipsCriteria('locations'));
        $client = $this->clientRepository->find($user->id);
        return Redirect::to('/profile');
    }

    public function history(Request $request)
    {
        $user = $request->session()->get('user', null);
        if ($user==null) {
            return redirect('/');
        }        $this->clientRepository->pushCriteria(new WithRelationshipsCriteria('locations'));
        $client = $this->clientRepository->find($user->id);
        $this->orderRepository->pushCriteria(new RequestCriteria($request));
        $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->orderRepository->pushCriteria(new WhereFieldCriteria('client_id', $user->id));
        $this->orderRepository->pushCriteria(new WithRelationshipsCriteria(['products', 'status']));
        $this->orderRepository->pushCriteria(new OrderByCriteria('created_at','desc'));
        $orders = $this->orderRepository->paginate(4)->toArray();
        return view('profile.history')
            ->with('client', $client)
            ->with('user', $user)
            ->with('orders', $orders);
    }

    public function remember(Request $request){
        $user = $request->session()->get('user', null);
        if ($user!=null) {
            return redirect('/');
        }
        return
            view('profile.remember')
                ->with('user', $user);

    }

    public function doRemember(Request $request){
        $user = $request->session()->get('user', null);
        $client = $this->clientRepository->findWhere(['email' => $request['email']])->first();
        if(empty($client)){
            return
                view('profile.result')
                    ->with('message', 'Correo electronico no encontrado')
                    ->with('user', $user);
        }
        if($client->social_id!=null){
            return
                view('profile.result')
                    ->with('message', 'Tu cuenta fue creada con una red social, por favor ingresa por medio de esta')
                    ->with('user', $user);
        }
        $client = $client->toArray();
        $now = new \DateTime();
        $code = md5($now->format("Y:M:d H:m:s"));
        $this->clientRepository->update(['remember_token' => $code], $client['id']);
        Mail::send('emails.remember', ['code' => $code, 'url' => env('APP_URL')], function ($message) use ($client) {
            $message
                ->from('soporte@licorera3jjjs.com');
            $message->subject('Solicitud para restablecer contraseña');
            $message->to($client['email']);
        });
        return
            view('profile.result')
                ->with('message', 'Contraseña enviada al correo electronico')
                ->with('user', $user);
    }

    public function changuePassword($code)
    {
        $user = $this->clientRepository->findWhere(['remember_token' => $code]);
        $user = $user->toArray();
        if (isset($user[0])) {
            //$this->clientRepository->update(['remember_token' => ""], $user[0]['id']);
            return
                view('profile.form_password', array('id' => $user[0]['id']))
                    ->with('user', null);
        } else {
            return view('errors.503');
        }

    }

    public function updatePassword(Request $request)
    {
        $input = $request->all();
        if (isset($input['password']) && isset($input['id'])) {
            $this->clientRepository->update(['password' => \bcrypt($input['password'])], $input['id']);
            return
                view('profile.result')
                    ->with('message', 'Contraseña cambiada exitosamente')
                    ->with('user', null);
        } else {
            return Response::view('errors.503');
        }

    }
}
