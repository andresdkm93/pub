<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Infrastructure\Repositories\Criterias\OrderByCriteria;
use App\Infrastructure\Repositories\Criterias\WhereFieldCriteria;
use App\Infrastructure\Repositories\Criterias\WhereHasCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Mobile\Category;
use App\Models\Mobile\Client;
use App\Repositories\Mobile\CategoryRepository;
use App\Repositories\Mobile\EventRepository;
use App\Repositories\Mobile\ProductRepository;
use App\Repositories\Mobile\PromotionRepository;
use App\Repositories\Mobile\StoreProductRepository;
use App\Services\TopProducts;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

class PromotionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /** @var  StoreProductsRepository */
    private $storeProductsRepository;
    private $productsRepository;


    /**
     * PromotionController constructor.
     * @param StoreProductRepository $storeProductsRepo
     * @param ProductRepository $productsRepo
     */
    public function __construct(StoreProductRepository $storeProductsRepo, ProductRepository $productsRepo)
    {
        $this->storeProductsRepository = $storeProductsRepo;
        $this->productsRepository = $productsRepo;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $user = $request->session()->get('user', null);
        if(!empty($user)){
            $user = Client::where('id', $user->id)->first();
        }
        $this->storeProductsRepository->pushCriteria(new RequestCriteria($request));
        $this->storeProductsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->storeProductsRepository->pushCriteria(new WhereFieldCriteria( 'store_type',2, '='));

        $this->storeProductsRepository->pushCriteria(new WithRelationshipsCriteria(
            ['product']
        ));
        $storeProducts = $this->storeProductsRepository->all();

        return view('promotions.home')
            ->with('user', $user)
            ->with('promotions',$storeProducts);

    }



}
