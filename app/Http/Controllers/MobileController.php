<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Infrastructure\Repositories\Criterias\OrderByCriteria;
use App\Repositories\Mobile\EventRepository;
use App\Repositories\Mobile\PromotionRepository;
use App\Services\TopProducts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class MobileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $eventRepository;

    private $promotionRepository;

    public function __construct(EventRepository $eventRepository, PromotionRepository $promotionRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->promotionRepository = $promotionRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('mobile');
    }



}
