<?php

namespace App\Http\Controllers;

use App\Infrastructure\Repositories\Criterias\OrderByCriteria;
use App\Infrastructure\Repositories\Criterias\WithRelationshipsCriteria;
use App\Models\Mobile\Client;
use App\Repositories\Mobile\EventRepository;
use App\Repositories\Mobile\PromotionRepository;
use App\Services\TopProducts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $eventRepository;

    private $promotionRepository;

    public function __construct(EventRepository $eventRepository, PromotionRepository $promotionRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->promotionRepository = $promotionRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->session()->get('user', null);
        if (!empty($user)) {
            $user = Client::where('id', $user->id)->first();
        }
        $this->promotionRepository->pushCriteria(new WithRelationshipsCriteria(['store_product']));
        $this->promotionRepository->pushCriteria(new OrderByCriteria('diageo', 'desc'));
        $this->promotionRepository->pushCriteria(new OrderByCriteria('start_date', 'desc'));
        $promotions = $this->promotionRepository->all()->toArray();
        $events = $this->eventRepository->all()->toArray();
        $topProductsService = new TopProducts();
        $topProducts = $topProductsService->getProducts()->toArray();
        $banners = array_merge($events, $promotions);
        return view('main.home')
            ->with('banners', $banners)
            ->with('user', $user)
            ->with('topProducts', $topProducts);

    }

    public function about(Request $request)
    {
        $user = $request->session()->get('user', null);
        return view('about')
            ->with('user', $user);

    }


    public function logout(Request $request)
    {
        $request->session()->forget('user');
        return Redirect::to('/');
    }

    public function apple()
    {
        return response()->json(json_decode("{\"applinks\":{\"apps\":[],\"details\":[{\"appID\":\"F3PPN8JK28.com.diegodiaz.licorera3jjjs\",\"paths\":[\"\/tienda\/identificador\"]}]}}"));
    }

    public function deletion(Request $request)
    {
        $user = $request->session()->get('user', null);
        $id = $request->id;
        $code = DB::table('remove_request')
            ->where('code', $id)
            ->get();
        return view('deletion')
            ->with('code', count($code) > 0 ? $code[0] : null)
            ->with('user', $user);
    }

    public function removeData(Request $request)
    {
        try {
            $signed_request = $request->get('signed_request');
            $data = $this->parse_signed_request($signed_request);
            $user_id = $data['user_id'];

            Mail::raw('Id Facebook: ' . $user_id, function ($message) {
                $message->subject('Solicitud de eliminación de datos por facebook');
                $message->to('atencionalcliente@applicorera3jjjs.com');
            });

            $code = $this->createRandomPassword();
            DB::table('remove_request')->insert(
                ['social_id' => $user_id, 'code' => $code]
            );
            $status_url = env('APP_URL') . '/deletion?id=' . $code; // URL to track the deletion
            $confirmation_code = $code; // unique code for the deletion request
            $data = array(
                'url' => $status_url,
                'confirmation_code' => $confirmation_code
            );
            return Response::json($data);
        } catch (\Exception $exception) {
            $data = array(
                'url' => '',
                'confirmation_code' => ''
            );
            return Response::json($data);
        }

    }

    function createRandomPassword()
    {

        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double)microtime() * 1000000);
        $i = 0;
        $pass = '';

        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }

        return $pass;

    }

    function parse_signed_request($signed_request)
    {
        list($encoded_sig, $payload) = explode('.', $signed_request, 2);

        $secret = "988af5a0ad9e5caed0e6d2a1bc789488"; // Use your app secret here
        $sig = $this->base64_url_decode($encoded_sig);
        $data = json_decode(base64_url_decode($payload), true);
        $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
        if ($sig !== $expected_sig) {
            error_log('Bad Signed JSON signature!');
            return null;
        }

        return $data;
    }

    function base64_url_decode($input)
    {
        return base64_decode(strtr($input, '-_', '+/'));
    }

}
