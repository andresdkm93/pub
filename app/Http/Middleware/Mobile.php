<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;
use Jenssegers\Agent\Agent;

class Mobile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $agent = new Agent();
        if(($agent->isMobile() || $agent->isTablet()) && !str_contains($request->fullUrl(), 'condiciones')){
            $data = str_replace($request->url(), '',$request->fullUrl());
            return Redirect::to('https://movil.licorera3jjjs.com/#'.$data);
        }
        return $next($request);
    }
}
