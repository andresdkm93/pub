<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Crypt;
use InfyOm\Generator\Utils\ResponseUtil;
use Mockery\Exception;
use Response;

class WebToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $header = $request->header('authorization');
            $id = Crypt::decrypt($header);
            $request->attributes->add(['userId' => $id]);
            return $next($request);
        } catch (Exception $exception) {
            return Response::json(ResponseUtil::makeError($exception->getMessage()));
        }

    }
}
