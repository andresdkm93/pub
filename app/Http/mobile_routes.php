<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/


Route::group(['middleware' => ['web.token']], function () {
    Route::resource('clients/locations', 'WebClientLocationsAPIController');
    Route::resource('weborders', 'WebOrderAPIController',["only"=>['store','update']]);

});


Route::resource('categories', 'CategoryAPIController',["only"=>['index','show']]);

Route::resource('categories/{categoryId}/products', 'CategoriesProductsAPIController');

Route::resource('clients/{clientId}/locations', 'ClientLocationsAPIController');


Route::resource('clients/{clientId}/points', 'ClientAPIController@points');


Route::resource('clients', 'ClientAPIController',["only"=>['show','store','update']]);

Route::resource('clientLocations', 'ClientLocationAPIController');


Route::resource('devices', 'DeviceAPIController',["only"=>['store']]);

Route::resource('events', 'EventAPIController',["only"=>['index','show']]);

Route::resource('storeProducts', 'StoreProductsAPIController',["only"=>['show']]);

Route::resource('promotionProducts', 'PromotionProductAPIController');

Route::resource('orders', 'OrderAPIController',["only"=>['store','update']]);

Route::resource('promotions', 'PromotionAPIController',["only"=>['index','show']]);

Route::post('login', 'ClientAPIController@login');

Route::post('loginWeb', 'ClientAPIController@loginWeb');


Route::get('isActive', 'SheduleAPIController@isActive');

Route::get('testpush', 'SheduleAPIController@test');


Route::post("updateResource", "ResourceAPIController@updateResource");

Route::post('searchCode', 'CodeAPIController@search');

Route::post('emailsuggest', 'EmailAPIController@suggest');

Route::resource('clients/{clientId}/messages', 'MessageAPIController',["only"=>['index']]);

Route::get('clients/{clientId}/orders', 'ClientOrderAPIController@index');

Route::get('clients/{clientId}/orders/has_current', 'ClientOrderAPIController@hasCurrent');

Route::get('clients/{clientId}/orders/current', 'ClientOrderAPIController@current');

Route::get('clients/{clientId}/orders/current/cancel', 'ClientOrderAPIController@updateCurrent');

Route::get('clients/{clientId}/orders/{id}/validate', 'ClientOrderAPIController@validOrder');

Route::get('clients/{clientId}/orders/{id}', 'ClientOrderAPIController@show');

Route::get("search", "SearchAPIController@search");

Route::post('deliveries', 'LocationAPIController@check');

Route::post('clients/rememberPassword', 'API\ClientsAPIController@rememeberPassword');

Route::get('suggestedProducts', 'StoreProductsAPIController@suggestedProducts');
