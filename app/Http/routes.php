<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*SEO URL*/


Route::group(['middleware' => ['mobile']], function () {
    Route::get('/', 'HomeController@index');

    Route::get('/nosotros', 'HomeController@about');

    Route::get('/tienda', 'ShopController@index');

    Route::get('/promociones', 'PromotionController@index');

    Route::get('/tienda/categorias/{name}', 'ShopController@categories');

    Route::get('/tienda/productos/{name}', 'ProductController@show');

	Route::get('/tienda/identificador', 'ProductController@showId');

	Route::get('/tienda/busqueda/{name}', 'ProductController@search');

    Route::get('/cart', 'CartController@index');

    Route::get('/terminos', 'ProductController@terms');

    Route::get('/condiciones', 'ProductController@conditions');

    Route::get('/ubicaciones/remove/{locationId}', 'ProfileController@removeAddress');

    Route::get('/ubicaciones/{locationId}', 'ProfileController@editAddress');

    Route::post('/ubicaciones/{locationId}', 'ProfileController@saveAddress');

    Route::post('/ubicaciones', 'ProfileController@createAddress');

    Route::get('/ubicaciones', 'ProfileController@newAddress');

    Route::get('/profile', 'ProfileController@index');

    Route::get('/profile/edit', 'ProfileController@edit');

    Route::post('/profile/edit', 'ProfileController@update');

    Route::get('/history', 'ProfileController@history');

	Route::get('/reports', 'ReportController@store');


});


Route::get('/movil', 'MobileController@index');

/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/



Route::group(['prefix' => 'api', 'namespace' => 'API\Mobile'], function () {
    Route::group(['prefix' => 'mobile'], function () {
        require app_path('Http/mobile_routes.php');
    });
});

Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {
    Route::group(['prefix' => 'v1'], function () {
        require app_path('Http/api_routes.php');
    });
});

Route::post('clients/rememberPassword', 'API\ClientsAPIController@rememeberPassword');


Route::auth();

Route::get('/home', 'HomeController@index');

Route::post('removeData', 'HomeController@removeData');
Route::get('deletion', 'HomeController@deletion');


Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'HomeController@logout');
Route::get('logoutWeb', 'HomeController@logout');


Route::post('loginFront', 'Auth\AuthController@postLoginFront');


// Password Reset Routes...
Route::get('password/reset', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('/admin', 'AdminController@index');
Route::get('/descarga', 'ProductController@download');

Route::get('products/{id}', 'ProductController@show');



Route::get('remember', 'ProfileController@remember');

Route::post('remember', 'ProfileController@doRemember');

Route::get('changuePassword/{code}', 'ProfileController@changuePassword');

Route::post('updatePassword', 'ProfileController@updatePassword');

Route::get('apple-app-site-association','HomeController@apple');