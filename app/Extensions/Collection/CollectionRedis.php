<?php
namespace App\Extensions\Collection;

/**
 * Created by PhpStorm.
 * User: andres
 * Date: 7/04/16
 * Time: 12:01 PM
 */
use  Illuminate\Support\Collection;

class CollectionRedis extends Collection
{

    public function whereLike($key, $value)
    {
        return $this->filter(function ($item) use ($key, $value) {
            str_replace(" ",'\s',$value);
            return preg_match("/" . $value . "/i", data_get($item, $key));
        });
    }

}