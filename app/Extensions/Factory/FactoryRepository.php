<?php
namespace App\Extensions\Factory;

/**
 * Created by PhpStorm.
 * User: AndresFabian
 * Date: 3/07/2016
 * Time: 12:20 AM
 */
class FactoryRepository
{
    /**
     * Store the alias between interfaces/parent classes and concrete/child classes
     * @var array
     */
    protected $alias = array();

    /**
     * Bind an alias to a real class name. i.e.: UserRepoInterface to UserRepo.
     * Can be use to replace parent classes with child classes.
     * (Be aware both classes would need the same interface).
     * @param $alias
     * @param $name
     */
    public function bind($alias, $name)
    {
        $this->alias[$alias] = $name;
    }

    /**
     * Get the real name used for a class, as indicated by the bind method above
     * Example: getRealName('UserRepoInterface') should return 'UserRepo'
     * @param $name
     * @return mixed
     */
    public function getRealName($name)
    {
        if (isset ($this->alias[$name])) {
            $name = $this->alias[$name];
        }

        return $name;
    }

    /**
     * Build a new object determines and instantiate its dependencies automatically
     * @param $name
     * @return object
     */
    public function build($name)
    {
        try {
            $name = $this->getRealName($name);

            $reflection = new \ReflectionClass($name);

            if (!$reflection->isInstantiable()) {
                throw new \Exception($name . " is not instantiable");
            }

            $constructor = $reflection->getConstructor();

            if (is_null($constructor)) {
                return new $name;
            }

            $parameters = $constructor->getParameters();

            $args = array();

            foreach ($parameters as $parameter) {
                $args[] = $this->build($parameter->getClass()->getName());
            }

            return $reflection->newInstanceArgs($args);
        } catch (\Exception $e) {
            exit('Error trying to build "' . $name . '": ' . $e->getMessage());
        }
    }

}