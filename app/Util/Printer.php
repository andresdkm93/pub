<?php

namespace App\Util;

class Printer
{
    private $lineWidth;
    private $cutoff = '…';
    private $spacing = 1;
    private $lines = array();
    private $logo;

    public function __construct($lineWidth, $logo = null)
    {
        $this->lineWidth = $lineWidth;
        $this->logo = $logo;
    }

    private function cutText($text, $maxlength = null)
    {
        if ($maxlength === null) {
            $maxlength = $this->lineWidth;
        }
        if (HUtf8::strlen($text) > $maxlength) {
            $text = HUtf8::substr($text, 0, $maxlength - 1) . $this->cutoff;
        }
        return $text;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @return Printer
     */
    public function left($text)
    {
        $text = $this->cutText($text);
        $this->lines[] = $text;
        return $this;
    }

    /**
     * @return Printer
     */
    public function center($text, $separator = ' ')
    {
        $text = $this->cutText($text);
        $this->lines[] = str_pad($text, $this->lineWidth, $separator, STR_PAD_BOTH);
        return $this;
    }

    /**
     * @return Printer
     */
    public function right($text)
    {
        $text = $this->cutText($text);
        $this->lines[] = str_pad($text, $this->lineWidth, ' ', STR_PAD_LEFT);
        return $this;
    }

    /**
     * @return Printer
     */
    public function blank($times = 1)
    {
        for ($i = 0; $i < $times; $i++) {
            $this->lines[] = '';
        }
        return $this;
    }

    /**
     * @return Printer
     */
    public function separator($char)
    {
        $this->lines[] = str_repeat($char, $this->lineWidth);
        return $this;
    }

    /**
     * @return Printer
     */
    public function separatorRight($char)
    {
        $this->lines[] = str_pad(str_repeat($char, 10), $this->lineWidth, ' ', STR_PAD_LEFT);
        return $this;
    }

    /**
     * @return Printer
     */
    public function line($left, $right)
    {
        $left = HUtf8::encode_safe($left);
        $right = HUtf8::encode_safe($right);
        $remaining = $this->lineWidth;

        if (strlen($right)) {
            $remaining -= HUtf8::strlen($right) + $this->spacing;
        }
        if (HUtf8::strlen($left) > $remaining) {
            $remaining -= HUtf8::strlen($this->cutoff);
            $extraSpacing = false;
            if ($left[$remaining] == ' ') {
                $extraSpacing = true;
                $remaining--;
            }
            $left = HUtf8::substr($left, 0, $remaining) . $this->cutoff;
            if ($extraSpacing) {
                $left .= ' ';
            }
            $line = $left . str_repeat(' ', $this->spacing) . $right;
        } else {
            $line = str_pad($left, $remaining, ' ', STR_PAD_RIGHT) . ' ' . $right;
        }
        $this->lines[] = $line;
        return $this;
    }


    public function threeLine($left, $center, $right)
    {
        $line = '';
        $first = intval($this->lineWidth * 0.6);
        $second = intval($this->lineWidth * 0.15);
        $third = intval($this->lineWidth * 0.25);
        $left = HUtf8::encode_safe($left);
        $right = HUtf8::encode_safe($right);
        $center = HUtf8::encode_safe($center);

        $lettersLeft=[];
        $lettersCenter=[];
        $lettersRight=[];

        if (HUtf8::strlen($left) > $first) {
            $leftArray = str_split($left, $first-2);
            foreach ($leftArray as $index =>$item) {
                if($index==0){
                    $remaining = $first - HUtf8::strlen($item);
                    $line.= $item.str_repeat(' ', $remaining);
                }else{
                    $lettersLeft[]=$item;
                }
            }
        } else {
            $remaining = $first - HUtf8::strlen($left);
            $line.= $left. str_repeat(' ', $remaining);
        }

        $remaining = $second - HUtf8::strlen($center);
        $remaining = $remaining>0?$remaining:0;
        $line.= $center. str_repeat(' ', $remaining);

        $remaining = $third - HUtf8::strlen($right);
        $line.= str_pad($right, $remaining, ' ', STR_PAD_RIGHT);


       if(count($lettersLeft)>0){
           foreach ($lettersLeft as $word) {
               $line.="\n";
               $remaining = $first - HUtf8::strlen($word);
               $line.= $word.str_repeat(' ', $remaining);
           }
       }
        $this->lines[] = $line;
        return $this;
    }

    private function textLine($text)
    {
        $lineWidth = $this->lineWidth;
        $remaining = HUtf8::strlen($text);
        while ($remaining > 0) {
            $this->lines[] = ltrim(HUtf8::substr($text, 0, $lineWidth));
            $text = HUtf8::substr($text, $lineWidth);
            $remaining = HUtf8::strlen($text);
        }
        return $this;
    }

    public function text($text)
    {
        $text = str_replace("\r", '', $text);
        if (strpos($text, "\n") === false) {
            $this->textLine($text);
        } else {
            $lines = explode("\n", $text);
            foreach ($lines as $line) {
                $this->textLine($line);
            }
        }
        return $this;
    }

    public function textCenter($text)
    {
        $lines = explode("\n", $text);
        foreach ($lines as $line) {
            $this->center(trim($line));
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getOutput()
    {
        return implode("\n", $this->lines);
    }

    public function getLineWidth()
    {
        return $this->lineWidth;
    }
}
