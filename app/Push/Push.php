<?php

namespace App\Push;

use App\Models\Message;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Created by PhpStorm.
 * User: AndresFabian
 * Date: 24/04/2016
 * Time: 2:28 PM
 */
class Push {
	public static function sendPush( $id, $input ) {

		if ( $id ) {
			$client = DB::table( 'clients' )->where( 'id', $id )->get();
			$device = DB::table( 'devices' )->where( 'uuid', $client[0]->uuid )->get();
            Log::debug( "new push ", [ "ID" =>$id, "input" => $input] );
            if ( isset( $device[0] ) ) {
				if ( isset( $device[0]->pushkey ) ) {
                    Log::debug( "new push ", [ "os_id" => $device[0]->os_id , "client" => $client] );
                    switch ( $device[0]->os_id ) {
						case 1:

							$push               = new PushNotification();
							$response           = $push->setMessage(
								[
									'title'   => $input['title'],
									'message' => $input['body'],
								]
							)
							                           ->setApiKey( 'AIzaSyDR-roFuaLvjgtRD3JA_fn5xH9w3GkEvvU' )
							                           ->setDevicesToken( [ $device[0]->pushkey ] )
							                           ->send()
							                           ->getFeedback();
							$message            = new Message();
							$message->client_id = $id;
							$message->data      = [
								'title' => $input['title'],
								'body'  => $input['body'],
							];
							$message->save();

							break;
						case 2:
							$payload = [
								'aps'            => [
									'alert' => [
										'title' => $input['title'],
										'body'  => $input['body']
									],
									'sound' => 'default',
									'badge' => 1,

								],
								'additionalData' => [
									'op' => $input['case'],
								]
							];

							$push = new PushNotification( 'apn' );

							$response = $push->setMessage( $payload )
							                 ->setDevicesToken( [ $device[0]->pushkey ] )
							                 ->send()
							                 ->getFeedback();
							break;
						case 3:
							$push        = new PushNotification( 'fcm' );
							$input['op'] = $input['case'];
							$payload     = [
								'notification' => [
									'title' => $input['title'],
									'body'  => $input['body'],
									'sound' => 'default',
									'badge' => 1,

								],
								'data'         => $input
							];
							$response    = $push->setMessage( $payload )
							                    ->setDevicesToken( [ $device[0]->pushkey ] )
							                    ->send()
							                    ->getFeedback();
							Log::debug( "Push Result FCM", [ "response" => $response ] );
							break;
						case 4:
							$push        = new PushNotification( 'fcm' );
							$input['op'] = $input['case'];
							$payload     = [
								'data' => $input
							];
							$response    = $push->setMessage( $payload )
							                    ->setDevicesToken( [ $device[0]->pushkey ] )
							                    ->send()
							                    ->getFeedback();
							Log::debug( "Push Result Android", [ "response" => $response ] );
							break;
					}
				}
			}

		}


	}



	public static function sendMasivePush( $input ) {
		$push         = new PushNotification( 'fcm' );
        $input['op'] = $input['case'];
        $payload      = [
            'data' => $input
        ];
        $response     = $push->setMessage( $payload )
            ->sendByTopic( 'promosAndroid' )
            ->getFeedback();

		/*****************************/
		$payload      = [
			'notification' => [
				'title' => $input['title'],
				'body'  => $input['body'],
				'sound' => 'default',
				'badge' => 1,

			],
            'data'         => $input
        ];
		$response     = $push->setMessage( $payload )
		                     ->sendByTopic( 'promos' )
		                     ->getFeedback();

	}

}