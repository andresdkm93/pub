<?php

namespace App\Repositories;

use App\Models\Devices;
use InfyOm\Generator\Common\BaseRepository;

class DevicesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Devices::class;
    }
}
