<?php

namespace App\Repositories;

use App\Models\ClientLocations;
use InfyOm\Generator\Common\BaseRepository;

class ClientLocationsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ClientLocations::class;
    }
}
