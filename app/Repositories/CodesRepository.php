<?php

namespace App\Repositories;

use App\Models\Codes;
use InfyOm\Generator\Common\BaseRepository;

class CodesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Codes::class;
    }
}
