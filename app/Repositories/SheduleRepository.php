<?php

namespace App\Repositories;

use App\Models\Shedule;
use InfyOm\Generator\Common\BaseRepository;

class SheduleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Shedule::class;
    }
}
