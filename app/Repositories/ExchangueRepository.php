<?php

namespace App\Repositories;

use App\Models\Exchangue;
use InfyOm\Generator\Common\BaseRepository;

class ExchangueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Exchangue::class;
    }
}
