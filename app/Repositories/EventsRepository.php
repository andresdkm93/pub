<?php

namespace App\Repositories;

use App\Models\Events;
use InfyOm\Generator\Common\BaseRepository;

class EventsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Events::class;
    }
}
