<?php

namespace App\Repositories;

use App\Models\Orders;
use InfyOm\Generator\Common\BaseRepository;

class OrdersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Orders::class;
    }


    public function values()
    {
        $data=[
          "new"=>  $this->model->where('order_status_id', 1)->count(),
            "route"=>$this->model->where('order_status_id', 2)->count(),
            "completed"=>$this->model->where('order_status_id', 3)->count(),
            "canceled"=>$this->model->where('order_status_id', 4)->count()
        ];
        return $data;

    }

   
}
