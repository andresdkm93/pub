<?php

namespace App\Repositories;

use App\Models\EventProducts;
use InfyOm\Generator\Common\BaseRepository;

class EventProductsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EventProducts::class;
    }
}
