<?php

namespace App\Repositories;

use App\Models\Clients;
use App\QueryCache\RepositoryCache;
use InfyOm\Generator\Common\BaseRepository;

class ClientsRepository extends RepositoryCache
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Clients::class;
    }
}
