<?php

namespace App\Repositories;

use App\Infrastructure\Repositories\Criterias\WhereHasCriteria;
use App\Models\Products;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\Cache;
use App\QueryCache\RepositoryCache;

class ProductsRepository extends RepositoryCache
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Products::class;
    }

    public function search($columns = array('*'))
    {

            $this->applyCriteria();
            $results = $this->model->with('store')->whereHas('store',
                function ($q) {
                    $q->where('store_type', 1);
                })->get();
            $data = $this->parserResult($results);
            return $data;
    }
}