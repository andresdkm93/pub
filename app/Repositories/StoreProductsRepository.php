<?php

namespace App\Repositories;

use App\Models\StoreProducts;
use InfyOm\Generator\Common\BaseRepository;

class StoreProductsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreProducts::class;
    }

    public function createProduct($data){
        return $this->model->product()->insert($data);
    }
}
