<?php

namespace App\Repositories;

use App\Models\Categories;
use InfyOm\Generator\Common\BaseRepository;

class CategoriesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];


    function getStoreProducts($categoryId)
    {
        $this->applyCriteria();
        $user = $this->model->where('id', $categoryId)->with(['products' =>
            function ($query)  {
                $query->with('store')->has('store');
            }])->get();
        return $user;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Categories::class;
    }
}
