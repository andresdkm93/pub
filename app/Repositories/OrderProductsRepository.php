<?php

namespace App\Repositories;

use App\Models\OrderProducts;
use InfyOm\Generator\Common\BaseRepository;

class OrderProductsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderProducts::class;
    }
}
