<?php
/**
 * Created by PhpStorm.
 * User: adiaz
 * Date: 12/12/2016
 * Time: 07:16 PM
 */

namespace App\Repositories;

use App\Infrastructure\Repositories\Criterias\WhereHasCriteria;
use App\Models\Products;
use App\Models\StoreProducts;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\Cache;
use App\QueryCache\RepositoryCache;

class stockProductsRepository  extends RepositoryCache
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreProducts::class;
    }

    public function search($columns = array('*'))
    {


            $this->applyCriteria();
            $results = $this->model->with('product')->where('store_type', 1)->get();
            foreach ($results as $result)
            {
                $result->name=$result->product->name;
                $result->serial=$result->product->serial;
            }
            $data = $this->parserResult($results);
            return $data;
        }

}