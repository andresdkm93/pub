<?php

namespace App\Repositories;

use App\Models\Zones;
use InfyOm\Generator\Common\BaseRepository;

class ZonesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Zones::class;
    }
}
