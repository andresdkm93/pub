<?php

namespace App\Repositories\Mobile;

use App\Models\Mobile\Code;
use InfyOm\Generator\Common\BaseRepository;

class CodeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Code::class;
    }
}
