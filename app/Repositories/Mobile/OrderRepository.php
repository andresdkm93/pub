<?php

namespace App\Repositories\Mobile;

use App\Models\Mobile\Order;
use App\Models\Mobile\StoreProduct;
use App\Models\Mobile\OrderProduct;
use App\Models\Mobile\Promotion;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Common\BaseRepository;

class OrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Order::class;
    }

    public function createOrderProducts($orderId, $pointsLimit, $data, $description = "")
    {
        $storeValue = 0;
        $eventsValue = 0;
        $pointsValue = 0;
        $promotions = Promotion::get();
        foreach ($data as $product) {
            $arr = explode(":", $product);
            $id = $arr[0];
            $quantity = $arr[1];
            $store = StoreProduct::find($id);
            if (!empty($store))
                if ($store->quantity >= $quantity || $store->quantity == null) {
                    switch ($store->store_type) {
                        case 1:
                            $total = (int)$store->price * $quantity;
                            $arr = $this->getPromotion($promotions, $store->id, $total, $quantity, $description);
                            $total = $arr['total'];
                            $quantity = $arr['quantity'];
                            $description = $arr['description'];
                            $storeValue += $total;
                            Log::debug("creando",["data"=>$arr]);
                            $this->createProduct($orderId, $store->id, $store->price, 0, $quantity, $total);
                            break;
                        case 2:
                            $points = (int)$store->points * $quantity;
                            if ($pointsValue+$points < $pointsLimit) {
                                $this->createProduct($orderId, $store->id, 0, $points, $quantity);
                                $pointsValue += $points;
                            }
                            break;
                        case 3:
                            $total = (int)$store->price * $quantity;
                            $eventsValue += $total;
                            $this->createProduct($orderId, $store->id, $store->price, 0, $quantity, $total);
                            break;
                        case 4:
                            $total = (int)$store->price * $quantity;
                            $eventsValue += $total;
                            $this->createProduct($orderId, $store->id, $store->price, 0, $quantity, $total);
                            break;
                    }
                } else {
                    $this->createProduct($orderId, $store->id);
                }

        }
        return array('storeValue' => $storeValue, 'eventsValue' => $eventsValue, 'pointsValue' => $pointsValue, "description" => $description);
    }


    function createProduct($orderId, $storeProductId, $price = 0, $points = 0, $quantity = 0, $subtotal = 0, $total_points = 0)
    {
        OrderProduct::create([
            'order_id' => $orderId,
            'store_product_id' => $storeProductId,
            'price' => $price,
            'points' => $points,
            'quantity' => $quantity,
            'subtotal' => $subtotal,
            'total_points' => $total_points
        ]);
    }

    function getPromotion($promotions, $productId, $total, $quantity, $description)
    {
        foreach ($promotions as $promotion) {
            if ($promotion->promotion_type > 1) {
                if ($promotion->store_product_id == $productId) {
                    switch ($promotion->promotion_type) {
                        case 2:
                            if ((int)$quantity >= $promotion->multiplier) {
                                $quantity = (int)(((int)$quantity / $promotion->multiplier) * $promotion->divider);
                                $description .= "Se aplica promocion " . $promotion->name . ", enviando " . $quantity . " un<br/>";
                            }
                            break;
                        case 3:
                            if ($quantity >= $promotion->quantity_minimal)
                                $total = $total - ($total * ($promotion->discount / 100));
                            $description .= "Se aplica promocion " . $promotion->name . ", con descuento " . $promotion->discount . "%<br/>";
                            break;
                    }
                    break;
                }
            }
        }

        return array('total' => $total, 'quantity' => $quantity, 'description' => $description);

    }


}
