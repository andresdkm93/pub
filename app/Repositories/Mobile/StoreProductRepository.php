<?php

namespace App\Repositories\Mobile;

use App\Models\Mobile\StoreProduct;
use InfyOm\Generator\Common\BaseRepository;

class StoreProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreProduct::class;
    }
}
