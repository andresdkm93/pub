<?php

namespace App\Repositories\Mobile;

use App\Models\Mobile\OrderProduct;
use InfyOm\Generator\Common\BaseRepository;

class OrderProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderProduct::class;
    }
}
