<?php

namespace App\Repositories\Mobile;

use App\Models\Mobile\EventProduct;
use InfyOm\Generator\Common\BaseRepository;

class EventProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EventProduct::class;
    }
}
