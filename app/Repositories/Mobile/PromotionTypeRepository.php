<?php

namespace App\Repositories\Mobile;

use App\Models\Mobile\PromotionType;
use InfyOm\Generator\Common\BaseRepository;

class PromotionTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PromotionType::class;
    }
}
