<?php

namespace App\Repositories\Mobile;

use App\Models\Mobile\Promotion;
use InfyOm\Generator\Common\BaseRepository;

class PromotionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Promotion::class;
    }
}
