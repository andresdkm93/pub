<?php

namespace App\Repositories\Mobile;

use App\Models\Mobile\Category;
use InfyOm\Generator\Common\BaseRepository;

class CategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    function getStoreProducts($categoryId)
    {
        $this->applyCriteria();
        $user = $this->model->where('id', $categoryId)->with(['products' =>
            function ($query)  {
                $query->with('store')->has('store');
            }])->get();
        return $user;
    }
    /**
     * Configure the Model
     **/
    public function model()
    {
        return Category::class;
    }
}
