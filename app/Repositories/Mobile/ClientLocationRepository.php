<?php

namespace App\Repositories\Mobile;

use App\Models\Mobile\ClientLocation;
use InfyOm\Generator\Common\BaseRepository;

class ClientLocationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ClientLocation::class;
    }
}
