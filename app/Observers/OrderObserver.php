<?php
namespace App\Observers;

/**
 * Created by PhpStorm.
 * User: adiaz
 * Date: 12/12/2016
 * Time: 03:07 PM
 */
use App\Models\Exchangue;
use App\Models\Mobile\Order;
use App\Models\Mobile\Client;
use App\Models\Mobile\OrderProduct;

class OrderObserver
{

    public function updated(Order $order)
    {
        switch ($order->order_status_id) {
            case 1:
                $client_id = $order->client_id;
                if ($order->points > 0) {
                    $client = Client::find($client_id);
                    $client->points = $client->points - $order->points;
                    $client->save();
                    Exchangue::create([
                        'client_id'=>$order->client_id,
                        'order_id'=>$order->id,
                        'total'=>$order->points,
                        'delivered'=>0
                    ]);
                }

                $order_products = OrderProduct::with('store')->where('order_id', $order->id)->get();
                foreach ($order_products as $order_product) {
                    $store = $order_product->store;
                    if ($store->quantity != null) {
                        $store->quantity -= $order_product->quantity;
                        $store->save();
                    }

                }
                break;


        }

    }
}