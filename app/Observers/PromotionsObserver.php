<?php
/**
 * Created by PhpStorm.
 * User: adiaz
 * Date: 12/12/2016
 * Time: 05:22 PM
 */

namespace App\Observers;

use App\Models\Promotions;
use App\Models\StoreProducts;
use App\Models\Products;

class PromotionsObserver
{
    public function created(Promotions $promotions)
    {
        if($promotions->promotion_type==1)
        {
            $store=StoreProducts::find($promotions->store_product_id);
            $store->quantity=$promotions->quantity;
            $store->start_date=$promotions->start_date;
            $store->end_date=$promotions->end_date;
            $store->price=$promotions->price;
            $store->save();

        }
    }

    public function updated(Promotions $promotions)
    {
        if($promotions->promotion_type==1)
        {
            $store=StoreProducts::find($promotions->store_product_id);
            $store->quantity=$promotions->quantity;
            $store->start_date=$promotions->start_date;
            $store->end_date=$promotions->end_date;
            $store->price=$promotions->price;
            $store->save();

        }

    }

    public function deleted(Promotions $promotions)
    {
        if($promotions->promotion_type==1)
        {
            $store=StoreProducts::find($promotions->store_product_id);
            $product_id=$store->product_id;
            Products::destroy($product_id);
            $store->delete();
        }

    }

}