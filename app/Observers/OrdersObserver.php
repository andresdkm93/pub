<?php
/**
 * Created by PhpStorm.
 * User: adiaz
 * Date: 12/12/2016
 * Time: 04:47 PM
 */

namespace App\Observers;

use App\Models\Exchangue;
use App\Models\Orders;
use App\Push\Push;
use App\Models\Clients;
use App\Models\OrderProducts;


class OrdersObserver
{
    public function updated(Orders $order)
    {
        switch ($order->order_status_id) {

            case 2:
                $client_id = $order->client_id;
                $push = array(
                    "title" => "Tu pedido va en camino",
                    "body" => "Tu pedido ya se encuentra en camino, espera al repartidor!",
                    "case" => 1,
                    "order_id" => $order->id,
                    "value" => $order->points
                );
                Push::sendPush($client_id, $push);
                break;
            case 3:
                //completado
                $points = (int)($order->total / 10000);
                $push = array(
                    "title" => "Tu pedido fue entregado",
                    "body" => "Tu pedido fue entregado a tiempo, por favor califica nuestro servicio",
                    "case" => 3,
                    "order_id" => $order->id,
                    "value" => $points
                );
                $client_id = $order->client_id;
                $client = Clients::find($client_id);
                $client->points = $client->points + $points;
                $client->save();
                if($order->points>0)
                {
                    $exchangue=Exchangue::where('order_id',$order->id)->first();
                    $exchangue->delivered=true;
                    $exchangue->save();
                }
                Push::sendPush($client_id, $push);

                break;
            case 4:
                $client_id = $order->client_id;
                $client = Clients::find($client_id);
                $order_products = OrderProducts::with('store')->where('order_id', $order->id)->get();
                foreach ($order_products as $order_product) {
                    $store = $order_product->store;
                    if (!is_null($store->quantity)) {
                        $store->quantity= $store->quantity+$order_product->quantity;
                        $store->save();
                    }
                    if ($order_product->points) {
                        $client->points = $client->points + $order_product->points;
                    }
                }
                $client->save();
                $push = array(
                    "title" => "Tu pedido fue cancelado",
                    "body" => "No podemos entregar tu pedido en el momento, lo sentimos mucho!",
                    "case" => 4,
                    "order_id" => $order->id,
                    "value" => $order->points
                );
                Push::sendPush($client_id, $push);
                break;

        }

    }
}