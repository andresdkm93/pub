<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 6/09/16
 * Time: 08:21 PM
 */

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

class DateScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $time = Carbon::now()->toDateTimeString();
        return $builder
            ->where(function ($query) use ($time) {
                $query->whereNull('start_date')
                    ->orWhere('start_date', '<', $time);
            })
            ->where(function ($query) use ($time) {
                $query->whereNull('end_date')
                    ->orWhere('end_date', '>', $time);
            });


    }
}