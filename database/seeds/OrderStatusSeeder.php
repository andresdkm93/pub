<?php

use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orderStatus = \App\Models\OrderStatuses::find(5);
        if(empty($orderStatus)){
            $orderStatus = new \App\Models\OrderStatuses();
            $orderStatus->id = 5;
            $orderStatus->name = 'Cancelado por el usuario';
            $orderStatus->save();
        }
    }
}
