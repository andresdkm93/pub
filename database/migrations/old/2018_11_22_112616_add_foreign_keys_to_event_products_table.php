<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEventProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('event_products', function(Blueprint $table)
		{
			$table->foreign('events_id', 'fk_event_products_events1')->references('id')->on('events')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('store_product_id', 'fk_event_products_store_products1')->references('id')->on('store_products')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('event_products', function(Blueprint $table)
		{
			$table->dropForeign('fk_event_products_events1');
			$table->dropForeign('fk_event_products_store_products1');
		});
	}

}
