<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients', function(Blueprint $table)
		{
			$table->string('id', 30)->primary();
			$table->string('name', 200);
			$table->string('last_name', 200)->nullable();
			$table->date('birthday')->nullable();
			$table->string('email', 100);
			$table->string('password', 300);
			$table->string('social_id', 100)->nullable();
			$table->string('photo', 200)->nullable();
			$table->string('cellphone', 100)->nullable();
			$table->integer('points')->nullable()->default(0);
			$table->string('uuid', 100);
			$table->string('remember_token', 512);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clients');
	}

}
