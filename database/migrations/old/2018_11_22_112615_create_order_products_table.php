<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_products', function(Blueprint $table)
		{
			$table->integer('order_id')->index('fk_oder_products_orders1_idx');
			$table->integer('store_product_id')->index('fk_oder_products_store_products1_idx');
			$table->float('price', 10, 0)->nullable();
			$table->integer('points')->nullable();
			$table->integer('quantity');
			$table->string('subtotal', 100);
			$table->string('total_points', 100);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_products');
	}

}
