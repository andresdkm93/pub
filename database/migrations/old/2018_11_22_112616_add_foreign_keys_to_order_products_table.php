<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrderProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_products', function(Blueprint $table)
		{
			$table->foreign('order_id', 'fk_oder_products_orders1')->references('id')->on('orders')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('store_product_id', 'fk_oder_products_store_products1')->references('id')->on('store_products')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_products', function(Blueprint $table)
		{
			$table->dropForeign('fk_oder_products_orders1');
			$table->dropForeign('fk_oder_products_store_products1');
		});
	}

}
