<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('amount', 100);
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
			$table->string('address')->nullable();
			$table->string('phone', 100)->nullable();
			$table->integer('qualification')->nullable();
			$table->string('client_id', 30)->index('fk_orders_clients1_idx');
			$table->integer('order_status_id')->index('fk_orders_order_statuses1_idx');
			$table->string('code', 100)->nullable()->index('fk_orders_codes1_idx');
			$table->string('delivery_time', 30)->nullable();
			$table->string('delivery_zone', 100);
			$table->string('delivery_value', 100);
			$table->string('points', 100)->nullable();
			$table->string('total', 100);
			$table->string('pay_method', 100)->nullable();
			$table->string('instructions', 300);
			$table->string('description', 500);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
