<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExchanguesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('exchangues', function(Blueprint $table)
		{
			$table->foreign('client_id', 'client_id_exchangue')->references('id')->on('clients')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('order_id', 'order_id_exchangue')->references('id')->on('orders')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('exchangues', function(Blueprint $table)
		{
			$table->dropForeign('client_id_exchangue');
			$table->dropForeign('order_id_exchangue');
		});
	}

}
