<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromotionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promotions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 300);
			$table->string('image', 300);
			$table->integer('quantity')->nullable();
			$table->dateTime('start_date')->nullable();
			$table->dateTime('end_date')->nullable();
			$table->integer('promotion_type')->nullable()->index('fk_promotions_promotion_types1_idx');
			$table->integer('store_product_id')->nullable()->index('fk_promotions_store_products1_idx');
			$table->integer('discount')->nullable();
			$table->integer('quantity_minimal')->nullable();
			$table->integer('divider')->nullable();
			$table->integer('multiplier')->nullable();
			$table->float('price', 10, 0)->nullable();
			$table->string('description', 300)->nullable();
			$table->boolean('diageo')->nullable()->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promotions');
	}

}
