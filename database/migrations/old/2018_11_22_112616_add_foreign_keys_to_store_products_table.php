<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStoreProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('store_products', function(Blueprint $table)
		{
			$table->foreign('product_id', 'fk_store_products1')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('store_type', 'fk_store_products_store_types1')->references('id')->on('store_types')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('store_products', function(Blueprint $table)
		{
			$table->dropForeign('fk_store_products1');
			$table->dropForeign('fk_store_products_store_types1');
		});
	}

}
