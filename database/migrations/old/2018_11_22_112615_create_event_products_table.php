<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_products', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('events_id')->index('fk_event_products_events1_idx');
			$table->integer('store_product_id')->index('fk_event_products_store_products1_idx');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_products');
	}

}
