<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('client_locations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 300);
			$table->string('address', 300)->nullable();
			$table->string('latitude', 150)->nullable()->default('7.0685091');
			$table->string('longitude', 150)->nullable()->default('-73.1044817');
			$table->string('client_id', 30)->index('fk_client_locations_clients1_idx');
			$table->string('detail', 400);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('client_locations');
	}

}
