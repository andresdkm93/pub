<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_products', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('product_id')->nullable()->index('fk_store_products1_idx');
			$table->integer('quantity')->nullable();
			$table->float('price', 10, 0)->nullable();
			$table->boolean('status')->default(0);
			$table->dateTime('start_date')->nullable();
			$table->dateTime('end_date')->nullable();
			$table->integer('store_type')->index('fk_store_products_store_types1_idx');
			$table->integer('points')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->float('ranking', 10, 0)->nullable();
			$table->boolean('recommended')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_products');
	}

}
