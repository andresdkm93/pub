<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPromotionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promotions', function(Blueprint $table)
		{
			$table->foreign('promotion_type', 'fk_promotions_promotion_types1')->references('id')->on('promotion_types')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('store_product_id', 'fk_promotions_store_products1')->references('id')->on('store_products')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promotions', function(Blueprint $table)
		{
			$table->dropForeign('fk_promotions_promotion_types1');
			$table->dropForeign('fk_promotions_store_products1');
		});
	}

}
