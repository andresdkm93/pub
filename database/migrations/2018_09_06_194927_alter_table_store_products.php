<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStoreProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_products', function(Blueprint $table)
        {
            $table->double('ranking')->nullable();
            $table->boolean('recommended')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ped_cart', function($table)
        {
            $table->dropColumn('store_products');
            $table->dropColumn('recommended');
        });
    }
}
