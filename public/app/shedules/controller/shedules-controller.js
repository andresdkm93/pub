/**
 * Created by andres on 23/06/16.
 */
angular.module('PubApp')
    .controller('ShedulesController', ['$scope',
        function($scope) {


            $scope.dataView = {
                name: 'shedules',
                display: "Horarios",
                autoInit: true,
                createPermission:true,
                readPermission:false,
                viewPermission:false,
                fields: [
                    {
                        name: 'opening_time',
                        title: 'Hoario de Apertura',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'time'
                        }

                    },{
                        name: 'closing_time',
                        title: 'Horario de Cierre',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'time'
                        }

                    }

                    
                ],
            };



        }]);