/**
 * Created by andres on 23/06/16.
 */
angular.module('PubApp')
    .controller('ClientsViewController', ['$scope', '$uibModal', 'serviceFactory','$stateParams','$sce',
        function ($scope, $uibModal, serviceFactory,$stateParams,$sce) {
            $scope.id = $stateParams.id;
            serviceFactory.getService("clients/"+$scope.id)
                .get(function (response) {
                    $scope.client=response.data;
                    console.log($scope.client);
                });
            $scope.dataViewOrders = {
                name: 'clients/' + $scope.id + '/orders',
                display: "Ordenes",
                readPermission: false,
                viewPermission: false,
                createPermission: false,
                updatePermission: false,
                deletePermission: false,

                fields: [
                    {
                        name: 'id',
                        title: 'Pedido #',
                        display: {
                            type: 'text'
                        },
                    }, {
                        name: 'total',
                        title: 'Valor ($)',
                        display: {
                            type: 'text'
                        },

                    }, {
                        name: 'address',
                        title: 'Direccion',
                        display: {
                            type: 'text'
                        },

                    }, {
                        name: 'order_status_id',
                        title: 'Estado',
                        display: {
                            type: 'html',
                            html: function (value) {
                                switch (value) {
                                    case 1:
                                        return $sce.trustAsHtml('<span class="label label-info">Nuevo</span>');
                                        break;
                                    case 2:
                                        return $sce.trustAsHtml('<span class="label label-warning">Enviado</span>');
                                        break;
                                    case 3:
                                        return $sce.trustAsHtml('<span class="label label-success">Completado</span>');
                                        break;
                                    case 4:
                                        return $sce.trustAsHtml('<span class="label label-danger">Cancelado</span>');
                                        break;
                                }

                            }
                        },

                    },
                    {
                        name: 'created_at',
                        title: 'Fecha',
                        display: {
                            type: 'text'
                        }

                    }


                ],
                buttons: [
                    {
                        icon: 'fa fa-eye',
                        title: 'Ver',
                        action: function (id) {
                            $scope.openOrder(id);
                        }
                    }]

            };


            $scope.dataView2 = {
                name: 'clients/' + $scope.id + '/exchangues',
                display: "Canjes",
                readPermission: false,
                viewPermission: false,
                createPermission: false,
                updatePermission: false,
                deletePermission: false,
                fields: [
                    {
                        name: 'order_id',
                        title: 'Pedido #',
                        display: {
                            type: 'text'
                        },
                    }, {
                        name: 'total',
                        title: 'Puntos canjeados',
                        display: {
                            type: 'text'
                        },

                    }, {
                        name: 'delivered',
                        title: 'Estado',
                        display: {
                            type: 'html',
                            html: function (value) {
                                switch (value) {
                                    case true:
                                        return $sce.trustAsHtml('<span class="label label-success">Canejado</span>');
                                        break;
                                    case false:
                                        return $sce.trustAsHtml('<span class="label label-danger">Declinado</span>');
                                        break;
                                }

                            }
                        },

                    },
                    {
                        name: 'created_at',
                        title: 'Fecha',
                        display: {
                            type: 'text'
                        }

                    }


                ]

            };



        }]);