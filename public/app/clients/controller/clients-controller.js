/**
 * Created by andres on 23/06/16.
 */
angular.module('PubApp')
    .controller('ClientsController', ['$scope','$uibModal','serviceFactory','$state',
        function($scope,$uibModal,serviceFactory,$state) {
            $scope.products = [];
            $scope.productId = null;
            $scope.dataView = {
                name: 'clients',
                display: "Clientes",
                autoInit: true,
                readPermission:true,
                createPermission:false,
                readAction:function(id)
                {
                    console.log(id);
                    $state.go('clientsview',{id:id});
                },
                fields: [
                    {
                        name: 'id',
                        title: 'Identificación',
                        display: {
                            type: 'text'
                        },

                    },
                    {
                        name: ['name', 'last_name'],
                        title: 'Nombres',
                        display: {
                            type: 'text'
                        },

                    },
                    {
                        name: 'email',
                        title: 'Correo',
                        display: {
                            type: 'text'
                        },

                    }, {
                        name: 'birthday',
                        title: 'Cumpleaños',
                        display: {
                            type: 'text'
                        },

                    },{
                        name: 'cellphone',
                        title: 'Celular',
                        display: {
                            type: 'text'
                        },

                    },
                    {
                        name: 'points',
                        title: 'Puntos',
                        display: {
                            type: 'text'
                        },
                        form:{
                            type:'input',
                            format:'number'
                        }

                    },




                ],
                buttons :[
                    {
                        icon: 'fa fa-comments',
                        option: 'push',
                        title: 'Notificacion',
                        action:function(id){
                            $scope.message = "";
                            $scope.title = "";
                            $scope.id = id;
                            $scope.modalInstance = $uibModal.open({
                                animation: true,
                                templateUrl: 'templates/clients/push.html',
                                size: 'lg',
                                scope: $scope

                            });
                        }

                    },
                ],searchFields: [
                    {name: "name", display: "Nombre"},
                    {name: "email", display: "Correo"}
                ]

            };


            $scope.push = function () {
                serviceFactory.getService("clients/sendpush").save(
                    {
                        id: $scope.id,
                        title: $scope.title,
                        message: $scope.message
                    });
                $scope.modalInstance.dismiss('cancel');
            };
            $scope.openPushMasive = function () {
                serviceFactory.getService("stockProducts?paginate=false").get(function (response) {
                    $scope.products = response.data;
                    $scope.message = "";
                    $scope.title = "";
                    $scope.modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: 'templates/clients/pushmasive.html',
                        size: 'lg',
                        scope: $scope

                    });
                })
            };

            $scope.pushMasive = function () {
                serviceFactory.getService("clients/sendmasivepush").save(
                    {
                        title: $scope.title,
                        message: $scope.message,
                        productId: $scope.productId
                    });
                $scope.modalInstance.dismiss('cancel');
            };

            $scope.cancel=function()
            {
                $scope.modalInstance.dismiss('cancel');

            }
        }]);