/**
 * Created by andres on 23/06/16.
 */
angular.module('PubApp')
    .controller('EventsViewController', ['$scope','$stateParams',"serviceFactory","$sce",
        function($scope,$stateParams,serviceFactory,$sce) {
            var id=$stateParams.id;

            serviceFactory.getService("events/"+id).get(function(response){
               $scope.event=response.data;
            });

            $scope.dataEvent = {
                name: "events/"+id+"/eventProducts",
                display: "Productos Evento",
                autoInit: true,
                readPermission:false,
                fields: [
                    {
                        name: 'store_product.product.name',
                        title: 'Nombre',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'text',

                        }

                    },
                    {
                        name: 'store_product.product.serial',
                        title: 'Serial',
                        form :{
                            type: 'input',
                            format :'text',
                            required:false
                        }

                    },
                    {
                        name: 'store_product.product.lot',
                        title: 'Lote',
                        form :{
                            type: 'input',
                            format :'text',
                            required:false
                        }

                    },
                    {
                        name: 'store_product.product.image',
                        title: 'Imagen',
                        display: {
                            type: 'image'
                        },
                        form :{
                            type: 'file',
                        }

                    },{
                        name: 'store_product.quantity',
                        title: 'Cantidad',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'number',
                            required:false

                        }

                    } ,{
                        name: 'store_product.price',
                        title: 'Precio ($)',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'number'
                        }

                    },{
                        name: 'store_product.status',
                        title: 'Estado',
                        display: {
                            type: 'html',
                            html:function(value){
                                if (value)
                                    return $sce.trustAsHtml('<i class="fa fa-check"></i>');
                                else
                                    return $sce.trustAsHtml('<i class="fa fa-times"></i>');                            }
                        },
                        form :{
                            type: 'select',
                            config: {
                                source: 'local',
                                name: 'statuses',
                                data: [{id: true, name: 'Activo'}, {id: false, name: 'Inactivo'}]
                            }
                        }

                    },{
                        name: 'store_product.product.description',
                        title: 'Descripción',
                        form :{
                            type: 'textarea',
                        }

                    }






                ],

            };



        }]);