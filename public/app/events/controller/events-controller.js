/**
 * Created by andres on 23/06/16.
 */
angular.module('PubApp')
    .controller('EventsController', ['$scope','$state',
        function($scope,$state) {


            $scope.dataView = {
                name: 'events',
                display: "Eventos",
                autoInit: true,
                readPermission:true,
                viewPermission:false,
                updatePermission:true,
                readAction:function(id)
                {
                    console.log(id);
                    $state.go('eventsview',{id:id});
                },
                fields: [
                    {
                        name: 'name',
                        title: 'Nombre',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'text'
                        }

                    },
                    {
                        name: 'date',
                        title: 'Fecha',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'date'
                        }

                    },
                    {
                        name: 'place',
                        title: 'Lugar',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'text'
                        }

                    },
                    {
                        name: 'image',
                        title: 'Imagen',
                        display: {
                            type: 'image'
                        },
                        form :{
                            type: 'file',
                        }

                    },
                    {
                        name: 'description',
                        title: 'Descripción',
                        form :{
                            type: 'textarea',
                        }

                    },





                ],
            };



        }]);