/**
 * Created by AndresFabian on 18/06/2016.
 */
/**
 * Created by AndresFabian on 18/06/2016.
 */
angular.module('PubApp')
    .controller('ZonesController', ['$scope', '$timeout', 'serviceFactory', '$http'
        , function ($scope, $timeout, serviceFactory, $http) {
            var random = function (inferior, superior) {
                numPosibilidades = superior - inferior
                aleat = Math.random() * numPosibilidades
                aleat = Math.floor(aleat)
                return parseInt(inferior) + aleat
            }
            var color = function () {
                hexadecimal = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F")
                color_aleatorio = "#";
                for (i = 0; i < 6; i++) {
                    posarray = random(0, hexadecimal.length)
                    color_aleatorio += hexadecimal[posarray]
                }
                return color_aleatorio
            };
            var setValues = function (latitude, longitude, zoom) {
                $scope.dataView.map.center.lat = latitude;
                $scope.dataView.map.center.lng = longitude;
                $scope.dataView.map.center.zoom = zoom;
                $scope.dataView.map.markers.search.lat = latitude;
                $scope.dataView.map.markers.search.lng = longitude;
                $scope.dataView.map.paths.c1.latlngs.lat = latitude;
                $scope.dataView.map.paths.c1.latlngs.lng = longitude;
                if ($scope.dataView.getScope().editData) {
                    $scope.dataView.getScope().editData.latitude = latitude;
                    $scope.dataView.getScope().editData.longitude = longitude;
                } else {
                    $scope.dataView.newData.latitude = latitude;
                    $scope.dataView.newData.longitude = longitude;
                }

            }

            var searchAdress = function () {
                $http.get("https://maps.googleapis.com/maps/api/geocode/json", {
                    params: {'address': $scope.dataView.newData.address}
                })
                    .then(function (response) {
                            if (response.data.results.length > 0) {
                                var latitude = response.data.results[0].geometry.location.lat,
                                    longitude = response.data.results[0].geometry.location.lng;
                                setValues(latitude, longitude, 16);
                            }
                        }, function (error) {

                        }
                    );
            }

            $scope.dataView = {
                name: 'zones',
                display: "Zonas",
                autoInit: true,
                urlModalCreate: 'templates/zones/create.html',
                urlModalEdit: 'templates/zones/edit.html',
                readPermission: false,
                fields: [
                    {
                        name: 'name',
                        title: 'Nombre',
                        display: {
                            type: 'text'
                        },

                    },
                    {
                        name: 'price',
                        title: 'Precio',
                        display: {
                            type: 'text'
                        },

                    },
                    {
                        name: 'latitude',
                        title: 'Latitud',
                        display: {
                            type: 'text'
                        },
                    },
                    {
                        name: 'longitude',
                        title: 'Longitud',
                        display: {
                            type: 'text'
                        },
                    },


                ],

                searchAdress: searchAdress,
                map: {
                    center: {
                        lat: 0,
                        lng: 0,
                        zoom: 12
                    },
                    defaults: {

                        scrollWheelZoom: true
                    },
                    markers: {
                        search: {
                            lat: 0,
                            lng: 0,
                            message: "Busqueda",
                            draggable: true,
                        }
                    },
                    paths: {
                        c1: {
                            weight: 5,
                            color: '#ff612f',
                            latlngs: {
                                lat: 0,
                                lng: 0
                            },
                            radius: 1,
                            type: 'circle'
                        },
                    }

                }
            };

            $scope.$watch('dataView.newData.radius', function (value) {
                if (value) {
                    $scope.dataView.map.paths.c1.radius = value;
                }
            })



            $scope.$watch('dataView.getScope().editData', function (value) {
                if (value) {
                    setValues($scope.dataView.getScope().editData.latitude, $scope.dataView.getScope().editData.longitude, 12)
                    $scope.editRadius=$scope.dataView.getScope().editData.radius;
                    $scope.$watch('dataView.getScope().editData.radius', function (value) {
                        console.log(value);
                        if (value) {
                            $scope.dataView.map.paths.c1.radius = value;
                        }
                    })
                }
            })

            $scope.$on("leafletDirectiveMarker.dragend", function (event, args) {
                var leafEvent = args.leafletEvent.target,
                    latitude = leafEvent._latlng.lat,
                    longitude = leafEvent._latlng.lng,
                    zoom = leafEvent._map._zoom;
                setValues(latitude, longitude, zoom);

            });

            $scope.map = {
                markers: {},
                layers: {
                    baselayers: {

                        osm: {
                            name: 'OpenStreetMap',
                            url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                            type: 'xyz',
                            "layerParams": {},
                            "layerOptions": {}
                        }
                    },
                    overlays: {}
                }
            };
            $scope.map.height = "600px";
            $scope.map.center = {
                lat: 7.12972, lng: -73.1258, zoom: 13
            };
            $scope.map.paths = {};
            $scope.map.defaults = {

                scrollWheelZoom: true
            };

            var loadZones = function () {
                serviceFactory.getService('zones').get(function (response) {
                    var index=0;
                    angular.forEach(response.data, function (value) {

                        $scope.map.markers[index] = {
                            lat: parseFloat(value.latitude),
                            lng: parseFloat(value.longitude),
                            message: value.name+" $"+value.price,
                            icon: {
                                type: 'awesomeMarker',
                                icon: 'fa-dot-circle-o',
                                prefix: 'fa',
                                markerColor: 'blue'
                            },
                            draggable: false,

                        };
                        $scope.map.paths[index] = {
                            weight: 2,
                            color: color(),
                            latlngs: {
                                lat: parseFloat(value.latitude),
                                lng: parseFloat(value.longitude)
                            },
                            radius: parseFloat(value.radius),
                            type: 'circle'
                        };

                        index++;
                    });
                });

            };

            loadZones();
        }]);
