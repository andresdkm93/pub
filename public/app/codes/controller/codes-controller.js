/**
 * Created by andres on 23/06/16.
 */
angular.module('PubApp')
    .controller('CodesController', ['$scope',
        function($scope) {


            $scope.dataView = {
                name: 'codes',
                display: "Codigos promocionales",
                autoInit: true,
                readPermission:false,
                viewPermission:false,
                fields: [
                    {
                        name: 'code',
                        title: 'Codigo',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'text'
                        }

                    }, {
                        name: 'discount',
                        title: 'Descuento (%)',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'number'
                        }

                    },{
                        name: 'quantity',
                        title: 'Cantidad',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'number'
                        }

                    },{
                        name: 'start_date',
                        title: 'Fecha de inicio',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'date'
                        }

                    },{
                        name: 'end_date',
                        title: 'Fecha de fin',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'date'
                        }

                    },

                    
                ],
            };



        }]);