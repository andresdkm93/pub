/**
 * Created by andres on 23/06/16.
 */
angular.module('PubApp')
    .controller('UsersController', ['$scope', '$uibModal', '$http','$sce',
        function($scope, $uibModal, $http,$sce) {


            $scope.dataView = {
                name: 'users',
                display: "Usuarios",
                autoInit: true,
                readPermission:false,
                viewPermission:false,
                updatePermission:false,
                fields: [

                    {
                        name: 'id',
                        title: 'Cedula',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'number'
                        }

                    },
                    {
                        name: 'name',
                        title: 'Nombre',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'text'
                        }

                    },
                    {
                        name: 'email',
                        title: 'Email',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'email'
                        }

                    },

                    {
                        name: 'password',
                        title: 'Contraseña',
                        form :{
                            type: 'input',
                            format :'text'
                        }

                    },




                ],
            };



        }]);