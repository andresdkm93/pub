/**
 * Created by andres on 23/06/16.
 */
angular.module('PubApp')
    .controller('PrinterController', ['$scope',
        function ($scope) {


            $scope.dataView = {
                name: 'printer',
                display: "Configuración impresora",
                autoInit: true,
                createPermission: false,
                readPermission: false,
                viewPermission: false,
                deletePermission: false,
                fields: [
                    {
                        name: 'value',
                        title: 'Ancho de papel',
                        display: {
                            type: 'text'
                        },
                        form: {
                            type: 'input',
                            format: 'number'
                        }
                    }
                ],
            };
        }]);