ServiceFactory.$inject = ['$resource', 'baseURL'];
function ServiceFactory($resource, baseURL)
{

    this.getService = function (entityName, params)
    {
        if (params)
            return $resource(baseURL + entityName, params,
                    {
                        'update': {method: 'PUT'}
                        
                    });
        else
            return $resource(baseURL + entityName, null,
                    {
                        'update': {method: 'PUT'}
                       
                    });

    };
}
;
angular.module('entities').service("serviceFactory", ServiceFactory);