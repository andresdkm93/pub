/**
 * Created by AndresFabian on 28/08/2016.
 */
angular.module('PubApp').factory("$firebaseFactory",function(){
    var config = {
        apiKey: "AIzaSyDugw8eyFAWmTvSRYU6asrZGoKTSg5DHG8",
        authDomain: "pubjjj-1363.firebaseapp.com",
        databaseURL: "https://pubjjj-1363.firebaseio.com",
        storageBucket: "pubjjj-1363.appspot.com",
    };
    firebase.initializeApp(config);

    return firebase.database().ref('orders');
});