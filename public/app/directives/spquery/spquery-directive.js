/**
 * Created by andres on 19/04/16.
 */
/**
 * Created by AndresFabian on 24/02/2016.
 */
angular.module('PubApp')
    .factory("storageHandler", function () {
        return {

            check: function (name) {
                return sessionStorage[name] == null;
            },
            get: function (name) {
                if (sessionStorage[name]) {
                    return angular.fromJson(sessionStorage[name])
                } else {
                    return null;
                }
            },
            save: function (name, object) {
                if (!sessionStorage[name]) {
                    sessionStorage[name] = angular.toJson(object);
                }
            }

        }
    })
    .directive('dynamicModel', ['$compile', function ($compile) {
        return {
            'link': function (scope, element, attrs) {
                scope.$watch(attrs.dynamicModel, function (dynamicModel) {
                    if (attrs.ngModel == dynamicModel || !dynamicModel) return;

                    element.attr('ng-model', dynamicModel);
                    if (dynamicModel == '') {
                        element.removeAttr('ng-model');
                    }

                    // Unbind all previous event handlers, this is necessary to remove previously linked models.
                    element.unbind();
                    $compile(element)(scope);
                });
            }
        };
    }])
    .directive('spQuery', [function () {
        return {
            restrict: "E",
            replace: true,
            scope: {
                config: "=",
            },
            templateUrl: "/public/app/directives/spquery/index.html",
            controller: [
                '$scope',
                'serviceFactory',
                '$uibModal',
                '$parse',
                '$filter',
                '$http',
                '$timeout',
                '$state',
                '$q',
                'storageHandler',
                '$rootScope',
                'FileUploader',
                'baseURL',
                function ($scope,
                          serviceFactory,
                          $uibModal,
                          $parse,
                          $filter,
                          $http,
                          $timeout,
                          $state,
                          $q,
                          storageHandler,
                          $rootScope,
                          FileUploader,
                          baseURL) {

                    $scope.config.getScope = function () {
                        return $scope;
                    }

                    var getStorageData = function (expression, data, name) {
                        var result = $filter('filter')(storageHandler.get(name), {id: $parse(expression)(data)});
                        if (result!=null && result.length > 0) {
                            return result[0].name;
                        } else
                            return " ";
                    };
                    var getSimpleData = function (expression, data) {
                        if (Array.isArray(expression)) {
                            var c = "";
                            angular.forEach(expression, function (value) {
                                c += $parse(value)(data) + " ";
                            });
                            return c;
                        } else {
                            return $parse(expression)(data);
                        }
                    };
                    var openModalCreate = function () {

                        $scope.config.newData = {};
                        $scope.rigthFields = [];
                        $scope.leftFields = [];
                        var i = 1;
                        angular.forEach($scope.formFields, function (value) {
                            if (value.sourceName) {
                                value.data = storageHandler.get(value.sourceName);
                            }
                            if (i % 2 == 0) {
                                $scope.rigthFields.push(value);
                            } else {
                                $scope.leftFields.push(value);
                            }
                            i++;
                        });

                        var countfile = $filter('filter')($scope.formFields, {type: 'file'});
                        if (countfile.length > 0) {
                            $scope.upload = true;
                            $scope.uploader = new FileUploader({
                                url: baseURL + 'updateResource'
                            });

                            $scope.uploader.onBeforeUploadItem = function (item) {
                                $rootScope.loading = true;
                                //console.info('onBeforeUploadItem', item);
                            };

                            $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                                $rootScope.loading = false;
                                //console.info('onSuccessItem', fileItem, response, status, headers);
                                $scope.config.newData.image = response.data.url;
                            };

                            $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
                                $rootScope.loading = false;

                            };
                            $scope.uploader.onCancelItem = function (fileItem, response, status, headers) {
                                $rootScope.loading = false;

                                //console.info('onCancelItem', fileItem, response, status, headers);
                            };

                        }


                        var countfile2 = $filter('filter')($scope.formFields, {type: 'banner'});
                        if (countfile2.length > 0) {
                            $scope.upload = true;
                            $scope.uploader2 = new FileUploader({
                                url: baseURL + 'updateResource'
                            });

                            $scope.uploader2.onBeforeUploadItem = function (item) {
                                $rootScope.loading = true;
                                //console.info('onBeforeUploadItem', item);
                            };

                            $scope.uploader2.onSuccessItem = function (fileItem, response, status, headers) {
                                $rootScope.loading = false;
                                //console.info('onSuccessItem', fileItem, response, status, headers);
                                $scope.config.newData.banner = response.data.url;
                            };

                            $scope.uploader2.onErrorItem = function (fileItem, response, status, headers) {
                                $rootScope.loading = false;

                            };
                            $scope.uploader2.onCancelItem = function (fileItem, response, status, headers) {
                                $rootScope.loading = false;

                                //console.info('onCancelItem', fileItem, response, status, headers);
                            };

                        }


                        if (config.dictionary) {
                            serviceFactory.getService($scope.config.dictionaryName).get(function (response) {
                                $scope.dinamycFieldsData = [];
                                $scope.newAgentAdittionalData = [];
                                angular.forEach(response.data, function (value) {
                                    value.field = config.parseTypeFields[value.field_type_id];
                                    $scope.dinamycFieldsData.push(value);
                                });
                                $scope.modal = $uibModal.open({
                                    animation: true,
                                    templateUrl: config.urlModalCreate,
                                    scope: $scope,
                                    size: 'lg'
                                });

                            });
                        } else {
                            $scope.modal = $uibModal.open({
                                animation: true,
                                templateUrl: config.urlModalCreate,
                                scope: $scope,
                                size: 'lg'
                            });
                        }

                    };
                    var openModalEdit = function (id) {
                        $scope.newAgentAdittionalData = {};
                        serviceFactory.getService($scope.config.name + "/" + id).get(function (response) {
                            if (response.data.start_date != null)
                                response.data.start_date = new Date(response.data.start_date);
                            if (response.data.end_date != null)
                                response.data.end_date = new Date(response.data.end_date);
                            $scope.editData = response.data;
                            console.log($scope.editData);

                            $scope.rigthFields = [];
                            $scope.leftFields = [];
                            var i = 1;
                            if (response.data.data) {
                                angular.forEach(response.data.data, function (value) {
                                    if (value.field.field_type_id == "4") {
                                        $scope.newAgentAdittionalData[value.field.id] = Number(value.value);
                                    } else {
                                        $scope.newAgentAdittionalData[value.field.id] = value.value;
                                    }
                                });
                            }
                            angular.forEach($scope.formFields, function (value) {
                                if (value.sourceName) {
                                    value.data = storageHandler.get(value.sourceName);
                                }
                                if (i % 2 == 0) {
                                    $scope.rigthFields.push(value);
                                } else {
                                    $scope.leftFields.push(value);
                                }
                                i++;
                            });

                            var countfile = $filter('filter')($scope.formFields, {type: 'file'});
                            if (countfile.length > 0) {
                                $scope.upload = true;
                                $scope.uploader = new FileUploader({
                                    url: baseURL + 'updateResource'
                                });

                                $scope.uploader.onBeforeUploadItem = function (item) {
                                    $rootScope.loading = true;
                                    //console.info('onBeforeUploadItem', item);
                                };

                                $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                                    $rootScope.loading = false;
                                    $scope.editData.image = response.data.url;


                                };

                                $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
                                    $rootScope.loading = false;

                                };
                                $scope.uploader.onCancelItem = function (fileItem, response, status, headers) {
                                    $rootScope.loading = false;

                                    //console.info('onCancelItem', fileItem, response, status, headers);
                                };
                            }


                            var countfile2 = $filter('filter')($scope.formFields, {type: 'banner'});
                            if (countfile2.length > 0) {
                                $scope.upload = true;
                                $scope.uploader2 = new FileUploader({
                                    url: baseURL + 'updateResource'
                                });

                                $scope.uploader2.onBeforeUploadItem = function (item) {
                                    $rootScope.loading = true;
                                    //console.info('onBeforeUploadItem', item);
                                };

                                $scope.uploader2.onSuccessItem = function (fileItem, response, status, headers) {
                                    $rootScope.loading = false;
                                    //console.info('onSuccessItem', fileItem, response, status, headers);
                                    $scope.editData.banner = response.data.url;
                                };

                                $scope.uploader2.onErrorItem = function (fileItem, response, status, headers) {
                                    $rootScope.loading = false;

                                };
                                $scope.uploader2.onCancelItem = function (fileItem, response, status, headers) {
                                    $rootScope.loading = false;

                                    //console.info('onCancelItem', fileItem, response, status, headers);
                                };

                            }
                            if (config.dictionary) {
                                serviceFactory.getService($scope.config.dictionaryName).get(function (response) {
                                    $scope.dinamycFieldsData = [];
                                    angular.forEach(response.data, function (value) {
                                        value.field = config.parseTypeFields[value.field_type_id];
                                        $scope.dinamycFieldsData.push(value);
                                    });

                                    $scope.modal = $uibModal.open({
                                        animation: true,
                                        templateUrl: config.urlModalEdit,
                                        scope: $scope,
                                        size: 'lg'
                                    });

                                });
                            } else {
                                $scope.modal = $uibModal.open({
                                    animation: true,
                                    templateUrl: config.urlModalEdit,
                                    scope: $scope,
                                    size: 'lg'
                                });
                            }

                        });
                    };
                    $scope.nativeOpenModalEdit=function (id ,urlTemplate) {
                        $scope.urlTemplate=urlTemplate;
                        $scope.newAgentAdittionalData = {};
                        serviceFactory.getService($scope.config.name + "/" + id).get(function (response) {
                            if (response.data.start_date != null)
                                response.data.start_date = new Date(response.data.start_date);
                            if (response.data.end_date != null)
                                response.data.end_date = new Date(response.data.end_date);
                            $scope.editData = response.data;

                            $scope.rigthFields = [];
                            $scope.leftFields = [];
                            var i = 1;
                            if (response.data.data) {
                                angular.forEach(response.data.data, function (value) {
                                    if (value.field.field_type_id == "4") {
                                        $scope.newAgentAdittionalData[value.field.id] = Number(value.value);
                                    } else {
                                        $scope.newAgentAdittionalData[value.field.id] = value.value;
                                    }
                                });
                            }
                            angular.forEach($scope.formFields, function (value) {
                                if (value.sourceName) {
                                    value.data = storageHandler.get(value.sourceName);
                                }
                                if (i % 2 == 0) {
                                    $scope.rigthFields.push(value);
                                } else {
                                    $scope.leftFields.push(value);
                                }
                                i++;
                            });

                            var countfile = $filter('filter')($scope.formFields, {type: 'file'});
                            if (countfile.length > 0) {
                                $scope.upload = true;
                                $scope.uploader = new FileUploader({
                                    url: baseURL + 'updateResource'
                                });

                                $scope.uploader.onBeforeUploadItem = function (item) {
                                    $rootScope.loading = true;
                                    //console.info('onBeforeUploadItem', item);
                                };

                                $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                                    $rootScope.loading = false;
                                    $scope.editData.image = response.data.url;


                                };

                                $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
                                    $rootScope.loading = false;

                                };
                                $scope.uploader.onCancelItem = function (fileItem, response, status, headers) {
                                    $rootScope.loading = false;

                                    //console.info('onCancelItem', fileItem, response, status, headers);
                                };


                            }
                            if (config.dictionary) {
                                serviceFactory.getService($scope.config.dictionaryName).get(function (response) {
                                    $scope.dinamycFieldsData = [];
                                    angular.forEach(response.data, function (value) {
                                        value.field = config.parseTypeFields[value.field_type_id];
                                        $scope.dinamycFieldsData.push(value);
                                    });

                                    $scope.modal = $uibModal.open({
                                        animation: true,
                                        templateUrl: $scope.urlTemplate,
                                        scope: $scope,
                                        size: 'lg'
                                    });

                                });
                            } else {
                                $scope.modal = $uibModal.open({
                                    animation: true,
                                    templateUrl: $scope.urlTemplate,
                                    scope: $scope,
                                    size: 'lg'
                                });
                            }

                        });
                    };

                    var init = function () {
                        $scope.displayTitles = [];
                        $scope.displayFields = [];
                        $scope.formFields = [];
                        $scope.promises = [];
                        angular.forEach($scope.config.fields, function (value) {
                            if (value.display) {
                                if (value.display.config) {
                                    switch (value.display.config.source) {
                                        case 'api':
                                            delete sessionStorage[value.display.config.name];
                                            break;
                                        case 'local':
                                            delete sessionStorage[value.display.config.name];
                                            break;
                                    }
                                }
                            } else if (value.form) {
                                if (value.form.config) {
                                    switch (value.form.config.source) {
                                        case 'api':
                                            delete sessionStorage[value.form.config.name];
                                            break;
                                        case 'local':
                                            delete sessionStorage[value.form.config.name];
                                            break;
                                    }
                                }
                            }
                        });


                        angular.forEach($scope.config.fields, function (value) {
                            if (value.display) {
                                var obj = {
                                    id: value.name,
                                    type: value.display.type,
                                };
                                if (value.display.config) {
                                    if (storageHandler.check(value.display.config.name)) {
                                        switch (value.display.config.source) {
                                            case 'api':
                                                var promise = serviceFactory.getService(value.display.config.name).get(function (response) {
                                                    storageHandler.save(value.display.config.name, response.data);
                                                });
                                                $scope.promises.push(promise);
                                                break;
                                            case 'local':
                                                storageHandler.save(value.display.config.name, value.display.config.data);
                                                break;
                                        }
                                    }

                                    obj.source = value.display.config.source;
                                    obj.sourceName = value.display.config.name;

                                }
                                if (value.display.type === "html") {
                                    obj.getHtml = value.display.html;
                                    if (value.display.action) {
                                        obj.action = value.display.action;

                                    }
                                }


                                $scope.displayTitles.push({
                                    id: value.name,
                                    name: value.title,
                                    width: value.display.width ? value.display.width : null
                                });
                                $scope.displayFields.push(obj);
                            }

                            if (value.form) {
                                if (Array.isArray(value.name)) {
                                    angular.forEach(value.name, function (item) {
                                        $scope.formFields.push({
                                            label: value.title,
                                            name: item,
                                            type: value.form.type,
                                            format: value.form.format,
                                            disabled: value.form.editable,
                                            required: value.form.required ? value.form.required : true,
                                        });
                                    });
                                } else {
                                    var obj = {
                                        label: value.title,
                                        name: value.name,
                                        type: value.form.type,
                                        format: value.form.format,
                                        disabled: value.form.editable,
                                        required: value.form.required != null ? value.form.required : true,
                                        html: value.form.html,

                                    };
                                    if (value.form.type === "html") {
                                        obj.action = value.display.action;
                                        obj.html = value.form.html;
                                    }
                                    if (value.form.config) {
                                        if (storageHandler.check(value.form.config.name)) {
                                            switch (value.form.config.source) {
                                                case 'api':
                                                    var promise = serviceFactory.getService(value.form.config.name).get(function (response) {
                                                        storageHandler.save(value.form.config.name, response.data);
                                                    });
                                                    $scope.promises.push(promise);
                                                    break;
                                                case 'local':
                                                    storageHandler.save(value.form.config.name, value.form.config.data);
                                                    break;
                                            }
                                        }
                                        obj.sourceName = value.form.config.name;

                                    }
                                    $scope.formFields.push(obj);
                                }


                            }


                        });

                        $q.all($scope.promises).then(function () {
                            $scope.config.query();
                        });
                    };
                    var config = {
                        dictionary: $scope.config.dictionary ? $scope.config.dictionary : false,
                        autoInit: $scope.config.autoInit != undefined ? $scope.config.autoInit : true,
                        urlModalCreate: $scope.config.urlModalCreate ? $scope.config.urlModalCreate : "/public/app/directives/spquery/create.html",
                        urlModalEdit: $scope.config.urlModalEdit ? $scope.config.urlModalEdit : "/public/app/directives/spquery/edit.html",
                        openModalCreate: $scope.config.openModalCreate ? $scope.config.openModalCreate : openModalCreate,
                        parseTypeFields: {
                            1: {type: 'input', format: 'text'},
                            2: {type: 'input-date'},
                            3: {type: 'input-hour'},
                            4: {type: 'input', format: 'number'},
                            5: {type: 'select-multiple'},
                            6: {type: 'select'},
                            7: {type: 'image'},
                            8: {type: 'textarea'},
                            9: {type: 'banner'},
                        }

                    };
                    $scope.config.create = function () {
                        if ($scope.upload && $scope.uploader.queue.length > 0) {
                            $scope.uploader.uploadAll();
                            $scope.uploader.onCompleteAll = function () {
                                if($scope.uploader2 && $scope.uploader2.queue.length > 0){
                                    $scope.uploader2.uploadAll();
                                    $scope.uploader2.onCompleteAll = function () {
                                        $scope.config.getAdittionalData();
                                        serviceFactory.getService($scope.config.name).save($scope.config.newData, function (response) {
                                            $scope.modal.dismiss('cancel');
                                            $timeout(function () {
                                                $scope.config.query();
                                                $scope.config.newData = undefined;
                                            }, 1000);
                                        });
                                    };
                                }else{
                                    $scope.config.getAdittionalData();
                                    serviceFactory.getService($scope.config.name).save($scope.config.newData, function (response) {
                                        $scope.modal.dismiss('cancel');
                                        $timeout(function () {
                                            $scope.config.query();
                                            $scope.config.newData = undefined;
                                        }, 1000);
                                    });
                                }

                            };
                        }else if($scope.uploader2 && $scope.uploader2.queue.length > 0){
                            $scope.uploader2.uploadAll();
                            $scope.uploader2.onCompleteAll = function () {
                                $scope.config.getAdittionalData();
                                serviceFactory.getService($scope.config.name).save($scope.config.newData, function (response) {
                                    $scope.modal.dismiss('cancel');
                                    $timeout(function () {
                                        $scope.config.query();
                                        $scope.config.newData = undefined;
                                    }, 1000);
                                });
                            };
                        }
                        else {
                            $scope.config.getAdittionalData();
                            serviceFactory.getService($scope.config.name).save($scope.config.newData, function (response) {
                                $scope.modal.dismiss('cancel');
                                $timeout(function () {
                                    $scope.config.query();
                                    $scope.config.newData = undefined;
                                }, 1000);
                            });
                        }


                    }


                    $scope.config.read = function (id) {
                        $state.go($scope.config.name + '.view', {id: id});
                    }
                    $scope.config.update = function (id) {

                        if ($scope.upload && $scope.uploader.queue.length > 0) {
                            $scope.uploader.uploadAll();
                            $scope.uploader.onCompleteAll = function () {
                                if($scope.uploader2 && $scope.uploader2.queue.length > 0){
                                    $scope.uploader2.uploadAll();
                                    $scope.uploader2.onCompleteAll = function () {
                                        if (config.dictionary) {
                                            $scope.editData.data = [];
                                            angular.forEach($scope.newAgentAdittionalData, function (value, key) {
                                                var obj = {};
                                                obj[$scope.config.dictionaryKey + '_id'] = $scope.editData.id;
                                                obj[$scope.config.dictionaryKey + '_field_id'] = key;
                                                obj.value = value;
                                                $scope.editData.data.push(obj);
                                            });
                                        }

                                        serviceFactory.getService($scope.config.name + "/" + $scope.editData.id).update($scope.editData, function (response) {
                                            $scope.modal.dismiss('cancel');
                                            $timeout(function () {
                                                $scope.config.query();
                                            }, 1000);
                                        });
                                    };
                                }else{

                                    if (config.dictionary) {
                                        $scope.editData.data = [];
                                        angular.forEach($scope.newAgentAdittionalData, function (value, key) {
                                            var obj = {};
                                            obj[$scope.config.dictionaryKey + '_id'] = $scope.editData.id;
                                            obj[$scope.config.dictionaryKey + '_field_id'] = key;
                                            obj.value = value;
                                            $scope.editData.data.push(obj);
                                        });
                                    }

                                    serviceFactory.getService($scope.config.name + "/" + $scope.editData.id).update($scope.editData, function (response) {
                                        $scope.modal.dismiss('cancel');
                                        $timeout(function () {
                                            $scope.config.query();
                                        }, 1000);
                                    });
                                }

                            };
                            {

                            }
                        } else if($scope.uploader2 && $scope.uploader2.queue.length > 0){
                            $scope.uploader2.uploadAll();
                            $scope.uploader2.onCompleteAll = function () {
                                if (config.dictionary) {
                                    $scope.editData.data = [];
                                    angular.forEach($scope.newAgentAdittionalData, function (value, key) {
                                        var obj = {};
                                        obj[$scope.config.dictionaryKey + '_id'] = $scope.editData.id;
                                        obj[$scope.config.dictionaryKey + '_field_id'] = key;
                                        obj.value = value;
                                        $scope.editData.data.push(obj);
                                    });
                                }
                                serviceFactory.getService($scope.config.name + "/" + $scope.editData.id).update($scope.editData, function (response) {
                                    $scope.modal.dismiss('cancel');
                                    $timeout(function () {
                                        $scope.config.query();
                                    }, 1000);
                                });
                            };
                        }else {
                            if (config.dictionary) {
                                $scope.editData.data = [];
                                angular.forEach($scope.newAgentAdittionalData, function (value, key) {
                                    var obj = {};
                                    obj[$scope.config.dictionaryKey + '_id'] = $scope.editData.id;
                                    obj[$scope.config.dictionaryKey + '_field_id'] = key;
                                    obj.value = value;
                                    $scope.editData.data.push(obj);
                                });
                            }

                            serviceFactory.getService($scope.config.name + "/" + $scope.editData.id).update($scope.editData, function (response) {
                                $scope.modal.dismiss('cancel');
                                $timeout(function () {
                                    $scope.config.query();
                                }, 1000);
                            });
                        }


                    }
                    $scope.config.delete = function (id) {

                        var modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: "/public/app/directives/spquery/delete.html",
                            controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
                                $scope.ok = function () {
                                    $uibModalInstance.close();
                                }
                                $scope.cancel = function () {
                                    $uibModalInstance.dismiss('cancel');
                                }
                            }],
                            size: 'sm'
                        });
                        modalInstance.result.then(function () {
                            serviceFactory.getService($scope.config.name + "/" + id).delete(function (response) {
                                $timeout(function () {
                                    $scope.config.query();
                                }, 1000);

                            });
                        }, function () {
                        });

                    };
                    $scope.config.getAdittionalData = function () {
                        if (config.dictionary) {
                            $scope.config.newData.data = [];
                            angular.forEach($scope.newAgentAdittionalData, function (value, key) {
                                var obj = {};
                                obj[$scope.config.dictionaryKey + '_id'] = $scope.config.newData.id;
                                obj[$scope.config.dictionaryKey + '_field_id'] = key;
                                obj.value = value;
                                $scope.config.newData.data.push(obj);
                            });
                            $scope.newAgentAdittionalData = undefined;
                        }
                    };
                    $scope.showImage = function (url) {
                        $scope.modalInstance = $uibModal.open({
                            animation: true,
                            template: '<img src="' + url + '" width="400"></img>',
                            size: 'sm',
                            scope: $scope

                        });
                    }
                    if (!$scope.config.query) {
                        $scope.config.query = function (page) {
                            if ($scope.textSearch != null && $scope.textSearch  && $scope.textSearch != "") {
                                serviceFactory.getService("search/" + $scope.config.name).save({
                                    key: $scope.fieldSearch.name,
                                    value: $scope.textSearch
                                }, function (response) {
                                    $scope.data = response.data;
                                });
                            }else {
                                if(page==null && $scope.config.paginate!=null)
                                {
                                    page=$scope.config.paginate.current_page;
                                }

                                serviceFactory.getService($scope.config.name + (page ? '?page=' + page : '')).get(function (response) {

                                    if (response.data.total!=null) {
                                        var response = response.data;
                                        $scope.data = response.data;
                                        $scope.config.paginate = {
                                            total: response.total,
                                            last_page: response.last_page,
                                            from: response.from,
                                            to: response.to,
                                            current_page: response.current_page
                                        };
                                        $scope.config.paginate.pages = [];
                                        for (i = 1; i <= $scope.config.paginate.last_page; i++) {
                                            $scope.config.paginate.pages.push(i);
                                        }
                                    } else {
                                        $scope.data = response.data;
                                    }


                                });
                            }
                        };

                    }


                    $scope.config.closeModal = function () {
                        $scope.modal.dismiss('cancel');
                        $scope.modal = undefined;
                    }

                    $scope.closeModal = function () {
                        $scope.modal.dismiss('cancel');
                        $scope.modal = undefined;
                    }
                    $scope.readPermission = $scope.config.readPermission != null ? $scope.config.readPermission : true;
                    $scope.createPermission = $scope.config.createPermission != null ? $scope.config.createPermission : true;
                    $scope.updatePermission = $scope.config.updatePermission != null ? $scope.config.updatePermission : true;
                    $scope.deletePermission = $scope.config.deletePermission != null ? $scope.config.deletePermission : true;
                    $scope.config.actions = {
                        read: $scope.config.readAction ? $scope.config.readAction : $scope.config.read,
                        delete: $scope.config.deleteAction ? $scope.config.deleteAction : $scope.config.delete,
                        create: $scope.config.createAction ? $scope.config.createAction : $scope.config.create,
                        update: $scope.config.updateAction ? $scope.config.updateAction : $scope.config.update,
                        openModalEdit: $scope.config.openModalEdit ? $scope.config.openModalEdit : openModalEdit,
                        openModalCreate: $scope.config.openModalCreate ? $scope.config.openModalCreate : openModalCreate,

                    };
                    $scope.getStorageData = function (name) {
                        return storageHandler.get(name);
                    };
                    $scope.getData = function (expression, data, display) {
                        switch (display.source) {
                            case 'api':
                                return getStorageData(expression, data, display.sourceName);
                                break;
                            case 'local':
                                return getStorageData(expression, data, display.sourceName);
                                break;
                            default:
                                return getSimpleData(expression, data);
                                break;
                        }
                    };

                    $scope.read = function (id) {
                        $state.go($scope.config.name + '.view', {id: id});
                    };
                    if ($scope.config.searchFields) {
                        if($scope.fieldSearch==null)
                            $scope.fieldSearch = $scope.config.searchFields[0];
                        $scope.textSearch = "";
                        $scope.oldText = "";
                        $scope.monitor = function () {
                            var watching = $scope.$watch('textSearch', function () {

                                if ($scope.textSearch != null && $scope.textSearch != $scope.oldText && $scope.textSearch != "") {
                                    $scope.oldText = $scope.textSearch;
                                    serviceFactory.getService("search/" + $scope.config.name).save({
                                        key: $scope.fieldSearch.name,
                                        value: $scope.textSearch
                                    }, function (response) {
                                        $scope.data = response.data;
                                        watching();
                                        $timeout(function () {
                                            $scope.monitor();
                                        }, 1500);

                                    });
                                } else if ($scope.textSearch === "" && $scope.oldText != "") {
                                    $scope.config.query();
                                }

                            });
                        };
                        $scope.monitor();
                    }

                    $scope.$parent.$on("paso-data2:query", function () {
                        $timeout(function () {
                            $scope.config.query();
                        }, 1000);
                    });

                    $scope.changueFieldSearch = function (field) {
                        $scope.fieldSearch = field;
                    }
                    $scope.config.init = function () {
                        init();

                    }

                    if ($scope.config.setQuery) {
                        $scope.config.query = $scope.config.setQuery;
                    }


                    if (config.autoInit) {
                        init();
                    }


                }]

        };
    }]);

