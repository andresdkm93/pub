/**
 * Created by andres on 23/06/16.
 */
angular.module('PubApp')
    .controller('CategoriesController', ['$scope','serviceFactory','$sce',
        function($scope,serviceFactory,$sce) {


            $scope.dataView = {
                name: 'categories',
                display: "Categorias",
                autoInit: true,
                readPermission:false,
                viewPermission:false,
                fields: [
                    {
                        name: 'name',
                        title: 'Nombre',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'text'
                        }

                    },{
                        name: 'image',
                        title: 'Imagen',
                        display: {
                            type: 'image'
                        },
                        form :{
                            type: 'file',
                        }

                    },
                    {
                        name: 'banner',
                        title: 'Banner',
                        display: {
                            type: 'image'
                        },
                        form :{
                            type: 'banner',
                        }

                    },
                    {
                        name: 'active',
                        title: 'Categoria Activa',
                        display: {
                            type: 'html',
                            html:function(value){
                                if (value)
                                    return $sce.trustAsHtml('<i class="fa fa-check"></i>');
                                else
                                    return $sce.trustAsHtml('<i class="fa fa-times"></i>');                            }
                        }

                    },

                    
                ],
                buttons :[
                    {
                        icon: 'fa fa-times',
                        option: 'push',
                        title: 'Desactivar',
                        action:function(id){
                            serviceFactory.getService("categories/"+id).update(
                                {
                                    deleted_at: 1,
                                }, function (response) {
                                    $scope.dataView.getScope().config.query();
                                    });
                        }
                    },
                    {
                        icon: 'fa fa-check',
                        option: 'push',
                        title: 'Activar',
                        action:function(id){
                            serviceFactory.getService("categories/"+id).update(
                                {
                                    deleted_at: 0,
                                }, function (response) {
                                    $scope.dataView.getScope().config.query();
                                });
                        }
                    },
                ]
            };



        }]);

