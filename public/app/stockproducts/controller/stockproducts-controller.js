/**
 * Created by andres on 23/06/16.
 */
angular.module('PubApp')
    .controller('StockProductsController', ['$scope','$sce'
        ,function($scope,$sce) {

            $scope.dataView = {
                name: 'stockProducts',
                display: "Productos en stock",
                autoInit: true,
                readPermission:false,
                viewPermission:false,
                updatePermission:true,
                fields: [
                    {
                        name: 'product.name',
                        title: 'Nombre',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'text'
                        }

                    },{
                        name: 'product.category_id',
                        title: 'Categoria',
                        display: {
                            type: 'text',
                            config: {
                                source: 'api',
                                name: 'categories',
                            }
                        },
                        form :{
                            type: 'select',
                            config: {
                                source: 'api',
                                name: 'categories',
                            }
                        }

                    },
                    {
                        name: 'product.serial',
                        title: 'Serial',
                        form :{
                            type: 'input',
                            format :'text',
                            required:false

                        }

                    },
                    {
                        name: 'product.lot',
                        title: 'Lote',
                        form :{
                            type: 'input',
                            format :'text',
                            required:false

                        }

                    },
                    {
                        name: 'product.image',
                        title: 'Imagen',
                        display: {
                            type: 'image'
                        },
                        form :{
                            type: 'file',
                        }

                    },{
                        name: 'quantity',
                        title: 'Cantidad',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'number',
                            required:false

                        }

                    } ,{
                        name: 'price',
                        title: 'Precio ($)',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'number'
                        }

                    },{
                        name: 'start_date',
                        title: 'Fecha de Inicio',
                        form :{
                            type: 'input',
                            format :'date',
                            required:false

                        }

                    },{
                        name: 'end_date',
                        title: 'Fecha de Fin',
                        form :{
                            type: 'input',
                            format :'date',
                            required:false

                        }

                    },{
                        name: 'status',
                        title: 'Estado',
                        display: {
                            type: 'html',
                            html:function(value){
                                if (value)
                                    return $sce.trustAsHtml('<i class="fa fa-check"></i>');
                                else
                                    return $sce.trustAsHtml('<i class="fa fa-times"></i>');                            }
                        },
                        form :{
                            type: 'select',
                            config: {
                                source: 'local',
                                name: 'statuses',
                                data: [{id: true, name: 'Activo'}, {id: false, name: 'Inactivo'}]
                            }
                        }

                    },{
                        name: 'recommended',
                        title: 'Recomendado',
                        display: {
                            type: 'html',
                            html:function(value){
                                if (value)
                                    return $sce.trustAsHtml('<i class="fa fa-check"></i>');
                                else
                                    return $sce.trustAsHtml('<i class="fa fa-times"></i>');                            }
                        },
                        form :{
                            type: 'select',
                            config: {
                                source: 'local',
                                name: 'statuses',
                                data: [{id: true, name: 'Si'}, {id: false, name: 'No'}]
                            }
                        }

                    },{
                        name: 'product.description',
                        title: 'Descripción',
                        form :{
                            type: 'textarea',
                        }

                    }






                ],
                searchFields: [
                    {name: "name", display: "Nombre"},
                    {name: "serial", display: "Serial"}
                ]
            };



        }]);