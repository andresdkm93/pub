/**
 * Created by andres on 23/06/16.
 */
angular.module('PubApp')
    .controller('ProductsController', ['$scope',
        function($scope) {


            $scope.dataView = {
                name: 'products',
                display: "Productos",
                autoInit: true,
                readPermission:false,
                viewPermission:false,
                updatePermission:true,
                fields: [
                    {
                        name: 'name',
                        title: 'Nombre',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'text'
                        }

                    },
                    {
                        name: 'serial',
                        title: 'Serial',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'text',
                            required:false
                        }

                    },
                    {
                        name: 'lot',
                        title: 'Lote',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'text',
                            required:false

                        }

                    },
                    {
                        name: 'image',
                        title: 'Imagen',
                        display: {
                            type: 'image'
                        },
                        form :{
                            type: 'file',
                        }

                    },
                    {
                        name: 'description',
                        title: 'Descripción',
                        form :{
                            type: 'textarea',
                            required:false

                        }

                    },
                    {
                        name: 'category_id',
                        title: 'Categoria',
                        display: {
                            type: 'text',
                            config: {
                                source: 'api',
                                name: 'categories',
                            }
                        },
                        form: {
                            type: 'select',
                            config: {
                                source: 'api',
                                name: 'categories',
                            }

                        }

                    },
                    {
                        name: 'url',
                        title: 'Url',
                        display: {
                            type: 'text'
                        }

                    },




                ],
            };



        }]);