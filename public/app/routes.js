angular.module('PubApp').config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('home', {
                url: '/home',
                templateUrl: "/public/templates/home/index.html",
            })
            .state('users', {
                url: '/users',
                templateUrl: "/public/templates/users/index.html",
                controller: 'UsersController'

            })
            .state('categories', {
                url: '/categories',
                templateUrl: "/public/templates/categories/index.html",
                controller: 'CategoriesController'
            })
            .state('products', {
                url: '/products',
                templateUrl: "/public/templates/products/index.html",
                controller: 'ProductsController'
            })
            .state('promotions', {
                url: '/promotions',
                templateUrl: "/public/templates/promotions/index.html",
                controller: 'PromotionsController'
            })
            .state('stockproducts', {
                url: '/stockproducts',
                templateUrl: "/public/templates/stockproducts/index.html",
                controller: 'StockProductsController'
            })
            .state('promotionproducts', {
                url: '/promotionproducts',
                templateUrl: "/public/templates/promotionproducts/index.html",
                controller: 'PromotionProductsController'
            })
            .state('clients', {
                url: '/clients',
                templateUrl: "/public/templates/clients/index.html",
                controller: 'ClientsController'

            })
            .state('clientsview', {
                url: '/clientsview',
                params: {
                    id: 0,
                },
                templateUrl: "/public/templates/clients/view.html",
                controller: 'ClientsViewController'
            })
            .state('codes', {
                url: '/codes',
                templateUrl: "/public/templates/codes/index.html",
                controller: 'CodesController'
            })
            .state('events', {
                url: '/events',
                templateUrl: "/public/templates/events/index.html",
                controller: 'EventsController'
            })
            .state('shedules', {
                url: '/shedules',
                templateUrl: "/public/templates/shedules/index.html",
                controller: 'ShedulesController'
            })
            .state('printer', {
                url: '/printer',
                templateUrl: "/public/templates/printer/index.html",
                controller: 'PrinterController'
            })
            .state('zones', {
                url: '/zones',
                templateUrl: "/public/templates/zones/index.html",
                controller: 'ZonesController'
            })
            .state('reports', {
                url: '/reports',
                templateUrl: "/public/templates/reports/index.html",
                controller: 'ReportsController'
            })
            .state('eventsview', {
                url: '/eventsview',
                params: {
                    id: 0,
                },
                templateUrl: "/public/templates/events/view.html",
                controller: 'EventsViewController'
            })
        ;
        $urlRouterProvider.otherwise('/home');


    }]);