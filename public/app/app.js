angular.module('PubApp',
        [
            'ui.router',
            'ui.bootstrap',
            'ngMaterial',
            'ngSanitize',
            'entities',
            'basic-directives',
            'angularFileUpload',
            'ui-leaflet',
            'ngToast',
            'ngAudio'
        ])
        .constant('baseURL', 'api/v1/')
        .factory('cordovaInterceptorHttp', ['$q', '$timeout', '$rootScope', function ($q, $timeout, $rootScope) {
                return {
                    // optional method
                    'request': function (config) {
                        $rootScope.loading = true;
                        return config;
                    },
                    // optional method
                    'requestError': function (rejection) {
                        $rootScope.loading = false;
                        return $q.reject(rejection);
                    },
                    // optional method
                    'response': function (response) {
                        $rootScope.loading = false;
                        return response;
                    },
                    // optional method
                    'responseError': function (rejection) {
                        $rootScope.loading = false;
                        return $q.reject(rejection);
                    }
                };
            }])
        .config(['$httpProvider','$mdThemingProvider',function ($httpProvider,$mdThemingProvider) {
                $httpProvider.interceptors.push('cordovaInterceptorHttp');
                $mdThemingProvider.theme('default');
            }]);