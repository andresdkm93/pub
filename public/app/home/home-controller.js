angular.module('PubApp')
    .controller("HomeController", ["$scope", "serviceFactory", "$uibModal", "$sce", "$firebaseFactory", "ngAudio", "ngToast", "$timeout", "$http"
        , function ($scope, serviceFactory, $uibModal, $sce, $firebaseFactory, ngAudio, ngToast, $timeout, $http) {
            mapInstance = function () {
                $scope.init = false;
                this.markers = {
                    licorera: {
                        lat: 7.0685091,
                        lng: -73.1044817,
                        message: "Pub"
                    }
                };
                this.center = {
                    lat: 7.12972, lng: -73.1258, zoom: 13
                };
                this.defaults = {
                    maxZoom: 18,
                    minZoom: 1,
                    doubleClickZoom: true,
                    scrollWheelZoom: true,
                    attributionControl: true,
                    worldCopyJump: true,
                    fadeAnimation: true
                };
                this.layers = {
                    baselayers: {
                        googleRoadmap: {
                            name: 'Google Streets',
                            layerType: 'ROADMAP',
                            type: 'google',

                        },
                        googleTerrain: {
                            name: 'Google Terrain',
                            layerType: 'TERRAIN',
                            type: 'google',

                        },
                        googleHybrid: {
                            name: 'Google Hybrid',
                            layerType: 'HYBRID',
                            type: 'google',

                        },

                    },
                    overlays: {
                        'new': {
                            name: "Nuevos",
                            type: "markercluster",
                            visible: true
                        }
                    },
                };
                this.controls = {};
                this.paths = {};


            };
            $scope.map = new mapInstance();
            $scope.new = {};
            $scope.route = [];
            $scope.getValues = function () {
                serviceFactory.getService("productValues")
                    .get(function (response) {
                        $scope.values = response.data;
                    });
            }


            $firebaseFactory.on('child_added', function (data) {
                var order = data.val();
                $scope.new[order.id] = order;
                if ($scope.init && order.order_status_id != 5) {
                    ngToast.create({
                        className: 'info',
                        content: '<div class="row">' +
                        '<div class="col-sm-3">' +
                        '<img  src="img/logo.png" class=" img-responsive" />' +
                        '</div>' +
                        '<div class="col-sm-9">' +
                        '<span><b>Nuevo Domicilio</b></span>' +
                        '<h4><b>' + order.address + '</b></h4>' +
                        '<h5><i>$ ' + order.total + '</i></h5>' +
                        '</div>' +
                        '</div>',
                        dismissOnTimeout: true,
                        timeout: 6000,
                        dismissOnClick: true,
                        combineDuplications: true,
                        compileContent: true,
                    });
                    var sound = ngAudio.load("public/audio/campana.mp3");
                    sound.play();

                }
                $scope.map.markers[order.id] = {
                    lat: parseFloat(order.latitude),
                    lng: parseFloat(order.longitude),
                    icon: {
                        type: 'awesomeMarker',
                        icon: 'fa-shopping-bag',
                        prefix: 'fa',
                        markerColor: "blue",
                        color: "#FFFFFF"
                    },
                    getMessageScope: function () {
                        return $scope;
                    },
                    message: '<div><button type="button" class="btn btn-success" ng-click="openOrder(' + order.id + ')">Ver Pedido</button></div>',
                    layer: "new",
                    draggable: false
                }
                $scope.$apply();
            });

            $firebaseFactory.on('child_changed', function (data) {
                var order = data.val();
                if (order.order_status_id == 5) {
                    $scope.new[order.id] = order;
                    $scope.map.markers[order.id].icon.markerColor = "danger";
                    if ($scope.init) {
                        ngToast.create({
                            className: 'danger',
                            content: '<div class="row">' +
                            '<div class="col-sm-3">' +
                            '<img  src="img/logo.png" class=" img-responsive" />' +
                            '</div>' +
                            '<div class="col-sm-9">' +
                            '<span><b>Pedido cancelado por el usuario</b></span>' +
                            '<h4><b>' + order.address + '</b></h4>' +
                            '<h5><i>$ ' + order.total + '</i></h5>' +
                            '</div>' +
                            '</div>',
                            dismissOnTimeout: true,
                            timeout: 6000,
                            dismissOnClick: true,
                            combineDuplications: true,
                            compileContent: true,
                        });
                        var sound = ngAudio.load("audio/campana.mp3");
                        sound.play();
                    }
                } else {
                    $scope.new[order.id] = order;
                    $scope.map.markers[order.id].icon.markerColor = "orange";
                }

                $scope.$apply();

            });

            $firebaseFactory.on('child_removed', function (data) {
                var order = data.val();
                delete $scope.new[order.id];
            });
            $scope.viewOrder = function (id) {
                $scope.map.center.lat = $scope.map.markers[id].lat;
                $scope.map.center.lng = $scope.map.markers[id].lng;
                $scope.map.center.zoom = 18;
            };
            $scope.openOrder = function (id) {
                serviceFactory.getService("orders/" + id)
                    .get(function (response) {
                        $scope.order = response.data;
                        $scope.modal = $uibModal.open({
                            animation: true,
                            templateUrl: "/public/templates/home/viewOrder.html",
                            scope: $scope,
                            size: 'lg'
                        });
                    });
            };

            $scope.sendOrder = function (id) {
                serviceFactory.getService("orders/" + id)
                    .update({
                        order_status_id: 2
                    }, function (response) {
                        $scope.getValues();
                        $scope.dataViewOrders.query();
                        $scope.modal.close();
                    });
            };

            $scope.printOrder = function (id) {
                serviceFactory.getService("orders/printer/" + id)
                    .get(function (response) {
                        var data = response.data;
                        let w = 400;
                        let h = 500;
                        let top = 50;
                        let left = 100;
                        let html = '<html mozNoMarginBoxes><head><style>';
                        html += '* {margin:0;padding:0}';
                        html += '@page {size: auto; margin: 0mm;}';
                        html += 'pre {width: ' + data.width + 'ch;}';
                        html += '</style></head><body onload="window.print()">';
                        html += '<pre>' + data.html + '</pre>';
                        html += '</body></html>';
                        let wnd = window.open('about:blank',
                            '',
                            'toolbar=no,location=no,scrollbars=yes,directories=no,status=no,menubar=no,copyhistory=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left);
                        wnd.document.write(html);
                        wnd.document.close();
                        wnd.resizeTo(w, h);
                        wnd.moveTo(left, top);
                        wnd.focus();
                        wnd.print();
                        wnd.close();
                    });
            }

            $scope.completeOrder = function (id) {
                serviceFactory.getService("orders/" + id)
                    .update({
                        order_status_id: 3
                    }, function (response) {
                        $scope.getValues();
                        $scope.dataViewOrders.query();
                        $scope.modal.close();

                    });
            }

            $scope.removeOrder = function (id) {
                serviceFactory.getService("orders/" + id)
                    .update({
                        order_status_id: 5
                    }, function (response) {
                        $scope.getValues();
                        $scope.dataViewOrders.query();
                        $scope.modal.close();

                    });
            };

            $scope.cancelOrder = function (id) {
                serviceFactory.getService("orders/" + id)
                    .update({
                        order_status_id: 4
                    }, function (response) {
                        $scope.getValues();
                        $scope.dataViewOrders.query();
                        $scope.modal.close();

                    });
            };

            $scope.dataViewOrders = {
                name: 'orders',
                display: "",
                autoInit: true,
                readPermission: false,
                viewPermission: false,
                createPermission: false,
                updatePermission: false,
                deletePermission: false,

                fields: [
                    {
                        name: 'id',
                        title: 'Pedido #',
                        display: {
                            type: 'text'
                        },
                    }, {
                        name: 'total',
                        title: 'Valor ($)',
                        display: {
                            type: 'text'
                        },

                    }, {
                        name: 'address',
                        title: 'Direccion',
                        display: {
                            type: 'text'
                        },

                    },{
                        name: 'qualification',
                        title: 'Calificación',
                        display: {
                            type: 'text'
                        },

                    }, {
                        name: 'order_status_id',
                        title: 'Estado',
                        display: {
                            type: 'html',
                            html: function (value) {
                                switch (value) {
                                    case 1:
                                        return $sce.trustAsHtml('<span class="label label-info">Nuevo</span>');
                                        break;
                                    case 2:
                                        return $sce.trustAsHtml('<span class="label label-warning">Enviado</span>');
                                        break;
                                    case 3:
                                        return $sce.trustAsHtml('<span class="label label-success">Completado</span>');
                                        break;
                                    case 4:
                                        return $sce.trustAsHtml('<span class="label label-danger">Cancelado</span>');
                                        break;
                                    case 5:
                                        return $sce.trustAsHtml('<span class="label label-danger">Cancelado por el usuario</span>');
                                        break;
                                }

                            }
                        },

                    },
                    {
                        name: 'created_at',
                        title: 'Fecha',
                        display: {
                            type: 'text'
                        },

                    }


                ],
                buttons: [
                    {
                        icon: 'fa fa-eye',
                        title: 'Ver',
                        action: function (id) {
                            $scope.openOrder(id);
                        }
                    }]
            };

            $scope.getValues();
            $timeout(function () {
                $scope.init = true;
            }, 5000);

        }]);
