/**
 * Created by andres on 23/06/16.
 */
angular.module('PubApp')
    .controller('PromotionsController', ['$scope', '$uibModal', 'FileUploader', '$rootScope', 'baseURL', 'serviceFactory','$filter','$sce',
        function ($scope, $uibModal, FileUploader, $rootScope, baseURL, serviceFactory,$filter,$sce) {

            $scope.closeModal = function () {
                $scope.modal.dismiss('cancel');
                $scope.modal = undefined;
            }

            $scope.closeSecondModal=function () {
                $scope.dataView.getScope().modal.dismiss('cancel');
            }
            var openModal = function () {
                $scope.dataView.newData = {};
                $scope.modal = $uibModal.open({
                    animation: true,
                    templateUrl: "/public/templates/promotions/typePromotion.html",
                    scope: $scope,
                    size: 'lg'
                });
            };

            $scope.selectProduct = function (type) {
                $scope.modal.dismiss('cancel');
                $scope.modal = undefined;
                switch (type) {
                    case 2:
                        var url = "/public/templates/promotions/selectProductType2.html";
                        break;
                    case 3:
                        var url = "/public/templates/promotions/selectProductType3.html";
                        break;
                }

                $scope.modal = $uibModal.open({
                    animation: true,
                    templateUrl: url,
                    scope: $scope,
                    size: 'lg'
                });
            }
            $scope.createPromotion = function (type) {
                console.log($scope.dataView);
                $scope.dataView.getScope().upload = true;
                $scope.dataView.getScope().uploader = new FileUploader({
                    url: baseURL + 'updateResource'
                });

                $scope.dataView.getScope().uploader.onBeforeUploadItem = function (item) {
                    $rootScope.loading = true;
                    //console.info('onBeforeUploadItem', item);
                };

                $scope.dataView.getScope().uploader.onSuccessItem = function (fileItem, response, status, headers) {
                    $rootScope.loading = false;
                    //console.info('onSuccessItem', fileItem, response, status, headers);
                    $scope.dataView.newData.image = response.data.url;
                };

                $scope.dataView.getScope().uploader.onErrorItem = function (fileItem, response, status, headers) {
                    $rootScope.loading = false;

                };
                $scope.dataView.getScope().uploader.onCancelItem = function (fileItem, response, status, headers) {
                    $rootScope.loading = false;

                    //console.info('onCancelItem', fileItem, response, status, headers);
                };
                $scope.uploader = $scope.dataView.getScope().uploader;
                switch (type) {
                    case 1:
                        var url = "/public/templates/promotions/type1.html";
                        $scope.dataView.newData.promotion_type = 1;
                        break;
                    case 2:
                        var url = "/public/templates/promotions/type2.html";
                        $scope.dataView.newData.promotion_type = 2;
                        break;
                    case 3:
                        var url = "/public/templates/promotions/type3.html";
                        $scope.dataView.newData.promotion_type = 3;
                        break;
                }
                $scope.modal.dismiss('cancel');
                $scope.modal = undefined;
                $scope.dataView.getScope().modal = $uibModal.open({
                    animation: true,
                    templateUrl: url,
                    scope: $scope,
                    size: 'lg'
                });
            }


            $scope.dataView = {
                name: 'promotions',
                display: "Promociones",
                autoInit: true,
                readPermission: false,
                viewPermission: false,
                updatePermission: true,
                openModalCreate: openModal,
                openModalEdit:function(id)
                {
                    var arr=$filter('filter')($scope.dataView.getScope().data, {id:id});
                    var url="";
                    switch(arr[0].promotion_type){
                        case 1:
                            url = "/public/templates/promotions/edit1.html";
                            break;
                        case 2:
                             url = "/public/templates/promotions/edit2.html";
                            break;
                        case 3:
                            url = "/public/templates/promotions/edit3.html";
                            break;

                    }
                    $scope.dataView.getScope().nativeOpenModalEdit(id,url);
                },
                fields: [
                    {
                        name: 'name',
                        title: 'Nombre',
                        display: {
                            type: 'text'
                        },
                        form: {
                            type: 'input',
                            format: 'text'
                        }

                    },
                    {
                        name: 'image',
                        title: 'Imagen',
                        display: {
                            type: 'image'
                        },
                        form :{
                            type: 'file',
                        }

                    }, {
                        name: 'start_date',
                        title: 'Fecha de Inicio',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'date',
                            required:false

                        }

                    }, {
                        name: 'end_date',
                        title: 'Fecha de Fin',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'date',
                            required:false

                        }
                    },
                    {
                        name: 'store_product.product.url',
                        title: 'Url',
                        display: {
                            type: 'text'
                        }

                    },
                    {
                        name: 'diageo',
                        title: 'Patrocinador',
                        display: {
                            type: 'html',
                            html:function(value){
                                if (value)
                                    return $sce.trustAsHtml('<i class="fa fa-check"></i>');
                                else
                                    return $sce.trustAsHtml('<i class="fa fa-times"></i>');                            }
                        },
                        form :{
                            type: 'select',
                            config: {
                                source: 'local',
                                name: 'diageos',
                                data: [{id: true, name: 'Activo'}, {id: false, name: 'Inactivo'}]
                            }
                        }

                    },
                    {
                        name: 'price',
                        title: 'Precio ($)',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'number'
                        }

                    },
                    {
                        name: 'quantity',
                        title: 'Cantidad',
                        display: {
                            type: 'text'
                        },
                        form :{
                            type: 'input',
                            format :'number'
                        }

                    },{
                        name: 'divider',
                        title: 'Divisor',
                        form :{
                            type: 'input',
                            format :'number'
                        }

                    },{
                        name: 'multiplier',
                        title: 'Multiplicador',
                        form :{
                            type: 'input',
                            format :'number'
                        }

                    },
                    {
                        name: 'quantity_minimal',
                        title: 'Cantidad Minima',
                        form :{
                            type: 'input',
                            format :'number'
                        }

                    },
                    {
                        name: 'discount',
                        title: 'Descuento',
                        form :{
                            type: 'input',
                            format :'number'
                        }

                    }


                ],
            };

            $scope.textSearch = "";

            $scope.search = function () {
                $scope.results = [];
                serviceFactory.getService("search/Products").save({
                    key: "name",
                    value: $scope.textSearch
                }, function (response) {
                    $scope.results = response.data;

                });

            }


        }]);