/**
 * Created by andres on 23/06/16.
 */
angular.module('PubApp')
    .controller('ReportsController', ['$scope',
        function ($scope) {
            $scope.startDate = null;
            $scope.endDate = null;

            var sd = new Date($scope.startDate);
            var ed = new Date($scope.endDate);
            $scope.makeReport = function () {
                if ($scope.startDate && $scope.endDate) {
                    var url = '/reports?start_date=' + encodeURIComponent($scope.startDate.toISOString()) + '&end_date=' + encodeURIComponent($scope.endDate.toISOString());
                    window.open(url, '_blank');
                }
            }
        }]);