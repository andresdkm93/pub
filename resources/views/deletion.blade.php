@extends('master.app')

@section('content')
    <section style="background-color: #303133 !important;margin-top: 143px">
        <br/>
        <div class="content-main">
            <div class="tab-content">
                <div role="tabpanel">
                    <div class="row" style="background-color:rgb(49, 50, 52) !important">
                        <div class="col-lg-4 col-lg-offset-1">
                            <div class="content-searh-result">
                                <h2>Solicitud de eliminación de datos</h2>
                            </div>
                            <p>
                                @if(empty($code))
                                    Tu solicitud no se ha encontrado
                                @else
                                    @if($code->completed == 1)
                                        Tu solicitud se ha procesado correctamente
                                    @else
                                        Tu solicitud se encuentra en espera
                                    @endif
                                @endif
                            </p>
                            <br/>
                            <div class="social">
                                <div class="content-get-app">
                                    <a href="https://itunes.apple.com/co/app/licorera-3jjjs/id1178470906?mt=8"
                                       target="_blank">
                                        <button href="#" class="btn-custom btn-custom-default"><span class="icon"><img
                                                        src="/public/front/assets/SVG-13.svg" alt="AppStore"></span>
                                            AppStore
                                        </button>
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.licorera3jjjs.app"
                                       target="_blank">
                                        <button class="btn-custom btn-custom-default"><span class="icon"><img
                                                        src="/public/front/assets/Assets-SVG-16.svg" alt="PlayStore"></span>
                                            PlayStore
                                        </button>
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <img src="/public/front/img/Assets-IMG-2.png" style="width: 99%">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
@section('scripts')

@endsection
