<li><a ui-sref="home"><i class="fa fa-home"></i><span>Inicio</span></a></li>
<li><a ui-sref="categories"><i class="fa fa-reorder"></i><span>Categorias</span></a></li>
<li><a ui-sref="clients"><i class="fa fa-child"></i><span>Usuarios</span></a></li>
<li><a ui-sref="codes"><i class="fa fa-gift"></i><span>Codigos promocionales</span></a></li>
<li><a ui-sref="events"><i class="fa fa-calendar-plus-o"></i><span>Eventos</span></a></li>
<li><a ui-sref="products"><i class="fa fa-shopping-bag"></i><span>Productos</span></a></li>
<li><a ui-sref="stockproducts"><i class="fa fa-cart-plus"></i><span>Productos en Stock</span></a></li>
<li><a ui-sref="promotionproducts"><i class="fa fa-bell"></i><span>Productos en Promoción</span></a></li>
<li><a ui-sref="promotions"><i class="fa fa-plus-square"></i><span>Promociones</span></a></li>
<li><a ui-sref="users"><i class="fa fa-users"></i><span>Administradores</span></a></li>
<li><a ui-sref="shedules"><i class="fa fa-clock-o"></i><span>Horarios</span></a></li>
<li><a ui-sref="zones"><i class="fa fa-globe"></i><span>Zonas de domicilio</span></a></li>
<li><a ui-sref="printer"><i class="fa fa-print"></i><span>Configuración de Impresora</span></a></li>
<li><a ui-sref="reports"><i class="fa fa-save"></i><span>Reportes</span></a></li>