@extends('master.app')

@section('content')
    <section class="main">
        <div class="content-nav-tabs background-alfa">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-tabs-center container" role="tablist">
                <li role="presentation">
                    <a href="/" aria-controls="search">
                    <span class="icon">
                      <img src="/public/front/assets/Assets-SVG-10.svg" alt="Inicio">
                    </span>
                        Inicio
                    </a>
                </li>
                <li role="presentation" >
                    <a href="/tienda" aria-controls="store">
                    <span class="icon">
                      <img src="/public/front/assets/Assets-SVG-11.svg" alt="Tienda">
                    </span>
                        Tienda
                    </a>
                </li>
                <li role="presentation" class="active">
                    <a href="/">
                    <span class="icon">
                      <img src="/public/front/assets/Assets-SVG-12.svg" alt="Canje">
                    </span>
                        Canje
                    </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="content-main">
                <div class="row">
                    <div class="col-md-12 col-lg-12 title-category">
                        <div class="content-nav-title container" style="padding-left: 180px">
                            <h2>Tenemos muchos más regalos</h2>
                            <p>No olvides canjear tus puntos por estas y más sorpresas<br/>
                            Recuerda que por cada 10.000 pesos en compras acumulas 1 punto</p>
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active container" id="search">
                        <div class="row">
                                <div class="box-product-result">
                                    @foreach ($promotions as $index => $storeProduct)
                                        <div class="col-md-6 col-lg-6 content-product">
                                            <div class="box-promotion-result-item">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6">
                                                        <img class="box-product-result-item-product"
                                                             src="{{$storeProduct['product']['image'].'&h=150'}}"
                                                             alt="Six Pack Club Colombia Dorada">
                                                    </div>
                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="box-product-result-item-text">
                                                            <p style="font-weight: bold">
                                                                {{$storeProduct['product']['name']}}
                                                            </p>
                                                            <p>
                                                                {{$storeProduct['product']['description']}}
                                                            </p>
                                                            <p style="color:#039442;font-weight: bold">
                                                                {{$storeProduct['points']}} Puntos
                                                            </p>
                                                        </div>
                                                        <button  @click="addPromotion({{$storeProduct['id']}})"
                                                                 class="box-product-canje-active-btn">
                                                            Canjear
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach


                                    {{--<div class="box-pagination">
                                        <nav aria-label="Page navigation">
                                            <ul class="pagination">
                                                <li>
                                                    <a href="#" aria-label="Previous">
                                                        PRIMERA
                                                        <span aria-hidden="true">&laquo;</span>
                                                    </a>
                                                </li>
                                                <li class="active"><a href="#">1</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">4</a></li>
                                                <li><a href="#">5</a></li>
                                                <li>
                                                    <a href="#" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                        ULTIMO
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>--}}
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <modal v-model="showPromotion" ref="modal" :footer="false" :header="false" size="lg" id="promotion-modal">
            <div class="row">
                <div class="col-lg-8">
                    <h3 class="item-carousel-title" >
                        Descarga<br/>
                        nuestra aplicación
                    </h3>
                    <p>
                        Disfruta de todos los beneficios que tenemos para ti
                    </p>
                    <ul>
                        <li>Promociones exclusivas</li>
                        <li>Cobertura Floridablanca, Bucaramanga, Piedecuesta<br/> y sus áreas metropolitanas</li>
                        <li>Tus compras se convierten en puntos canjeables</li>
                        <li>Todo lo que necesitas para tu rumba</li>
                    </ul>
                    <div class="social">
                        <div class="content-get-app">
                            <a href="https://itunes.apple.com/co/app/licorera-3jjjs/id1178470906?mt=8" target="_blank" >
                                <button href="#" class="btn-custom btn-custom-default"><span class="icon"><img src="/public/front/assets/SVG-13.svg" alt="AppStore"></span>
                                    AppStore
                                </button>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.licorera3jjjs.app" target="_blank" >
                                <button class="btn-custom btn-custom-default" ><span class="icon"><img src="/public/front/assets/Assets-SVG-16.svg" alt="PlayStore"></span>
                                    PlayStore
                                </button>
                            </a>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                </div>
            </div>
        </modal>

    </section>
@endsection
@section('scripts')

@endsection