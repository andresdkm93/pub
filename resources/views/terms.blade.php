<html>
<head>
</head>
<body>
<section class="main">
    <br/>
    <div class="content-main">
        <div class="tab-content">
            <div role="tabpanel">

                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="content-searh-result">
                                    <p>Inicio / Términos y condiciones</p>
                                </div>
                            </div>
                        </div>
                        <div class="content-searh-result">
                            <h2>TÉRMINOS Y CONDICIONES</h2>
                        </div>
                        <p class="p1">
                            Bienvenido a nuestra aplicación móvil. Si continúa navegando y utilizando esta aplicación móvil, usted
                            acepta
                            cumplir y estar sujeto a los siguientes términos y condiciones de uso, que junto con nuestra política de
                            privacidad gobiernan Licorera Tres Jotas App la relación con usted en relación a esta aplicación móvil. Si no
                            está
                            de
                            acuerdo con alguna parte de estos términos y condiciones, por favor no haga uso de nuestra aplicacin móvil
                            y
                            proceda a desinstalarla de su dispositivo celular.
                            </br></br>
                            El término “Licorera Tres Jotas App o “nosotros” se refiere al titular de esta aplicación móvil. El término
                            “usted”
                            se
                            refiere al usuario o visitante de nuestra aplicación móvil.
                        </p>
                        <HR width="100%" align="center">
                        <p class="titulo">
                            EL USO DE Licorera Tres Jotas APP ESTÁ SUJETO A LAS SIGUIENTES CONDICIONES DE USO:
                        </p>
                        <p class="p1">
                            • El contenido de las secciones de la aplicación móvil es para su información y uso general.</br>
                            • Está sujeto a cambios sin previo aviso.</br>
                            • Ni nosotros ni ningún tercero ofrece una total garantía en cuanto a la exactitud, puntualidad,
                            rendimiento,
                            integridad o adecuación de la información y los materiales encontrados u ofrecidos en esta aplicación para
                            cualquier propósito particular.</br>
                            • Usted reconoce que dicha información y materiales pueden contener inexactitudes o errores y expreso
                            excluimos la
                            responsabilidad por cualquier inexactitud o errores en la máxima medida permitida por la ley.</br>
                            • El uso de cualquier información o materiales contenidos en esta aplicación es bajo su propio riesgo, para
                            lo
                            cual no se hace responsable.</br>
                             Será su responsabilidad para garantizar que cualquier producto, servicio o información disponible a través
                            de
                            esta aplicación mvil a sus necesidades específicas.</br>
                             Esta aplicación móvil contiene material que es propiedad de nosotros.</br>
                            • Este material incluye, pero no limitado a, el diseño, presentación, apariencia y gráficos.</br>
                            • La reproducción está prohibida salvo de conformidad con la nota de copyright, que forma parte de estos
                            términos
                            y condiciones.</br>
                            • Todas las marcas reproducidas en esta aplicación móvil, que no son propiedad de, o con licencia para el
                            operador, son reconocidas en la aplicación móvil.</br>
                            • El uso no autorizado de esta aplicación móvil puede dar lugar a una reclamación por daños y perjuicios y /
                            o
                            constituir un delito.</br>
                            • Ocasionalmente, esta aplicación móvil también puede incluir enlaces a sitios web ajenos a nosotros. Estos
                            enlaces son proporcionados para su conveniencia para proporcionar más información. Esto no significa que
                            estamos
                            de acuerdo con el sitio web (s). No tenemos ninguna responsabilidad sobre el contenido del sitio web
                            vinculado
                            (s).</br>

                            </br>
                            El uso de esta aplicación móvil y cualquier disputa que surja de ese uso de la aplicación móvil está sujeto
                            a
                            las
                            leyes de Colombia.</br>

                        </p>
						<HR width="100%" align="center">
                        <p class="titulo">
                            DATOS DE USUARIO
                        </p>
                        <p class="p1">
                            Para Licorera Tres Jotas es muy importante la transparencia en todo momento, es por eso que queremos dejarle muy claro cuales son los datos que a traves del inicio de sesión relectamos en nuestra App.
							</br>
                            Datos recolectados:</br>
							</br>
                            • Correo electrnico.</br>
                            • Nombre.</br>
                        </p>
                        <HR width="100%" align="center">
                        <p class="titulo">
                            USO DE INFORMACIÓN DE USUARIO
                        </p>
                        <p class="p1">
                            La información que usted como usuario nos has suministrado (correo electrnico y nombre) será usada para la creación del perfil de usuario dentro de la aplicación. Esto le permitirá realizar pedidos y acumular puntos por cada compra y redimir esos puntos posteriomente. 
							</br>
                            Además el correo puede ser usado para enviar información relevante acerca de algunas promociones o productos nuevos que Licorera Tres Jotas quiera poner en su conocimiento. Así como también será usado para enviar instrucciones en el caso de restablecimiento de contraseña.
                        </p>
                        <HR width="100%" align="center">
                        <p class="titulo">
                            DERECHOS DE USUARIO SOBRE LA INFORMACIÓN
                        </p>
                        <p class="p1">
                            Para Licorera Tres Jotas la información suministrada por cada usuario es muy importante y valiosa, es por eso que le damos el mejor trato y seguridad. Por eso queremos informarle que como usuario usted tiene derecho a:
							</br>
                            • Crear cuenta de usuario con su información.</br>
                            • Actualizar información.</br>
                            • Solicitar la eliminación de datos de cuenta de usuario (Para ello, debe seguir los pasos en la siguiente sección).</br>

                        </p>
                        <HR width="100%" align="center">
                        <p class="titulo">
                            ELIMINACIÓN DE DATOS DE USUARIO
                        </p>
                        <p class="p1">
                            Su privacidad es muy importante para nosotros. Por eso, no guardamos ninguna información que usted no quiera. 
                            Por eso ponemos a su disposición una forma de realizar una solicitud para eliminar los datos que nos ha suministrado. Para ello puede seguir este paso a paso:
							</br>
                            1. Ingrese a www.licorera3jjjs.com</br>
                            2. Navegue hasta la parte inferior de la página.</br>
                            3. Seleccione la opción Solicitud de eliminación de datos.</br>
                            4. En la ventana emergente que se muestra, diligencie la información requerida como número teléfonico y correo electrónico.</br> 
                            que usó para registrarse en la aplicación Licorera Tres Jotas.</br>
                            5. Una vez se diligencie esta información iniciaremos el proceso de eliminacin de su información de usuario en nuestra base de datos.</br>
                            6. En breve, sus datos serán verificados y eliminados de nuestra base de datos.</br>
							</br>
                            Tenga en cuenta que una vez eliminada esta información no podra acceder a su historial de pedidos y los puntos acumulados se perdern en su totalidad.
                        </p>
                        <HR width="100%" align="center">
                        <p class="titulo">
                            INDEMNIZACIÓN
                        </p>
                        <p class="p1">
                            Usted se obliga a indemnizar y a eximir a Licorera Tres Jotas App, sus filiales, representantes, agentes, socios
                            y
                            empleados, de cualquier pérdida, dao, queja, reclamos o demandas, incluidos los honorarios de abogado y
                            procurador, de terceros, o incurridos o sufridos por Licorera Tres Jotas App en relación con o provenientes de
                            cualquier uso o transmisión de información a través de la aplicación móvil, que ocurra bajo la contraseña o
                            cuenta
                            del usuario y que viole los términos y condiciones de uso aquí mencionados, cualquier ley o reglamento
                            local,
                            nacional o internacional aplicable o bien, derechos de terceros.
                        </p>
                        <HR width="100%" align="center">
                        <p class="titulo">
                            PROHIBICIÓN DE REVENDER LOS PRODUCTOS
                        </p>
                        <p class="p1">
                            Usted se obliga a no reproducir, duplicar, copiar, vender, revender o explotar para fines comerciales,
                            cualquier
                            sección de la aplicación mvil o producto en ella ofrecido, uso o acceso al mismo.
                        </p>
                        <HR width="100%" align="center">
                        <p class="titulo">
                            LINKS (VINCULOS)
                        </p>
                        <p class="p1">
                            La aplicación móvil podrá proporcionar vínculos a otros sitios o recursos de la red mundial. Debido a que
                            Licorera
                            3jjjs App no tiene control alguno sobre dichos sitios o recursos, usted reconoce y acepta que Licorera Tres Jotas
                            App
                            no será responsable por la disponibilidad de los sitios o recursos externos ni por cualquier contenido,
                            publicidad, productos, servicios u otro tipo de material contenido  a disposición en tales sitios o
                            recursos.
                            El
                            usuario reconoce y acepta que Licorera Tres Jotas App no será responsable, directa o indirectamente, por
                            cualquier
                            dao
                            o perjuicio causado o que se presuma que sea causado por tales contenidos, productos o servicios disponibles
                            en
                            dichos sitios o recursos externos, o por la utilización o confianza depositada por el usuario en tales
                            contenidos,
                            productos o servicios.
                        </p>
                        <HR width="100%" align="center">
                        <p class="titulo">
                            DERECHOS DE PROPIEDAD
                        </p>
                        <p class="p1">
                            Usted reconoce y acepta que la aplicacin móvil contiene información confidencial y de propiedad ajena
                            protegida
                            por la legislación de propiedad intelectual y otras disposiciones legales. Además usted reconoce y acepta
                            que
                            el
                            contenido incluyendo, pero no limitándose, textos, programas, canciones, sonidos, fotografías, gráficos,
                            videos u
                            otros material contenidos en propagandas disponibles en la aplicación móvil, así como también informaciones
                            divulgadas al usuario a través de la aplicación móvil, está protegido por derechos de autor, marcas
                            comerciales,
                            patentes y otros derechos de propiedad intelectual y legislación aplicable. El usuario reconoce y acuerda
                            que
                            solamente podrá utilizar tales materiales e informaciones según lo expresamente autorizado por Licorera
                            3jjjs
                            App,
                            y no podrá copiar, reproducir, transmitir, distribuir o crear obras derivadas a partir de tales materiales o
                            informaciones sin la expresa autorización del respectivo propietario. Licorera Tres Jotas App le otorga una
                            licencia y
                            derecho personal, intransferible y no exclusivo para utilizar el código objeto de su aplicación móvil en un
                            solo
                            dispositivo, siempre que Usted (y sin permitírselo a un tercero) no copie, modifique, cree un trabajo
                            derivado
                            de
                            ello, invierta el proceso, invierta el montaje o de algún modo intente descubrir algún código de acceso,
                            vender,
                            ceder, sublicenciar, prestar garantía sobre o de algún modo transferir cualesquiera derecho en la aplicación
                            mvil. Usted se obliga a no modificar la aplicación móvil de ninguna manera, o utilizar versiones de la
                            aplicación
                            móvil modificadas con el fin, entre otros, de obtener acceso no autorizado a la aplicación móvil. Usted se
                            obliga
                            a no acceder a la aplicacin móvil por otros medios que no sean a través de la interface que Licorera Tres Jotas
                            App
                            proporciona.
                        </p>
                        <HR width="100%" align="center">
                        <p class="titulo">
                            GARANTÍA LIMITADA
                        </p>
                        <p class="p1">
                            A- El uso de la aplicacin móvil será bajo exclusivo riesgo y responsabilidad del usuario. La aplicación
                            móvil
                            es
                            suministrada gratuitamente y depende de la funcionalidad de varios factores, como la interacción de
                            servidores
                            y
                            servicios de telecomunicaciones de terceros, la adecuación de los equipos de usuario y destinatario del
                            mensaje.
                            Considerando tales factores, Licorera Tres Jotas App empleará sus mejores esfuerzos para que los mensajes de
                            e-mail
                            enviados a través de la aplicación móvil, sean recibidos por el destinatario correcto y sin interferencias.
                            Sin
                            embargo, por estas mismas razones, Licorera Tres Jotas App no puede garantizar que los mensajes sern entregados
                            al
                            destinatario correcto, en un plazo adecuado, o no sufrirn extravío, divulgación o violación por partes de
                            terceros no autorizados, como por ejemplos "hackers". Se aconseja al usuario no confiar exclusivamente en la
                            aplicación móvil para enviar informaciones importantes o confidenciales y Licorera Tres Jotas App no se
                            responsabilizará por ninguna falla resultante del envío de mensajes por parte del usuario.
                            </br>B- Licorera Tres Jotas App no ofrece garantías de cualquier naturaleza con relación a la aplicación móvil,
                            ya
                            sean
                            expresas o implícitas, incluyendo, pero no limitándose a las garantías implcitas de comercialización,
                            idoneidad
                            para un proposito particular.
                            </br>C- Licorera Tres Jotas App no garantiza que cumplirá con los requisitos y/o necesidades del usuario, o que
                            la
                            apliación móvil se prestará de manera ininterrumpida, segura o libre de error.
                            </br>D- Licorera Tres Jotas App tampoco otorga ninguna garantía en cuanto a los resultados que se puedan obtener
                            del
                            uso de la aplicación móvil o en relación con la exactitud o confiabilidad de cualquier información obtenida
                            a
                            través de la aplicación mvil, ni que los defectos en los programas serán corregidos.
                            </br>E- Ninguna asesoría o informacin, ya sea oral o por escrito obtenida por el usuario de la aplicación
                            mvil
                            dará origen a ninguna garantía que no sea expresamente especificada en el presente acuerdo.
                            </br>F- El usuario reconoce y acuerda que cualquier material y/o información obtenida a través de la
                            aplicación
                            móvil o de su utilización, estará sujeta a su entero criterio y riesgo; y que será el único responsable de
                            cualquier daño ocurrido en su dispositivo celular o pérdida de datos que pueda resultar de recibir tal
                            material.
                            </br>G- Licorera Tres Jotas App no se hace responsable del uso excesivo de la aplicación móvil y de las
                            complicaciones
                            que esto pueda traer para el usuario.
                        </p>
                        <HR width="100%" align="center">
                        <p class="titulo">
                            CONDUCTA DEL USUARIO
                        </p>
                        <p class="p1">
                            El usuario acepta que toda la información, datos, textos, software, música, sonido, fotografías, gráficos,
                            vídeos,
                            mensajes u otro material, ya sea públicamente enviado, u objeto de transmisiones personales ("Contenido"),
                            serán
                            de la exclusiva responsabilidad de la persona donde dicho Contenido se haya originado. Esto significa que el
                            usuario, y no Licorera Tres Jotas App, es plenamente responsable de todo el Contenido que ponga a disposición de
                            los
                            demás, exhiba, envíe por e-mail o de cualquier manera transmita. Licorera Tres Jotas App no ejerce control sobre
                            el
                            contenido enviado a través de la aplicación móvil y, así, no puede garantizar la precisión, integridad o
                            calidad
                            de dicho Contenido. El usuario entiende que, al utilizar la aplicación móvil, es susceptible de visualizar
                            contenidos que puedan considerarse ofensivos, indecentes o inaceptables. Licorera Tres Jotas App no asumirá
                            responsabilidad alguna, bajo ninguna circunstancia, por el contenido, incluyendo, sin limitación, errores u
                            omisiones en el contenido, daños o perjuicios derivados del uso del contenido exhibido, enviado por e-mail
                            o,
                            de
                            cualquier modo, transmitido a través de la aplicación móvil. El usuario se obliga a no utilizar la
                            aplicacin
                            móvil con el fin de:
                            <br>a. Poner a disposición de los demás, enviar por e-mail o, de cualquier modo, transmitir contenidos
                            ilegales,
                            dañinos, molestos, amenazadores, abusivos, tortuosos, difamatorios, vulgares, obscenos, invasores de la
                            intimidad
                            de terceros, odiosos, xenófobos, racistas o, de algún modo, inaceptables; b. Perjudicar a menores, de manera
                            alguna; c. Hacerse pasar o fingir ser cualquier otra persona o entidad, incluyendo, sin limitación, un
                            representante de Licorera Tres Jotas App, fundador, guía o anfitrin de forums, o de cualquier otro modo mentir o
                            fingir sobre su relación con cualquier otra persona o afiliación a cualquier entidad; d. Falsificar rúbricas
                            o, de
                            otro modo manipular identificativos con el fin de disfrazar la naturaleza del contenido transmitido a través
                            de la
                            aplicación móvil; e. Poner a disposición de los demás usuarios, enviar por e-mail o, de algún modo
                            transmitir,
                            cualquier Contenido que, de acuerdo con las disposiciones aplicables o relaciones contractuales existentes,
                            Usted
                            no está autorizado a transmitir (tales como información privilegiada, información protegida por derechos de
                            propiedad industrial o intelectual o información sobre la cual tiene un deber de confidencialidad). f. Poner
                            a
                            disposición de los demás usuarios, enviar por e-mail o, de algún modo, transmitir Contenido alguno
                            susceptible
                            de
                            infringir patentes, marcas, secretos comerciales, derechos de autor u otros derechos de terceros
                            ("Derechos");
                            g.
                            Poner a disposición de los demás usuarios, enviar por e-mail o, de algún modo, transmitir publicidad no
                            solicitada
                            o autorizada, material publicitario, "correo basura", "cartas en cadena", "estructuras piramidales", o
                            cualquier
                            otra forma de solicitud, excepto en aquellas áreas (tales como espacios comerciales) que hayan sido
                            exclusivamente
                            concebidos para ello; h. Poner a disposición de los demás usuarios, enviar por e-mail o, de algún modo,
                            transmitir
                            material alguno que sea portador de virus o cualquier otro código informático, archivos o programas
                            diseñados
                            para
                            interrumpir, destruir o limitar el funcionamiento de cualquier software, hardware o equipo de
                            telecomunicaciones;
                            i. Interrumpir el curso normal de las conversaciones, o de algún modo actuar de manera que afecte de forma
                            negativa a la posibilidad de comunicarse en tiempo real; j. Interferir o interrumpir la aplicación móvil,
                            servidores o redes conectados a la aplicacin móvil, o incumplir los requisitos, procedimientos y
                            regulaciones
                            de
                            la política de redes conectadas a la aplicacin móvil; k. Violar con/sin intención cualquier ley aplicable,
                            ya
                            sea
                            local, de las Comunidades Autónomas, nacional o internacional, incluidas, sin limitación, las disposiciones
                            reguladoras de los mercados de valores. l. "Acechar" o de algún modo hostigar a terceros; o l. Recoger o
                            almacenar
                            información personal sobre otros usuarios.
                            El usuario reconoce y acepta que Licorera Tres Jotas App no examina los Contenidos con anterioridad a su puesta
                            en
                            disposición o transmisión, pero éste y sus representantes estarán facultados (pero no obligados) a rechazar
                            o
                            desplazar cualquier Contenido que est disponible en la aplicación mvil. Sin perjuicio de lo anterior,
                            Licorera
                            3jjjs App y sus representantes estarán plenamente facultados para suprimir cualquier Contenido que vulnere
                            las
                            Condiciones o que de algún modo sea inaceptable. Usted se obliga a evaluar los Contenidos y asume todos los
                            riesgos derivados de los mismos o del uso de cualquier Contenido, incluyendo la fiabilidad, utilidad y la
                            finalidad de tal Contenido. A estos efectos, Usted reconoce y acepta que Licorera Tres Jotas App no está
                            garantizando
                            la fiabilidad de cualquier Contenido creado o disponible en Licorera Tres Jotas App.
                            El usuario reconoce y acepta que Licorera Tres Jotas App no se hace responsable del consumo en exceso de los
                            productos
                            ofrecidos en la aplicación móvil y así mismo el usuario es consciente y acepta las complicaciones de salud
                            que
                            de
                            estos se deriven.
                            El usuario reconoce y acepta que Licorera Tres Jotas App puede conservar o revelar el Contenido si es requerido
                            para
                            ello en virtud de las disposiciones legales aplicables o, de buena fe, lo considerara necesario para: (a)
                            Dar
                            cumplimiento a la ley o a procedimientos legales, tales como órdenes judiciales o de órganos administrativos
                            competentes; (b) Hacer cumplir las presentes Condiciones; (c) Contestar reclamaciones relativas a
                            violaciones
                            de
                            derechos de terceros; o (d) Proteger los legítimos intereses de Licorera Tres Jotas App, sus usuarios y el
                            público
                            en
                            general.
                            Usted entiende y acepta que el proceso técnico y transmisión de la aplicación móvil, incluido su Contenido,
                            puede
                            implicar: (a) Transmisiones a través de diversas redes; y (b) Modificaciones o cambios realizados al objeto
                            de
                            compatibilizar el Contenido con las necesidades técnicas de conexión de redes o dispositivos.
                        </p>
                        <HR width="100%" align="center">
                        <p class="titulo">
                            GANANCIA DE PUNTOS
                        </p>
                        <p class="p1">
                            Usted como usuario puede ganar puntos por cada compra que haga efectiva a través de Licorera Tres Jotas App. Por
                            cada $ 10.000 (diez mil pesos m/cte) en compras de productos de la Licorera Tres Jotas a través de Licorera Tres Jotas
                            App usted puede ganar y acumular 1 punto (llamado J en el App) que puede ser redimido única y exclusivamente
                            por productos de canje dentro de Licorera Tres Jotas App. Estos puntos pueden ser redimidos en cualquier momento
                            siempre y cuando usted tenga disponible la cantidad correcta para hacerlo. No aplica para la compra de
                            boleteria de los eventos promocionados en Licorera Tres Jotas App ya que estos eventos son de proveedores
                            externos
                            y Licorera Tres Jotas App solo gestiona el canal de venta.
                        </p>
                        <HR width="100%" align="center">
                        <p class="titulo">
                            DESCUENTOS
                        </p>
                        <p class="p1">
                            Usted como usuario puede obtener descuentos sobre el total de sus pedidos a través de Licorera Tres Jotas App.
                            Estos descuentos son aplicables única y exclusivamente sobre los productos de la Licorera Tres Jotas, es decir,
                            no
                            aplica para los eventos promocionados ya que son eventos de proveedores externos y Licorera Tres Jotas App solo
                            gestiona el canal de venta. Tampoco se aplican descuentos sobre las promociones, ya que estos productos de
                            promociones ya tienen descuento aplicado. Los descuentos se obtienen ingresando, en el formulario de entrega
                            del pedido, códigos promocionales que Licorera Tres Jotas hará público en sus redes sociales. Estos códigos de
                            descuento tienen fecha de expiración y límite de redenciones.
                        </p>
                        <HR width="100%" align="center">
                        <p class="titulo">
                            DOMICILIOS
                        </p>
                        <p class="p1">
                            El valor del domicilio será calculado según la distancia recorrida desde la ubicación de la Licorera Tres Jotas
                            hasta el punto de entrega. Durante el primer año de funcionamiento del Licorera Tres Jotas App, usted como
                            usuario
                            no tendrá que pagar el valor del domicilio siempre y cuando el valor total de su pedido sea igual o mayor a
                            $40.000 (cuarenta mil pesos m/cte) y el lugar de recepcin del domicilio se encuentre dentro del perímetro
                            urbano de Bucaramanga y Floridablanca.
                        </p>
                    </div>
                    <div class="col"></div>
                </div>
            </div>
        </div>

    </div>
</section>

</body>
</html>