<modal v-model="showRegister" ref="modal" id="modal-register" :footer="false">
    <h2 class="item-carousel-title">
        Registro
    </h2>
    <div class="form">
        <form v-on:submit.prevent="onRegister">
            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" class="form-control" id="name" v-model="register.name" required>
            </div>

            <div class="form-group">
                <label for="email">Correo</label>
                <input type="email" class="form-control" id="email" v-model="register.email" required>
            </div>

            <div class="form-group">
                <label for="nid">Número de identificación</label>
                <input type="number" class="form-control form-number" id="nid" v-model="register.id" required>
            </div>

            <div class="form-group">
                <label for="cellphone">Número de celular</label>
                <input type="number" class="form-control form-number" id="cellphone" v-model="register.cellphone" required
                       maxlength="10">
            </div>

            <div class="form-group">
                <label for="cellphone">Fecha de cumpleaños</label>
                <input type="date" class="form-control" id="birthdate" v-model="register.birthday" required>
            </div>

            <div class="form-group">
                <label for="cellphone">Contraseña</label>
                <input type="password" class="form-control" id="password" v-model="register.password" required>
            </div>

            <div class="form-group">
                <label for="cellphone">Confirmar Contraseña</label>
                <input type="password" class="form-control" id="password_confirm"
                       v-model="register.password_confirm" required>
            </div>
            <p class="error" v-html="registerError"></p>
            <button type="submit" class="btn btn-login">
                Registrar
            </button>

        </form>
        <p>o usa tus redes sociales</p>
        <template>
            <fb-signin-button
                    :params="fbSignInParams"
                    @success="onRegisterFb"
                    @error="onSignInErrorFb">
                <template>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         width="40px" height="30px" viewBox="336.551 -105.036 20 30"
                         enable-background="new 336.551 -105.036 20 30"
                         xml:space="preserve">
                        <g>
                            <path fill="#333333"
                                  d="M349.832-97.059h2.233v-3.51h-2.772l0,0h-0.574c0,0-2.104-0.052-3.444,1.691c0,0-0.914,0.862-0.929,3.384l0,0v2.633h-3.309v3.727h3.309v9.63h3.814v-9.63h3.282l0.457-3.727h-3.739v-2.633h-0.001C348.168-95.788,348.299-97.085,349.832-97.059z"/>
                        </g>
                    </svg>
                    Registrar con Facebook
                </template>
            </fb-signin-button>
        </template>
        <template>
            <g-signin-button
                    :params="googleSignInParams"
                    @success="onRegisterGm"
                    @error="onSignInErrorGm">
                <svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="40px" height="30px" viewBox="275.955 -105.037 40 30"
                     enable-background="new 275.955 -105.037 40 30"
                     xml:space="preserve">
                    <g>
                        <g>
                            <g>
                                <g>
                                    <path fill="#333333" d="M300.06-91.525h-0.109h-3.809h-6.34v3.664h6.145c-0.889,2.624-3.338,4.509-6.221,4.509
                                        c-3.633,0-6.578-2.992-6.578-6.688c0-3.69,2.945-6.683,6.578-6.683c1.592,0,3.047,0.575,4.186,1.531h0.172l2.706-2.75
                                        c-1.866-1.719-4.342-2.769-7.063-2.769c-5.801,0-10.506,4.78-10.506,10.671c0,5.899,4.705,10.676,10.506,10.676
                                        c0.76,0,1.521-0.08,2.263-0.248c1.504-0.331,2.925-1.011,4.139-1.959c1.208-0.942,2.203-2.152,2.907-3.51
                                        c0.357-0.696,0.643-1.428,0.841-2.183c0.247-0.943,0.187-1.891,0.187-2.861C300.06-90.589,300.06-91.056,300.06-91.525z"/>
                                    <rect x="306.554" y="-94.665" fill="#333333" width="3.076" height="9.195"/>
                                    <rect x="303.494" y="-91.605" fill="#333333" width="9.196" height="3.076"/>
                                </g>
                            </g>
                        </g>
                    </g>
                    </svg>
                Registrar con google
            </g-signin-button>
        </template>
    </div>

</modal>
