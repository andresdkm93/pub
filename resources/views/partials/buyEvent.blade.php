<modal v-model="showBuyEvent" ref="modal" :footer="false" size="lg">
    <div class="row" v-if="buyEvent">
        <div class="col-lg-6">
            <img v-bind:src="buyEvent.image+'&w=400'" class="product-image"/>
        </div>
        <div class="col-lg-6">
            <h4 class="item-carousel-title" v-html="buyEvent.name"></h4>
            <p v-html="buyEvent.description"></p>
            <p style="font-size: 10px;">LOCALIDAD</p>
            <select class="select-event" @change="setBuyProduct(itemEvent)" v-model="itemEvent">
                <option disabled selected value="null">Seleccione</option>
                <option v-for="item in buyEvent.items" v-bind:value="item">@{{item.product.name}}</option>
            </select>
            <p class="product-price" style="height: 32px;">
                <span v-if="buyProduct">
                    @{{ buyProduct.price | currency('$ ',0) }}
                </span>
            </p>
            <div class="row">
                <div class="col-sm-2" @click="changeQuantity(-1)">
                    <img src="/public/front/assets/Assets-SVG-24.svg" />
                </div>
                <div class="col-sm-1" >
                    <p class="product-price">
                        <span v-if="buyProduct">@{{buyProduct.quantityShop}}</span>
                        <span v-else>0</span>
                    </p>
                </div>
                <div class="col-sm-2" @click="changeQuantity(+1)">
                    <img src="/public/front/assets/Assets-SVG-25.svg" />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button class="add-to-cart" @click="saveInOrderEvent()">
                        Agregar al pedido
                    </button>
                </div>
            </div>

        </div>
    </div>
</modal>
