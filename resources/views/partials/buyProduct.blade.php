<modal v-model="showBuyProduct" ref="modal"  :footer="false"   size="lg">
    <div class="row" v-if="buyProduct">
        <div class="col-lg-6">
            <img v-bind:src="buyProduct.product.image+'&w=400'" class="product-image" />
        </div>
        <div class="col-lg-6">
            <h4 class="item-carousel-title" v-html="buyProduct.product.name"></h4>
            <p class="product-price">@{{ buyProduct.price | currency('$ ',0) }}</p>
            <p v-html="buyProduct.product.description"></p>
            <p><small>Cantidad</small></p>
            <div class="row">
                <div class="col-sm-2" @click="changeQuantity(-1)">
                    <img src="/public/front/assets/Assets-SVG-24.svg"/>
                </div>
                <div class="col-sm-1" style="text-align: center;">
                    <p class="product-price"><span>@{{buyProduct.quantityShop}}</span></p>
                </div>
                <div class="col-sm-2" @click="changeQuantity(+1)">
                    <img src="/public/front/assets/Assets-SVG-25.svg"/>
                </div>
                <div class="col-sm-1"></div>
            </div>
            <div class="row">
                <div class="col-sm-10">
                    <button class="add-to-cart" @click="saveInOrder()">
                        Agregar al pedido
                    </button>
                </div>
            </div>

        </div>
    </div>
</modal>
