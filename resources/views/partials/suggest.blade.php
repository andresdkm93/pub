<modal v-model="showSuggest" ref="modal" id="modal-suggest" :footer="false">

    <div class="form">
        <form v-on:submit.prevent="onSuggest">
            <h2 class="item-carousel-title" style="text-align: left">
                Sugerencias
            </h2>
            <p>
                Para nosotros es muy importante tu opinión, ayúdanos a mejorar. ¿Tienes alguna sugerencia?
            </p>
            <div style="padding-left: 15px;padding-top: 15px;">
                <div class="form-group">
                    <label for="name">Título</label>
                    <input type="text" class="form-control" id="title" v-model="suggest.title" required>
                </div>

                <div class="form-group">
                    <label for="email">Correo</label>
                    <input type="email" class="form-control" id="email" v-model="suggest.email" required>
                </div>

                <div class="form-group">
                    <label for="nid">Mensaje</label>
                    <textarea class="form-control" id="suggest" v-model="suggest.suggest" required style="resize: none;
    border-top: none;
    border-left: none;
    border-right: none;
    border-bottom: 1px solid #EBEBEB;
    box-shadow: none;">
                    </textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-login">
                Enviar
            </button>
            <br/>
        </form>
    </div>
</modal>
<modal v-model="showRemoveData" ref="modal" id="modal-remove" :footer="false">

    <div class="form">
        <form v-on:submit.prevent="onRemoveData">
            <h2 class="item-carousel-title" style="text-align: left">
                Solicitud de eliminación de datos
            </h2>
            <p>
                Tu privacidad es muy importante para nosotros. Por eso, no guardamos ninguna información que no quieras.

                Sentimos mucho que quieras irte y te extrañaremos. Por aquí estaremos esperándote de nuevo cuando quieras regresar. Ingresa tu información en el siguiente formulario y eliminaremos tu cuenta de nuestra base de datos
            </p>
            <div style="padding-left: 15px;padding-top: 15px;">
                <div class="form-group">
                    <label for="name">Número de telefono</label>
                    <input type="text" class="form-control" id="title" v-model="removeData.phone" required>
                </div>

                <div class="form-group">
                    <label for="email">Correo</label>
                    <input type="email" class="form-control" id="email" v-model="removeData.email" required>
                </div>
            </div>
            <button type="submit" class="btn btn-login">
                Enviar
            </button>
            <br/>
        </form>
    </div>
</modal>
