<modal v-model="showValidateAge" ref="modal"  :footer="false" :header="false"  size="lg" :backdrop="false" :keyboard="false">
    <div class="row">
        <div class="col-lg-6">
            <img src="/public/front/img/icono_positivo.jpg" class="img-responsive">
        </div>
        <div class="col-lg-6">
            <h4 class="item-carousel-title">Bienvenido</h4>
            <p>Por favor ingresa tu fecha de nacimiento</p>
            <form v-on:submit.prevent="validateOrder">
                <div class="row boxValidateAge">
                    <div class="col-sm-3">
                        <input type="number" v-model="validateAgeData.day" class="validateAge" size="2" max="31" min="1" placeholder="DD" required>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" v-model="validateAgeData.month" class="validateAge" size="2" max="12" min="1" placeholder="MM" required>
                    </div>
                    <div class="col-sm-6">
                        <input type="number" v-model="validateAgeData.year" class="validateAge" size="4" max="9999" min="1900" placeholder="YYYY" required>
                    </div>
                </div>
                <input type="checkbox" required>
                <span style="color:#000">
                    Acepto los términos y condiciones

                </span>
                <p class="error" v-html="registerError"></p>
                <button class="validate-age-button" type="submit">
                    Continuar
                </button>
            </form>

        </div>
    </div>
</modal>
