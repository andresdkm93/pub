<modal v-model="showLogin" ref="modal" id="modal-login" :footer="true" >
    <h2 class="item-carousel-title">
        Inicio de sesión
    </h2>
    <div class="form">
        <form v-on:submit.prevent="submit">
            <div class="form-group">
                <label for="email">CORREO</label>
                <input type="email" class="form-control" id="email" v-model="email" required>
            </div>
            <div class="form-group">
                <label for="pwd">CONTRASEÑA</label>
                <input type="password" class="form-control" id="pwd" v-model="password" required>
            </div>
            <p class="error" v-html="registerError"></p>
            <a href="/remember">¿Olvidaste tu contraseña? haz click aquí</a>
            <br/>
            <button type="submit" class="btn btn-login">
                Iniciar
            </button>
        </form>
        <p>o usa tus redes sociales</p>
        <template>
            <fb-signin-button
                    :params="fbSignInParams"
                    @success="onSignInSuccessFb"
                    @error="onSignInErrorFb">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="40px" height="30px" viewBox="336.551 -105.036 20 30"
                     enable-background="new 336.551 -105.036 20 30"
                     xml:space="preserve">
                        <g>
                            <path fill="#333333"
                                  d="M349.832-97.059h2.233v-3.51h-2.772l0,0h-0.574c0,0-2.104-0.052-3.444,1.691c0,0-0.914,0.862-0.929,3.384l0,0v2.633h-3.309v3.727h3.309v9.63h3.814v-9.63h3.282l0.457-3.727h-3.739v-2.633h-0.001C348.168-95.788,348.299-97.085,349.832-97.059z"/>
                        </g>
                    </svg>
                Iniciar sesión con Facebook
            </fb-signin-button>
        </template>
        <template>
            <g-signin-button
                    :params="googleSignInParams"
                    @success="onSignInSuccessGm"
                    @error="onSignInErrorGm">
                <svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="40px" height="30px" viewBox="275.955 -105.037 40 30"
                     enable-background="new 275.955 -105.037 40 30"
                     xml:space="preserve">
                    <g>
                        <g>
                            <g>
                                <g>
                                    <path fill="#333333" d="M300.06-91.525h-0.109h-3.809h-6.34v3.664h6.145c-0.889,2.624-3.338,4.509-6.221,4.509
                                        c-3.633,0-6.578-2.992-6.578-6.688c0-3.69,2.945-6.683,6.578-6.683c1.592,0,3.047,0.575,4.186,1.531h0.172l2.706-2.75
                                        c-1.866-1.719-4.342-2.769-7.063-2.769c-5.801,0-10.506,4.78-10.506,10.671c0,5.899,4.705,10.676,10.506,10.676
                                        c0.76,0,1.521-0.08,2.263-0.248c1.504-0.331,2.925-1.011,4.139-1.959c1.208-0.942,2.203-2.152,2.907-3.51
                                        c0.357-0.696,0.643-1.428,0.841-2.183c0.247-0.943,0.187-1.891,0.187-2.861C300.06-90.589,300.06-91.056,300.06-91.525z"/>
                                    <rect x="306.554" y="-94.665" fill="#333333" width="3.076" height="9.195"/>
                                    <rect x="303.494" y="-91.605" fill="#333333" width="9.196" height="3.076"/>
                                </g>
                            </g>
                        </g>
                    </g>
                    </svg>
                Iniciar sesión con google
            </g-signin-button>
        </template>
        <br/>
        <br/>
        <br/>

    </div>
    <div slot="footer">
        <div class="footer-login">
            <span>¿Aún no tienes una cuenta?</span><br/>
            <a @click="showLogin=false;showRegister=true;"
               style="color:#fff;font-weight: bold;text-decoration: underline;">
                Registrate gratis aqui
            </a>
        </div>
    </div>
</modal>
