@extends('master.app')

@section('content')
    <section class="main">
        <div class="col-md-12 col-lg-12">
            <div class="content-nav-title container">
                <h2>Bienvenido {{$client->name}}</h2>
                <h3>Aquí encontrarás toda la información de tu perfil</h3>
            </div>
        </div>
        <div class="content-nav-tabs">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs container" role="tablist">
                <li role="presentation" class="active">
                    <a href="/profile">Perfil</a>
                </li>
                <li role="presentation">
                    <a href="/history">Compras</a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active container" id="profile">
                    <div class="resumen-compras-title">
                        Editar dirección
                    </div>
                    <edit-address-component  inline-template>
                        <div class="row info-compras">
                            <form method="POST" action="/ubicaciones/{{$location->id}}">
                                <div class="col-md-12 col-lg-12 location">
                                    <div class="box-direction" id="map">
                                    </div>
                                    <input type="hidden" name="latitude" v-model="latitude">
                                    <input type="hidden" name="longitude" v-model="longitude">

                                    <div class="description-compras content-ubicacion">
                                        <div class="col-md-4 col-lg-4 description-compras-title">Dirección</div>
                                        <div class="col-md-8 col-lg-8 description-compras-text">
                                            <div class="my-location" style="margin-top: 10px;padding-left: 0px">
                                                <div class="my-location-item">
                                                    <div class="address-input" style="display: inline-block;width: 100%">
                                                        <input style="width: 100%;" type="text"  id="address-1" name="address" value="{{$location->address}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="description-compras" style="margin-top: 50px;">
                                        <div class="col-md-4 col-lg-4 description-compras-title">Nombre</div>
                                        <div class="col-md-8 col-lg-8 description-compras-text">
                                            <input type="text" style="resize: none;
                                                                                    width: 100%;
                                                                                    background: transparent;
                                                                                    outline: 0;
                                                                                    border: 0px;
                                                                                    border-bottom: 1px solid #7F7F7F;"
                                                   value="{{$location->name}}"
                                                   name="name"
                                                   placeholder="Nombre de la ubicación">
                                        </div>
                                    </div>
                                    <div class="description-compras" style="margin-top: 50px;">
                                        <div class="col-md-4 col-lg-4 description-compras-title">Detalle del lugar</div>
                                        <div class="col-md-8 col-lg-8 description-compras-text">
                                        <textarea name="detail"
                                                  id="address-4" cols="30"
                                                  rows="3"
                                                  placeholder="Escriba aquí detalles adicionales de la entrega">{{$location->detail}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4">
                                        <div class="cupon-compras">
                                            <button class="content-cupon-btn" type="submit">
                                                Guardar
                                            </button>
                                        </div>
                                    </div>

                                </div>

                            </form>
                        </div>
                    </edit-address-component>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        window.myLatitude = {{$location->latitude}};
        window.myLongitude = {{$location->longitude}};
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOsKGCw925e5ea6GvXNB98pCyLe-02sBM&libraries=places"></script>
@endsection