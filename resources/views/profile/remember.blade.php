@extends('master.app')

@section('content')
    <section class="main">
        <br/>
        <div class="row">
            <div class="col-lg-4 col-lg-offset-2 col-md-12">
                <div class="form-reset">
                    <form method="POST" action="/remember">
                        <div class="form-group">
                            <label for="email">Correo</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Correo electrónico" required>
                        </div>
                        <button type="submit" class="btn btn-login">
                            Reestablecer contraseña
                        </button>

                    </form>
                </div>

            </div>
        </div>
    </section>
@endsection
@section('scripts')

@endsection