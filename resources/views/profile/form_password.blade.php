@extends('master.app')

@section('content')
    <section class="main">
        <br/>
        <div class="row">
            <div class="col-lg-4 col-lg-offset-2 col-md-12">
                <div class="form-reset">
                    <form method="post" action="/updatePassword">
                        <input type="hidden" name="id" value="{{$id}}">
                        <div class="form-group">
                            <label for="password">Nueva Contraseña</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Nueva Contraseña" required>
                        </div>
                        <button type="submit" class="btn btn-login">
                            Cambiar contraseña
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

@endsection