@extends('master.app')

@section('content')
    <section class="main">
        <br/>
        <div class="row">
            <div class="col-lg-4 col-lg-offset-2 col-md-12">
                <div class="form-reset">
                   <p>{{$message}}</p>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

@endsection