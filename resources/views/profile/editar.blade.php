@extends('master.app')

@section('content')
    <section class="main">
        <div class="col-md-12 col-lg-12">
            <div class="content-nav-title container">
                <h2>Bienvenido {{$client->name}}</h2>
                <h3>Aquí encontrarás toda la información de tu perfil</h3>
            </div>
        </div>
        <div class="content-nav-tabs">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs container" role="tablist">
                <li role="presentation" class="active">
                    <a href="/profile">Perfil</a>
                </li>
                <li role="presentation">
                    <a href="/history">Compras</a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active container" id="profile">
                    <form action="/profile/edit" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="content-info-profile">
                                    <div class="img-avatar-content img-circle"
                                         style="background-image: url({{$client->photo?$client->photo:'/public/front/img/avatar.png'}});">
                                    </div>
                                    <div class="content-name-user">
                                        <div class="box-name-user">
                                            <strong class="name-user">{{$client->name}}</strong>
                                        </div>
                                        <span class="points">
                            Puntos acumulados {{$client->points}} Js.
                          </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row content-full-info">
                            <div class="col-md-5 col-lg-7">
                                <table class="table-info-user">
                                    <tr>
                                        <td>PERSONAL</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="title-info-user">NOMBRE</td>
                                        <td>
                                            <input class="profile-input-edit" type="text" value="{{$client->name}}"
                                                   name="name" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="title-info-user">APELLIDOS</td>
                                        <td>
                                            <input class="profile-input-edit" type="text" value="{{$client->lastName}}"
                                                   name="lastName">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="title-info-user">E-MAIL</td>
                                        <td>
                                            <input class="profile-input-edit" type="email" value="{{$client->email}}"
                                                   name="email" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="title-info-user">NÚMERO DE CELULAR</td>
                                        <td>
                                            <input class="profile-input-edit" type="number"
                                                   value="{{$client->cellphone}}" name="cellphone" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="title-info-user">FECHA DE CUMPLEAÑOS</td>
                                        <td>
                                            <input class="profile-input-edit" type="date" value="{{$client->birthday}}"
                                                   name="birthday">
                                        </td>
                                    </tr>

                                </table>
                                <div class="col-md-4 col-lg-4">
                                    <div class="cupon-compras">
                                        <button class="content-cupon-btn" type="submit">
                                            Guardar
                                        </button>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4 col-lg-4">
                                <div class="location">
                                    <h3 class="title">UBICACIÓN</h3>
                                    <div class="box-my-locations">
                                        <h4 class="subtitle">MIS UBICACIONES</h4>
                                        @foreach($client->locations as $location)
                                            <div class="my-location">
                                                <div class="icon">
                                                    <label class="container-radio">
                                                        <input type="radio" checked name="radio-location">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="my-location-item">
                                                    <div class="place">{{$location->name}}</div>
                                                    <div class="address">{{$location->address}}</div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

@endsection