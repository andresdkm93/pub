@extends('master.app')

@section('content')
    <section class="main">
        <div class="col-md-12 col-lg-12">
            <div class="content-nav-title container">
                <h2>Bienvenido {{$client->name}}</h2>
                <h3>Aquí encontrarás toda la información de tu perfil</h3>
            </div>
        </div>
        <div class="content-nav-tabs">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs container" role="tablist">
                <li role="presentation">
                    <a href="/profile">Perfil</a>
                </li>
                <li role="presentation" class="active">
                    <a href="/history">Compras</a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active container" id="shopping">
                    <div class="resumen-compras">
                        <div class="resumen-compras-title">
                            Resumen de los pedidos
                        </div>
                        <div class="resumen-compra-box-title">
                            <div class="col-md-3 col-lg-3">FECHA</div>
                            <div class="col-md-2 col-lg-2">LUGAR</div>
                            <div class="col-md-4 col-lg-4">ITEMS</div>
                            <div class="col-md-1 col-lg-1">CANTIDAD</div>
                            <div class="col-md-2 col-lg-2">PRECIO</div>
                        </div>
                        @foreach($orders['data'] as $order)
                            <div class="resumen-compra-box" style="text-align: center">
                                <div class="col-md-3 col-lg-3">
                                    <span>{{$order['created_at']}}</span><br/>
                                    <b>{{$order['status']['name']}}</b>
                                </div>
                                <div class="col-md-2 col-lg-2">
                                    {{$order['address']}}
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    @foreach($order['products'] as $product)
                                        <span>{{$product['store']['product']['name']}}</span><br/>
                                    @endforeach
                                </div>
                                <div class="col-md-1 col-lg-1">
                                    @foreach($order['products'] as $product)
                                        <span>{{$product['quantity']}}</span><br/>
                                    @endforeach
                                </div>
                                <div class="col-md-2 col-lg-2">
                                    <span>{{sprintf('$ %s', number_format($order['total'], 0))}}</span><br/>
                                </div>
                            </div>
                        @endforeach
                        @if($orders['last_page']>1)
                            <div class="box-pagination">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination">
                                        @for ($i = 1; $i <= $orders['last_page']; $i++)
                                            <li class="{{$orders['current_page']==$i?'active':''}}">
                                                <a href="{{Request::fullUrl().(strpos(Request::fullUrl(),'?')?'&page='.$i:'?page='.$i)}}">{{$i}}</a>
                                            </li>
                                        @endfor
                                    </ul>
                                </nav>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>

    </style>
@endsection
@section('scripts')

@endsection