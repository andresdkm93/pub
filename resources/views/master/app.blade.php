<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Licorera Tres Jotas</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="/public/front/css/main.css">
    <link rel="stylesheet" href="/public/front/css/owl.carousel.min.css">
    @yield('scripts')

</head>
<body>
<div class="wrapper" id="home">
    <nav class="navbar navbar-default" style="min-height: 15px;">
        <div class="container">
       {{--     <div class="navbar-header">
                <img src="/public/front/assets/Assets-SVG-04.svg" alt="Llamenos">
                <span>6392474 - 6799493 - 3153521966 - 3154835560</span>
                <div class="navbar-header-email">licorera3jjjs@gmail.com</div>
            </div>

            <div id="navbar">
                <ul class="nav navbar-nav navbar-right">
--}}{{--
                    <li class="active"><a href="#">Promociones del día</a></li>
--}}{{--
                    <li @click="showSuggest=true;"><a>Sugerencias</a></li>
--}}{{--
                    <li><a href="#ayuda">Ayuda</a></li>
--}}{{--
                </ul>
            </div><!--/.nav-collapse -->--}}
        </div>
    </nav>

    <header class="header">
        <div class="container">
            <a href="/" class="col-md-2 col-lg-2 img-logo"></a>
            <div class="col-md-4 col-lg-4">
                <div class="col-md-12 col-lg-12">
                    <div class="input-group search-group">
                        {{--
                                                <input type="text" class="form-control input-search-header" placeholder="¿Qué Necesitas?">
                        --}}
                        <autocomplete
                                ref="autocomplete"
                                source="/api/mobile/search?q="
                                results-property="data"
                                input-class="form-control input-search-header"
                                placeholder="¿Qué Necesitas?"
                                :show-no-results="false"
                                :results-display="formattedDisplay"
                                @selected="selectedProduct">
                        </autocomplete>
                        <span class="input-group-btn">
                      <button class="btn btn-default" type="button" @click="enterSearchProduct"><span
                                  class="glyphicon glyphicon-search"
                                  aria-hidden="true"></span></button>
                    </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="col-md-8 col-lg-8 perfil-header ">
                    @if($user)
                        <div class="perfil-header-white">
                            <div class="perfil-header-white">Hola {{$user->name}}</span>
                                <br/>Tienes {{$user->points}}</span> Js
                            </div>
                            {{-- <div class="perfil-header-yellow">Calle 78 #66 - 89 <img src="/public/front/assets/Assets-SVG-21.svg"
                                                                                      alt="Down" style="fill:antiquewhite;">
                             </div>--}}
                        </div>
                    @else
                        <div class="perfil-header-white">
                            <span @click="showRegister=true" style="color: #F2D733">Registrarse</span> |
                            <span @click="showLogin=true">Iniciar de sesión</span>
                        </div>
                    @endif

                    {{--
                                        <div class="perfil-header-yellow">Calle 78 #66 - 89 <img src="/public/front/assets/Assets-SVG-21.svg" alt="Down" style="fill:antiquewhite;"></div>
                    --}}
                </div>
                @if($user)
                    <div class="col-md-2 col-lg-2">
                        <div class="btn-circle btn-gray btn-medium user-circle" id="btnProfile">
                            <img src="/public/front/assets/Assets-SVG-07.svg" alt="Usuario">
                        </div>
                        <popover target="#btnProfile" placement="bottom">
                            <template slot="popover">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="{{$user->photo?$user->photo:'/public/front/img/avatar.png'}}"
                                             class="img-profile"/>
                                    </div>
                                    <div class="col-lg-12">
                                        <h3>{{$user->name}}</h3>
                                        <p class="name">Puntos acumulados {{$user->points}} J's</p>
                                    </div>
                                </div>
                                <ul>
                                    <li>
                                        <a href="/profile">
                                            Perfil
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/history">
                                            Compras
                                        </a>
                                    </li>
                                </ul>
                                <div @click="logout()">
                                    <p style="padding: 20px 0 10px 20px;">Cerrar sesión</p>
                                </div>
                            </template>
                        </popover>
                    </div>
                @endif

                <div class="col-md-2 col-lg-2">
                    <div class="btn-circle btn-gray btn-medium user-circle" id="btnCart">
                        <img src="/public/front/assets/Assets-SVG-06.svg" alt="Carrito">
                    </div>
                    <popover target="#btnCart" placement="bottom" class="orderPopOver" v-on:show="showOrder"
                             v-on:hide="hideOrder">
                        <template slot="popover">
                            <h3 style="text-align: left;padding-left: 15px">Tu orden</h3>
                            <div class="order-content">
                                <div class="row order-preview-product" v-for="product in orderProducts">
                                    <div v-if="product.quantityShop>0">
                                        <div class="col-sm-4">
                                            <img v-bind:src="product.product.image+'&h=70'"
                                                 class="order-preview-image"/>
                                        </div>
                                        <div class="col-sm-8">
                                            <p style="font-weight: bold;margin: 0" v-html="product.product.name"></p>
                                            <p v-if="product.points" style="margin: 0">@{{ product.points}} J's</p>
                                            <p v-else style="margin: 0">@{{ product.price | currency('$ ',0) }}</p>
                                            <div class="row">
                                                <div v-if="!product.points" class="col-sm-2" style="padding: 0;"
                                                     @click="changeQuantityOrder(product,-1)">
                                                    <img src="/public/front/assets/Assets-SVG-24.svg"
                                                         style="margin: auto;display: block;width: 25px"/>
                                                </div>
                                                <div v-if="!product.points" class="col-sm-2"
                                                     style="text-align: center;">
                                                    <p style="font-weight: bold;margin: 0"><span>@{{product.quantityShop}}</span>
                                                    </p>
                                                </div>
                                                <div v-if="!product.points" class="col-sm-2" style="padding: 0;"
                                                     @click="changeQuantityOrder(product,+1)">
                                                    <img src="/public/front/assets/Assets-SVG-25.svg"
                                                         style="margin: auto;display: block;width: 25px"/>
                                                </div>
                                                <div v-if="product.points" class="col-sm-2"><p
                                                            style="font-weight: bold;margin: 0;color:#F93333"
                                                            @click="removePromotion(product)">Quitar</p></div>
                                                <div v-else class="col-sm-2"><p
                                                            style="font-weight: bold;margin: 0;color:#F93333"
                                                            @click="removeProductOrder(product)">Quitar</p></div>
                                                <div class="col-sm-2"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-else
                                         style="height:70px;width: auto;background-color: #4D4D4D;color:#fff;text-align: center;padding: 5px">
                                        <p style="margin: 0">
                                            ¿Deseas eliminar este producto?
                                        </p>
                                        <div class="row">
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-5">
                                                <button class="order-preview-button"
                                                        @click="removeProductOrder(product)">
                                                    Si
                                                </button>
                                            </div>
                                            <div class="col-sm-5">
                                                <button class="order-preview-button"
                                                        @click="changeQuantityOrder(product,+1)">
                                                    No
                                                </button>
                                            </div>
                                            <div class="col-sm-1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="order-footer" @click="createCart()">
                                <p>Hacer Pedido @{{ orderSum | currency('$ ',0)}}</p>
                            </div>
                        </template>
                    </popover>
                    <div class="init-hide hidden">
                        <div class="tooltip-count-cart " v-if="totalProducts>0">+@{{totalProducts}}</div>
                    </div>

                </div>
            </div>
        </div>
    </header>

    @yield('content')

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-4 footer-item">
                    <div class="content-location">
                        <div class="content-logo">
                            <img class="logo-footer" src="/public/front/img/logo.png" alt="LicoreraTripleJJJ">
                        </div>
                        <div class="location">
                            <p class="country">Floridablanca, Santander, Colombia</p>
                            <p class="address">Cra 26 # 33-17 Cañaveral, Floridablanca</p>
                            <p class="phone">6392474 - 6799493 - 3153521966 - 3154835560</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 footer-item">
                    <div class="content-links">
                        <div class="box-links">
                            <a href="/nosotros" style="color:#fff" class="box-links-item">Quines somos</a>
                            <a href="/condiciones" target="_blank"
                               style="color:#fff"  class="box-links-item">Términos y condiciones</a>
                            {{--
                                                        <a href="#" class="box-links-item">Preguntas frecuentes</a>
                            --}}
                        </div>
                        <div class="box-links">
                            <a style="color:#fff" class="box-links-item" @click="showSuggest=true;">Sugerencias</a>
                            <a href="#" class="box-links-item" @click="showRemoveData=true;">Solicitud de eliminación de datos</a>
                        </div>
                        {{-- <div class="box-links">
                             <a href="#" class="box-links-item"></a>
                             <a href="#" class="box-links-item">Quejas y reclamos</a>
                         </div>--}}
                        <p class="cory-right">Todos los derechos reservados © 2018 Licorera Tres Jotas.</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 footer-item">
                    <div class="social">
                        <div class="content-get-app">
                            <label>Descarga el App aquí</label><br/>
                            <a href="https://itunes.apple.com/co/app/licorera-3jjjs/id1178470906?mt=8" target="_blank">
                                <button href="#" class="btn-custom btn-custom-default"><span class="icon"><img
                                                src="/public/front/assets/SVG-13.svg" alt="AppStore"></span>
                                    AppStore
                                </button>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.licorera3jjjs.app"
                               target="_blank">
                                <button class="btn-custom btn-custom-default"><span class="icon"><img
                                                src="/public/front/assets/Assets-SVG-16.svg" alt="PlayStore"></span>
                                    PlayStore
                                </button>
                            </a>

                        </div>
                        <div class="content-social-media">
                            <label>Síguenos</label><br/>
                            <a href="https://instagram.com/licoreratresjotas" target="_blank" class="link-social-media">
                                <img class="link-social-media-img" src="/public/front/assets/Assets-SVG-13.svg"
                                     alt="Instagram">
                            </a>
                            <a href="https://www.facebook.com/licorera3jjjs" target="_blank" class="link-social-media">
                                <img class="link-social-media-img" src="/public/front/assets/Assets-SVG-14.svg" alt="Facebook">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    @include('partials.login')
    @include('partials.register')
    @include('partials.buyProduct')
    @include('partials.validateAge')
    @include('partials.suggest')
</div>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
        crossorigin="anonymous"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script src="/public/front/js/jquery.geocomplete.min.js" type="application/javascript"></script>

<script src="/public/front/js/owl.carousel.min.js"></script>

<script src="/public/front/js/lazysizes.min.js" async=""></script>

<script src="https://apis.google.com/js/api:client.js"></script>


<script src="/public/front/js/main.bundle.js"></script>


<script type="application/javascript">
    // Instantiate the Bootstrap carousel
    $(document).ready(function () {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 4
                },
                1400: {
                    items: 5
                }
            }
        })
    });

    window.fbAsyncInit = function () {
        FB.init({
            appId: '288163334886248',
            cookie: true,  // enable cookies to allow the server to access the session
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v16.0'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

</body>
</html>