@extends('master.app')

@section('content')
    <section class="main">
        <div class="content-nav-tabs background-alfa">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-tabs-center container" role="tablist">
                <li role="presentation" class="active">
                    <a href="#search" aria-controls="search">
                    <span class="icon">
                      <img src="/public/front/assets/Assets-SVG-10.svg" alt="Inicio">
                    </span>
                        Inicio
                    </a>
                </li>
                <li role="presentation">
                    <a href="/tienda" aria-controls="store">
                    <span class="icon">
                      <img src="/public/front/assets/Assets-SVG-11.svg" alt="Tienda">
                    </span>
                        Tienda
                    </a>
                </li>
                <li role="presentation">
                    <a href="/promociones" aria-controls="canje">
                    <span class="icon">
                      <img src="/public/front/assets/Assets-SVG-12.svg" alt="Canje">
                    </span>
                        Canje
                    </a>
                </li>
            </ul>
            <!-- Tab panes -->
            {{--

            --}}
            <div class="content-main">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                @for($i=0;$i<count($banners);$i++)
                                    <li data-target="#myCarousel" data-slide-to="{{$i}}"></li>
                                @endfor

                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">

                                @foreach ($banners as $index => $banner)
                                    <div class="item item-carousel {{$index==0?'active':''}}">
                                        <div class="content">
                                            {{-- <div class="img-carousel" style="width: 100%;
                                                     height: 600px;
                                                     background-image: url({{$banner['image'].'&h=150'}});
                                                     background-size: contain;
                                                     background-position: 50% 50%;
                                                     background-repeat: no-repeat">
                                             </div>--}}
                                            <img data-src="{{$banner['image'].'&h=600'}}"
                                                 src="/public/front/img/placeholder.png"
                                                 class="lazyload img-carousel"/>
                                            <div class="item-carousel-description">
                                                <div class="item-carousel-description-content">
                                                    @if(isset($banner['promotion_type']))
                                                        @if(isset($banner['start_date']))
                                                            <h3>
                                                                <span class="yellow">Promoción</span> {{$banner['start_date']}}
                                                                - {{$banner['end_date']}}</h3>
                                                        @else
                                                            <h3><span class="yellow">Promoción</span>- Hasta agotar
                                                                existencias</h3>
                                                        @endif
                                                    @else
                                                        <h3><span class="yellow">Evento</span>
                                                            - {{$banner['created_at']}}</h3>
                                                    @endif
                                                    <h2 class="item-carousel-title">
                                                        {{$banner['name']}}
                                                    </h2>
                                                    <h1 class="item-carousel-title" style="font-size: 70px;">
                                                        @if(isset($banner['promotion_type']) && $banner['promotion_type']==2)
                                                            {{sprintf('$ %s', number_format($banner['store_product']['price']*$banner['multiplier'], 0))}}
                                                        @elseif(isset($banner['store_product']))
                                                            {{sprintf('$ %s', number_format($banner['store_product']['price'], 0))}}
                                                        @endif
                                                    </h1>
                                                    <p class="item-carousel-text">
                                                        {{$banner['description']}}
                                                    </p>
                                                    @if(isset($banner['store_product_id']))
                                                        @if($banner['promotion_type']==2)
                                                            <button class="item-carousel-button"
                                                                    @click="addToCartPromotion({{$banner['id']}})">
                                                                Ver más
                                                            </button>
                                                        @else
                                                            <button class="item-carousel-button"
                                                                    @click="addToCart({{$banner['store_product_id']}})">
                                                                Ver más
                                                            </button>
                                                        @endif
                                                    @else
                                                        <button class="item-carousel-button"
                                                                @click="addEvent({{$banner['id']}})">
                                                            Ver más
                                                        </button>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-80">
                    <div class="col-lg-8 col-lg-offset-2">
                        <h2 class="item-carousel-title">
                            Productos Destacados
                        </h2>
                        <p>Encuentra en Licorera Tres Jotas lo mejor para tu rumba</p>
                    </div>
                    <div class="col-lg-8 col-lg-offset-2 margin-top-40">
                        <div class="owl-carousel owl-theme box-product-result">
                            @foreach ($topProducts as $index => $storeProduct)
                                <div class="content-product">
                                    <div class="box-product-result-item">
                                        <img alt="{{$storeProduct['product']['name']}}"
                                             data-src="{{$storeProduct['product']['image'].'&h=100'}}"
                                             class="lazyload box-product-result-item-product home-owl-carousel"/>

                                        <div class="box-product-result-item-text">
                                            <div class="box-product-result-item-text-price">
                                                {{sprintf('$ %s', number_format($storeProduct['price'], 0))}}
                                            </div>
                                            <p class="box-product-result-item-text-title">
                                                {{$storeProduct['product']['name']}}
                                            </p>
                                        </div>
                                        <button class="box-product-result-btn"
                                                @click="addToCart({{$storeProduct['id']}})">
                                            Ver más
                                        </button>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('partials.buyEvent')
@endsection
@section('scripts')

@endsection