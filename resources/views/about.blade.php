@extends('master.app')

@section('content')
    <section  style="background-color: #303133 !important;margin-top: 143px">
        <br/>
        <div class="content-main" >
            <div class="tab-content">
                <div role="tabpanel" >
                    <div class="row" style="background-color:rgb(49, 50, 52) !important">
                        <div class="col-lg-4 col-lg-offset-1">
                            <div class="content-searh-result">
                                <h2>¿Quiénes somos?</h2>
                            </div>
                            <p>
                                Los mejores licores, precios y confianza desde 1997.

                                ¿ Quieres pedir un domicilio para tu rumba ? Llámanos y con gusto te llevaremos lo que
                                necesites. Domicilios: 6392474 - 6799493 - 315 352 19 66 - 315 483 55 60.

                                ¿ Quieres acumular puntos por tus compras y descuentos exclusivos ?

                                Descarga el App en las tiendas de aplicaciones y aprovecha todos los descuentos
                                exclusivos que tenemos y además acumulas puntos por tus compras y podrás redimirlos por
                                grandes premios en el App. Descargala aquí:
                            </p>
                            <br/>
                            <div class="social">
                                <div class="content-get-app">
                                    <a href="https://itunes.apple.com/co/app/licorera-3jjjs/id1178470906?mt=8" target="_blank">
                                        <button href="#" class="btn-custom btn-custom-default"><span class="icon"><img
                                                        src="/public/front/assets/SVG-13.svg" alt="AppStore"></span>
                                            AppStore
                                        </button>
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.licorera3jjjs.app"
                                       target="_blank">
                                        <button class="btn-custom btn-custom-default"><span class="icon"><img
                                                        src="/public/front/assets/Assets-SVG-16.svg" alt="PlayStore"></span>
                                            PlayStore
                                        </button>
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <img src="/public/front/img/Assets-IMG-2.png" style="width: 99%">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
@section('scripts')

@endsection
