<!doctype html><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><title>Restablecer contraseña</title><!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]--><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="viewport" content="width=device-width,initial-scale=1"><style type="text/css">#outlook a { padding:0; }
          body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
          table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
          img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
          p { display:block;margin:13px 0; }</style><!--[if mso]>
        <noscript>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        </noscript>
        <![endif]--><!--[if lte mso 11]>
        <style type="text/css">
          .mj-outlook-group-fix { width:100% !important; }
        </style>
        <![endif]-->
  <style type="text/css">@media only screen and (min-width:480px) {
        .mj-column-per-100 { width:100% !important; max-width: 100%; }
      }
  </style>
  <style media="screen and (min-width:480px)">.moz-text-html .mj-column-per-100 { width:100% !important; max-width: 100%; }</style>
  <style type="text/css">[owa] .mj-column-per-100 { width:100% !important; max-width: 100%; }</style>
  <style type="text/css">@media only screen and (max-width:480px) {
      table.mj-full-width-mobile { width: 100% !important; }
      td.mj-full-width-mobile { width: auto !important; }
    }</style>
  </head>
  <body style="word-spacing:normal;background-color:#F4F4F4;">
    <div style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">Solicitud de restablecimiento de contraseña.</div>
    <div style="background-color:#F4F4F4;"><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#000000;background-color:#000000;width:100%;">
      <tbody><tr><td><!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#000000" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <div style="margin:0px auto;max-width:600px;">
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody><tr><td style="direction:ltr;font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;padding-left:0px;padding-right:0px;padding-top:0px;text-align:center;"><!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
              <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                  <tbody><tr><td align="center" style="font-size:0px;padding:10px 50px 30px 50px;padding-top:10px;padding-right:50px;padding-bottom:30px;padding-left:50px;word-break:break-word;">
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                      <tbody><tr><td style="width:500px;">
                        <img alt="" height="auto" src="https://03u7t.mjt.lu/tplimg/03u7t/b/1quno/s5or.png" style="border:none;border-radius:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="500"></td></tr></tbody>
                    </table></td></tr>
                  </tbody></table></div>
              <!--[if mso | IE]></td></tr></table><![endif]-->
              </td></tr></tbody>
          </table></div>
        <!--[if mso | IE]></td></tr></table><![endif]-->
        </td></tr></tbody>
      </table>
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
        <tbody><tr><td><!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
          <div style="margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
              <tbody><tr><td style="direction:ltr;font-size:0px;padding:20px 0px 20px 0px;text-align:center;"><!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
                <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                    <tbody><tr><td align="left" style="font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;">
                      <div style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                        <h1 class="text-build-content" data-testid="Igfs2gT7BO7OK" style="margin-top: 10px; margin-bottom: 10px; font-weight: normal;">Restablecer contraseña</h1>
                      </div></td></tr><tr>
                      <td align="left" style="font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;">
                        <div style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                          <p class="text-build-content" data-testid="CsOORYoh3H-W5" style="margin: 10px 0; margin-top: 10px; margin-bottom: 10px;">
                            <span style="color:#55575d;font-family:Arial;font-size:13px;line-height:22px;">Lamentamos saber que no puedes recordar tu contraseña. Pero no te preocupes, te vamos a ayudar. Para restablecer tu contraseña solo tienes que dar click en el siguiente botón.</span></p>
                        </div>
                      </td></tr><tr>
                      <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                        <p style="border-top:solid 2px #E6E6E6;font-size:1px;margin:0px auto;width:100%;"></p>
                        <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 2px #E6E6E6;font-size:1px;margin:0px auto;width:550px;" role="presentation" width="550px" ><tr><td style="height:0;line-height:0;"> &nbsp;
</td></tr></table><![endif]-->
                      </td></tr><tr>
                      <td align="center" vertical-align="middle" style="font-size:0px;padding:10px 25px 10px 25px;padding-right:25px;padding-left:25px;word-break:break-word;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;">
        					<tbody><tr><td align="center" bgcolor="#3051bf" role="presentation" style="border:none;border-radius:3px;cursor:auto;mso-padding-alt:10px 25px 10px 25px;background:#3051bf;" valign="middle">
                              <p style="display:inline-block;background:#3051bf;color:#ffffff;font-family:Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px 10px 25px;mso-padding-alt:0px;border-radius:3px;">
                                <a href="{{$url}}/changuePassword/{{$code}}">Restablecer ahora</a>
                              </td></tr></tbody>
                        </table></td></tr>
                      <tr><td align="center" style="font-size:0px;padding:10px 25px 10px 25px;padding-right:25px;padding-left:25px;word-break:break-word;">
                        <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" ><tr><td><![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                          <tbody><tr><td style="padding:4px;vertical-align:middle;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#3B5998;border-radius:50%;width:30;">
                              <tbody><tr><td style="padding:0px 0px 0px 0px;font-size:0;height:30;vertical-align:middle;width:30;">
                                <a href="https://web.facebook.com/licoreratresjotas" target="_blank">
                                  <img height="30" src="https://www.mailjet.com/images/theme/v1/icons/ico-social/facebook.png" style="border-radius:50%;display:block;" width="30"></a></td></tr>
                              </tbody>
                            </table></td></tr>
                          </tbody>
                        </table><!--[if mso | IE]></td><td><![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                          <tbody><tr><td style="padding:4px;vertical-align:middle;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#405DE6;border-radius:50%;width:30;">
                              <tbody><tr><td style="padding:0px 0px 0px 0px;font-size:0;height:30;vertical-align:middle;width:30;">
                                <a href="https://www.instagram.com/licoreratresjotas" target="_blank">
                                  <img height="30" src="https://www.mailjet.com/images/theme/v1/icons/ico-social/instagram.png" style="border-radius:50%;display:block;" width="30"></a></td></tr>
                              </tbody>
                            </table></td></tr>
                          </tbody>
                        </table><!--[if mso | IE]></td></tr></table><![endif]-->
                        </td></tr>
                    </tbody>
                  </table>
                </div><!--[if mso | IE]></td></tr></table><![endif]-->
                </td></tr>
              </tbody>
            </table>
          </div><!--[if mso | IE]></td></tr></table><![endif]-->
          </td></tr></tbody>
      </table>
    </div>
  </body>
</html>








