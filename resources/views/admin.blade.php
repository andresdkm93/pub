@extends('layouts.app')

@section('content')
    <div ui-view>

    </div>
@endsection
@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8RyRumyUa6aSeX7hLHrfpq45yscLsX3o"
            type="text/javascript"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0-rc.0/angular-sanitize.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>
    <script src="/public/libs/leaflet/leaflet.js" type="text/javascript"></script>
    <script src="/public/libs/layer/tile/Google.js" type="text/javascript"></script>
    <script src="/public/libs/angular-logger/angular-simple-logger.min.js" type="text/javascript"></script>
    <script src="/public/libs/awesome-markers/dist/leaflet.awesome-markers.js" type="text/javascript"></script>
    <script src="/public/libs/extra-markers/dist/js/leaflet.extra-markers.min.js" type="text/javascript"></script>
    <script src="/public/libs/angular-ui-leaflet/ui-leaflet.min.js" type="text/javascript"></script>
    <script src="/public/libs/cluster-markers/leaflet.markercluster.js" type="text/javascript"></script>
    <script src="/public/libs/moment/moment.js" type="text/javascript"></script>
    <script src="/public/libs/ng-toast/ngToast.js" type="text/javascript"></script>
    <script src="/public/libs/angular-file-upload.min.js" type="text/javascript"></script>
    <script src="https://rawgit.com/elesdoar/ui-leaflet-layers/master/dist/ui-leaflet-layers.min.js"></script>

    <!-- Angular Material Library -->
    <script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>

    <!--angular libraries-->
    <script src="/public/libs/angular-ui-router.min.js"></script>
    <script src="/public/libs/angular-resource.min.js"></script>
    <script src="/public/libs/ui-bootstrap-tpls-1.0.3.min.js"></script>
    <script src="/public/libs/angular-file-upload.min.js" type="text/javascript"></script>
    <script src="/public/libs/angular-audio/angular-audio.js" type="text/javascript"></script>

    <!--angular app-->
    <script src="/public/app/app.js"></script>
    <script src="/public/app/routes.js"></script>
    <script src="/public/app/commons/main-commons.js"></script>
    <script src="/public/app/directives/spquery/spquery-directive.js"></script>

    <script src="/public/app/commons/services/serviceFactory.js"></script>
    <script src="/public/app/commons/services/firebaseFactory.js"></script>

    <script src="/public/app/home/home-controller.js" type="text/javascript"></script>
    <script src="/public/app/users/controller/users-controller.js" type="text/javascript"></script>
    <script src="/public/app/categories/controller/categories-controller.js" type="text/javascript"></script>
    <script src="/public/app/products/controller/products-controller.js" type="text/javascript"></script>
    <script src="/public/app/stockproducts/controller/stockproducts-controller.js" type="text/javascript"></script>
    <script src="/public/app/promotionproducts/controller/promotionproducts-controller.js" type="text/javascript"></script>
    <script src="/public/app/clients/controller/clients-controller.js" type="text/javascript"></script>
    <script src="/public/app/codes/controller/codes-controller.js" type="text/javascript"></script>
    <script src="/public/app/events/controller/events-controller.js" type="text/javascript"></script>
    <script src="/public/app/events/controller/eventsview-controller.js" type="text/javascript"></script>
    <script src="/public/app/shedules/controller/shedules-controller.js" type="text/javascript"></script>
    <script src="/public/app/promotions/controller/promotions-controller.js" type="text/javascript"></script>
    <script src="/public/app/zones/controller/zones-controller.js" type="text/javascript"></script>
    <script src="/public/app/clients/controller/clientsview-controller.js" type="text/javascript"></script>
    <script src="/public/app/printer/controller/printer-controller.js" type="text/javascript"></script>
    <script src="/public/app/reports/controller/reports-controller.js" type="text/javascript"></script>

@endsection