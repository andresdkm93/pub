@extends('master.app')

@section('content')
    <section class="main">
        <div class="content-nav-tabs background-alfa">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-tabs-center container" role="tablist">
                <li role="presentation">
                    <a href="/" aria-controls="search">
                    <span class="icon">
                      <img src="/public/front/assets/Assets-SVG-10.svg" alt="Inicio">
                    </span>
                        Inicio
                    </a>
                </li>
                <li role="presentation" class="active">
                    <a href="/tienda" aria-controls="store">
                    <span class="icon">
                      <img src="/public/front/assets/Assets-SVG-11.svg" alt="Tienda">
                    </span>
                        Tienda
                    </a>
                </li>
                <li role="presentation">
                    <a href="/promociones">
                    <span class="icon">
                      <img src="/public/front/assets/Assets-SVG-12.svg" alt="Canje">
                    </span>
                        Canje
                    </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="content-main">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active container" id="search">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="content-searh-result">
                                    <p>Inicio / Tienda / {{$currentCategory['name']}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-lg-3">

                                <div class="content-filter">
                                    <h3 class="title-section">Tienda</h3>
                                    <div class="box-filter open types">
                                        <div class="box-filter-select">
                                            Categorias
                                        </div>
                                        <ul class="box-type-select">
                                            @foreach($categories as $category)
                                                <li class="box-type-select-item {{$currentCategory['id']==$category['id']?'selected':''}}">
                                                    <a class="category"
                                                       href="{{'/tienda/categorias/'.strtolower(urlencode($category['name']))}}">
                                                        {{$category['name']}}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    {{-- <div class="box-filter categories">
                                         <div class="box-filter-select">
                                             Categorias
                                         </div>
                                         <ul class="box-type-select">
                                             <li class="box-type-select-item">Categoria 01</li>
                                             <li class="box-type-select-item">Categoria 02</li>
                                             <li class="box-type-select-item">Categoria 03</li>
                                             <li class="box-type-select-item selected">Categoria 04</li>
                                             <li class="box-type-select-item">Categoria 05</li>
                                         </ul>
                                     </div>
                                     <div class="box-filter prices">
                                         <div class="box-filter-select">
                                             Precio
                                         </div>
                                         <div class="box-filter-range">
                                             <div class="box-filter-range-eje"></div>
                                             <div class="box-filter-range-item box-filter-range-menor">
                                                 10k
                                             </div>
                                             <div class="box-filter-range-item box-filter-range-mayor">
                                                 80k
                                             </div>
                                         </div>
                                     </div>
                                     <div class="box-filter marcas open">
                                         <div class="box-filter-select">
                                             Marca
                                         </div>
                                         <form class="box-search">
                                             <input class="box-search-input" type="text"
                                                    placeholder="Buscar la marca...">
                                             <span class="box-search-icon glyphicon glyphicon-search"
                                                   aria-hidden="true"></span>
                                         </form>
                                         <ul class="box-type-select">
                                             <li class="box-type-select-item">Marca 01</li>
                                             <li class="box-type-select-item">Marca 02</li>
                                             <li class="box-type-select-item">Marca 03</li>
                                             <li class="box-type-select-item selected">Marca 04</li>
                                             <li class="box-type-select-item">Marca 05</li>
                                             <li class="box-type-select-item">Marca 06</li>
                                             <li class="box-type-select-item">Marca 07</li>
                                             <li class="box-type-select-item">Marca 08</li>
                                             <li class="box-type-select-item">Marca 09</li>
                                             <li class="box-type-select-item">Marca 10</li>
                                             <li class="box-type-select-item">Marca 11</li>
                                         </ul>
                                     </div>--}}
                                </div>
                            </div>
                            <div class="col-md-9 col-lg-9">

                                <div class="content-result">
                                    <div class="row padding-left-30 padding-right-30">
                                        <div class="col-md-12 col-lg-12 title-category" style="{{isset($currentCategory['banner'])?'
                                        background: -webkit-gradient(linear, left top, left bottom, from(rgba(0, 0, 0, .7)), to(rgba(0, 0, 0, .7))), url('.$currentCategory['banner'].');
                                        background: linear-gradient(rgba(0, 0, 0, .7), rgba(0, 0, 0, .7)), url('.$currentCategory['banner'].');':''}}">
                                            <div class="content-nav-title container">
                                                <h2>{{$currentCategory['name']}}</h2>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-bar-filter-selected">
                                        <span>Ordenar por /</span>
                                        <div class="dropdown select-sort-price">
                                            <dropdown ref="dropdown">
                                                <button class="btn btn-default dropdown-toggle" type="button"
                                                        id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="true">
                                                    {{$order}}
                                                    <span class="caret"></span>
                                                </button>
                                                <template slot="dropdown">
                                                    <li><a href="{{Request::url().'?sort=price:asc'}}"> Menor precio</a></li>
                                                    <li><a href="{{Request::url().'?sort=price:desc'}}"> Mayor precio</a></li>
                                                    <li><a href="{{Request::url().'?sort=alphabetically'}}"> Alfabeticamente</a></li>
                                                    <li><a href="{{Request::url().'?sort=recommended'}}"> Recomendados</a></li>
                                                    <li><a href="{{Request::url()}}"> Popularidad</a></li>
                                                </template>
                                            </dropdown>
                                        </div>
                                    </div>

                                    <div class="box-product-result">
                                        @foreach ($products['data'] as $index => $product)
                                            <div class="col-md-3 col-lg-3 content-product">
                                                <div class="box-product-result-item">
                                                    <img class="lazyload box-product-result-item-product"
                                                         src="/public/front/img/gris.jpg"
                                                         data-src="{{$product['product']['image'].'&h=100'}}"
                                                         alt="Six Pack Club Colombia Dorada">
                                                    <div class="box-product-result-item-text">
                                                        <div class="box-product-result-item-text-price">
                                                            {{sprintf('$ %s', number_format($product['price'], 0))}}
                                                        </div>
                                                        <div class="box-product-result-item-text-title">
                                                            {{$product['product']['name']}}
                                                        </div>
                                                    </div>
                                                    <button class="box-product-result-btn"
                                                            @click="addToCart({{$product['id']}})">
                                                        Ver más
                                                    </button>
                                                </div>
                                            </div>
                                        @endforeach


                                    </div>
                                    @if($products['last_page']>1)
                                        <div class="box-pagination">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination">
                                                    <li>
                                                        <a href="{{Request::fullUrl().(strpos(Request::fullUrl(),'?')?'&page=1':'?page=1')}}"
                                                           aria-label="Previous">
                                                            PRIMERA
                                                            <span aria-hidden="true">&laquo;</span>
                                                        </a>
                                                    </li>
                                                    </li>
                                                    @for ($i = 1; $i <= $products['last_page']; $i++)
                                                        @if($products['current_page']==$i)
                                                            <li class="active">
                                                        @else
                                                            <li>
                                                                @endif
                                                                <a href="{{Request::fullUrl().(strpos(Request::fullUrl(),'?')?'&page='.$i:'?page='.$i)}}">{{$i}}</a>
                                                            </li>
                                                            @endfor
                                                            <li>
                                                                <a href="{{Request::fullUrl().(strpos(Request::fullUrl(),'?')?'&page='.$products['last_page']:'?page='.$products['last_page'])}}"
                                                                   aria-label="Next">
                                                                    <span aria-hidden="true">&raquo;</span>
                                                                    ULTIMO
                                                                </a>
                                                            </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade container" id="store"> CONTENIDO TIENDA</div>
                    <div role="tabpanel" class="tab-pane fade container" id="canje"> CONTENIDO CANJE</div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

@endsection