@extends('master.app')

@section('content')
    <section class="main">
        <div class="content-nav-tabs background-alfa">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-tabs-center container" role="tablist">
                <li role="presentation">
                    <a href="/" aria-controls="search">
                    <span class="icon">
                      <img src="/public/front/assets/Assets-SVG-10.svg" alt="Inicio">
                    </span>
                        Inicio
                    </a>
                </li>
                <li role="presentation" class="active">
                    <a href="/tienda" aria-controls="store">
                    <span class="icon">
                      <img src="/public/front/assets/Assets-SVG-11.svg" alt="Tienda">
                    </span>
                        Tienda
                    </a>
                </li>
                <li role="presentation">
                    <a href="/promociones">
                    <span class="icon">
                      <img src="/public/front/assets/Assets-SVG-12.svg" alt="Canje">
                    </span>
                        Canje
                    </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="content-main">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active container" id="search">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="content-searh-result">
                                    <h2>Productos destacados</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-lg-3">

                                <div class="content-filter">
                                    <h3 class="title-section">Tienda</h3>
                                    <div class="box-filter open types">
                                        <div class="box-filter-select">
                                            Categorias
                                        </div>
                                        <ul class="box-type-select">
                                            @foreach($categories as $category)
                                                <li class="box-type-select-item">
                                                    <a class="category"
                                                       href="{{'/tienda/categorias/'.strtolower(urlencode($category['name']))}}">
                                                        {{$category['name']}}
                                                    </a>
                                                </li>
                                            @endforeach
                                            {{--
                                                                                            <li class="box-type-select-item selected">Tipo 04</li>
                                            --}}
                                        </ul>
                                    </div>
                                    {{-- <div class="box-filter categories">
                                         <div class="box-filter-select">
                                             Categorias
                                         </div>
                                         <ul class="box-type-select">
                                             <li class="box-type-select-item">Categoria 01</li>
                                             <li class="box-type-select-item">Categoria 02</li>
                                             <li class="box-type-select-item">Categoria 03</li>
                                             <li class="box-type-select-item selected">Categoria 04</li>
                                             <li class="box-type-select-item">Categoria 05</li>
                                         </ul>
                                     </div>
                                     <div class="box-filter prices">
                                         <div class="box-filter-select">
                                             Precio
                                         </div>
                                         <div class="box-filter-range">
                                             <div class="box-filter-range-eje"></div>
                                             <div class="box-filter-range-item box-filter-range-menor">
                                                 10k
                                             </div>
                                             <div class="box-filter-range-item box-filter-range-mayor">
                                                 80k
                                             </div>
                                         </div>
                                     </div>
                                     <div class="box-filter marcas open">
                                         <div class="box-filter-select">
                                             Marca
                                         </div>
                                         <form class="box-search">
                                             <input class="box-search-input" type="text"
                                                    placeholder="Buscar la marca...">
                                             <span class="box-search-icon glyphicon glyphicon-search"
                                                   aria-hidden="true"></span>
                                         </form>
                                         <ul class="box-type-select">
                                             <li class="box-type-select-item">Marca 01</li>
                                             <li class="box-type-select-item">Marca 02</li>
                                             <li class="box-type-select-item">Marca 03</li>
                                             <li class="box-type-select-item selected">Marca 04</li>
                                             <li class="box-type-select-item">Marca 05</li>
                                             <li class="box-type-select-item">Marca 06</li>
                                             <li class="box-type-select-item">Marca 07</li>
                                             <li class="box-type-select-item">Marca 08</li>
                                             <li class="box-type-select-item">Marca 09</li>
                                             <li class="box-type-select-item">Marca 10</li>
                                             <li class="box-type-select-item">Marca 11</li>
                                         </ul>
                                     </div>--}}
                                </div>
                            </div>
                            <div class="col-md-9 col-lg-9">
                                <div class="content-result">
                                   {{-- <div class="box-bar-filter-selected">
                                        <span>Ordenar por /</span>
                                        <div class="dropdown select-sort-price">
                                            <button class="btn btn-default dropdown-toggle" type="button"
                                                    id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="true">
                                                Menor precio
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1"
                                                style="display:none">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                        <div class="box-filter-selected">
                                            <span class="text">Filtros:</span>
                                            <span class="box-filter-selected-item">Tipo 04</span>
                                            <span class="box-filter-selected-item">Marca 04</span>
                                        </div>
                                    </div>--}}

                                    <div class="box-product-result">
                                        @foreach ($topProducts as $index => $storeProduct)
                                            <div class="col-md-3 col-lg-3 content-product">
                                                <div class="box-product-result-item">
                                                    <img class="lazyload box-product-result-item-product"
                                                         src="/public/front/img/gris.jpg"
                                                         data-src="{{$storeProduct['product']['image'].'&h=100'}}"
                                                         alt="product.product.name">
                                                    <div class="box-product-result-item-text">
                                                        <div class="box-product-result-item-text-price">
                                                            {{sprintf('$ %s', number_format($storeProduct['price'], 0))}}
                                                        </div>
                                                        <div class="box-product-result-item-text-title">
                                                            {{$storeProduct['product']['name']}}
                                                        </div>
                                                    </div>
                                                    <button class="box-product-result-btn"
                                                            @click="addToCart({{$storeProduct['id']}})">
                                                        Ver más
                                                    </button>
                                                </div>
                                            </div>{{----}}
                                        @endforeach


                                    </div>
                                    {{--<div class="box-pagination">
                                        <nav aria-label="Page navigation">
                                            <ul class="pagination">
                                                <li>
                                                    <a href="#" aria-label="Previous">
                                                        PRIMERA
                                                        <span aria-hidden="true">&laquo;</span>
                                                    </a>
                                                </li>
                                                <li class="active"><a href="#">1</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">4</a></li>
                                                <li><a href="#">5</a></li>
                                                <li>
                                                    <a href="#" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                        ULTIMO
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade container" id="store"> CONTENIDO TIENDA</div>
                    <div role="tabpanel" class="tab-pane fade container" id="canje"> CONTENIDO CANJE</div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

@endsection