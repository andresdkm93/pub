@extends('master.app')

@section('content')
    <cart-component inline-template>
        <section class="main">
            <div class="col-md-12 col-lg-12 title-compra">
                <div class="content-nav-title container">
                    <h2>Confirmación de pedido</h2>
                    <h3>Por favor completa los siguientes datos para enviarte tu pedido con exito. El costo de tu
                        domicilio es GRATIS por compras superiores a $40.000. Aplican condiciones y restricciones.</h3>
                </div>
            </div>
            <div class="content-compras">
                <div class="container">
                    <div class="col-md-8 col-lg-8">
                        <div class="subtitle_compras">
                            Por favor llena los siguientes datos
                        </div>
                        <div class="content-ubicacion">
                            <div class="subtitle-compras-sub">
                                Ubicación
                            </div>
                            <div class="box-direction" id="map">

                            </div>
                            <div class="subtitle-compras-sub font-gray">
                                Mis Ubicaciones
                            </div>
                            <div class="content-full-info info-compras">
                                <div class="location">
                                    <div class="box-my-locations">
                                        @foreach($client->locations as $location)
                                            <div class="my-location">
                                                <div class="icon">
                                                    <label class="container-radio">
                                                        <input type="radio" name="radio-location"
                                                               @click="setLocation({{$location->id}})">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="my-location-item">
                                                    <div class="address"
                                                         style="width: 100%;display: block">{{$location->address}}</div>
                                                    <div class="place"
                                                         style="width: 100%;display: block">{{$location->name}}</div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="my-location">
                                            <div class="icon">
                                                <label class="container-radio" @click="cleanLocation()">
                                                    <input type="radio" checked name="radio-location">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="my-location-item">
                                                <div class="address" style="display: inline-block;width: 10%">OTRA</div>
                                                <div class="row" style="width: 70%">
                                                    <div class="col-md-3 col-lg-3" style="padding: 0">
                                                        <select class="address-selector-options" v-model="component1">
                                                            <option value="Carrera">Carrera</option>
                                                            <option value="Calle">Calle</option>
                                                            <option value="Avenida Calle">Avenida Calle</option>
                                                            <option value="Avenida">Avenida Carrera</option>
                                                            <option value="Circular">Circular</option>
                                                            <option value="Circunvalar">Circunvalar</option>
                                                            <option value="Diagonal">Diagonal</option>
                                                            <option value="Manzana">Manzana</option>
                                                            <option value="Transversal">Transversal</option>
                                                            <option value="Via">Via</option>

                                                        </select>
                                                    </div>
                                                    <div class="col-md-2 col-lg-2">
                                                        <input type="text" class="address-selector-options"
                                                               v-model="component2"
                                                               name="component2">
                                                    </div>
                                                    <div class="col-md-2 col-lg-2" style="padding: 0">
                                                        #<input type="text" class="address-selector-options"
                                                                style="width: 70%" v-model="component3"
                                                                name="component3">
                                                    </div>
                                                    <div class="col-md-2 col-lg-2" style="padding: 0">
                                                        - <input type="text" class="address-selector-options"
                                                                 style="width: 70%" v-model="component4"
                                                                 name="component4">
                                                    </div>
                                                    <div class="col-md-3 col-lg-3 " style="padding: 0">
                                                        <select class="address-selector-options" v-model="component5">
                                                            <option value="Bucaramanga">Bucaramanga</option>
                                                            <option value="Floridablanca">Floridablanca</option>
                                                            <option value="Piedecuesta">Piedecuesta</option>
                                                            <option value="Giron">Giron</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="address-input address-input-text">
                                                    <label class="container-check">Guardar esta ubicación
                                                        <input type="checkbox" v-model="saveDirection"
                                                               name="save-address"
                                                               id="save-address">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="error">
                                        <span v-if="errors.address!=null" v-html="errors.address"></span>
                                    </p>
                                </div>
                            </div>
                            <div class="description-compras">
                                <div class="col-md-4 col-lg-4 description-compras-title">Teléfono</div>
                                <div class="col-md-8 col-lg-8 description-compras-text">
                                    <input type="tel" name="phone-4"
                                           style="background: transparent;outline: 0;border: 0px;border-bottom: 1px solid #7F7F7F;"
                                           v-model="phone"
                                           id="phone-4"
                                           placeholder="Teléfono de contacto"/>
                                </div>
                                <p class="error">
                                    <span v-if="errors.phone!=null" v-html="errors.phone"></span>
                                </p>
                            </div>
                            <br/>
                            <br/>

                            <div class="description-compras">
                                <div class="col-md-4 col-lg-4 description-compras-title">Detalle del lugar</div>
                                <div class="col-md-8 col-lg-8 description-compras-text">
                                        <textarea name="address-4"
                                                  v-model="details"
                                                  id="address-4" cols="30"
                                                  rows="3"
                                                  placeholder="Escriba aquí detalles adicionales de la entrega"></textarea>
                                </div>
                                <p class="error">
                                    <span v-if="errors.details!=null" v-html="errors.details"></span>
                                </p>
                            </div>
                        </div>
                        <div class="pago-compras">
                            <div class="metodo-pago metodo-pago-title">Método de pago</div>
                            <div class="metodo-pago">
                                <label class="container-radio">Efectivo
                                    <input type="radio" name="radio-pago" value="efectivo" v-model="payMethod">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="metodo-pago">
                                <label class="container-radio">Tarjeta
                                    <input type="radio" name="radio-pago" value="tarjeta" v-model="payMethod">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <p class="error">
                                <span v-if="errors.payMethod!=null" v-html="errors.payMethod"></span>
                            </p>
                        </div>
                        <div class="resumen-compras">
                            <div class="resumen-compras-title">
                                Resumen del pedido
                            </div>
                            <div class="resumen-compra-box-title">
                                <div class="col-md-6 col-lg-6">Item</div>
                                <div class="col-md-2 col-lg-2">Precio + IVA</div>
                                <div class="col-md-2 col-lg-2">Cantidad</div>
                                <div class="col-md-2 col-lg-2">Precio Total</div>
                            </div>
                            <div class="resumen-compra-box" v-for="product in products">
                                <div class="col-md-6 col-lg-6">
                                    <div class="resumen-compra-image">
                                        <img class="img-resumen-compra"
                                             v-bind:src="product.product.image+'&h=70'"
                                             v-bind:alt="product.product.name">
                                    </div>
                                    <div class="resumen-compra-image-text" v-if="product.quantityShop>0">
                                        <div class="image-text-nombre">
                                            @{{ product.product.name }}
                                        </div>
                                        <div class="image-text-medidas">
                                            Cantidad: @{{ product.quantityShop }}
                                        </div>
                                        <div v-if="product.points" class="image-text-action"
                                             @click="removePromotion(product)">
                                            Quitar
                                        </div>
                                        <div v-else class="image-text-action"
                                             @click="removeProductOrder(product)">
                                            Quitar
                                        </div>
                                    </div>
                                    <div v-else
                                         style="height:100px;width: auto;color:#fff;text-align: center;padding: 5px">
                                        <p style="margin: 0">
                                            ¿Deseas eliminar este producto?
                                        </p>
                                        <div class="row">
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-5">
                                                <button class="order-preview-button"
                                                        @click="removeProductOrder(product)">
                                                    Si
                                                </button>
                                            </div>
                                            <div class="col-sm-5">
                                                <button class="order-preview-button"
                                                        @click="changeQuantityOrder(product,+1)">
                                                    No
                                                </button>
                                            </div>
                                            <div class="col-sm-1"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2" v-if="product.quantityShop>0">@{{ product.price |
                                    currency('$ ',0) }}
                                </div>
                                <div class="col-md-2 col-lg-2" v-if="!product.points && product.quantityShop>0">
                                    <img src="/public/front/assets/Assets-SVG-24.svg"
                                         alt="Menor"
                                         width="20"
                                         height="20"
                                         @click="changeQuantityOrder(product,-1)">
                                    @{{ product.quantityShop }}
                                    <img src="/public/front/assets/Assets-SVG-25.svg"
                                         alt="Mayor"
                                         width="20"
                                         height="20"
                                         @click="changeQuantityOrder(product,+1)">
                                </div>
                                <div class="col-md-2 col-lg-2" v-if="product.quantityShop>0">@{{ product.total |
                                    currency('$ ',0)}}
                                </div>
                            </div>
                        </div>
                        <div class="total-compras">
                            <div class="col-md-8 col-lg-8">
                                <div class="cupon-text">Código promocional</div>
                                <div class="cupon-input">
                                    <input type="text" name="input-codigo-cupon"
                                           v-model="code"
                                           id="input-codigo-cupon" value="10off3jjjs">
                                    <img v-if="codeValid" src="/public/front/assets/Assets-SVG-28.svg" alt="Validado">
                                    <img v-else-if="!codeValid && code" src="/public/front/assets/Assets-SVG-34.svg"
                                         alt="Validado">
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <div class="col-md-5 col-lg-5 total-text">Sub.total</div>
                                <div class="col-md-7 col-lg-7 total-amount">
                                    @{{ orderEventsSum + orderSum | currency('$ ',0)}}
                                </div>
                                <div class="col-md-5 col-lg-5 total-text" v-if="discount>0">Descuento</div>
                                <div class="col-md-7 col-lg-7" v-if="discount>0">
                                    @{{ (orderSum*discount) | currency('$ ',0)}}
                                </div>
                                <div class="col-md-5 col-lg-5 total-text">Domicilio</div>
                                <div class="col-md-7 col-lg-7"> @{{ deliveryValue | currency('$ ',0) }}</div>
                                <div class="col-md-12 col-lg-12 divisor-total"></div>
                                <div class="col-md-5 col-lg-5 total-text">Total</div>
                                <div class="col-md-7 col-lg-7 total-amount-total">@{{ orderEventsSum +
                                    orderSum-(orderSum*discount) + deliveryValue | currency('$ ',0) }}
                                </div>
                            </div>
                        </div>
                        <div class="cupon-compras" v-if="!sendOrder">
                            <div class="content-cupon-box">Obtienes por tu pedido <span>@{{parseInt(orderSum/10000)}} J's</span>
                            </div>
                            <button class="content-cupon-btn" @click="createOrder()" style="height: 35px;">
                                Hacer pedido
                            </button>
                        </div>
                        <div class="loader" v-else></div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="subtitle_compras">
                            Te podría interesar
                        </div>
                        <div class="interes-col">
                            @foreach ($products as $index => $storeProduct)
                                <div class="col-md-10  col-lg-10 producto">
                                    <div class="producto-image">
                                        <img class="lazyload img-product"
                                             src="/public/front/img/gris.jpg"
                                             alt="{{$storeProduct['product']['name']}}"
                                             data-src="{{$storeProduct['product']['image'].'&h=100'}}"/>
                                    </div>
                                    <div class="producto-precio">
                                        {{sprintf('$ %s', number_format($storeProduct['price'], 0))}}
                                    </div>
                                    <div class="producto-titulo">
                                        {{$storeProduct['product']['name']}}
                                    </div>
                                    <div class="btn-white" @click="addToCart({{$storeProduct['id']}})">
                                        Agregar al pedido
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
            <modal v-model="showSaveAddress" ref="modal" :footer="false" :header="false">
                <h4 class="item-carousel-title" align="center">
                    Guardar dirección
                </h4>
                <div class="form">
                    <form v-on:submit.prevent="saveAddress">
                        <div class="form-group">
                            <label for="nameAddress">NOMBRE</label>
                            <input type="nameAddress" class="form-control" id="nameAddress" v-model="nameAddress"
                                   required>
                        </div>
                        <button type="submit" class="btn btn-login" style="height: 35px;">
                            Guardar
                        </button>
                    </form>
                </div>
            </modal>
            <modal v-model="showSuccessOrder" ref="modal" :footer="false" size="sm" :backdrop="false">
                <img src="/public/front/assets/Assets-SVG-28.svg" style="width: 70px;display: block;margin: auto;"/>
                <h4 class="item-carousel-title" align="center">
                    Felicitaciones
                </h4>
                <p align="center">
                    Tu orden ha sido aceptada y estará por llegar en unos minutos
                    <br/>
                    <b>Tiempo aproximado :</b> @{{ deliveryTime }} min<br/>
                    <b>Valor domicilio :</b> @{{ deliveryValueTotal | currency('$ ',0) }}<br/>
                </p>
                <a href="/" class="btn btn-login" style="display: block;margin: auto;width: 210px;margin-bottom: 20px">
                    Volver a la tienda
                </a>
            </modal>
        </section>
    </cart-component>
    @include('partials.buyEvent')
@endsection
@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOsKGCw925e5ea6GvXNB98pCyLe-02sBM&libraries=places"></script>
@endsection